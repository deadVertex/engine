@echo off

set CompilerFlags=-MT -F16777216 -nologo -Gm- -GR- -EHa -Od -W4 -WX -wd4305 -wd4127 -wd4201 -wd4189 -wd4100 -wd4996 -wd4505 -FC -Z7  /I..\src /I..\thirdparty /I..\windows-dependencies\include /I..\windows-dependencies\include\Bullet -DPLATFORM_WINDOWS
set Libraries=..\windows-dependencies\lib\BulletDynamics.lib ..\windows-dependencies\lib\BulletCollision.lib ..\windows-dependencies\lib\LinearMath.lib
set LinkerFlags=-opt:ref -incremental:no
IF NOT EXIST build mkdir build
pushd build
cl %CompilerFlags% ..\src\bullet_physics.cpp -Fe:bullet_physics -LD -link -PDB:bullet_physics.pdb %LinkerFlags% %Libraries% -EXPORT:CreatePhysicsSystem -EXPORT:DestroyPhysicsSystem

