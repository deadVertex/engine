#! /bin/bash
COMPILER=g++

CFLAGS="-O0 -g -fno-exceptions -Wall -Wextra -Wno-unused-variable \
-Wno-unused-function -Wno-comment -Wno-unused-parameter -Wno-implicit-fallthrough \
-Wno-missing-field-initializers -Wno-type-limits -Wno-unused-but-set-variable \
-std=c++11 -Isrc -Ithirdparty -I/usr/local/include/bullet"

# Build platform executable
$COMPILER src/main.cpp -o build/main $CFLAGS -lGLEW -lglfw -lGL -ldl -lBulletDynamics -lBulletCollision -lLinearMath

# Build game runtime
$COMPILER src/game.cpp -o build/game_temp.so $CFLAGS -shared -rdynamic -fPIC -lassimp -lopenal

mv build/game_temp.so build/game.so
