@echo off

ctime -begin engine.ctm
set CompilerFlags=-MT -F16777216 -nologo -Gm- -GR- -EHa -Od -W4 -WX -wd4305 -wd4127 -wd4201 -wd4189 -wd4100 -wd4996 -wd4505 -FC -Z7  /I..\src /I..\thirdparty /I..\windows-dependencies\include /I..\windows-dependencies\include\Bullet -DPLATFORM_WINDOWS
set PlatformLibraries=..\windows-dependencies\lib\glfw3dll.lib opengl32.lib Ws2_32.lib user32.lib gdi32.lib winmm.lib
set GameLibraries=..\windows-dependencies\lib\OpenAL32.lib ..\windows-dependencies\lib\glew32.lib opengl32.lib ..\windows-dependencies\lib\assimp-vc140-mt-release.lib 
set LinkerFlags=-opt:ref -incremental:no
IF NOT EXIST build mkdir build
pushd build
del *.pdb > NUL 2> NUL
echo WATING FOR PDB > lock.tmp
cl %CompilerFlags% ..\src\game.cpp -Fe:game_lib -LD -link -PDB:game_lib_%random%.pdb %LinkerFlags% %GameLibraries% -EXPORT:GameUpdate -EXPORT:GameServerUpdate
del lock.tmp
cl %CompilerFlags% ..\src\windows_main.cpp -link %LinkerFlags% %PlatformLibraries%

REM Build tests
REM cl %CompilerFlags% ..\src\tests.cpp
popd
ctime -end engine.ctm
