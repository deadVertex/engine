#define NETSIM_MAX_PACKET_LENGTH 400
#define NETSIM_MAX_PACKETS 64

enum
{
    NetSim_EmptyPacket,
    NetSim_OutgoingPacket,
    NetSim_IncomingPacket,
};

struct NetSim_QueuedPacket
{
    u32 type;
    float releaseTime;
    u32 length;
    u8 data[NETSIM_MAX_PACKET_LENGTH];
};

struct NetSim_State
{
    NetSim_QueuedPacket packets[NETSIM_MAX_PACKETS];

    RandomNumberGenerator rng;
    float outgoingPacketDelay;
    float incomingPacketDelay;
    float packetLoss;
};

internal void NetSim_Initialize(NetSim_State *netSim, u32 seed0 = 0xFF50489E,
        u32 seed1 = 0x72F03BD5)
{
    netSim->rng = CreateRandomNumberGenerator(seed0, seed1);
}

inline NetSim_QueuedPacket *NetSim_AllocatePacket(NetSim_State *netSim, u32 packetType)
{
    for (u32 i = 0; i < ArrayCount(netSim->packets); ++i)
    {
        NetSim_QueuedPacket *packet = netSim->packets + i;
        if (packet->type == NetSim_EmptyPacket)
        {
            packet->type = packetType;
            return packet;
        }
    }

    Assert(!"Failed to allocate network simulator packet");
    return NULL;
}

internal void NetSim_QueuePacket_(NetSim_State *netSim, const void *packetData,
        u32 packetLength, float releaseTime, u32 type )
{
    Assert(type != NetSim_EmptyPacket);
    if (netSim->packetLoss > 0.0f)
    {
        // TODO: Double check that this is correct, probably want and Probability test math function
        float p = RandomFloat(&netSim->rng);
        if (p < netSim->packetLoss)
        {
            LOG_DEBUG("Dropped packet, p=%g loss=%g", p, netSim->packetLoss);
            return; // Drop packet
        }
    }
    NetSim_QueuedPacket *packet = NetSim_AllocatePacket(netSim, type);
    packet->releaseTime = releaseTime;
    packet->length = packetLength;
    Assert(packetLength < NETSIM_MAX_PACKET_LENGTH);
    memcpy(packet->data, packetData, packetLength);
}

internal u32 NetSim_GetPacket_(NetSim_State *netSim, float currentTime,
        void *packetData, u32 length, u32 type)
{
    Assert(type != NetSim_EmptyPacket);
    for (u32 i = 0; i < ArrayCount(netSim->packets); ++i)
    {
        NetSim_QueuedPacket *packet = netSim->packets + i;
        if (packet->type == type)
        {
            if (packet->releaseTime <= currentTime)
            {
                Assert(length >= packet->length);
                memcpy(packetData, packet->data, packet->length);
                packet->type = NetSim_EmptyPacket; // Free the packet
                return packet->length;
            }
        }
    }
    return 0;
}

inline void NetSim_QueueOutgoingPacket(NetSim_State *netSim, const void *packetData,
        u32 packetLength, float currentTime)
{
    float releaseTime = currentTime + netSim->outgoingPacketDelay;
    NetSim_QueuePacket_(netSim, packetData, packetLength, releaseTime, NetSim_OutgoingPacket);
}
inline void NetSim_QueueIncomingPacket(NetSim_State *netSim, const void *packetData,
        u32 packetLength, float currentTime)
{
    float releaseTime = currentTime + netSim->incomingPacketDelay;
    NetSim_QueuePacket_(netSim, packetData, packetLength, releaseTime, NetSim_IncomingPacket);
}

inline u32 NetSim_GetOutgoingPacket(NetSim_State *netSim, float currentTime,
        void *packetData, u32 length)
{
    return NetSim_GetPacket_(netSim, currentTime, packetData, length, NetSim_OutgoingPacket);
}

inline u32 NetSim_GetIncomingPacket(NetSim_State *netSim, float currentTime,
        void *packetData, u32 length)
{
    return NetSim_GetPacket_(netSim, currentTime, packetData, length, NetSim_IncomingPacket);
}
