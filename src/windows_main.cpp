#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <TimeAPI.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <cstdio>

#include "utils.h"
#include "game.h"
#include "logging.h"
#include "socket_layer.cpp"

#define PHYSICS_LIB "bullet_physics.dll"
#define GAME_LIB "game_lib.dll"
#define TEMP_GAME_LIB "game_lib_temp.dll"
#define GAME_CODE_LOCK "lock.tmp"

u32 log_activeChannels = (u32)-1;

struct GameCode
{
    HMODULE handle;
    GameUpdateFunction *update;
    GameServerUpdateFunction *serverUpdate;

    bool isValid;
    FILETIME lastWriteTime;
};

internal FILETIME GetFileLastWriteTime( const char *path )
{
  FILETIME lastWriteTime = {};
  WIN32_FILE_ATTRIBUTE_DATA data;
  if ( GetFileAttributesEx( path, GetFileExInfoStandard, &data ) )
  {
    lastWriteTime = data.ftLastWriteTime;
  }
  return lastWriteTime;
}

struct PhysicsCode
{
    HMODULE handle;
    CreatePhysicsSystemFunction *createPhysicsSystem;
    DestroyPhysicsSystemFunction *destroyPhysicsSystem;
    bool isValid;
};
internal PhysicsCode LoadPhysicsCode(const char *libraryName)
{
    PhysicsCode result = {};
    result.handle = LoadLibraryA(libraryName);

    if (result.handle)
    {
        result.createPhysicsSystem = (CreatePhysicsSystemFunction *)GetProcAddress(
                result.handle, "CreatePhysicsSystem");
        result.destroyPhysicsSystem = (DestroyPhysicsSystemFunction *)GetProcAddress(
                result.handle, "DestroyPhysicsSystem");
        result.isValid = result.createPhysicsSystem && result.destroyPhysicsSystem;
    }
    else
    {
        DWORD error = GetLastError();
        LPVOID msgBuffer;
        FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                              FORMAT_MESSAGE_FROM_SYSTEM |
                              FORMAT_MESSAGE_IGNORE_INSERTS,
                      NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                      (LPTSTR)&msgBuffer, 0, NULL);

        LOG_ERROR("Failed to load game code from %s, %s\n", libraryName,
                  (char *)msgBuffer);
        LocalFree(msgBuffer);
    }

    return result;
}
internal bool IsLockFileActive(const char *lockFileName)
{
    WIN32_FILE_ATTRIBUTE_DATA ignored;
    if (!GetFileAttributesEx(lockFileName, GetFileExInfoStandard, &ignored))
        return false;

    return true;
}
internal GameCode LoadGameCode( const char *libraryName, const char *tempLibraryName)
{
    GameCode result = {};
    CopyFile( libraryName, tempLibraryName, FALSE );
    result.lastWriteTime = GetFileLastWriteTime( libraryName );

    result.handle = LoadLibraryA( tempLibraryName );

    if ( result.handle )
    {
        result.update = (GameUpdateFunction*)GetProcAddress(result.handle, "GameUpdate");
        result.serverUpdate = (GameServerUpdateFunction*)GetProcAddress(result.handle, "GameServerUpdate");
        result.isValid = result.update && result.serverUpdate;
    }
    else
    {
        DWORD error = GetLastError();
        LPVOID msgBuffer;
        FormatMessage(
                FORMAT_MESSAGE_ALLOCATE_BUFFER | 
                FORMAT_MESSAGE_FROM_SYSTEM |
                FORMAT_MESSAGE_IGNORE_INSERTS,
                NULL,
                error,
                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                (LPTSTR) &msgBuffer,
                0, NULL );

        LOG_ERROR( "Failed to load game code from %s, %s\n", libraryName, (char*)msgBuffer);
        LocalFree(msgBuffer);
    }

    return result;
}

internal void UnloadGameCode( GameCode *gameCode )
{
  if ( gameCode->handle )
  {
    FreeLibrary( gameCode->handle );
    gameCode->handle = 0;
  }
  ZeroPointerToStruct( gameCode );
}

internal void *AllocateMemory( size_t numBytes, size_t baseAddress = 0 )
{
  void *result = VirtualAlloc( (void*)baseAddress, numBytes, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE );

  Assert( result );
  return result;
}

internal void FreeMemory( void *p )
{
  VirtualFree( p, 0, MEM_RELEASE );
}

internal DEBUG_FREE_FILE_MEMORY( DebugFreeFileMemory )
{
  FreeMemory( memory );
}

internal DEBUG_READ_ENTIRE_FILE( DebugReadEntireFile )
{
  ReadFileResult result = {};
  HANDLE file = CreateFileA( path, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0 );
  if ( file != INVALID_HANDLE_VALUE )
  {
    LARGE_INTEGER tempSize;
    if ( GetFileSizeEx( file, &tempSize ) )
    {
      result.size = SafeTruncateU64ToU32( tempSize.QuadPart );
      result.memory = AllocateMemory( result.size );
      if ( result.memory )
      {
        DWORD bytesRead;
        if ( !ReadFile( file, result.memory, result.size, &bytesRead, 0 ) || ( result.size != bytesRead ) )
        {
          LOG_ERROR( "Failed to read file %s", path );
          DebugFreeFileMemory( result.memory );
          result.memory = nullptr;
          result.size = 0;
        }
      }
      else
      {
        LOG_ERROR( "Failed to allocate %d bytes for file %s", result.size, path );
      }
    }
    else
    {
      LOG_ERROR( "Failed to read file size for file %s", path );
    }
    CloseHandle( file );
  }
  else
  {
    LOG_ERROR( "Failed to open file %s", path );
  }
  return result;
}

internal PlatformEvent platformEvents[1024];
internal u32 platformEventCount;

#define KEY_HELPER(NAME)                                                       \
    case GLFW_KEY_##NAME:                                                      \
        return K_##NAME;
internal u8 ConvertKey(int key)
{
    if (key >= GLFW_KEY_SPACE && key <= GLFW_KEY_GRAVE_ACCENT)
    {
        return (u8)key;
    }
    switch (key)
    {
        KEY_HELPER(BACKSPACE);
        KEY_HELPER(TAB);
        KEY_HELPER(INSERT);
        KEY_HELPER(HOME);
        KEY_HELPER(PAGE_UP);
    // Can't use KEY_HELPER( DELETE ) as windows has a #define for DELETE
    case GLFW_KEY_DELETE:
        return K_DELETE;
        KEY_HELPER(END);
        KEY_HELPER(PAGE_DOWN);
        KEY_HELPER(ENTER);

        KEY_HELPER(LEFT_SHIFT);
    case GLFW_KEY_LEFT_CONTROL:
        return K_LEFT_CTRL;
        KEY_HELPER(LEFT_ALT);
        KEY_HELPER(RIGHT_SHIFT);
    case GLFW_KEY_RIGHT_CONTROL:
        return K_RIGHT_CTRL;
        KEY_HELPER(RIGHT_ALT);

        KEY_HELPER(LEFT);
        KEY_HELPER(RIGHT);
        KEY_HELPER(UP);
        KEY_HELPER(DOWN);

        KEY_HELPER(ESCAPE);

        KEY_HELPER(F1);
        KEY_HELPER(F2);
        KEY_HELPER(F3);
        KEY_HELPER(F4);
        KEY_HELPER(F5);
        KEY_HELPER(F6);
        KEY_HELPER(F7);
        KEY_HELPER(F8);
        KEY_HELPER(F9);
        KEY_HELPER(F10);
        KEY_HELPER(F11);
        KEY_HELPER(F12);
    case GLFW_KEY_KP_0:
        return K_NUM0;
    case GLFW_KEY_KP_1:
        return K_NUM1;
    case GLFW_KEY_KP_2:
        return K_NUM2;
    case GLFW_KEY_KP_3:
        return K_NUM3;
    case GLFW_KEY_KP_4:
        return K_NUM4;
    case GLFW_KEY_KP_5:
        return K_NUM5;
    case GLFW_KEY_KP_6:
        return K_NUM6;
    case GLFW_KEY_KP_7:
        return K_NUM7;
    case GLFW_KEY_KP_8:
        return K_NUM8;
    case GLFW_KEY_KP_9:
        return K_NUM9;
    case GLFW_KEY_KP_DECIMAL:
        return K_NUM_DECIMAL;
    case GLFW_KEY_KP_DIVIDE:
        return K_NUM_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY:
        return K_NUM_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT:
        return K_NUM_MINUS;
    case GLFW_KEY_KP_ADD:
        return K_NUM_PLUS;
    case GLFW_KEY_KP_ENTER:
        return K_NUM_ENTER;
    }
    return K_UNKNOWN;
}

internal void KeyCallback(GLFWwindow *window, int key, int scancode, int action,
                          int mods)
{
    PlatformEvent event = {};
    event.key = ConvertKey(key);
    if (action == GLFW_RELEASE)
        event.type = PlatformEvent_KeyRelease;
    else if (action == GLFW_PRESS)
        event.type = PlatformEvent_KeyPress;

    if (platformEventCount < ArrayCount(platformEvents))
    {
        platformEvents[platformEventCount++] = event;
    }
}

internal u8 ConvertMouseButton(int button)
{
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT:
        return K_MOUSE_BUTTON_LEFT;
    case GLFW_MOUSE_BUTTON_MIDDLE:
        return K_MOUSE_BUTTON_MIDDLE;
    case GLFW_MOUSE_BUTTON_RIGHT:
        return K_MOUSE_BUTTON_RIGHT;
    }
    return K_UNKNOWN;
}

internal void MouseButtonCallback(GLFWwindow *window, int button, int action,
                                  int mods)
{
    PlatformEvent event = {};
    event.key = ConvertMouseButton(button);
    event.type = (action == GLFW_PRESS) ? (u8)PlatformEvent_KeyPress
                                        : (u8)PlatformEvent_KeyRelease;
    if (platformEventCount < ArrayCount(platformEvents))
    {
        platformEvents[platformEventCount++] = event;
    }

    Unused(window);
    Unused(mods);
}

internal void CharacterCallback(GLFWwindow *window, u32 codepoint)
{
    PlatformEvent event = {};
    event.type = PlatformEvent_Character;
    event.character = codepoint;
    if (platformEventCount < ArrayCount(platformEvents))
    {
        platformEvents[platformEventCount++] = event;
    }
}

global u32 windowWidth = 1024;
global u32 windowHeight = 768;
internal void WindowResizeCallback( GLFWwindow *window, int width, int height )
{
    windowWidth = width;
    windowHeight = height;
}

global bool gWindowHasFocus = true;
internal void WindowFocusCallback(GLFWwindow *window, int focused)
{
    gWindowHasFocus = focused == GLFW_TRUE;
}


global GLFWwindow *window;
internal void SetMouseCursor(u8 cursor)
{
    if (cursor == MOUSE_CURSOR_HIDDEN)
    {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
    else if (cursor == MOUSE_CURSOR_VISIBLE)
    {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
}

global u64 globalPerfFrequency;
global LARGE_INTEGER globalStartTime;
inline LARGE_INTEGER Win32_GetWallClock()
{
  LARGE_INTEGER result;
  QueryPerformanceCounter(&result);
  return result;
}

inline double Win32_GetSecondsElapsed(LARGE_INTEGER start, LARGE_INTEGER end)
{
  u64 elapsed = end.QuadPart - start.QuadPart;
  double result = (double)elapsed / (double)globalPerfFrequency;
  return result;
}

inline double Win32_GetTime()
{
    LARGE_INTEGER currentTime = Win32_GetWallClock();
    return Win32_GetSecondsElapsed(globalStartTime, currentTime);
}

internal SEND_PACKET_TO_SERVER(SendPacketToServer)
{
    Assert(length > 0);
    sock_ClientSendPacket(data, (int)length);
}

global struct sockaddr_storage clientAddresses[1024];
global u32 clientAddressCount = 1; // Skip 0th index to reserve it for invalid address id

internal SEND_PACKET_TO_CLIENT(SendPacketToClient)
{
    Assert(length > 0 && clientId > 0);
    Assert(clientId < clientAddressCount);
    sock_ServerSendPacket(data, length, &clientAddresses[clientId]);
}

internal DEBUG_GET_TIME(DebugGetTime)
{
    return glfwGetTime();
}

global PhysicsCode physicsCode;

global GameMemory gameMemory;
global GameMemory gameServerMemory;
global PhysicsSystem *serverPhysicsSystem;
global bool gIsServerRunning = false;
global bool gIsClientRunning = false;
internal void StartServer()
{
    if (!gIsServerRunning)
    {
        sock_StartServer();
        LOG_DEBUG("Started listen server");

        gameServerMemory.persistentStorageSize = 16 * 1024 * 1024;
        gameServerMemory.persistentStorageBase =
            AllocateMemory(gameServerMemory.persistentStorageSize, 0);
        gameServerMemory.readEntireFile = &DebugReadEntireFile;
        gameServerMemory.freeFileMemory = &DebugFreeFileMemory;
        gameServerMemory.sendPacketToClient = &SendPacketToClient;

        serverPhysicsSystem = physicsCode.createPhysicsSystem();
        gameServerMemory.serverPhysicsSystem = serverPhysicsSystem;


        gameMemory.serverPhysicsSystem = serverPhysicsSystem;
        gIsServerRunning = true;
    }
}

internal void StartClient()
{
    if (!gIsClientRunning)
    {
        if (sock_ConnectToServer("localhost", "18000"))
        {
            LOG_DEBUG("Successfully opened UDP socket to server '%s'", "localhost");
        }
        gIsClientRunning = true;
    }
}

global bool gIsRunning = true;
internal PERFORM_PLATFORM_OP(PerformPlatformOp)
{
    switch (op)
    {
        case PlatformOp_StartServer:
            StartServer();
            break;
        case PlatformOp_ConnectToServer:
            StartClient();
            break;
        case PlatformOp_Exit:
            gIsRunning = false;
            break;
        default:
            break;
    }
}

#define MAX_CMD_ARGS 64
internal const char **GetCommandLineArguments(char *commandLine, int *argv)
{
    static const char *commandLineArguments[MAX_CMD_ARGS];

    u32 count = 0;
    char *cursor = commandLine;
    bool complete = false;

    while (count < ArrayCount(commandLineArguments) && !complete)
    {
        commandLineArguments[count++] = cursor;

        bool inQuotes = false;
        while (*cursor != '\0')
        {
            if (*cursor == '\"')
            {
                inQuotes = !inQuotes;
            }
            if (*cursor == ' ' && !inQuotes)
            {
                *cursor = '\0';
                cursor++;
                break;
            }
            cursor++;
        }
        if (*cursor == '\0')
            break;
    }

    *argv = (int)count;
    return commandLineArguments;
}

int CALLBACK
WinMain(
   HINSTANCE instance,
   HINSTANCE prevInstance,
   LPSTR cmdLine,
   int cmdShow
)
{
    int argc;
    const char **argv = GetCommandLineArguments(GetCommandLine(), &argc);
    if (argv == NULL)
    {
        LOG_ERROR("Failed to parse command line arguments.");
        return -1;
    }

    PlatformEvent initEvents[64];
    u32 initEventCount = 0;
    for (int i = 1; i < argc; ++i)
    {
        if (strcmp(argv[i], "--listen") == 0)
        {
            PlatformEvent event = {};
            event.type = PlatformEvent_StartServerArg;
            if (initEventCount < ArrayCount(initEvents))
            {
                initEvents[initEventCount++] = event;
            }
        }
        else if (strcmp(argv[i], "--connect") == 0)
        {
            PlatformEvent event = {};
            event.type = PlatformEvent_ConnectToArg;
            if (initEventCount < ArrayCount(initEvents))
            {
                initEvents[initEventCount++] = event;
            }
        }
    }

    const char *connectToAddress = "localhost";

    LARGE_INTEGER perfFrequencyResult;
    QueryPerformanceFrequency(&perfFrequencyResult);
    globalPerfFrequency = perfFrequencyResult.QuadPart;

    globalStartTime = Win32_GetWallClock();

    u32 targetSchedulerMilliseconds = 1;
    bool isSleepGranular = (timeBeginPeriod(targetSchedulerMilliseconds) ==
            TIMERR_NOERROR);

    if (glfwInit() != GLFW_TRUE)
    {
        LOG_DEBUG("Failed to initialize GLFW.");
        return -1;
    }
    glfwWindowHint(GLFW_DECORATED, true);
    glfwWindowHint(GLFW_RESIZABLE, true);

    window = glfwCreateWindow(1024, 768, "Engine", NULL, NULL);

    if (window == NULL)
    {
        LOG_DEBUG("Failed to create window.");
        glfwTerminate();
        return -1;
    }
    glfwSetMouseButtonCallback(window, MouseButtonCallback);
    glfwSetKeyCallback(window, KeyCallback);
    glfwSetWindowFocusCallback(window, WindowFocusCallback);
    glfwSetFramebufferSizeCallback( window, WindowResizeCallback );
    glfwSetCharCallback(window, CharacterCallback);

    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);

    sock_Initialize();

    physicsCode = LoadPhysicsCode(PHYSICS_LIB);
    Assert(physicsCode.isValid);

    GameCode gameCode = LoadGameCode( GAME_LIB, TEMP_GAME_LIB);
    if (!gameCode.isValid)
    {
        LOG_ERROR("Failed to load game code.");
        glfwTerminate();
        return -1;
    }

    gameMemory.persistentStorageSize = 256 * 1024 * 1024;
    gameMemory.persistentStorageBase =
            AllocateMemory(gameMemory.persistentStorageSize, 0);
    gameMemory.readEntireFile = &DebugReadEntireFile;
    gameMemory.freeFileMemory = &DebugFreeFileMemory;
    gameMemory.setMouseCursor = &SetMouseCursor;
    gameMemory.sendPacketToServer = &SendPacketToServer;
    gameMemory.getTime = &DebugGetTime;
    gameMemory.performPlatformOp = &PerformPlatformOp;

    i32 serverUpdateRate = 60;
    double serverFixedTimestep = 1.0 / serverUpdateRate;
    double timeOfLastServerUpdate = 0;
    i32 frameRateCap = 145; // FIXME: Capping to 60 because client send rate tied to frame rate
    double minFrameTime = 1.0 / (double)frameRateCap;
    double maxFrameTime = 0.25;
    double currentTime = glfwGetTime();
    double sleepMaxError = 0.0;
    while (gIsRunning)
    {
        double loopFrameTime = glfwGetTime() - currentTime;
        /* LOG_DEBUG("loopFrameTime: %g", loopFrameTime * 1000.0); */

        double newTime = glfwGetTime();
        double dt = newTime - currentTime;
        if (dt > maxFrameTime)
            dt = maxFrameTime;

        currentTime = newTime;

        u32 fps = (u32)(1.0 / dt);
        //LOG_DEBUG("FPS: %u (%g ms)", fps, dt * 1000.0);

        platformEventCount = 0;
        for (u32 i = 0; i < initEventCount; ++i)
        {
            if (platformEventCount < ArrayCount(platformEvents))
            {
                platformEvents[platformEventCount++] = initEvents[i];
            }
        }
        initEventCount = 0;

        glfwPollEvents();
        if (glfwWindowShouldClose(window))
        {
            gIsRunning = false;
        }

        if (gWindowHasFocus)
        {
            double x, y;
            glfwGetCursorPos(window, &x, &y);
            PlatformEvent event;
            event.type = PlatformEvent_MouseMotion;
            event.mouseMotion.x = x;
            event.mouseMotion.y = y;
            if (platformEventCount < ArrayCount(platformEvents))
            {
                platformEvents[platformEventCount++] = event;
            }
        }

        FILETIME newWriteTime = GetFileLastWriteTime(GAME_LIB);
        if ( CompareFileTime( &newWriteTime, &gameCode.lastWriteTime ) != 0 )
        {
            if (!IsLockFileActive(GAME_CODE_LOCK))
            {
                UnloadGameCode( &gameCode );
                auto newGameCode = LoadGameCode( GAME_LIB, TEMP_GAME_LIB);
                if (newGameCode.isValid)
                {
                    gameCode = newGameCode;
                    gameMemory.wasCodeReloaded = true;
                    gameServerMemory.wasCodeReloaded = true;
                    LOG_DEBUG("Game code reloaded!");
                }
            }
            //else
            //{
                //LOG_ERROR("Failed to load new game code, trying again");
                //for (u32 attempt = 0; attempt < 10; ++attempt)
                //{
                    //Sleep(1);
                    //newGameCode = LoadGameCode( GAME_LIB, TEMP_GAME_LIB, GAME_CODE_LOCK );
                    //if (newGameCode.isValid)
                    //{
                        //gameCode = newGameCode;
                        //gameMemory.wasCodeReloaded = true;
                        //gameServerMemory.wasCodeReloaded = true;
                        //LOG_DEBUG("Game code reloaded!");
                        //break;
                    //}
                //}
                //if (!newGameCode.isValid)
                //{
                    //LOG_ERROR("Unable to load game code");
                //}
            //}
        }

        if (gIsServerRunning)
        {
#define MAX_PACKET_LENGTH 400
#define MAX_PACKETS_PER_SERVER_TICK 64
            // TODO: Handle case where we may need to perform multiple server updates due to frame hitch
            if (glfwGetTime() - timeOfLastServerUpdate > serverFixedTimestep)
            {
                timeOfLastServerUpdate = glfwGetTime();
                // TODO: Handle packets from more than one client or more than one packet per server tick
                char packetData[MAX_PACKETS_PER_SERVER_TICK][MAX_PACKET_LENGTH]; // 25.6KB
                sockaddr_storage receiveAddress;
                PlatformEvent serverEvents[MAX_PACKETS_PER_SERVER_TICK];
                u32 serverEventCount = 0;

                for (u32 packetCount = 0; packetCount < MAX_PACKETS_PER_SERVER_TICK; ++packetCount)
                {
                    u32 receiveAddressLength = sizeof(receiveAddress);
                    int dataLength = NonBlockingReceiveFrom((int)serverSocket, packetData[packetCount], sizeof(packetData[packetCount]),
                            (struct sockaddr *)&receiveAddress,
                            &receiveAddressLength);
                    if (dataLength > 0)
                    {
                        if (dataLength > MAX_PACKET_LENGTH)
                        {
                            if (dataLength == WSAEMSGSIZE)
                            {
                                // Drop packet - message exceeds max packet length
                                LOG_WARN("Error received packet larger than MAX_PACKET_LENGTH of %u", MAX_PACKET_LENGTH);
                                continue;
                            }
                            else
                            {
                                // Drop packet - some other error occurred
                                LOG_WARN("Error occurred while receiving packet from client: %d", dataLength);
                                continue;
                            }
                        }

                        // Map receiveAddress to clientId
                        u32 clientId = 0;

                        // Check cache to see if we have already received a packet from this client
                        for (u32 i = 0; i < clientAddressCount; ++i)
                        {
                            if (sock_CompareAddresses(&receiveAddress, &clientAddresses[i]))
                            {
                                clientId = i;
                                break;
                            }
                        }

                        // If address not in cache then this must be a new client connection so we add it to the cache
                        if (clientId == 0)
                        {
                            // TODO: Use freelist
                            if (clientAddressCount < ArrayCount(clientAddresses))
                            {
                                clientAddresses[clientAddressCount] = receiveAddress;
                                clientId = clientAddressCount++;
                            }
                            else
                            {
                                // Refuse connection - server is full, assuming we are using a freelist
                                // This might be a good reason to move client address management into game code
                                // as that way we can use the same mechanism for refusing connections as it uses.
                                continue;
                            }
                        }

                        serverEvents[packetCount].type = PlatformEvent_PacketReceived;
                        serverEvents[packetCount].packetReceived.packetData = (u8*)packetData[packetCount];
                        serverEvents[packetCount].packetReceived.packetLength = SafeTruncateU32ToU16(dataLength);
                        serverEvents[packetCount].packetReceived.addressId = clientId;
                        serverEventCount++;
                    }
                }

                // Server Update
                gameCode.serverUpdate(&gameServerMemory, (float)dt, serverEvents, serverEventCount);

                // TODO: Free packet memory
            }
        }

        if (gIsClientRunning)
        {
#define NET_SERVER_ADDRESS_ID 0xFFFFFFFF
            // Receive packets from server
            // TODO: Support receiving more than 1 packet per frame
            u8 packetData[MAX_PACKET_LENGTH];
            for (u32 packetCount = 0; packetCount < 1; ++packetCount)
            {
                i32 receiveAddressLength = sizeof(serverAddress);
                int dataLength = NonBlockingReceiveFrom((int)clientSocket, packetData, sizeof(packetData),
                        NULL, 0);
                if (dataLength > 0)
                {
                    if (dataLength > MAX_PACKET_LENGTH)
                    {
                        if (dataLength == WSAEMSGSIZE)
                        {
                            // Drop packet - message exceeds max packet length
                            LOG_WARN("Error received packet larger than MAX_PACKET_LENGTH of %u", MAX_PACKET_LENGTH);
                            continue;
                        }
                        else
                        {
                            // Drop packet - some other error occurred
                            LOG_WARN("Error occurred while receiving packet from client: %d", dataLength);
                            continue;
                        }
                    }

                    PlatformEvent packetReceivedEvent = {};
                    packetReceivedEvent.type = PlatformEvent_PacketReceived;
                    packetReceivedEvent.packetReceived.packetData = packetData;
                    packetReceivedEvent.packetReceived.packetLength = SafeTruncateU32ToU16(dataLength);
                    packetReceivedEvent.packetReceived.addressId = NET_SERVER_ADDRESS_ID;

                    if (platformEventCount < ArrayCount(platformEvents))
                        platformEvents[platformEventCount++] = packetReceivedEvent;
                }
            }
        }

        gameCode.update(&gameMemory, (float)dt, platformEvents, platformEventCount, windowWidth, windowHeight);
        gameMemory.wasCodeReloaded = false;
        gameServerMemory.wasCodeReloaded = false;

        glfwSwapBuffers(window);
        double totalFrameTime = glfwGetTime() - currentTime;
        if (totalFrameTime < minFrameTime)
        {
            double remainder = minFrameTime - totalFrameTime;
            /* LOG_DEBUG("totalFrameTime: %g ms", totalFrameTime * 1000.0); */
            if (isSleepGranular)
            {
                u32 millisecondsRemaining = (u32)(remainder * 1000.0);
                if (millisecondsRemaining > 2)
                {
                    /* LOG_DEBUG("millisecondsRemaining: %u", millisecondsRemaining); */
                    millisecondsRemaining -= 1;
                    double sleepStart = glfwGetTime();
                    Sleep(millisecondsRemaining);
                    double sleepTime = glfwGetTime() - sleepStart;
                    double sleepError = sleepTime - ((double)millisecondsRemaining / (1000.0));
                    if (sleepError > sleepMaxError)
                        sleepMaxError = sleepError;
                    /* LOG_DEBUG("sleepTime: %g ms error %g ms, max error %g ms", sleepTime * 1000.0, sleepError * 1000.0, sleepMaxError * 1000.0); */
                }
            }

            totalFrameTime = glfwGetTime() - currentTime;
            remainder = minFrameTime - totalFrameTime;
            if (remainder > 0.0)
            {
                double targetTime = glfwGetTime() + remainder;
                while (glfwGetTime() <= targetTime);
            }

            double waitedFrameTime = glfwGetTime() - currentTime;
            /* LOG_DEBUG("target: %g ms waitedFrameTime: %g ms error: %g ms", minFrameTime * 1000.0, waitedFrameTime * 1000.0, (waitedFrameTime - minFrameTime) * 1000.0); */
        }

        double ifFrameTime = glfwGetTime() - currentTime;
        /* LOG_DEBUG("ifFrameTime: %g", ifFrameTime * 1000.0); */

        double a = glfwGetTime();
        double b = glfwGetTime();
        /* LOG_DEBUG("Difference: %g ms", (b - a) * 1000.0); */

    }
    if (serverPhysicsSystem != NULL)
        physicsCode.destroyPhysicsSystem(serverPhysicsSystem);

    sock_CleanupSockets(); 
    glfwTerminate();
    return 0;
}
