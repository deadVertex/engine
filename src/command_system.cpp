typedef void CommandFunction(u32 argc, char **argv);

#define cmd_NameLength 20
struct Command
{
    char name[cmd_NameLength];
    CommandFunction *function;
};

#define cmd_MaxCommands 64
struct CommandSystem
{
    Command commands[cmd_MaxCommands];
    u32 count;
};

internal void cmd_Register(CommandSystem *cmdSystem, const char *name,
                           CommandFunction *function)
{
    Assert(cmdSystem->count < ArrayCount(cmdSystem->commands));
    Command *cmd = cmdSystem->commands + cmdSystem->count++;
    strncpy(cmd->name, name, ArrayCount(cmd->name));
    cmd->function = function;
}

inline bool StringCompare(const char *a, const char *b, u32 length)
{
    for (u32 i = 0; i < length; ++i)
    {
        if (a[i] != b[i])
            return false;
    }
    return true;
}

internal void cmd_Exec(CommandSystem *cmdSystem, const char *string)
{
    const char *cursor = string;
    const char *cmdStart = string;
    const char *argStart = string;
    u32 cmdLength = 0;

    // Find where command ends and arguments start
    while (*cursor != '\0')
    {
       if (*cursor == ' ') 
       {
           argStart = cursor + 1;
           break;
       }
       cursor++;
       cmdLength++;
    }

    // FIXME: Potential for matching multiple commands, string "ech" would match
    // "echo1" and "echo2"
    if (cmdLength > 0)
    {
        for (u32 cmdIndex = 0; cmdIndex < cmdSystem->count; ++cmdIndex)
        {
            if (StringCompare(cmdSystem->commands[cmdIndex].name, string, cmdLength))
            {
                // TODO: Handle arguments
                cmdSystem->commands[cmdIndex].function(0, NULL);
                break;
            }
        }
    }
}
