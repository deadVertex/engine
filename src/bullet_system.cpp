#define MAX_BULLETS 4096
struct BulletState
{
    vec3 position;
    vec3 velocity;
    u32 owner;
    PhysicsBodyHandle owningBody;
    float damage;
};

struct BulletSystem
{
    BulletState bullets[MAX_BULLETS];
    u32 count;
    float maxVelocity;
    vec3 gravity;
};

inline vec3 CalculateVelocity(vec3 acceleration, vec3 velocity, vec3 gravity, float dt)
{
    acceleration += gravity;
    float airFriction = 0.05f;
    acceleration += (velocity * airFriction);
    vec3 result = acceleration * dt + velocity;
    return result;
}

internal void BulletSystem_RemoveAtIndex(BulletSystem *bulletSystem, u32 index)
{
    if (bulletSystem->count > 0)
    {
        u32 last = bulletSystem->count - 1;
        bulletSystem->bullets[index] = bulletSystem->bullets[last];
        bulletSystem->count--;
    }
}

inline void BulletSystem_DrawBulletTrace(BulletState *bullet, float maxVelocity, float dt)
{
    vec3 end = bullet->position + bullet->velocity * dt;
    float speed = Length(bullet->velocity);
    float t = speed / maxVelocity;
    vec3 colour = Lerp(Vec3(1,0,0), Vec3(0,1,0), t);
    Debug_DrawLine(bullet->position, end, colour, 10.0f);
}

struct BulletImpactEvent
{
    u32 owner;
    vec3 hitPoint;
    vec3 hitNormal;
    PhysicsBodyHandle hitBody;
};

internal u32 BulletSystem_Update(BulletSystem *bulletSystem, PhysicsSystem *physicsSystem, float dt,
        BulletImpactEvent *impactEvents, u32 length)
{
    u32 count = 0;
    u32 index = 0;
    while (index < bulletSystem->count)
    {
        BulletState *bullet = bulletSystem->bullets + index;
        BulletSystem_DrawBulletTrace(bullet, bulletSystem->maxVelocity, dt);

        PhysicsRaycastParameters raycastParams = {};
        raycastParams.start = bullet->position;
        raycastParams.end = bullet->position + bullet->velocity * dt;
        raycastParams.filterGroup = COLLISION_TYPE_DEFAULT;
        raycastParams.filterMask = COLLISION_TYPE_ALL;
        // TODO: Ignore owning body
        //raycastParams.ignored = &bullet->owningBody;
        //raycastParams.ignoredCount = 1;


        PhysicsRaycastResult result = {};
        if (PhysicsRaycast(physicsSystem, &raycastParams, &result))
        {
            bullet->position = result.point;
            bullet->velocity = Vec3(0);
            //Debug_DrawPoint(bullet->position, Vec3(0,1,0), 0.2f, 10.0f);

            BulletSystem_RemoveAtIndex(bulletSystem, index);
            // If server check if we hit hitbox and do damage
            if (count < length)
            {
                BulletImpactEvent *impactEvent = impactEvents + count++;
                impactEvent->hitPoint = result.point;
                impactEvent->hitNormal = result.normal;
                impactEvent->hitBody = result.body;
                impactEvent->owner = bullet->owner;
            }
        }
        else
        {
            bullet->position += bullet->velocity * dt;
            bullet->velocity = CalculateVelocity(Vec3(0), bullet->velocity,
                    bulletSystem->gravity, dt);
            ++index;
        }
    }

    return count;
}

internal void BulletSystem_Spawn(BulletSystem *bulletSystem, vec3 origin,
        vec3 direction, float acceleration, float dt, u32 owner, float damage)
{
    if (bulletSystem->count < ArrayCount(bulletSystem->bullets))
    {
        BulletState *bullet = bulletSystem->bullets + bulletSystem->count++;
        bullet->owner = owner;
        bullet->damage = damage;
        bullet->position = origin;
        bullet->velocity = CalculateVelocity(direction * acceleration, Vec3(0),
                bulletSystem->gravity, dt);
        float magnitude = Length(bullet->velocity);
        bulletSystem->maxVelocity = Max(bulletSystem->maxVelocity, Length(bullet->velocity));
    }
}
