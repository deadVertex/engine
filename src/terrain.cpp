struct HeightMap
{
  float *values;
  u32 width;
  u32 height;
};

internal HeightMap CreateHeightMap(const void *data, u32 length,
    MemoryArena *arena)
{
    HeightMap result = {};
    i32 w, h, n;
    stbi_set_flip_vertically_on_load( 1 );

    u8 *pixels = stbi_load_from_memory( (const u8 *)data,
                                         length, &w, &h, &n, 1 );
    if ( pixels )
    {
      Assert( w == h );// TODO: remove this
      result.values = AllocateArray( arena, float, w * w );
      result.width = w;
      result.height = h;
      for ( i32 y = 0; y < w; ++y )
      {
        for ( i32 x = 0; x < w; ++x )
        {
          result.values[y * w + x] = pixels[y * w + x] / 255.0f;
        }
      }
      stbi_image_free( pixels );
    }
    return result;
}

#define SampleHeightMap( X, Y ) heightMap.values[(Y)*heightMap.width + (X)]

inline float HeightMapGetValue( HeightMap heightMap, float u, float v )
{
  Assert( u >= 0.0f && u <= 1.0f );
  Assert( v >= 0.0f && v <= 1.0f );
  u = u * ( heightMap.width - 1 );
  v = v * ( heightMap.height - 1 );
  int x = Floor( u );
  int y = Floor( v );
  float s = u - x;
  float t = v - y;
  //printf( "x=%d y=%d u=%g v=%g s=%g t=%g\n", x, y, u, v, s, t );
  float negS = 1.0f - s;
  float negT = 1.0f - t;
  return ( SampleHeightMap( x, y ) * negS + SampleHeightMap( x + 1, y ) * s ) * negT +
         ( SampleHeightMap( x, y + 1 ) * negS + SampleHeightMap( x + 1, y + 1 ) * s ) *
           t;
}

struct QuadPatchVertex
{
  vec3 position;
  vec3 normal;
  vec2 textureCoordinate;
};

struct Terrain
{
  QuadPatchVertex *vertices;
  u32 *indices;
  u32 numVertices;
  u32 numIndices;
};

struct TerrainParameters
{
  vec3 origin;
  vec3 scale;
  vec2 uvOffset;
  float uvScale;
  u32 rows;
  u32 cols;
};

Terrain CreateTerrain(HeightMap heightMap, TerrainParameters params,
                        MemoryArena *arena )
{
  Terrain result = {};
  result.numVertices = ( params.rows + 1 ) * ( params.cols + 1 );
  result.vertices = AllocateArray( arena, QuadPatchVertex, result.numVertices );
  result.numIndices = params.rows * params.cols * 6;
  result.indices = AllocateArray( arena, u32, result.numIndices );

  u32 verticesIndex = 0;
  for ( u32 row = 0; row < params.rows + 1; ++row )
  {
    for ( u32 col = 0; col < params.cols + 1; ++col )
    {
      auto vertex = result.vertices + verticesIndex++;
      // Calculate what percentage of the dimensions the vertex should be at.
      float x = (float)col / (float)params.cols;
      float y = (float)row / (float)params.rows;
      float z = HeightMapGetValue( heightMap, x, y );
      vertex->position = params.origin + Vec3( x, z, y ) * params.scale;
      vertex->textureCoordinate =
        params.uvOffset + Vec2(x, y) * params.uvScale;
      vertex->normal = Vec3( 0, 1, 0 );
    }
  }

  u32 indicesIndex = 0;
  for ( u32 row = 0; row < params.rows; ++row )
  {
    for ( u32 col = 0; col < params.cols; ++col )
    {
      u32 pitch = params.cols + 1;
      // Calculate the indices for the first triangle in the quad.
      u32 i = col + ( row * pitch );
      u32 j = ( col + 1 ) + ( row * pitch );
      u32 k = ( col + 1 ) + ( ( row + 1 ) * pitch );
      vec3 v0 = result.vertices[i].position;
      vec3 v1 = result.vertices[j].position;
      vec3 v2 = result.vertices[k].position;
      vec3 normal = Cross( v0 - v1, v2 - v1 );
      if (!Normalize(&normal))
      {
          Assert(!"Failed to normalize");
      }

      result.vertices[i].normal = normal;
      result.vertices[j].normal = normal;
      result.vertices[k].normal = normal;
      result.indices[indicesIndex] = i;
      result.indices[indicesIndex + 1] = j;
      result.indices[indicesIndex + 2] = k;

      // Calculate the indices for the second triangle in the quad.
      i = ( col + 1 ) + ( ( row + 1 ) * pitch );
      j = col + ( ( row + 1 ) * pitch );
      k = col + ( row * pitch );
      v0 = result.vertices[i].position;
      v1 = result.vertices[j].position;
      v2 = result.vertices[k].position;
      normal = Cross( v0 - v1, v2 - v1 );
      if (!Normalize(&normal))
      {
          Assert(!"Failed to normalize");
      }
      result.vertices[i].normal = normal;
      result.vertices[j].normal = normal;
      result.vertices[k].normal = normal;
      result.indices[indicesIndex + 3] = i;
      result.indices[indicesIndex + 4] = j;
      result.indices[indicesIndex + 5] = k;
      indicesIndex += 6;
    }
  }

  return result;
}

internal OpenGL_StaticMesh CreateTerrainMesh(Terrain terrain)
{
  OpenGL_VertexAttribute attribs[3];
  attribs[0].index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = OPENGL_VERTEX_ATTRIBUTE_NORMAL;
  attribs[1].numComponents = 3;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof( float ) * 3;
  attribs[2].index = OPENGL_VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
  attribs[2].numComponents = 2;
  attribs[2].componentType = GL_FLOAT;
  attribs[2].normalized = GL_FALSE;
  attribs[2].offset = sizeof( float ) * 6;

  return OpenGL_CreateStaticMesh(terrain.vertices, terrain.numVertices,
    terrain.indices, terrain.numIndices, sizeof( QuadPatchVertex ),
    attribs, 3, GL_TRIANGLES );
}
