#pragma once

#include "utils.h"
#include "math_lib.h"

#define PHYSICS_DEBUG_MAX_LINES 0x10000
#define PHYSICS_INVALID_HANDLE 0x0

typedef uint32_t PhysicsBodyHandle;
typedef uint32_t PhysicsShapeHandle;

struct PhysicsDebugLine
{
    vec3 to;
    vec3 from;
    vec3 colour;
};

enum
{
  PHYSICS_SPHERE_SHAPE = 1,
  PHYSICS_BOX_SHAPE,
  PHYSICS_CAPSULE_SHAPE,
  PHYSICS_TRIANGLE_MESH_SHAPE,
  PHYSICS_TERRAIN_SHAPE,
};

struct PhysicsShapeParameters
{
  uint8_t type;
  vec3 scale;
  union
  {
    struct // Sphere
    {
        float radius;
    };

    vec3 dimensions; // Box

    struct // Capsule
    {
        float radius;
        float height;
    };

    struct // Triangle mesh
    {
        vec3 *vertices;
        u32 count;
    };

    struct // Terrain
    {
        u32 heightfieldWidth;
        u32 heightfieldLength;
        float *heightfieldData;
        float minHeight;
        float maxHeight;
    };
  };
};

enum
{
  RIGID_BODY_DYNAMIC,
  RIGID_BODY_STATIC,
  RIGID_BODY_KINEMATIC,
};

enum
{
  COLLISION_TYPE_DEFAULT = Bit( 0 ),
  COLLISION_TYPE_CHARACTER = Bit( 1 ),
  COLLISION_TYPE_KINEMATIC = Bit( 2 ),
  COLLISION_TYPE_STATIC = Bit( 3 ),
  COLLISION_TYPE_ALL = -1
};

struct PhysicsBodyParameters
{
  vec3 position;
  vec3 localOrigin;
  quat orientation;
  float mass;
  PhysicsBodyHandle shape;
  u32 type;
  i32 collidesWith;
};

struct PhysicsSweepTestParameters
{
  PhysicsShapeHandle shape;
  vec3 start;
  vec3 end;
  float slope;
  int flags;
  PhysicsBodyHandle *ignored;
  uint32_t numIgnored;
  int filterGroup;
  int filterMask;
};

struct PhysicsSweepTestResult
{
  vec3 hitNormal;
  vec3 hitPoint;
  float hitFraction;
  PhysicsBodyHandle physicsHandle;
  bool hasHit;
};

struct PhysicsRaycastParameters
{
    vec3 start;
    vec3 end;
    PhysicsBodyHandle *ignored;
    u32 ignoredCount;
    i32 filterGroup;
    i32 filterMask;
};

struct PhysicsRaycastResult
{
    vec3 point;
    vec3 normal;
    float fraction;
    PhysicsBodyHandle body;
    bool hasHit;
};

struct PhysicsUpdateKinematicBodyParameters
{
    PhysicsBodyHandle *handles;
    vec3 *positions;
    quat *orientations;
    u32 count;
};

struct PhysicsUpdateShapeParameters
{
    PhysicsShapeHandle shape;
    uint8_t type;
    union
    {
        struct // Sphere
        {
            float radius;
        };

        vec3 dimensions; // Box

        struct // Capsule
        {
            float radius;
            float height;
        };
    };
};

struct PhysicsSystemState;

#define PHYSICS_UPDATE( NAME ) void NAME( PhysicsSystemState *state, float dt)
typedef PHYSICS_UPDATE( PhysicsUpdateFunction );

#define PHYSICS_DRAW(NAME) u32 NAME(PhysicsSystemState *state, PhysicsDebugLine *lines, u32 count)
typedef PHYSICS_DRAW(PhysicsDrawFunction);

#define PHYSICS_CREATE_SHAPE( NAME ) PhysicsShapeHandle NAME( PhysicsSystemState *state, PhysicsShapeParameters params )
typedef PHYSICS_CREATE_SHAPE( PhysicsCreateShapeFunction );

#define PHYSICS_CREATE_BODY(NAME) PhysicsBodyHandle NAME( PhysicsSystemState *state, PhysicsBodyParameters params)
typedef PHYSICS_CREATE_BODY( PhysicsCreateBodyFunction);

#define PHYSICS_SWEEP_TEST( NAME ) PhysicsSweepTestResult NAME( PhysicsSystemState *state, PhysicsSweepTestParameters *params)
typedef PHYSICS_SWEEP_TEST( PhysicsSweepTestFunction );

#define PHYSICS_UPDATE_KINEMATIC_BODIES(NAME) void NAME(PhysicsSystemState *state, PhysicsUpdateKinematicBodyParameters *params)
typedef PHYSICS_UPDATE_KINEMATIC_BODIES( PhysicsUpdateKinematicBodiesFunction );

#define PHYSICS_UPDATE_SHAPE(NAME) void NAME(PhysicsSystemState *state, PhysicsUpdateShapeParameters *params)
typedef PHYSICS_UPDATE_SHAPE(PhysicsUpdateShapeFunction);

#define PHYSICS_RAYCAST( NAME ) bool NAME( PhysicsSystemState *state, PhysicsRaycastParameters *params, PhysicsRaycastResult *result)
typedef PHYSICS_RAYCAST(PhysicsRaycastFunction);

#define PHYSICS_DESTROY_BODY(NAME) void NAME(PhysicsSystemState *state, PhysicsBodyHandle handle)
typedef PHYSICS_DESTROY_BODY(PhysicsDestroyBodyFunction);

#define PHYSICS_DESTROY_SHAPE(NAME) void NAME(PhysicsSystemState *state, PhysicsShapeHandle handle)
typedef PHYSICS_DESTROY_SHAPE(PhysicsDestroyShapeFunction);

extern PHYSICS_UPDATE( PhysicsUpdate_ );
extern PHYSICS_DRAW( PhysicsDraw_ );
extern PHYSICS_CREATE_SHAPE( PhysicsCreateShape_ );
extern PHYSICS_CREATE_BODY( PhysicsCreateBody_);
extern PHYSICS_SWEEP_TEST( PhysicsSweepTest_ );
extern PHYSICS_UPDATE_KINEMATIC_BODIES( PhysicsUpdateKinematicBodies_ );
extern PHYSICS_UPDATE_SHAPE(PhysicsUpdateShape_);
extern PHYSICS_RAYCAST(PhysicsRaycast_);
extern PHYSICS_DESTROY_BODY(PhysicsDestroyBody_);
extern PHYSICS_DESTROY_SHAPE(PhysicsDestroyShape_);

extern "C"
{
    typedef struct PhysicsSystem
    {
        PhysicsSystemState *state;
        PhysicsUpdateFunction *update;
        PhysicsDrawFunction *draw;
        PhysicsCreateShapeFunction *createShape;
        PhysicsCreateBodyFunction *createBody;
        PhysicsSweepTestFunction *sweepTest;
        PhysicsUpdateKinematicBodiesFunction *updateKinematicBodies;
        PhysicsUpdateShapeFunction *updateShape;
        PhysicsRaycastFunction *raycast;
        PhysicsDestroyBodyFunction *destroyBody;
        PhysicsDestroyShapeFunction *destroyShape;
        // PhysicsOverlapTestFunction *overlapTest;
        // PhysicsSetBodyStateFunction *setBodyState;
        // PhysicsDestroyBodyFunction *destroyBody;
        // PhysicsDestroyAllBodiesFunction *destroyAllBodies;
    } PhysicsSystem;
}
#define CREATE_PHYSICS_SYSTEM(NAME) PhysicsSystem* NAME(void)
#define DESTROY_PHYSICS_SYSTEM(NAME) void NAME(PhysicsSystem *physicsSystem)

typedef CREATE_PHYSICS_SYSTEM(CreatePhysicsSystemFunction);
typedef DESTROY_PHYSICS_SYSTEM(DestroyPhysicsSystemFunction);

extern "C" CREATE_PHYSICS_SYSTEM(CreatePhysicsSystem);
extern "C" DESTROY_PHYSICS_SYSTEM(DestroyPhysicsSystem);

#define PhysicsUpdate(PHYSICS_SYSTEM, DT) (PHYSICS_SYSTEM->update(PHYSICS_SYSTEM->state, DT))
#define PhysicsDraw(PHYSICS_SYSTEM, LINES, COUNT) (PHYSICS_SYSTEM->draw(PHYSICS_SYSTEM->state, LINES, COUNT))
#define PhysicsCreateShape(PHYSICS_SYSTEM, PARAMETERS) (PHYSICS_SYSTEM->createShape(PHYSICS_SYSTEM->state, PARAMETERS))
#define PhysicsCreateBody(PHYSICS_SYSTEM, PARAMETERS) (PHYSICS_SYSTEM->createBody(PHYSICS_SYSTEM->state, PARAMETERS))
#define PhysicsSweepTest(PHYSICS_SYSTEM, PARAMETERS) (PHYSICS_SYSTEM->sweepTest(PHYSICS_SYSTEM->state, PARAMETERS))
#define PhysicsUpdateKinematicBodies(PHYSICS_SYSTEM, PARAMETERS) (PHYSICS_SYSTEM->updateKinematicBodies(PHYSICS_SYSTEM->state, PARAMETERS))
#define PhysicsUpdateShape(PHYSICS_SYSTEM, PARAMETERS) (PHYSICS_SYSTEM->updateShape(PHYSICS_SYSTEM->state, PARAMETERS))
#define PhysicsRaycast(PHYSICS_SYSTEM, PARAMETERS, RESULT) (PHYSICS_SYSTEM->raycast(PHYSICS_SYSTEM->state, PARAMETERS, RESULT))
#define PhysicsDestroyBody(PHYSICS_SYSTEM, BODY) (PHYSICS_SYSTEM->destroyBody(PHYSICS_SYSTEM->state, BODY))
#define PhysicsDestroyShape(PHYSICS_SYSTEM, SHAPE) (PHYSICS_SYSTEM->destroyShape(PHYSICS_SYSTEM->state, SHAPE))
