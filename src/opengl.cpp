// TODO: Reformat with ClangFormat
// TODO: Merge with OpenGLDynamicMesh when we add that.



#include <cstring>

struct OpenGL_StaticMesh
{
  u32 vao, vbo, ibo, numIndices, numVertices, primitive, indexType;
};

struct OpenGL_DynamicMesh
{
  u32 vao;
  u32 vbo;
  u32 numVertices;
  u32 primitive;
  u32 maxVertices;
  u32 vertexSize;
  void *vertices;
};

struct OpenGL_VertexAttribute
{
  u32 index;
  int numComponents;
  int componentType;
  int normalized;
  int offset;
};

enum
{
  OPENGL_VERTEX_ATTRIBUTE_POSITION = 0,
  OPENGL_VERTEX_ATTRIBUTE_NORMAL = 1,
  OPENGL_VERTEX_ATTRIBUTE_TEXTURE_COORDINATE = 2,
  OPENGL_VERTEX_ATTRIBUTE_COLOUR = 3,
  OPENGL_VERTEX_ATTRIBUTE_BONE_ID = 4,
  OPENGL_VERTEX_ATTRIBUTE_BONE_WEIGHT = 5,

  OPENGL_VERTEX_ATTRIBUTE_INSTANCE_TRANSFORM = 8,
  // .. 12
  OPENGL_VERTEX_ATTRIBUTE_INSTANCE_COLOUR = 12,
  OPENGL_MAX_VERTEX_ATTRIBUTES,
};

void OpenGL_DeleteStaticMesh( OpenGL_StaticMesh mesh )
{
    if (mesh.vao != 0)
    {
        glBindVertexArray( 0 );
        glDeleteVertexArrays( 1, &mesh.vao );
        glDeleteBuffers( 1, &mesh.vbo );
        if ( mesh.numIndices )
        {
            glDeleteBuffers( 1, &mesh.ibo );
        }
    }
}

inline const void* ConvertU32ToPointer( u32 i )
{
  return (const void*)( (u8*)0 + i );
}

OpenGL_StaticMesh
OpenGL_CreateStaticMesh( const void *vertices, u32 numVertices,
                        u32 *indices, u32 numIndices,
                        u32 vertexSize,
                        OpenGL_VertexAttribute *vertexAttributes,
                        u32 numVertexAttributes, int primitive )
{
  OpenGL_StaticMesh mesh = {};
  mesh.primitive = primitive;
  mesh.indexType =
    GL_UNSIGNED_INT; // TODO: Use better index type if numVertices allowes it.
  mesh.numIndices = numIndices;
  mesh.numVertices = numVertices;

  glGenVertexArrays( 1, &mesh.vao );
  glBindVertexArray( mesh.vao );

  glGenBuffers( 1, &mesh.vbo );
  if ( mesh.numIndices )
  {
    glGenBuffers( 1, &mesh.ibo );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mesh.ibo );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( u32 ) * numIndices,
        indices, GL_STATIC_DRAW );
  }

  glBindBuffer( GL_ARRAY_BUFFER, mesh.vbo );

  glBufferData( GL_ARRAY_BUFFER, vertexSize * numVertices, vertices,
                GL_STATIC_DRAW );

  for ( u32 i = 0; i < numVertexAttributes; ++i )
  {
    OpenGL_VertexAttribute *attrib = vertexAttributes + i;
    if ( attrib->index >= OPENGL_MAX_VERTEX_ATTRIBUTES )
    {
      OpenGL_DeleteStaticMesh( mesh );
      // TODO: Zero memory.
      mesh.numIndices = 0;
      break;
    }
    glEnableVertexAttribArray( attrib->index);

    // TODO: Expand this to include other integer sizes
    if (attrib->componentType == GL_UNSIGNED_INT || attrib->componentType == GL_INT)
    {
        glVertexAttribIPointer(attrib->index, attrib->numComponents, attrib->componentType,
                               vertexSize, ConvertU32ToPointer( attrib->offset ) );
    }
    else
    {
        glVertexAttribPointer( attrib->index, attrib->numComponents,
                               attrib->componentType, (GLboolean)attrib->normalized,
                               vertexSize,
                               ConvertU32ToPointer( attrib->offset ) );
    }
  }

  glBindVertexArray( 0 );
  return mesh;
}

void OpenGL_DrawStaticMesh( OpenGL_StaticMesh mesh )
{
  glBindVertexArray( mesh.vao );
  if ( mesh.numIndices )
  {
    glDrawElements( mesh.primitive, mesh.numIndices, mesh.indexType, NULL );
  }
  else
  {
    glDrawArrays( mesh.primitive, 0, mesh.numVertices );
  }
}

OpenGL_StaticMesh OpenGL_CreateQuadMesh()
{
  float vertices[ ] = {
    -1, 1, 0, 0,1,
     1, 1, 0, 1,1,
     1,-1, 0, 1,0,
    -1,-1, 0, 0,0
  };

  OpenGL_VertexAttribute attribs[2];
  attribs[0].index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = OPENGL_VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
  attribs[1].numComponents = 2;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof( float ) * 3;

  return OpenGL_CreateStaticMesh( vertices, 4, nullptr, 0, sizeof( float ) * 5,
                                 attribs, 2, GL_QUADS );
}

OpenGL_StaticMesh OpenGL_CreateAxisMesh()
{
  // clang-format off
  float vertices[] =
  {
    // X Axis
    0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

    // Y Axis
    0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

    // Z Axis
    0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
    0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f
  };
  // clang-format on

  OpenGL_VertexAttribute attribs[2];
  attribs[0].index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = OPENGL_VERTEX_ATTRIBUTE_COLOUR;
  attribs[1].numComponents = 3;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof( float ) * 3;

  return OpenGL_CreateStaticMesh( vertices, 6, nullptr, 0, sizeof( float ) * 6,
                                 attribs, 2, GL_LINES );
}

OpenGL_StaticMesh OpenGL_CreateCubeMesh()
{
    float vertices[] = {
      // Top
      -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
      0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
      0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,

      // Bottom
      -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,

      // Back
      -0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,
      0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f,
      -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,

      // Front
      -0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,
      0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
      -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

      // Left
      -0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
      -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
      -0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
      -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,

      // Right
      0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,
      0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
      0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
      0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,
    };

    u32 indices[] = {
      2, 1, 0,
      0, 3, 2,

      4, 5, 6,
      6, 7, 4,

      8, 9, 10,
      10, 11, 8,

      14, 13, 12,
      12, 15, 14,

      16, 17, 18,
      18, 19, 16,

      22, 21, 20,
      20, 23, 22
    };

    OpenGL_VertexAttribute attribs[3];
    attribs[0].index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
    attribs[0].numComponents = 3;
    attribs[0].componentType = GL_FLOAT;
    attribs[0].normalized = GL_FALSE;
    attribs[0].offset = 0;
    attribs[1].index = OPENGL_VERTEX_ATTRIBUTE_NORMAL;
    attribs[1].numComponents = 3;
    attribs[1].componentType = GL_FLOAT;
    attribs[1].normalized = GL_FALSE;
    attribs[1].offset = sizeof( float ) * 3;
    attribs[2].index = OPENGL_VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
    attribs[2].numComponents = 2;
    attribs[2].componentType = GL_FLOAT;
    attribs[2].normalized = GL_FALSE;
    attribs[2].offset = sizeof( float ) * 6;

    return OpenGL_CreateStaticMesh( vertices, 24, indices, 36,
                                   sizeof( float ) * 8, attribs, 3,
                                   GL_TRIANGLES );
}
OpenGL_StaticMesh OpenGL_CreateWireframeCubeMesh()
{
  // clang-format off
  float vertices[] =
  {
    -0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, -0.5f,
    0.5f, 0.5f, 0.5f,
    -0.5f, 0.5f, 0.5f,

    -0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, -0.5f,
    0.5f, -0.5f, 0.5f,
    -0.5f, -0.5f, 0.5f,
  };

  u32 indices[] =
  {
    // Top
    0, 1,
    1, 2,
    2, 3,
    3, 0,

    // Bottom
    4, 5,
    5, 6,
    6, 7,
    7, 4,

    // Back
    0, 4,
    1, 5,

    // Front
    3, 7,
    2, 6
  };

  OpenGL_VertexAttribute attrib;
  attrib.index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
  attrib.numComponents = 3;
  attrib.componentType = GL_FLOAT;
  attrib.normalized = GL_FALSE;
  attrib.offset = 0;

  return OpenGL_CreateStaticMesh(
    vertices, 8, indices, 24, sizeof( float ) * 3, &attrib, 1, GL_LINES );
}

bool OpenGL_CompileShader( GLuint shader, const char* src, bool isVertex )
{
  int len = (int)strlen( src );
  glShaderSource( shader, 1, &src, &len );
  glCompileShader( shader );

  int success;
  glGetShaderiv( shader, GL_COMPILE_STATUS, &success );
  if ( !success ) // Check if the shader was compiled successfully.
  {
    int logLength;
    glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &logLength );

    char log[4096];
    Assert( (u32)logLength < ArrayCount(log) );

    len = 0;
    glGetShaderInfoLog( shader, logLength, &len, log );

    if ( isVertex )
    {
      LOG_ERROR("[VERTEX]\n%s", log);
    }
    else
    {
      LOG_ERROR("[FRAGMENT]\n%s", log);
    }
    return false;
  }
  return true;
}

bool OpenGL_LinkShader( GLuint vertex, GLuint fragment, GLuint program )
{
  glAttachShader( program, vertex );
  glAttachShader( program, fragment );
  glLinkProgram( program );

  int success;
  glGetProgramiv( program, GL_LINK_STATUS, &success );
  if ( !success ) // Check if the shader was linked successfully.
  {
    int logLength;
    glGetShaderiv( program, GL_INFO_LOG_LENGTH, &logLength );

    char log[4096];
    int len = 0;
    glGetProgramInfoLog( program, logLength, &len, log );
    LOG_ERROR( "[LINKER]\n%s", log);
    return false;
  }
  return true;
}

void OpenGL_DeleteShader( GLuint program )
{
    if (program != 0)
    {
        glUseProgram( 0 );
        GLsizei count = 0;
        GLuint shaders[ 2 ];
        // Need to retrieve the vertex and fragment shaders and delete them
        // explicitly, deleting the shader program only detaches the two shaders, it
        // does not free the resources for them.
        glGetAttachedShaders( program, 2, &count, shaders );
        glDeleteProgram( program );
        glDeleteShader( shaders[ 0 ] );
        glDeleteShader( shaders[ 1 ] );
    }
}

GLuint OpenGL_CreateShader( const char *vertexSource, const char *fragmentSource )
{
  GLuint vertex = glCreateShader( GL_VERTEX_SHADER );
  GLuint fragment = glCreateShader( GL_FRAGMENT_SHADER );
  GLuint program = glCreateProgram();

  bool vertexSuccess = OpenGL_CompileShader( vertex, vertexSource, true );
  bool fragmentSuccess = OpenGL_CompileShader( fragment, fragmentSource, false );

  if ( vertexSuccess && fragmentSuccess )
  {
    if ( OpenGL_LinkShader( vertex, fragment, program ) )
    {
      return program;
    }
  }
  OpenGL_DeleteShader( program );
  return 0;
}

internal void DeleteTexture(u32 texture)
{
    if (texture != 0)
    {
        glDeleteTextures( 1, &texture );
    }
}

void OpenGL_DeleteDynamicMesh( OpenGL_DynamicMesh mesh )
{
  glBindVertexArray( 0 );
  glDeleteVertexArrays( 1, &mesh.vao );
  glDeleteBuffers( 1, &mesh.vbo );
}

OpenGL_DynamicMesh
OpenGL_CreateDynamicMesh( void *vertices, u32 maxVertices,
                         u32 vertexSize,
                         OpenGL_VertexAttribute *vertexAttributes,
                         u32 numVertexAttributes, int primitive )
{
  OpenGL_DynamicMesh mesh = {};
  mesh.primitive = primitive;
  mesh.maxVertices = maxVertices;
  mesh.vertices = vertices;
  mesh.vertexSize = vertexSize;

  glGenVertexArrays( 1, &mesh.vao );
  glBindVertexArray( mesh.vao );

  glGenBuffers( 1, &mesh.vbo );
  glBindBuffer( GL_ARRAY_BUFFER, mesh.vbo );

  glBufferData( GL_ARRAY_BUFFER, vertexSize * maxVertices, NULL,
                GL_DYNAMIC_DRAW );

  for ( u32 i = 0; i < numVertexAttributes; ++i )
  {
    OpenGL_VertexAttribute *attrib = vertexAttributes + i;
    if ( attrib->index >= OPENGL_MAX_VERTEX_ATTRIBUTES )
    {
      OpenGL_DeleteDynamicMesh( mesh );
      ClearToZero( &mesh, sizeof( mesh ) );
      break;
    }
    glEnableVertexAttribArray( attrib->index);

    glVertexAttribPointer( attrib->index, attrib->numComponents,
                           attrib->componentType, (GLboolean)attrib->normalized,
                           vertexSize,
                           ConvertU32ToPointer( attrib->offset ) );
  }

  glBindVertexArray( 0 );
  return mesh;
}

void OpenGL_UpdateDynamicMesh( OpenGL_DynamicMesh mesh )
{
  glBindVertexArray( mesh.vao );
  glBindBuffer( GL_ARRAY_BUFFER, mesh.vbo );
  glBufferSubData( GL_ARRAY_BUFFER, 0, mesh.vertexSize * mesh.numVertices,
                   mesh.vertices );

  glBindVertexArray( 0 );
}

void OpenGL_DrawDynamicMesh( OpenGL_DynamicMesh mesh )
{
  glBindVertexArray( mesh.vao );
  glDrawArrays( mesh.primitive, 0, mesh.numVertices );
  glBindVertexArray( 0 );
}

void OpenGL_DrawDynamicMesh( OpenGL_DynamicMesh mesh, u32 start,
                            u32 count )
{
  glBindVertexArray( mesh.vao );
  glDrawArrays( mesh.primitive, start, count );
  glBindVertexArray( 0 );
}
