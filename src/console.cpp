#define console_OutputBufferLength 8192
#define console_InputBufferLength 80

struct console_State
{
    char outputBuffer[console_OutputBufferLength];
    char inputBuffer[console_InputBufferLength];
    u32 outputBufferLength;
    float t;

    TextInputState textInput;
};

inline vec4 Colour(u32 r, u32 g, u32 b, u32 a = 255)
{
    vec4 result = Vec4((float)r, (float)g, (float)b, (float)a);
    result = result * ( 1.0f / 255.0f);
    return result;
}

internal void console_Init(console_State *console)
{
    console->textInput = TextInputCreate(console->inputBuffer,
                                         ArrayCount(console->inputBuffer));
}

internal void console_Draw(console_State *console, renderer_State *renderer,
                           Font *font, mat4 *projection, u32 windowWidth,
                           u32 windowHeight, float t)
{
    float consoleHeight = (float)windowHeight - 100;
    float consoleWidth = (float)windowWidth - 120;

    vec2 topLeft = Vec2(60, 0);
    vec2 bottomLeft = Vec2(60, consoleHeight);
    vec4 bgColour = Colour(43, 48, 59);
    renderer_DrawRect(renderer, topLeft, consoleWidth,
                      consoleHeight, bgColour,
                      projection);
    renderer_DrawRect(renderer, bottomLeft + Vec2(5, -25), consoleWidth - 10, 20,
            bgColour + Vec4(0.1, 0.1, 0.1, 0), projection);

    renderer_DrawString(renderer, topLeft + Vec2(5, 0), console->outputBuffer, font,
            Vec4(1, 1, 1, 0.9), projection);

    renderer_DrawString(renderer, bottomLeft + Vec2(7, -22), console->inputBuffer, font,
            Vec4(1), projection);

    float a = Round(0.5f * Sin(4.0f * t) + 0.5f);
    float xOffset = CalculateTextLength(font, console->inputBuffer, console->textInput.cursor);
    renderer_DrawRect(renderer, bottomLeft + Vec2(5 + xOffset, -25), 1.0f, 20.0f,
            Vec4(1, 1, 1, a), projection);
}

internal void console_Puts(console_State *console, const char *string, u32 length = 0)
{
   if (length == 0)
       length = (u32)strlen(string);

   if (console->outputBufferLength + length < console_OutputBufferLength)
   {
       strncpy(console->outputBuffer + console->outputBufferLength, string, length);
       console->outputBufferLength += length;
   }
}

//internal void console_AddInput(console_State *console, const char *string, u32 length = 0)
//{
   //if (length == 0)
       //length = (u32)strlen(string);

   //if (console->inputBufferLength + length < console_InputBufferLength)
   //{
       //strncpy(console->inputBuffer + console->inputBufferLength, string, length);
       //console->inputBufferLength += length;
   //}
//}

//internal void console_RemoveCharacterBeforeCursor(console_State *console)
//{
    //if (console->inputBufferLength > 0)
    //{
        //console->inputBufferLength--;
        //console->inputBuffer[console->inputBufferLength] = '\0';
    //}
//}

internal void console_Execute(console_State *console)
{
}
