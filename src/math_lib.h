#pragma once

#include <cmath>

#define PI 3.14159265359f
#define EPSILON 0.000001f

// TODO: Epsilon values

inline float Radians( float degrees )
{
  float result = degrees * ( PI / 180.0f );
  return result;
}

inline float Abs(float x)
{
    float result = fabs(x);
    return result;
}

inline float Fmod(float a, float b)
{
    float result = fmod(a, b);
    return result;
}

inline float Max(float a, float b)
{
    float result = (a > b) ? a : b;
    return result;
}

inline float Min(float a, float b)
{
    float result = (a < b) ? a : b;
    return result;
}

inline u32 Max(u32 a, u32 b)
{
    u32 result = (a > b) ? a : b;
    return result;
}

inline u32 Min(u32 a, u32 b)
{
    u32 result = (a < b) ? a : b;
    return result;
}

inline void Swap(float *a, float *b)
{
    float temp = *a;
    *a = *b;
    *b = temp;
}

inline float Clamp(float x, float min, float max)
{
  if ( x < min )
  {
    x = min;
  }
  else if ( x > max )
  {
    x = max;
  }
  return x;
}

// FIXME: Return value should probably be a float
inline int Floor(float x)
{
    int result = (int)floor(x);
    return result;
}

// FIXME: Return value should probably be a float
inline int Ceil(float x)
{
    int result = (int)ceil(x);
    return result;
}

inline float Round(float x)
{
    float result = round(x);
    return result;
}

inline float SquareRoot(float x)
{
  float result = sqrtf(x);
  return result;
}

inline float Sin( float x )
{
  float result = sin( x );
  return result;
}

inline float Cos( float x )
{
  float result = cos( x );
  return result;
}

inline float Tan( float x )
{
  float result = tan( x );
  return result;
}

inline float Lerp(float a, float b, float t)
{
    return a * (1.0f - t) + b * t;
}

union vec2
{
  struct
  {
    float x;
    float y;
  };
  struct
  {
    float s;
    float t;
  };
  float data[2];
};

inline vec2 Vec2(float x)
{
    vec2 result;
    result.x = x;
    result.y = x;
    return result;
}

inline vec2 Vec2(float x, float y)
{
    vec2 result;
    result.x = x;
    result.y = y;
    return result;
}

inline vec2 operator*(vec2 a, float b)
{
  vec2 result;
  result.x = a.x * b;
  result.y = a.y * b;
  return result;
}

inline vec2& operator*=(vec2 &a, float b)
{
  a = a * b;
  return a;
}

inline vec2 operator+(vec2 a, vec2 b)
{
  vec2 result;
  result.x = a.x + b.x;
  result.y = a.y + b.y;
  return result;
}

inline vec2 operator-(vec2 a)
{
  vec2 result;
  result.x = -a.x;
  result.y = -a.y;
  return result;
}

inline vec2 operator-(vec2 a, vec2 b)
{
  vec2 result;
  result.x = a.x - b.x;
  result.y = a.y - b.y;
  return result;
}

inline vec2& operator-=(vec2 &a, vec2 b)
{
  a = a - b;
  return a;
}

inline float Dot( vec2 a, vec2 b )
{
  float result = a.x * b.x + a.y * b.y;
  return result;
}

inline float LengthSq( vec2 a )
{
  float result = Dot( a, a );
  return result;
}

inline float Length( vec2 a )
{
  float result = SquareRoot( LengthSq( a ) );
  return result;
}

union vec3
{
  struct
  {
    float x;
    float y;
    float z;
  };
  struct
  {
    float r;
    float g;
    float b;
  };
  float data[3];
};

inline vec3 Vec3(float x)
{
  vec3 result;
  result.x = x;
  result.y = x;
  result.z = x;
  return result;
}

inline vec3 Vec3(float x, float y, float z)
{
  vec3 result;
  result.x = x;
  result.y = y;
  result.z = z;
  return result;
}

inline vec3 operator*(vec3 a, float b)
{
  vec3 result;
  result.x = a.x * b;
  result.y = a.y * b;
  result.z = a.z * b;
  return result;
}

inline vec3 operator*(float a, vec3 b)
{
  vec3 result;
  result.x = a * b.x;
  result.y = a * b.y;
  result.z = a * b.z;
  return result;
}

inline vec3 operator*(vec3 a, vec3 b)
{
    vec3 result;
    result.x = a.x * b.x;
    result.y = a.y * b.y;
    result.z = a.z * b.z;
    return result;
}

inline vec3& operator*=(vec3 &a, float b)
{
  a = a * b;
  return a;
}

inline vec3 operator+(vec3 a, vec3 b)
{
  vec3 result;
  result.x = a.x + b.x;
  result.y = a.y + b.y;
  result.z = a.z + b.z;
  return result;
}

inline vec3& operator+=( vec3 &a, vec3 b )
{
  a = a + b;
  return a;
}

inline vec3 operator-(vec3 a)
{
  vec3 result;
  result.x = -a.x;
  result.y = -a.y;
  result.z = -a.z;
  return result;
}

inline vec3 operator-(vec3 a, vec3 b)
{
  vec3 result;
  result.x = a.x - b.x;
  result.y = a.y - b.y;
  result.z = a.z - b.z;
  return result;
}

inline vec3& operator-=(vec3 &a, vec3 b)
{
  a = a - b;
  return a;
}

inline float Dot( vec3 a, vec3 b )
{
  float result = a.x * b.x + a.y * b.y + a.z * b.z;
  return result;
}

inline vec3 Cross(vec3 a, vec3 b)
{
    vec3 result;

    result.x = (a.y * b.z) - (a.z * b.y);
    result.y = (a.z * b.x) - (a.x * b.z);
    result.z = (a.x * b.y) - (a.y * b.x);

    return result;

}

inline float LengthSq( vec3 a )
{
  float result = Dot( a, a );
  return result;
}

inline float Length( vec3 a )
{
  float result = SquareRoot( LengthSq( a ) );
  return result;
}

inline bool Normalize( vec3 *a )
{
  float length = Length( *a );
  if ( length > 0.0f )
  {
      a->x = a->x / length;
      a->y = a->y / length;
      a->z = a->z / length;
      return true;
  }
  return false;
}

inline vec3 Normalize(vec3 a)
{
    vec3 result = a;
    if (!Normalize(&result))
    {
        return Vec3(0);
        //Assert(!"Failed to normalize zero vector.");
    }
    return result;
}

inline vec3 Lerp( vec3 a, vec3 b, float t )
{
  vec3 result;
  result = a * ( 1.0f - t ) + b * t;
  return result;
}

inline vec3 Clamp(vec3 v, vec3 min, vec3 max)
{
    vec3 result;
    result.x = Clamp(v.x, min.x, max.x);
    result.y = Clamp(v.y, min.y, max.y);
    result.z = Clamp(v.z, min.z, max.z);
    return result;
}

inline bool IsZero(vec3 v)
{
    float lengthSq = LengthSq(v);
    bool result = (lengthSq > -0.00001f) && (lengthSq < 0.00001f);
    return result;
}

union vec4
{
  struct
  {
    float x;
    float y;
    float z;
    float w;
  };
  struct
  {
    float r;
    float g;
    float b;
    float a;
  };
  float data[4];
};

inline vec4 Vec4( float x )
{
  vec4 result;
  result.x = x;
  result.y = x;
  result.z = x;
  result.w = x;
  return result;
}

inline vec4 Vec4( float x, float y, float z, float w )
{
  vec4 result;
  result.x = x;
  result.y = y;
  result.z = z;
  result.w = w;
  return result;
}

inline vec4 Vec4(vec3 v, float w)
{
    vec4 result;
    result.x = v.x;
    result.y = v.y;
    result.z = v.z;
    result.w = w;
    return result;
}

inline vec4 operator*( vec4 a, float b )
{
  vec4 result;
  result.x = a.x * b;
  result.y = a.y * b;
  result.z = a.z * b;
  result.w = a.w * b;
  return result;
}

inline vec4 operator+( vec4 a, vec4 b )
{
  vec4 result;
  result.x = a.x + b.x;
  result.y = a.y + b.y;
  result.z = a.z + b.z;
  result.w = a.w + b.w;
  return result;
}

inline vec4& operator+=( vec4 &a, vec4 b )
{
  a = a + b;
  return a;
}

inline float Dot( vec4 a, vec4 b )
{
  float result = a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
  return result;
}

inline vec4 Lerp( vec4 a, vec4 b, float t )
{
  vec4 result;
  result = a * ( 1.0f - t ) + b * t;
  return result;
}

inline float LengthSq( vec4 a )
{
  float result = Dot( a, a );
  return result;
}

inline float Length( vec4 a )
{
  float result = SquareRoot( LengthSq( a ) );
  return result;
}

inline bool Normalize( vec4 *a )
{
  float length = Length( *a );
  if ( length > 0.0f )
  {
      a->x = a->x / length;
      a->y = a->y / length;
      a->z = a->z / length;
      a->w = a->w / length;
      return true;
  }
  return false;
}

inline vec4 Clamp(vec4 v, vec4 min, vec4 max)
{
    vec4 result;
    result.x = Clamp(v.x, min.x, max.x);
    result.y = Clamp(v.y, min.y, max.y);
    result.z = Clamp(v.z, min.z, max.z);
    result.w = Clamp(v.w, min.w, max.w);
    return result;
}

union quat
{
    struct
    {
        float x;
        float y;
        float z;
        float w;
    };

    struct
    {
        vec3 v;
        float s;
    };
};

inline quat Quat()
{
    quat result = {};
    result.s = 1.0f;
    return result;
}

inline quat Quat(vec3 axis, float angle)
{
    quat result;
    float halfAngle = angle * 0.5f;
    result.v = axis * Sin(halfAngle);
    result.s = Cos(halfAngle);
    return result;
}

inline quat Quat(float x, float y, float z, float w)
{
    quat result;
    result.v = Vec3(x, y, z);
    result.s = w;
    return result;
}

inline quat operator*(quat p, quat q)
{
    quat result;
    result.v = p.s * q.v + q.s * p.v + Cross(p.v, q.v);
    result.s = p.s * q.s - Dot(p.v, q.v);
    return result;
}

inline quat& operator*=(quat &p, quat q)
{
    p = p * q;
    return p;
}

inline quat operator-(quat a)
{
    quat result;
    result.v = -a.v;
    result.s = a.s;
    return result;
}

inline quat Normalize(quat p)
{
    float lengthSq = LengthSq(p.v) + p.s * p.s;
    Assert(lengthSq > 0.0f);
    float invLength = 1.0f / SquareRoot(lengthSq);
    quat result;
    result.x = p.x * invLength;
    result.y = p.y * invLength;
    result.z = p.z * invLength;
    result.w = p.w * invLength;
    return result;
}

inline quat Lerp(quat p, quat q, float t)
{
    quat result;
    result.x = Lerp(p.x, q.x, t);
    result.y = Lerp(p.y, q.y, t);
    result.z = Lerp(p.z, q.z, t);
    result.w = Lerp(p.w, q.w, t);
    return Normalize(result);
}

inline bool IsNormalized(quat p)
{
    float length = LengthSq(p.v) + p.s * p.s;
    return ((length > 1.0f - EPSILON) && (length < 1.0f + EPSILON));
}

union mat4
{
  vec4 col[ 4 ];
  float raw[ 16 ];
};

inline mat4 Identity()
{
  mat4 result = {};
  result.col[ 0 ] = Vec4( 1, 0, 0, 0 );
  result.col[ 1 ] = Vec4( 0, 1, 0, 0 );
  result.col[ 2 ] = Vec4( 0, 0, 1, 0 );
  result.col[ 3 ] = Vec4( 0, 0, 0, 1 );
  return result;
}

inline mat4 Orthographic( float left, float right, float top, float bottom, float n = -1.0f, float f = 1.0f)
{
  mat4 result = {};
  result.col[ 0 ] = Vec4( 2.0f / ( right - left ), 0, 0, 0 );
  result.col[ 1 ] = Vec4( 0, 2.0f / ( top - bottom ), 0, 0 );
  result.col[ 2 ] = Vec4( 0, 0, -2 / ( f -n ), 0.0f );
  result.col[ 3 ] = Vec4( -( right + left ) / ( right - left ), -( top + bottom ) / ( top - bottom ), -(f + n)/(f-n), 1 );
  return result;
}

inline mat4 Perspective( float fov, float aspect, float n, float f )
{
  float tanHalfFovy = Tan( fov * 0.5f );

  mat4 result = {};
  result.col[0].x = 1.0f / ( aspect * tanHalfFovy );
  result.col[1].y = 1.0f / tanHalfFovy;
  result.col[2].z = - ( f + n ) / ( f - n );
  result.col[2].w = -1;
  result.col[3].z = -( 2.0f * f * n ) / ( f - n );
  return result;
}

inline mat4 Translate( vec3 p )
{
  mat4 result = Identity();
  result.col[ 3 ].x = p.x;
  result.col[ 3 ].y = p.y;
  result.col[ 3 ].z = p.z;
  return result;
}

inline mat4 Scale( vec3 p )
{
  mat4 result = Identity();
  result.col[ 0 ].x = p.x;
  result.col[ 1 ].y = p.y;
  result.col[ 2 ].z = p.z;
  return result;
}

inline mat4 Scale(float x)
{
    return Scale(Vec3(x));
}

inline mat4 Rotate(quat rotation)
{
  mat4 result = Identity();
  float x = rotation.x;
  float y = rotation.y;
  float z = rotation.z;
  float w = rotation.w;

  result.col[0].x = 1.0f - 2.0f * y * y - 2.0f * z * z;
  result.col[0].y = 2.0f * x * y + 2.0f * z * w;
  result.col[0].z = 2.0f * x * z - 2.0f * y * w;

  result.col[1].x = 2.0f * x * y - 2.0f * z * w;
  result.col[1].y = 1.0f - 2.0f * x * x - 2.0f * z * z;
  result.col[1].z = 2.0f * y * z + 2.0f * x * w;

  result.col[2].x = 2.0f * x * z + 2.0f * y * w;
  result.col[2].y = 2.0f * y * z - 2.0f * x * w;
  result.col[2].z = 1.0f - 2.0f * x * x - 2.0f * y * y;

  return result;
}

inline mat4 RotateX( float angle )
{
  mat4 result = Identity();
  result.col[1].y = Cos( angle );
  result.col[1].z = Sin( angle );
  result.col[2].y = -Sin( angle );
  result.col[2].z = Cos( angle );
  return result;
}

inline mat4 RotateY( float angle )
{
  mat4 result = Identity();
  result.col[0].x = Cos( angle );
  result.col[0].z = -Sin( angle );
  result.col[2].x = Sin( angle );
  result.col[2].z = Cos( angle );
  return result;
}

inline mat4 RotateZ( float angle )
{
  mat4 result = Identity();
  result.col[0].x = Cos( angle );
  result.col[0].y = Sin( angle );
  result.col[1].x = -Sin( angle );
  result.col[1].y = Cos( angle );
  return result;
}

inline mat4 operator*( mat4 a, mat4 b )
{
  mat4 result;
  result.col[ 0 ] = a.col[ 0 ] * b.col[ 0 ].x + a.col[ 1 ] * b.col[ 0 ].y + a.col[ 2 ] * b.col[ 0 ].z + a.col[ 3 ] * b.col[ 0 ].w;
  result.col[ 1 ] = a.col[ 0 ] * b.col[ 1 ].x + a.col[ 1 ] * b.col[ 1 ].y + a.col[ 2 ] * b.col[ 1 ].z + a.col[ 3 ] * b.col[ 1 ].w;
  result.col[ 2 ] = a.col[ 0 ] * b.col[ 2 ].x + a.col[ 1 ] * b.col[ 2 ].y + a.col[ 2 ] * b.col[ 2 ].z + a.col[ 3 ] * b.col[ 2 ].w;
  result.col[ 3 ] = a.col[ 0 ] * b.col[ 3 ].x + a.col[ 1 ] * b.col[ 3 ].y + a.col[ 2 ] * b.col[ 3 ].z + a.col[ 3 ] * b.col[ 3 ].w;
  return result;
}

inline mat4& operator*=( mat4 &a, mat4 b )
{
  a = a * b;
  return a;
}

inline mat4 Transpose( mat4 a )
{
  mat4 result;
  result.raw[ 0 ] = a.raw[ 0 ];
  result.raw[ 1 ] = a.raw[ 4 ];
  result.raw[ 2 ] = a.raw[ 8 ];
  result.raw[ 3 ] = a.raw[ 12 ];

  result.raw[ 4 ] = a.raw[ 1 ];
  result.raw[ 5 ] = a.raw[ 5 ];
  result.raw[ 6 ] = a.raw[ 9 ];
  result.raw[ 7 ] = a.raw[ 13 ];

  result.raw[ 8 ] = a.raw[ 2 ];
  result.raw[ 9 ] = a.raw[ 6 ];
  result.raw[ 10 ] = a.raw[ 10 ];
  result.raw[ 11 ] = a.raw[ 14 ];

  result.raw[ 12 ] = a.raw[ 3 ];
  result.raw[ 13 ] = a.raw[ 7 ];
  result.raw[ 14 ] = a.raw[ 11 ];
  result.raw[ 15 ] = a.raw[ 15 ];

  return result;
}

inline vec4 operator*( mat4 a, vec4 b )
{
  vec4 result = {};
  a = Transpose( a );
  result.x = Dot( b, a.col[0] );
  result.y = Dot( b, a.col[1] );
  result.z = Dot( b, a.col[2] );
  result.w = Dot( b, a.col[3] );
  return result;
}

// http://fabiensanglard.net/doom3_documentation/37726-293748.pdf
inline void DecomposeRotationAndTranslation(mat4 mat, quat *rotation, vec3 *translation)
{
    float *m = &mat.raw[0];
    float *q = &rotation->x;
    if (m[0 * 4 + 0] + m[1 * 4 + 1] + m[2 * 4 + 2] > 0.0f)
    {
        float t = +m[0 * 4 + 0] + m[1 * 4 + 1] + m[2 * 4 + 2] + 1.0f;
        float s = 0.5f / SquareRoot(t);
        q[3] = s * t;
        q[2] = (m[0 * 4 + 1] - m[1 * 4 + 0]) * s;
        q[1] = (m[2 * 4 + 0] - m[0 * 4 + 2]) * s;
        q[0] = (m[1 * 4 + 2] - m[2 * 4 + 1]) * s;
    }
    else if (m[0 * 4 + 0] > m[1 * 4 + 1] && m[0 * 4 + 0] > m[2 * 4 + 2])
    {
        float t = +m[0 * 4 + 0] - m[1 * 4 + 1] - m[2 * 4 + 2] + 1.0f;
        float s = 0.5f / SquareRoot(t);
        q[0] = s * t;
        q[1] = (m[0 * 4 + 1] + m[1 * 4 + 0]) * s;
        q[2] = (m[2 * 4 + 0] + m[0 * 4 + 2]) * s;
        q[3] = (m[1 * 4 + 2] - m[2 * 4 + 1]) * s;
    }
    else if (m[1 * 4 + 1] > m[2 * 4 + 2])
    {
        float t = -m[0 * 4 + 0] + m[1 * 4 + 1] - m[2 * 4 + 2] + 1.0f;
        float s = 0.5f / SquareRoot(t);
        q[1] = s * t;
        q[0] = (m[0 * 4 + 1] + m[1 * 4 + 0]) * s;
        q[3] = (m[2 * 4 + 0] - m[0 * 4 + 2]) * s;
        q[2] = (m[1 * 4 + 2] + m[2 * 4 + 1]) * s;
    }
    else
    {
        float t = -m[0 * 4 + 0] - m[1 * 4 + 1] + m[2 * 4 + 2] + 1.0f;
        float s = 0.5f / SquareRoot(t);
        q[2] = s * t;
        q[3] = (m[0 * 4 + 1] - m[1 * 4 + 0]) * s;
        q[0] = (m[2 * 4 + 0] + m[0 * 4 + 2]) * s;
        q[1] = (m[1 * 4 + 2] + m[2 * 4 + 1]) * s;
    }
    translation->x = m[3 * 4 + 0];
    translation->y = m[3 * 4 + 1];
    translation->z = m[3 * 4 + 2];
}

// XORShift128plus
struct RandomNumberGenerator
{
  u64 state[2];
};

inline RandomNumberGenerator CreateRandomNumberGenerator( u64 s0,
                                                          u64 s1 )
{
  RandomNumberGenerator result = {};
  result.state[0] = s0;
  result.state[1] = s1;
  return result;
}

inline u64 NextRandomNumber( RandomNumberGenerator *generator )
{
  u64 s1 = generator->state[0];
  u64 s0 = generator->state[1];
  generator->state[0] = s0;
  s1 ^= s1 << 32;
  generator->state[1] = ( s1 ^ s0 ^ ( s1 >> 17 ) ^ ( s0 >> 26 ) );
  return generator->state[1] + s0;
}

inline u32 RandomNumberInRange( RandomNumberGenerator *generator,
                                       u32 min, u32 max )
{
  Assert( max > min );
  uint32_t range = 1 + max - min;
  uint32_t buckets = 0xFFFFFFFF / range;
  uint32_t limit = buckets * range;

  uint32_t r = NextRandomNumber( generator ) % 0xFFFFFFFF;
  while ( r >= limit )
  {
    r = NextRandomNumber( generator ) % 0xFFFFFFFF;
  }

  return min + ( r / buckets );
}

inline float RandomFloat( RandomNumberGenerator *generator )
{
  uint32_t n = RandomNumberInRange( generator, 0, 0xFFFFFFF0 );
  return (float)((double)n/(double)(0xFFFFFFF0));
}

inline float RandomBinomial( RandomNumberGenerator *generator )
{
  return RandomFloat( generator ) - RandomFloat( generator );
}

struct Plane
{
    vec3 normal; // NOTE: Must be a unit vector
    float distance;
};

// NOTE: normal must be unit length
inline Plane CreatePlane(vec3 normal, vec3 pointOnPlane)
{
    {
        float lengthSq = LengthSq(normal);
        Assert(lengthSq >= 0.9999f && lengthSq < 1.0001f);
    }
    Plane result;
    result.normal = normal;
    result.distance = Dot(result.normal, pointOnPlane);
    return result;
};

inline float Distance(Plane plane, vec3 point)
{
    float result = Dot(point, plane.normal) - plane.distance;
    return result;
}

inline vec3 ClosestPointOnPlane(Plane plane, vec3 point)
{
    float t = Dot(plane.normal, point) - plane.distance;
    vec3 result = point - t * plane.normal;
    return result;
}

inline vec3 ClosestPointOnLineSegment(vec3 point, vec3 start, vec3 end)
{
    vec3 direction = end - start;
    float t = Dot(point - start, direction) / Dot(direction, direction);
    t = Clamp(t, 0.0f, 1.0f);
    vec3 result = start + t * direction;
    return result;
}

struct rect2
{
  vec2 min;
  vec2 max;
};

rect2 RectMinMax2( vec2 min, vec2 max )
{
  rect2 result;
  result.min = min;
  result.max = max;
  return result;
}

rect2 Rect2( vec2 position, vec2 dimensions )
{
  rect2 result;
  result.min = position;
  result.max = position + dimensions;
  return result;
}

bool ContainsPoint( rect2 rect, vec2 p )
{
  if ( ( p.x >= rect.min.x ) && ( p.x <= rect.max.x ) )
  {
    if ( ( p.y >= rect.min.y ) && ( p.y <= rect.max.y ) )
    {
      return true;
    }
  }
  return false;
}

rect2 Union(rect2 a, rect2 b)
{
  rect2 result = {};
  result.min.x = Min(a.min.x, b.min.x);
  result.min.y = Min(a.min.y, b.min.y);
  result.max.x = Max(a.max.x, b.max.x);
  result.max.y = Max(a.max.y, b.max.y);

  return result;
}

inline vec3 Vec3(vec4 v)
{
    vec3 result;
    result.x = v.x;
    result.y = v.y;
    result.z = v.z;
    return result;
}

inline float MapToUnitRange(float x, float min, float max)
{
    float range = max - min;
    float result = (x - min) / range;
    return result;
}

inline vec2 CalculateScreenPosition(mat4 mvp, float windowWidth, float windowHeight)
{
    vec4 v = mvp * Vec4(0,0,0,1);
    v = v * (1.0f / v.w);
    vec2 p = Vec2(v.x, v.y);
    p.x = (p.x + 1.0f) * windowWidth * 0.5f;
    p.y = (p.y + 1.0f) * windowHeight * 0.5f;
    p.y = windowHeight - p.y;

    return p;
}

inline vec3 UVec3(float x, float y, float z)
{
    vec3 result = Vec3(x, y, z);
    result = Normalize(result);
    return result;
}
