#pragma once

struct DebugSystem;
global DebugSystem *gDebugSystem;

internal void Debug_DrawLine(DebugSystem *debugSystem, vec3 start, vec3 end, vec3 colour, float lifetime);
internal void Debug_DrawPoint(DebugSystem *debugSystem, vec3 position, vec3 colour, float scale, float lifetime);
internal void Debug_DrawAxis(DebugSystem *debugSystem, mat4 transform, float lifetime);

inline void Debug_DrawLine(vec3 start, vec3 end, vec3 colour, float lifetime = 0.0f)
{
    if (gDebugSystem != NULL)
    {
        Debug_DrawLine(gDebugSystem, start, end, colour, lifetime);
    }
}

inline void Debug_DrawPoint(vec3 position, vec3 colour, float scale = 0.2f, float lifetime = 0.0f)
{
    if (gDebugSystem != NULL)
    {
        Debug_DrawPoint(gDebugSystem, position, colour, scale, lifetime);
    }
}

inline void Debug_DrawAxis(mat4 transform, float lifetime = 0.0f)
{
    if (gDebugSystem != NULL)
    {
        Debug_DrawAxis(gDebugSystem, transform, lifetime);
    }
}
