/*
 * TODO
 * - Viewport resizing [x]
 * - Freeroam camera [x]
 * - Network synchronized time value [ ]
 * - Connecting state [x]
 * - Backtrace on crash
 * - Grass rendering [x]
 * - Engine Systems
 *     - Debug console
 *     - CVars
 *     - Console commands [x]
 *     - Asset system
 *     - Event system
 *     - Profiling
 *     - Audio System
 * - Text rendering [x]
 * - Gameplay prototype?
 *     - Player controller
 *     - Weapon system
 *        - Hitboxes
 *        - Bullet drop
 *        - ADS
 *     - AI
 *     - CSG for level geometry
 * - Networking system
 *     - Client/server architecture
 *     - Connecting to server
 *     - Clientside prediction
 *     - Handle reconnecting
 *     - Network events
 *     - RPCs
 * - Animation System
 *     - Cleanup
 *     - Fix code reloading, [x]
 *     - Check that animation frame rates are correct [x]
 *     - Global animation clock/timeline
 *     - Animation blending
 *     - B-spline compression
 * - Audio System
 *     - Audio occludision
 *     - Audio effects, low pass, reverb
 * - Unknowns priority
 *     - Animation
 *     - Networking
 *     - AI?
 *     - Terrain rendering
 *     - Foliage rendering
 *     - Post processing
 *     - Occlusion culling
 *     - Rendering pipeline (PBR)
 */

/*
 * MVP GOAL - Parity with survival shooter prototype in UE4
 * - Player movement, including sprinting
 *     # Basic XZ player movement [x]
 *     # Sprinting [ ]
 *     # Ground plane/box [x]
 * - Entity System
 *
 * - Client server networking (Clientside prediction)
 *     # Copy over networking code [x]
 *     # Basic data definition language for network serialization ?
 *     # Auto connect to server [x]
 *     # Server must support 2 clients [x]
 * - Player world model animations for movement (Animation blending?)
 *     # Skeleton + animation importing
 *     # Animation playing
 * - Player view model animations, sprinting, ADS?, recoil?
 *     # Diffuse colour/texture only
 * - Melee only AI, runs at player and damages if too close
 *     # Pathfinding (Not needed if we are just using a ground plane)
 * - Player death and respawning
 * - Audio, at least gunshot sound
 * - Basic test geometry world (CSG) - LOW
 *     # Import CSG library [ ]
 *     # CSG rendering (all one material for now) [ ]
 *     # CSG collision (requires capsule vs triangle mesh sweep test) [ ]
 *
 * Network Synchronized Animation
 * Requires the use of a global clock for animation which places several limits
 * on what we can do with animation. For example, we can no longer adjust the
 * animation rate at runtime.
 *
 * SHORT-TERM TODO:
 * - Player view model [?]
 * - Bullet impact particle effect
 * - Player strafe animations [ ]
 * - Mute game audio when window out of focus
 * - Aabb triangle sweep test (requires GJK implementation)
 *      = CSG map geometry
 *      = Terrains (requires player controller improvements to traversing support sloped surfaces)
 * - Entity replication system (without prioritization, interpolation, prediction)
 *      = Player death and respawning
 * - RPC system
 *      = Server game events (Broadcast without return value)
 *      = Per client (SendToClient without return value)
 * - Font kerning
 * - Asset system
 * - Entity system
 * - Client side snapshot interpolation + debug viewer
 */
#include "game.h"

#include <cstdio>

#include <GL/glew.h>

#include "math_lib.h"
#include "logging.h"

#include "opengl.cpp"

#if !defined(STB_IMAGE_IMPLEMENTATION)
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#endif

#include "terrain.cpp"

#include "debug.cpp"
#include "audio.cpp"
#include "renderer.cpp"
#include "model.cpp"
#include "animation_system.cpp"

#include "server.cpp"
#include "net_sim.cpp"

#include "text_input.cpp"
#include "console.cpp"
#include "command_system.cpp"

#define ENABLE_RENDERER 1

u32 log_activeChannels = (u32)-1;

#define ToMicroseconds(seconds) (seconds * 1000.0 * 1000.0)

enum
{
    ClientGameState_Uninitialized,
    ClientGameState_MainMenu,
    ClientGameState_Connecting,
    ClientGameState_Connected,
    ClientGameState_AnimationTest,
};

struct Camera
{
    vec3 position;
    vec3 velocity;
    vec3 rotation;
};

struct GameState
{
    MemoryArena testArena;
    MemoryArena assetArena; // NOTE: This is cleared on a code reload
    InputSystem inputSystem;
    renderer_State renderer;
    Scene playerModelScene;
    Scene playerViewModelScene;
    Scene pistolScene;
    //SkeletonPose playerBindPose;
    SkeletonPose playerViewModelBindPose;
    Sk_SkinnedMesh testSkinnedMesh;
    Sk_SkinnedMesh testSkinnedMesh2;
    Sk_Skeleton testSkeleton2;
    Sk_AnimationClip testClip2;
    Sk_AnimationPlaybackState testPlaybackState2;
    Sk_SkinnedMesh testSkinnedMesh3;
    Sk_Skeleton vmAkSkeleton;
    Sk_AnimationClip vmAkIdle;
    Sk_AnimationPlaybackState vmAkPlaybackState;
    Sk_SkinnedMesh vmAkHandsMesh;
    ModelImportData viewModelData;
    mat4 offsetMatrices[0xFF];
    OpenGL_StaticMesh vmAkMeshes[6];
    u32 vmAkNodes[6];
    Sk_Skeleton playerSkeleton;
    Sk_AnimationClip playerRunAnimation;
    Sk_AnimationClip playerIdleAnimation;
    Sk_AnimationClip playerWalkAnimation;
    u32 testTexture;
    u32 grassTexture;
    u32 hitMarkerTexture;
    u32 skyboxTexture;
    u32 grassOpacityTexture;
    u32 rockDiffuseTexture;
    u32 testAudioClip;
    u32 gunShotClip;
    u32 hitMarkerClip;
    u32 uiClickClip;
    u32 uiRolloverClip;
    HeightMap terrainHeightMap;
    Terrain terrainData;
    OpenGL_StaticMesh terrainMesh;
    InstanceDataBuffer rockInstancingDataBuffer;
    OpenGL_StaticMesh rockMesh;
    float timeElapsed;
    RandomNumberGenerator rng;
    DebugSystem debugSystem;
    DebugLineVertex debugLinesVertices[8192];
    OpenGL_DynamicMesh debugLinesMesh;
    DebugLineVertex physicsDebugLinesVertices[PHYSICS_DEBUG_MAX_LINES];
    OpenGL_DynamicMesh physicsDebugLinesMesh;
    Camera camera;
    b32 useFreeRoamCamera;
    PlayerController players[64];
    float playerHealth[64];
    u32 playerCount;
    Sk_AnimationController playerAnimationControllers[64];
    InventoryState playerInventoryStates[64];
    Font testFont;
    Font testFontLarge;
    Font fontMontserratLarge;
    char debugTextBuffer[4096];
    u32 debugTextBufferLength;
    u8 state;
    u32 audioSourceIcon;
    GameEvent events[64];
    u32 eventCount;
    NetSim_State netSim;
    AckSystem ackSystem;
    AnimationController playerViewModelAnimationController;
    AnimationClip vmIdleClip;

    u64 packetReturnTimestamp;
    u16 packetSequenceNumber;
    u16 expectedPacketSequenceNumber;
    u64 roundTripTime;
    u16 incomingPacketSize;
    u16 outgoingPacketSize;
    u64 totalPacketsDropped;
    u64 totalPacketsSent;
    u32 samplePacketsDropped;
    u32 samplePacketsSent;
    float sampleTimeRemaining;
    float packetLoss;

    u32 currentServerTick;
    u32 currentTick;
    double currentLocalTime;

    u32 localPlayerIndex;
    float globalT;

    float hitMarkerTimeRemaining;

    console_State console;
    b32 isConsoleVisible;
    CommandSystem cmdSystem;

    AudioMixer audioMixer;

    b32 isHoveringOverButton;
    b32 showInventory;
};

internal void Debug_Printf(GameState *gameState, const char *format, ...)
{
    va_list args;
    va_start(args, format);
    i32 remaining = ArrayCount(gameState->debugTextBuffer) - gameState->debugTextBufferLength;
    i32 result = vsnprintf(gameState->debugTextBuffer + gameState->debugTextBufferLength,
        remaining, format, args);

    if (result > 0 && result < remaining)
        gameState->debugTextBufferLength += result;

    va_end(args);
}

internal void InitializeGame(GameState *gameState, GameMemory *memory)
{
    size_t total = memory->persistentStorageSize - sizeof(GameState);
    size_t testArenaSize = (64 * 1024);
    Assert(total > testArenaSize);
    total -= testArenaSize;
    MemoryArenaInitialize(&gameState->testArena, testArenaSize,
                          (u8 *)memory->persistentStorageBase +
                                  sizeof(GameState));

    MemoryArenaInitialize(&gameState->assetArena,
                          memory->persistentStorageSize - sizeof(GameState) -
                                  testArenaSize,
                          (u8 *)memory->persistentStorageBase +
                                  sizeof(GameState) + testArenaSize);

    NetSim_Initialize(&gameState->netSim);
    gameState->rng = CreateRandomNumberGenerator(0xFF1823AC, 0x723CFAE1);

    console_Init(&gameState->console);
    gameState->state = ClientGameState_MainMenu;

    InitAckSystem(&gameState->ackSystem);
    gameState->packetSequenceNumber = 1; // 0 is invalid
    gameState->expectedPacketSequenceNumber = 1; // FIXME: These must be reset for each connection
}

internal void TestCommand(u32 argc, char **argv)
{
    LOG_DEBUG("Test command!");
}

global GameMemory *gDebugMemory;
internal void ExitCommand(u32 argc, char **argv)
{
    gDebugMemory->performPlatformOp(PlatformOp_Exit);
}

global GameState *gDebugGameState;

internal void ReloadContent(GameState *gameState, GameMemory *memory)
{
    double startTime = memory->getTime();
    gameState->cmdSystem.count = 0;
    cmd_Register(&gameState->cmdSystem, "test", &TestCommand);
    cmd_Register(&gameState->cmdSystem, "exit", &ExitCommand);
    // Clear asset arena
    gameState->assetArena.used = 0;

#if ENABLE_RENDERER

    renderer_ReloadContent(&gameState->renderer);
    LOG_INFO("renderer_Reload: %g seconds", memory->getTime() - startTime);

    OpenGL_DeleteDynamicMesh(gameState->debugLinesMesh);
    OpenGL_DeleteDynamicMesh(gameState->physicsDebugLinesMesh);
    //ZeroStruct(gameState->debugLinesMesh);

    OpenGL_DeleteStaticMesh(gameState->terrainMesh);

    OpenGL_VertexAttribute vertexAttributes[2];
    vertexAttributes[0].index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
    vertexAttributes[0].numComponents = 3;
    vertexAttributes[0].componentType = GL_FLOAT;
    vertexAttributes[0].normalized = GL_FALSE;
    vertexAttributes[0].offset = 0;
    vertexAttributes[1].index = OPENGL_VERTEX_ATTRIBUTE_COLOUR;
    vertexAttributes[1].numComponents = 3;
    vertexAttributes[1].componentType = GL_FLOAT;
    vertexAttributes[1].normalized = GL_FALSE;
    vertexAttributes[1].offset = sizeof(float) * 3;

    gameState->debugLinesMesh = OpenGL_CreateDynamicMesh(
            gameState->debugLinesVertices, ArrayCount(gameState->debugLinesVertices),
            sizeof(DebugLineVertex), vertexAttributes,
            ArrayCount(vertexAttributes), GL_LINES);
    gameState->physicsDebugLinesMesh = OpenGL_CreateDynamicMesh(
            gameState->physicsDebugLinesVertices, ArrayCount(gameState->physicsDebugLinesVertices),
            sizeof(DebugLineVertex), vertexAttributes,
            ArrayCount(vertexAttributes), GL_LINES);

    DeleteTexture(gameState->testTexture);
    DeleteTexture(gameState->hitMarkerTexture);
    DeleteTexture(gameState->audioSourceIcon);
    DeleteTexture(gameState->skyboxTexture);
    DeleteScene(&gameState->playerModelScene);
    DestroyAudioClip(gameState->testAudioClip);
    DestroyAudioClip(gameState->gunShotClip);
    DestroyAudioClip(gameState->hitMarkerClip);
    DeleteScene(&gameState->playerViewModelScene);
    DeleteScene(&gameState->pistolScene);
    DeleteFont(gameState->testFont);
    DeleteFont(gameState->testFontLarge);
    DeleteFont(gameState->fontMontserratLarge);
    LOG_INFO("delete: %g seconds", memory->getTime() - startTime);

    ReadFileResult testTextureFile =
            memory->readEntireFile("../content/dev_grid64.png");
    if (testTextureFile.memory != NULL)
    {
        gameState->testTexture =
                CreateTexture(testTextureFile.memory, testTextureFile.size);
        memory->freeFileMemory(testTextureFile.memory);
    }
    ReadFileResult audioSourceIconFile =
            memory->readEntireFile("../content/audio_source.png");
    if (audioSourceIconFile.memory != NULL)
    {
        gameState->audioSourceIcon = CreateTexture(audioSourceIconFile.memory, audioSourceIconFile.size, true);
        memory->freeFileMemory(audioSourceIconFile.memory);
    }
    ReadFileResult hitMarkerTextureFile =
            memory->readEntireFile("../content/hitmarker.png");
    if (hitMarkerTextureFile.memory != NULL)
    {
        gameState->hitMarkerTexture = CreateTexture(hitMarkerTextureFile.memory, hitMarkerTextureFile.size, true);
        memory->freeFileMemory(hitMarkerTextureFile.memory);
    }

    ReadFileResult skyboxTextureFile = memory->readEntireFile("../content/skybox_clear.png");
    if (skyboxTextureFile.memory != NULL)
    {
        gameState->skyboxTexture = CreateCubeMapCrossTexture(
                skyboxTextureFile.memory, skyboxTextureFile.size,
                &gameState->assetArena);
        memory->freeFileMemory(skyboxTextureFile.memory);
    }
    LOG_INFO("textures: %g seconds", memory->getTime() - startTime);

    OpenGL_DeleteStaticMesh(gameState->testSkinnedMesh.mesh);
    OpenGL_DeleteStaticMesh(gameState->testSkinnedMesh2.mesh);
    ReadFileResult playerModelFile =
            memory->readEntireFile("../content/xbot.fbx");
    if (playerModelFile.memory != NULL)
    {
        // TODO: Use temp arena
        ModelImportData modelData =
                ImportModel(playerModelFile.memory, playerModelFile.size,
                            &gameState->assetArena, ModelImport_Hierarchy |
                            ModelImport_Normals | ModelImport_TextureCoords |
                            ModelImport_VertexWeights | ModelImport_Meshes, 0.01f);
        //ScaleModelData(&modelData, 0.01f);
        Sk_CreateSkinnedMesh(&gameState->testSkinnedMesh, &modelData.meshes[0],
                             &gameState->assetArena);
        Sk_CreateSkinnedMesh(&gameState->testSkinnedMesh2, &modelData.meshes[1],
                             &gameState->assetArena);

        Sk_CreateSkeleton(&gameState->playerSkeleton, &modelData, &gameState->assetArena);
        memory->freeFileMemory(playerModelFile.memory);
    }
    LOG_INFO("playerModelFile: %g seconds", memory->getTime() - startTime);

    OpenGL_DeleteStaticMesh(gameState->testSkinnedMesh3.mesh);
    ReadFileResult playerViewModelFile = memory->readEntireFile("../content/ViewModel_Base_idle.fbx");
    if (playerViewModelFile.memory != NULL)
    {
        ModelImportData modelData = ImportModel(
                playerViewModelFile.memory, playerViewModelFile.size,
                &gameState->assetArena,
                ModelImport_Hierarchy | ModelImport_Normals |
                        ModelImport_TextureCoords | ModelImport_VertexWeights |
                        ModelImport_Meshes);

        Sk_CreateSkinnedMesh(&gameState->testSkinnedMesh3, &modelData.meshes[0],
                             &gameState->assetArena);

        Sk_CreateSkeleton(&gameState->testSkeleton2, &modelData, &gameState->assetArena);

        AnimationImportData data = ImportAnimationData(playerViewModelFile.memory,
                playerViewModelFile.size, &gameState->assetArena);
        Sk_CreateAnimationClip(&gameState->testClip2, &data, &gameState->assetArena,
                &gameState->testSkeleton2, true, "vm_armature|anim_Idle");
        Sk_InitialisePlaybackState(&gameState->testPlaybackState2, &gameState->testClip2,
                &gameState->assetArena);
        memory->freeFileMemory(playerViewModelFile.memory);
    }
    LOG_INFO("ViewModel_Base_idle: %g seconds", memory->getTime() - startTime);

    OpenGL_DeleteStaticMesh(gameState->vmAkHandsMesh.mesh);
    for (u32 i = 0; i < 6; ++i)
    {
        OpenGL_DeleteStaticMesh(gameState->vmAkMeshes[i]);
    }
    ReadFileResult vmAkFile = memory->readEntireFile("../content/ViewModel_FireADS.fbx");
    if (vmAkFile.memory != NULL)
    {
        ModelImportData modelData = ImportModel(
                vmAkFile.memory, vmAkFile.size,
                &gameState->assetArena,
                ModelImport_Hierarchy | ModelImport_Normals |
                        ModelImport_TextureCoords | ModelImport_VertexWeights |
                        ModelImport_Meshes);
        gameState->viewModelData = modelData;
        Sk_CreateSkinnedMesh(&gameState->vmAkHandsMesh,
                &modelData.meshes[0], &gameState->assetArena);
        gameState->vmAkNodes[0] = 58;
        gameState->vmAkNodes[1] = 59;
        gameState->vmAkNodes[2] = 60;
        gameState->vmAkNodes[3] = 62;
        gameState->vmAkNodes[4] = 64;
        gameState->vmAkNodes[5] = 66;
        for (u32 i = 0; i < 6; ++i)
        {
            Node *node = &modelData.nodes[gameState->vmAkNodes[i]];
            mat4 transform = Translate(node->position) * Rotate(node->rotation) * Scale(node->scale);
            gameState->vmAkMeshes[i] =
                    CreateMeshFromData(&modelData.meshes[i + 1],
                                       &gameState->assetArena, &transform);
        }
        Sk_CreateSkeleton(&gameState->vmAkSkeleton, &modelData, &gameState->assetArena);
        AnimationImportData data = ImportAnimationData(vmAkFile.memory,
                vmAkFile.size, &gameState->assetArena);
        Sk_CreateAnimationClip(&gameState->vmAkIdle, &data, &gameState->assetArena,
                &gameState->vmAkSkeleton, true, "Armature|ANIM_Idle01");
        Sk_InitialisePlaybackState(&gameState->vmAkPlaybackState, &gameState->vmAkIdle,
                &gameState->assetArena);

        memory->freeFileMemory(vmAkFile.memory);
    }
    LOG_INFO("ViewModel_FireADS: %g seconds", memory->getTime() - startTime);

    ReadFileResult runAnimationFile =
            memory->readEntireFile("../content/Running.fbx");
    if (runAnimationFile.memory != NULL)
    {
        AnimationImportData data = ImportAnimationData(runAnimationFile.memory,
                                                       runAnimationFile.size,
                                                       &gameState->assetArena, 0.01f);
        //ScaleAnimationData(&data, 0.01f);
        Sk_CreateAnimationClip(&gameState->playerRunAnimation, &data, &gameState->assetArena,
                &gameState->playerSkeleton, true);

        memory->freeFileMemory(runAnimationFile.memory);
    }

    ReadFileResult idleAnimationFile = memory->readEntireFile("../content/Idle.fbx");
    if (idleAnimationFile.memory != NULL)
    {
        AnimationImportData data = ImportAnimationData(idleAnimationFile.memory,
                                                       idleAnimationFile.size,
                                                       &gameState->assetArena, 0.01f);
        //ScaleAnimationData(&data, 0.01f);
        Sk_CreateAnimationClip(&gameState->playerIdleAnimation, &data, &gameState->assetArena,
                &gameState->playerSkeleton, true);
        memory->freeFileMemory(idleAnimationFile.memory);
    }

    ReadFileResult walkAnimationFile = memory->readEntireFile("../content/Walking.fbx");
    if (walkAnimationFile.memory != NULL)
    {
        AnimationImportData data = ImportAnimationData(walkAnimationFile.memory,
                                                       walkAnimationFile.size,
                                                       &gameState->assetArena, 0.01f);
        //ScaleAnimationData(&data, 0.01f);
        Sk_CreateAnimationClip(&gameState->playerWalkAnimation, &data, &gameState->assetArena,
                &gameState->playerSkeleton, true);
        memory->freeFileMemory(walkAnimationFile.memory);
    }
    LOG_INFO("animations: %g seconds", memory->getTime() - startTime);

    ReadFileResult hackFontFile = memory->readEntireFile("../content/Hack-Regular.ttf");
    if (hackFontFile.memory != NULL)
    {
        gameState->testFont =
                CreateFont(hackFontFile.memory, hackFontFile.size,
                           &gameState->assetArena, 2048, 2048, 18);
        gameState->testFontLarge =
                CreateFont(hackFontFile.memory, hackFontFile.size,
                           &gameState->assetArena, 2048, 2048, 28);
        memory->freeFileMemory(hackFontFile.memory);
    }

    ReadFileResult montserrateFontFile =
            memory->readEntireFile("../content/ui/Montserrat-Light.ttf");
    if (montserrateFontFile.memory != NULL)
    {
        gameState->fontMontserratLarge = CreateFont(montserrateFontFile.memory, montserrateFontFile.size,
                &gameState->assetArena, 1024, 1024, 72);
    }

    ReadFileResult wavFile = memory->readEntireFile("../content/test.wav");
    if (wavFile.memory != NULL)
    {
        gameState->testAudioClip = CreateAudioClip(wavFile.memory, wavFile.size);
        memory->freeFileMemory(wavFile.memory);

        u32 source;
        alGenSources(1, &source);
        Assert(alGetError() == AL_NO_ERROR);
        alSourcei(source, AL_BUFFER, gameState->testAudioClip);
        Assert(alGetError() == AL_NO_ERROR);
        alSource3f(source, AL_POSITION, 5.0f, 2.0f, 0.0f);
        alSourcei(source, AL_LOOPING, true);
        //alSourcePlay(source);
    }
    ReadFileResult gunShotWavFile = memory->readEntireFile("../content/pistol.wav");
    if (gunShotWavFile.memory != NULL)
    {
        gameState->gunShotClip = CreateAudioClip(gunShotWavFile.memory, gunShotWavFile.size);
        memory->freeFileMemory(gunShotWavFile.memory);
    }

    ReadFileResult hitMarkerWavFile = memory->readEntireFile("../content/hitmarker.wav");
    if (hitMarkerWavFile.memory != NULL)
    {
        gameState->hitMarkerClip = CreateAudioClip(hitMarkerWavFile.memory, hitMarkerWavFile.size);
        memory->freeFileMemory(hitMarkerWavFile.memory);
    }

    ReadFileResult uiClickWavFile = memory->readEntireFile("../content/ui/click1.wav");
    if (uiClickWavFile.memory != NULL)
    {
        gameState->uiClickClip = CreateAudioClip(uiClickWavFile.memory, uiClickWavFile.size);
        memory->freeFileMemory(uiClickWavFile.memory);
    }

    ReadFileResult uiRolloverWavFile = memory->readEntireFile("../content/ui/rollover2.wav");
    if (uiRolloverWavFile.memory != NULL)
    {
        gameState->uiRolloverClip = CreateAudioClip(uiRolloverWavFile.memory,
                                                    uiRolloverWavFile.size);
    }

    TerrainParameters terrainParams = {};
    ReadFileResult heightMapFile = memory->readEntireFile("../content/heightmap_small.png");
    if (heightMapFile.memory != NULL)
    {
        gameState->terrainHeightMap =
                CreateHeightMap(heightMapFile.memory, heightMapFile.size,
                                &gameState->assetArena);

        terrainParams.origin = Vec3(-100, 0, 0);
        terrainParams.scale = Vec3(200, 40, 200);
        terrainParams.uvScale = 128.0f;
        terrainParams.rows = 64;
        terrainParams.cols = 64;
        gameState->terrainData =
                CreateTerrain(gameState->terrainHeightMap, terrainParams,
                              &gameState->assetArena);

        gameState->terrainMesh = CreateTerrainMesh(gameState->terrainData);

        LOG_DEBUG("Terrain Triangle Count: %u", gameState->terrainData.numIndices / 3);
    }

    ReadFileResult grassTextureFile = memory->readEntireFile("../content/grass.jpg");
    if (grassTextureFile.memory != NULL)
    {
        gameState->grassTexture = CreateTexture(grassTextureFile.memory, grassTextureFile.size);
    }

    ReadFileResult grassOpacityFile = memory->readEntireFile("../content/grass_opacity.tga");
    if (grassOpacityFile.memory != NULL)
    {
        gameState->grassOpacityTexture = CreateTexture(
                grassOpacityFile.memory, grassOpacityFile.size);
        memory->freeFileMemory(grassOpacityFile.memory);
    }

    ReadFileResult rockDiffuseFile = memory->readEntireFile("../content/desert_rock01/rock_diffuse.png");
    if (rockDiffuseFile.memory != NULL)
    {
        gameState->rockDiffuseTexture = CreateTexture(rockDiffuseFile.memory, rockDiffuseFile.size);
        memory->freeFileMemory(rockDiffuseFile.memory);
    }

    // FIXME: Only allocating enough for 4 players for now
    for (u32 i = 0; i < 4; ++i)
    {
        Sk_AnimationController *controller = gameState->playerAnimationControllers + i;
        Sk_InitializeAnimationController(controller,
            PlayerAnimation_Max, &gameState->playerSkeleton, &gameState->assetArena);

        Sk_InitialisePlaybackState(
                &controller->playbackStates[PlayerAnimation_Idle],
                &gameState->playerIdleAnimation, &gameState->assetArena);
        Sk_InitialisePlaybackState(
                &controller->playbackStates[PlayerAnimation_Walk],
                &gameState->playerWalkAnimation, &gameState->assetArena);
        Sk_InitialisePlaybackState(
                &controller->playbackStates[PlayerAnimation_Run],
                &gameState->playerRunAnimation, &gameState->assetArena);
    }

    DeleteInstanceDataBuffer(gameState->rockInstancingDataBuffer);
    ReadFileResult rockMeshFile = memory->readEntireFile("../content/desert_rock01/rock01.obj");
    if (rockMeshFile.memory != NULL)
    {
        ModelImportData rockData = ImportModel(
                rockMeshFile.memory, rockMeshFile.size, &gameState->assetArena,
                ModelImport_Meshes | ModelImport_Normals | ModelImport_TextureCoords);

        OpenGL_StaticMesh rockMesh =
                CreateMeshFromData(&rockData.meshes[0], &gameState->assetArena);

        //OpenGL_StaticMesh rockMesh = rockScene.meshes[0].geometry;
        gameState->rockMesh = rockMesh;
        gameState->rockInstancingDataBuffer = CreateInstanceDataBuffer(
                rockMesh.vao, rockMesh.vbo, rockMesh.numVertices,
                rockMesh.ibo, rockMesh.numIndices,
                rockMesh.primitive);
        memory->freeFileMemory(rockMeshFile.memory);

        InstanceDataBuffer dataBuffer = gameState->rockInstancingDataBuffer;
        glBindVertexArray(dataBuffer.vao);
        u32 count = 1000;
        mat4 *transforms = AllocateArray(&gameState->assetArena, mat4, count);
        for (u32 i = 0; i < count; ++i)
        {
            float x = 100.0f * RandomBinomial(&gameState->rng) + 100.0f;
            float y = 100.0f * RandomBinomial(&gameState->rng) + 100.0f;
            vec3 worldPosition = Vec3((float)x, 0, (float)y);
            vec3 relativePosition = worldPosition;
            float u = Clamp(relativePosition.x / terrainParams.scale.x, 0.0f, 1.0f);
            float v = Clamp(relativePosition.z / terrainParams.scale.z, 0.0f, 1.0f);
            worldPosition.y =
                HeightMapGetValue(gameState->terrainHeightMap, u, v) *
                terrainParams.scale.y;
            worldPosition.x -= 100.0f;
            float scale = 2.4f * RandomBinomial(&gameState->rng) + 1.2f;
            float angle = PI * RandomBinomial(&gameState->rng);
            transforms[i] = Translate(worldPosition) * RotateY(angle) * Scale(scale);
        }
        glBindBuffer(GL_ARRAY_BUFFER, dataBuffer.transformsVbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(mat4) * count, transforms[0].raw, GL_STATIC_DRAW);
        MemoryArenaFree(&gameState->assetArena, transforms);
        glBindVertexArray(0);
    }

    {
        InstanceDataBuffer dataBuffer = gameState->renderer.grassInstancingDataBuffer;
        glBindVertexArray(dataBuffer.vao);
        u32 count = 40000;
        mat4 *transforms = AllocateArray(&gameState->assetArena, mat4, count);
        u32 index = 0;
        // TODO: Update this dynamically based on AABB around camera
        for (i32 y = 0; y < 200; ++y)
        {
            for (i32 x = 0; x < 200; ++x)
            {
                vec3 worldPosition = Vec3((float)x, 0, (float)y);
                vec3 relativePosition = worldPosition;
                float u = Clamp(relativePosition.x / terrainParams.scale.x, 0.0f, 1.0f);
                float v = Clamp(relativePosition.z / terrainParams.scale.z, 0.0f, 1.0f);
                worldPosition.y =
                        HeightMapGetValue(gameState->terrainHeightMap, u, v) *
                        terrainParams.scale.y;
                worldPosition.x -= 100.0f;
                float scale = 1.8f * RandomBinomial(&gameState->rng) + 1.7f;
                float angle = PI * RandomBinomial(&gameState->rng);
                transforms[index++] = Translate(worldPosition) * RotateY(angle) * Scale(scale);
            }
        }
        Assert(index == count);
        glBindBuffer(GL_ARRAY_BUFFER, dataBuffer.transformsVbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(mat4) * count, transforms[0].raw, GL_STATIC_DRAW);
        MemoryArenaFree(&gameState->assetArena, transforms);
        vec3 *colours = AllocateArray(&gameState->assetArena, vec3, count);
        for (u32 i = 0; i < count; ++i)
        {
            float r = 0.1f * RandomBinomial(&gameState->rng) + 0.05f;
            float g = 0.15f * RandomBinomial(&gameState->rng) - 0.05f;
            vec3 colour = Vec3(0.25f + r, 0.5f + g, 0.0f);
            colours[i] = colour;
        }
        glBindBuffer(GL_ARRAY_BUFFER, dataBuffer.coloursVbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vec3) * count, colours[0].data, GL_STATIC_DRAW);
        MemoryArenaFree(&gameState->assetArena, colours);
        glBindVertexArray(0);
    }
#endif

    double endTime = memory->getTime();
    LOG_INFO("Asset loading took %g seconds", endTime - startTime);
}

//internal void UpdateViewModelAnimationController(AnimationController *controller, float globalT)
//{
    //BlendTree *blendTree = &controller->blendTree;

    //UpdatePose(&blendTree->clipStates[0], globalT);
    //LerpPose(blendTree->clipStates[0].currentPose, blendTree->clipStates[0].currentPose,
            //0.0f, blendTree->blendedPose, controller->skeleton->boneCount);

    //controller->currentPose = blendTree->blendedPose;
//}

void DrawString(GameState *gameState, vec2 p, const char *text, Font *font,
                vec4 colour, mat4 *viewProjection,
                u32 alignment = TEXT_ALIGN_LEFT)
{
    renderer_DrawString(&gameState->renderer, p, text, font, colour, viewProjection, alignment);
}

internal void InitializeOpenGL()
{
#if ENABLE_RENDERER
    GLenum result = glewInit();
    if (result != GLEW_OK)
    {
        printf("%s\n", glewGetErrorString(result));
    }
#endif
}

internal void InitializeOpenAL()
{
    ALCcontext *previousContext = alcGetCurrentContext();
    if (previousContext != NULL)
    {
        ALCdevice *previousDevice = alcGetContextsDevice(previousContext);
        alcDestroyContext(previousContext);
        if (previousDevice != NULL)
        {
            alcCloseDevice(previousDevice);
        }
    }
    // TODO: Audio device selection;
    ALCdevice *device = alcOpenDevice(NULL);
    if (device != NULL)
    {
        ALCcontext *context = alcCreateContext(device, NULL);
        if (context != NULL)
        {
            alcMakeContextCurrent(context);
        }
        else
        {
            LOG_ERROR("Failed to create OpenAL context");
        }
    }
    else
    {
        LOG_ERROR("Failed to open OpenAL device");
    }
}

internal void DrawSceneNode(GameState *gameState, SceneNode *node,
                            mat4 viewProjection)
{
    // Draw model space axis
    VertexColourShader shader = gameState->renderer.vertexColourShader;
    glUseProgram(shader.program);
    mat4 mvp = viewProjection * node->transform * Scale(Vec3(0.1f));
    glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
    OpenGL_DrawStaticMesh(gameState->renderer.axisMesh);
}

internal void UpdateCamera(Camera *camera, InputSystem *inputSystem, float dt,
                           GameMemory *memory, u32 windowWidth,
                           u32 windowHeight)
{
    if (IsKeyDown(inputSystem, K_MOUSE_BUTTON_RIGHT))
    {
        memory->setMouseCursor(MOUSE_CURSOR_HIDDEN);

        float sensitivity = 0.5f;
        float mouseX = (float)(inputSystem->mouseX - inputSystem->prevMouseX) / (float)windowWidth;
        float mouseY = (float)(inputSystem->mouseY - inputSystem->prevMouseY) / (float)windowHeight;
        vec3 cameraRotation = Vec3(mouseY * -2.0f * PI, -2.0f * PI * mouseX, 0);
        cameraRotation *= sensitivity;
        camera->rotation += cameraRotation;
        camera->rotation.x = Clamp(camera->rotation.x, -PI * 0.5f, PI * 0.5f);
        if (camera->rotation.y > PI)
        {
            camera->rotation.y -= 2.0f * PI;
        }
        else if (camera->rotation.y < -PI)
        {
            camera->rotation.y += 2.0f * PI;
        }
    }
    else
    {
        memory->setMouseCursor(MOUSE_CURSOR_VISIBLE);
    }

    vec3 acceleration = {};
    float speed = 80.0f;
    if (IsKeyDown(inputSystem, K_W))
        acceleration.z -= 1.0f;
    if (IsKeyDown(inputSystem, K_S))
        acceleration.z += 1.0f;
    if (IsKeyDown(inputSystem, K_A))
        acceleration.x -= 1.0f;
    if (IsKeyDown(inputSystem, K_D))
        acceleration.x += 1.0f;

    if (Normalize(&acceleration))
    {
        acceleration *= speed;
        camera->velocity += acceleration * dt;
    }
    camera->velocity -= camera->velocity * 7.0f * dt;
    vec4 v = Vec4(camera->velocity, 0.0f);
    v = RotateY(camera->rotation.y) * RotateX(camera->rotation.x) * v;
    vec3 velocity = Vec3(v.x, v.y, v.z);
    camera->position += velocity * dt;
}

internal PlayerCommand CreatePlayerCommand(InputSystem *inputSystem, u32 windowWidth, u32 windowHeight)
{
    PlayerCommand result = {};
    if (IsKeyDown(inputSystem, K_W))
        result.forwardMove -= 1.0f;
    if (IsKeyDown(inputSystem, K_S))
        result.forwardMove += 1.0f;
    if (IsKeyDown(inputSystem, K_A))
        result.rightMove -= 1.0f;
    if (IsKeyDown(inputSystem, K_D))
        result.rightMove += 1.0f;

    if (IsKeyDown(inputSystem, K_MOUSE_BUTTON_LEFT))
        result.actions = PlayerAction_PrimaryFire;

    float sensitivity = 0.5f;
    float mouseX = (float)inputSystem->mouseX / (float)windowWidth;
    float mouseY = (float)inputSystem->mouseY / (float)windowHeight;
    result.angles = Vec2(mouseY * -2.0f * PI, -2.0f * PI * mouseX);
    result.angles *= sensitivity;

    return result;
}

internal b32 ui_Button(GameState *gameState, vec2 p, const char *text,
                        Font *font, InputSystem *inputSystem, mat4 *projection,
                        b32 *isHover = NULL)
{
    b32 result = false;
    float width = CalculateTextLength(font, text) + 40.0f;
    float height = font->ascent + font->descent + font->lineGap + 20.0f;
    vec4 colour = Vec4( 0.15, 0.15, 0.15, 1 );
    vec4 hoverColour = Vec4( 0.3, 0.3, 0.3, 1 );
    vec4 clickColour = Vec4( 0.5, 0.5, 0.5, 1 );
    vec4 textColour = Vec4( 0.7, 0.7, 0.7, 1 );

    vec2 mousePosition = Vec2((float)inputSystem->mouseX,
            (float)inputSystem->mouseY);

    rect2 rect = Rect2( p, Vec2( width, height ) );
    if ( ContainsPoint( rect, mousePosition ) )
    {
        if ( IsKeyDown( inputSystem, K_MOUSE_BUTTON_LEFT ) )
        {
            // Click
            colour = clickColour;
        }
        else
        {
            if ( WasKeyReleased( inputSystem, K_MOUSE_BUTTON_LEFT ) )
            {
                // Process click
                result = true;
            }
            else
            {
                // Hover
                colour = hoverColour;
                if (isHover)
                    *isHover = true;
            }
        }
    }
    //if ( isSelected )
    //{
        //colour = clickColour;
    //}
    renderer_DrawRect(&gameState->renderer, p, width, height, colour, projection);
    DrawString(gameState, Vec2(p.x + 20.0f, p.y + 10.0f), text, font, textColour, projection);

    return result;
}

internal void DrawModel(GameState *gameState, Scene *scene, mat4 mvp, mat4 *skinningMatrices = NULL, u32 skinningMatricesCount = 0)
{
    for (u32 i = 0; i < scene->nodeCount; ++i)
    {
        SceneNode *node = scene->nodes + i;
        for (u32 j = 0; j < node->meshCount; ++j)
        {
            u32 meshIndex = node->meshes[j];
            Assert(meshIndex < scene->meshCount);
            SceneMesh mesh = scene->meshes[meshIndex];
            if (mesh.isSkeletalMesh && skinningMatrices != NULL)
            {
                SkinnedColourShader shader = gameState->renderer.skinnedColourShader;
                glUseProgram(shader.program);
                mat4 nodeMvp = mvp * node->transform;
                glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, nodeMvp.raw);
                glUniformMatrix4fv(shader.boneTransforms, skinningMatricesCount,
                        GL_FALSE, skinningMatrices[0].raw);

                float x = i * 0.1f;
                vec4 colour = Vec4((float)x, (float)x, (float)x, 1.0f);
                glUniform4fv(shader.colour, 1, colour.data);
                OpenGL_DrawStaticMesh(mesh.geometry);
            }
            else
            {
                ColourShader shader = gameState->renderer.colourShader;
                glUseProgram(shader.program);
                mat4 nodeMvp = mvp * node->transform;
                glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, nodeMvp.raw);

                float x = i * 0.1f;
                vec4 colour = Vec4((float)x, (float)x, (float)x, 1.0f);
                glUniform4fv(shader.colour, 1, colour.data);
                OpenGL_DrawStaticMesh(mesh.geometry);
            }
        }
    }

#if 0
    glDisable(GL_DEPTH_TEST);
    for (u32 i = 0; i < scene->nodeCount; ++i)
    {
        SceneNode *node = scene->nodes + i;
        VertexColourShader vertexColourShader = gameState->renderer.vertexColourShader;
        glUseProgram(vertexColourShader.program);
        mat4 nodeMvp = mvp * node->transform * Scale(0.02f);
        glUniformMatrix4fv(vertexColourShader.mvp, 1, GL_FALSE, nodeMvp.raw);
        OpenGL_DrawStaticMesh(gameState->renderer.axisMesh);
    }
    glEnable(GL_DEPTH_TEST);
#endif
}

internal void Render(GameState *gameState, PhysicsSystem *physicsSystem, float dt, u32 windowWidth, u32 windowHeight)
{
    glViewport(0, 0, windowWidth, windowHeight);
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.15f, 0.15f, 0.15f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    float aspectRatio = (float)windowWidth / (float)windowHeight;
    mat4 projection = Perspective(Radians(90.0f), aspectRatio, 0.1f, 500.0f);
    mat4 view = Identity();
    mat4 orthoProjection =
            Orthographic(0, (float)windowWidth, 0, (float)windowHeight);

    vec3 cameraPosition = {};
    vec3 cameraRotation = {};

    vec3 listenerUp = Vec3(0, 1, 0);
    vec3 listenerForward = {};
    if (gameState->useFreeRoamCamera)
    {
        view = RotateX(-gameState->camera.rotation.x) *
               RotateY(-gameState->camera.rotation.y) *
               Translate(-gameState->camera.position);
        cameraPosition = gameState->camera.position;
        cameraRotation = gameState->camera.rotation;
        listenerForward = Vec3(Sin(gameState->camera.rotation.y), 0.0f,
                               -Cos(gameState->camera.rotation.y));
    }
    else
    {
        PlayerController *player =
                gameState->players + gameState->localPlayerIndex;
        cameraPosition = (player->position + Vec3(0.0f, 0.6f, 0.0f));
        cameraRotation = Vec3(player->viewAngles.x, player->viewAngles.y, 0.0f);
        listenerForward = Vec3(Sin(player->viewAngles.y), 0.0f,
                               -Cos(player->viewAngles.y));
        view = RotateX(-player->viewAngles.x) * RotateY(-player->viewAngles.y) *
               Translate(-cameraPosition);
    }
    alListener3f(AL_POSITION, cameraPosition.x, cameraPosition.y,
                 -cameraPosition.z);
    vec3 orientation[2] = {listenerForward, listenerUp};
    alListenerfv(AL_ORIENTATION, orientation[0].data);
    Assert(alGetError() == AL_NO_ERROR);

    glDisable(GL_DEPTH_TEST);
    {
        mat4 skyboxViewMatrix = RotateX(-cameraRotation.x) * RotateY(-cameraRotation.y);
        CubeMapShader shader = gameState->renderer.cubeMapShader;
        glUseProgram(shader.program);
        mat4 mvp = projection * skyboxViewMatrix;
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, gameState->skyboxTexture);
        glUniform1i(shader.cubeTexture, 0);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);
        OpenGL_DrawStaticMesh(gameState->renderer.cubeMesh);
        glDisable(GL_CULL_FACE);
    }

    glEnable(GL_DEPTH_TEST);
    {
        DiffuseTextureShader shader = gameState->renderer.diffuseTextureShader;
        glUseProgram(shader.program);
        mat4 mvp = projection * view;
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, gameState->grassTexture);
        glUniform1i(shader.diffuseTexture, 0);
        OpenGL_DrawStaticMesh(gameState->terrainMesh);
    }

    {
        // Skeletal mesh
        Scene *scene = &gameState->playerModelScene;
        ColourShader shader = gameState->renderer.colourShader;
        glUseProgram(shader.program);
        mat4 model = RotateX(-PI * 0.5f);
        mat4 mvp = projection * view * model;
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        for (u32 i = 0; i < scene->meshCount; ++i)
        {
            float x = i * 0.1f;
            vec4 colour = Vec4(x, x, x, 1.0f);
            glUniform4fv(shader.colour, 1, colour.data);
            // OpenGL_DrawStaticMesh(scene->meshes[i]);
        }

        // glDisable(GL_DEPTH_TEST);
        // mat4 viewProjection = projection * view;
        // for (u32 i = 0; i < scene->nodeCount; ++i)
        //{
        // DrawSceneNode(gameState, scene->nodes + i, viewProjection);
        //}
    }

    {
        Scene *scene = &gameState->pistolScene;
        mat4 model = Translate(Vec3(4, 2, 0));
        mat4 mvp = projection * view * model;
        DrawModel(gameState, scene, mvp);
    }

    {
        mat4 model = Translate(Vec3(0, 0, 0));// * Scale(10.0f);
        mat4 mvp = projection * view * model;
        GrassShader shader = gameState->renderer.grassShader;
        glUseProgram(shader.program);
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        glBindTexture(GL_TEXTURE_2D, gameState->grassOpacityTexture);
        glUniform1i(shader.diffuseTexture, 0);
        glUniform1f(shader.time, (float)gameState->currentLocalTime);
        InstanceDataBuffer dataBuffer = gameState->renderer.grassInstancingDataBuffer;
        glBindVertexArray(dataBuffer.vao);
        glDrawArraysInstanced(dataBuffer.primitive, 0, dataBuffer.numVertices, 40000);
    }

    {
        // Rocks
        mat4 model = Translate(Vec3(0, 0, 0));// * Scale(10.0f);
        mat4 mvp = projection * view * model;
        RockShader shader = gameState->renderer.rockShader;
        glUseProgram(shader.program);
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        glBindTexture(GL_TEXTURE_2D, gameState->rockDiffuseTexture);
        glUniform1i(shader.diffuseTexture, 0);
        InstanceDataBuffer dataBuffer = gameState->rockInstancingDataBuffer;
        glBindVertexArray(dataBuffer.vao);
        glDrawElementsInstanced(dataBuffer.primitive, dataBuffer.numIndices, GL_UNSIGNED_INT, 0, 1000);
    }

#if 0
    {
        Scene *scene = &gameState->playerViewModelScene;

        AnimationController *animationController =
                &gameState->playerViewModelAnimationController;
        Skeleton *skeleton = animationController->skeleton;

        mat4 *boneTransforms =
                AllocateArray(&gameState->testArena, mat4, skeleton->boneCount);

        // gameState->playerViewModelBindPose.localPose[5].translation =
        // Vec3(0,1,0);
        Assert(animationController->currentPose);
        // animationController->currentPose[0].rotation = Quat(Vec3(1,0,0), PI *
        // -0.25f);
        for (u32 boneIdx = 0; boneIdx < skeleton->boneCount; ++boneIdx)
        {
            boneTransforms[boneIdx] = CalculateBonePoseWorldTransform(
                    *skeleton, animationController->currentPose, (u8)boneIdx);
        }

        mat4 *skinningMatrices =
                AllocateArray(&gameState->testArena, mat4, skeleton->boneCount);
        for (u32 boneIdx = 0; boneIdx < skeleton->boneCount; ++boneIdx)
        {
            skinningMatrices[boneIdx] = boneTransforms[boneIdx] *
                                        skeleton->bones[boneIdx].invBindPose;
        }

        // for (u32 nodeIndex = 0; nodeIndex < scene->nodeCount / 2;
        // ++nodeIndex)
        //{
        // SceneNode *node = scene->nodes + nodeIndex;
        // quat rotation = node->localRotation;
        // Debug_Printf(gameState, "%u: %g %g %g %g\n", nodeIndex, rotation.x,
        // rotation.y, rotation.z, rotation.w);
        //}

        mat4 model = Translate(Vec3(0, 2, 0));
        mat4 mvp = projection * view * model;
        DrawModel(gameState, scene, mvp, skinningMatrices, skeleton->boneCount);

        glDisable(GL_DEPTH_TEST);
        for (u32 boneIdx = 0; boneIdx < skeleton->boneCount; ++boneIdx)
        {
            VertexColourShader vertexColourShader =
                    gameState->renderer.vertexColourShader;
            glUseProgram(vertexColourShader.program);
            mat4 boneModel = boneTransforms[boneIdx];
            mat4 boneMvp = mvp * boneModel * Scale(Vec3(0.04f));
            glUniformMatrix4fv(vertexColourShader.mvp, 1, GL_FALSE,
                               boneMvp.raw);
            OpenGL_DrawStaticMesh(gameState->renderer.axisMesh);

            const char *text = skeleton->bones[boneIdx].name;
            vec2 p = CalculateScreenPosition(boneMvp, (float)windowWidth,
                                             (float)windowHeight);
            float textWidth = CalculateTextLength(&gameState->testFont, text);
            float textHeight =
                    gameState->testFont.ascent - gameState->testFont.descent;
            renderer_DrawRect(&gameState->renderer, p, textWidth, textHeight,
                              Vec4(0, 0, 0, 0.4), &orthoProjection);
            DrawString(gameState, p, text, &gameState->testFont, Vec4(1),
                       &orthoProjection);
        }
        glEnable(GL_DEPTH_TEST);

        MemoryArenaFree(&gameState->testArena, skinningMatrices);
        MemoryArenaFree(&gameState->testArena, boneTransforms);
    }
#endif

    for (u32 playerIndex = 0; playerIndex < gameState->playerCount;
         ++playerIndex)
    {
        if (playerIndex == gameState->localPlayerIndex &&
            !gameState->useFreeRoamCamera)
            continue;

        Sk_AnimationController *animationController =
                gameState->playerAnimationControllers + playerIndex;
        PlayerController *player = gameState->players + playerIndex;
        Sk_Skeleton *skeleton = animationController->skeleton;

        mat4 *boneTransforms =
                AllocateArray(&gameState->testArena, mat4, skeleton->boneCount);

        Assert(animationController->currentPose);
        Sk_CalculatePoseWorldTransform(boneTransforms,
                                       animationController->currentPose,
                                       skeleton, &gameState->assetArena);

        mat4 *skinningMatrices =
                AllocateArray(&gameState->testArena, mat4, skeleton->boneCount);
        for (u32 boneIdx = 0; boneIdx < skeleton->boneCount; ++boneIdx)
        {
            skinningMatrices[boneIdx] = boneTransforms[boneIdx] *
                                        gameState->testSkinnedMesh.invBindPose[boneIdx];
        }

        Scene *scene = &gameState->playerModelScene;
        mat4 model = Translate(player->position - Vec3(0.0f, 1.0f, 0.0f)) *
                     RotateY(player->viewAngles.y - PI) * Scale(0.01f);
        mat4 mvp = projection * view * model;
        SkinnedColourShader shader = gameState->renderer.skinnedColourShader;
        glUseProgram(shader.program);
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        glUniformMatrix4fv(shader.boneTransforms, skeleton->boneCount,
                GL_FALSE, skinningMatrices[0].raw);
        vec4 colour = Vec4(0.1, 0.1, 0.1, 1.0);
        glUniform4fv(shader.colour, 1, colour.data);
        OpenGL_DrawStaticMesh(gameState->testSkinnedMesh.mesh);

        for (u32 boneIdx = 0; boneIdx < skeleton->boneCount; ++boneIdx)
        {
            skinningMatrices[boneIdx] = boneTransforms[boneIdx] *
                                        gameState->testSkinnedMesh2.invBindPose[boneIdx];
        }

        colour = Vec4(0.4, 0.4, 0.4, 1.0);
        glUniform4fv(shader.colour, 1, colour.data);
        glUniformMatrix4fv(shader.boneTransforms, skeleton->boneCount,
                GL_FALSE, skinningMatrices[0].raw);
        OpenGL_DrawStaticMesh(gameState->testSkinnedMesh2.mesh);

        MemoryArenaFree(&gameState->testArena, skinningMatrices);
        MemoryArenaFree(&gameState->testArena, boneTransforms);
    }

    {
        glDisable(GL_DEPTH_TEST);
        gameState->debugLinesMesh.numVertices = 0;
        mat4 axisTransforms[1024];
        u32 axisCount = Debug_Render(&gameState->debugSystem,
                                     &gameState->debugLinesMesh, axisTransforms,
                                     ArrayCount(axisTransforms));
        OpenGL_UpdateDynamicMesh(gameState->debugLinesMesh);

#if 0
        if (physicsSystem != NULL)
        {
            gameState->physicsDebugLinesMesh.numVertices = 0;
            PhysicsDebugLine lines[PHYSICS_DEBUG_MAX_LINES];
            u32 count = PhysicsDraw(physicsSystem, lines, ArrayCount(lines));
            for (u32 i = 0; i < count; ++i)
            {
                Debug_AddLineVertices(&gameState->physicsDebugLinesMesh, lines[i].from,
                        lines[i].to, lines[i].colour);
            }

            OpenGL_UpdateDynamicMesh(gameState->physicsDebugLinesMesh);
        }
#endif

        // Draw debug lines
        VertexColourShader shader = gameState->renderer.vertexColourShader;
        glUseProgram(shader.program);
        mat4 mvp = projection * view;
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        OpenGL_DrawDynamicMesh(gameState->debugLinesMesh);
        if (physicsSystem != NULL)
        {
            OpenGL_DrawDynamicMesh(gameState->physicsDebugLinesMesh);
        }

        for (u32 i = 0; i < axisCount; ++i)
        {
            mat4 axisMvp = projection * view * axisTransforms[i];
            glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, axisMvp.raw);
            OpenGL_DrawStaticMesh(gameState->renderer.axisMesh);
        }
    }

    {
        ParticleTextureShader shader =
                gameState->renderer.particleTextureShader;
        glUseProgram(shader.program);
        glBindTexture(GL_TEXTURE_2D, gameState->audioSourceIcon);
        glUniform1i(shader.colourTexture, 0);

        vec3 position = Vec3(5, 2, 0);
        vec3 forward = Normalize(position - cameraPosition);
        vec3 right = Normalize(Cross(forward, Vec3(0.0f, 1.0f, 0.0f)));
        if (IsZero(right))
        {
            right = Vec3(1, 0, 0);
        }
        vec3 up = Normalize(Cross(forward, right));
        mat4 rotation = Identity();
        rotation.col[0] = Vec4(right, 0.0f);
        rotation.col[1] = Vec4(up, 0.0f);
        rotation.col[2] = Vec4(forward, 0.0f);

        mat4 model = Translate(position) * rotation * Scale(Vec3(0.5f));
        mat4 mvp = projection * view * model;
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        OpenGL_DrawStaticMesh(gameState->renderer.quadMesh);
    }

    if (!gameState->useFreeRoamCamera)
    {
        // Draw cross hair
        vec4 crossHairColour = Vec4(1);
        vec4 crossHairShadowColour = Vec4(0, 0, 0, 1);
        vec2 p = Vec2((float)windowWidth, (float)windowHeight) * 0.5f;
        vec2 shadowOffset = Vec2(1);
        vec3 zero = Vec3(0);
        float w = 1.0f;
        float h = 8.0f;
        float q = 8.0f;
        renderer_DrawRect(&gameState->renderer, p + Vec2(0, q) + shadowOffset,
                          w, h, crossHairShadowColour, &orthoProjection, zero);
        renderer_DrawRect(&gameState->renderer, p - Vec2(0, q) + shadowOffset,
                          w, h, crossHairShadowColour, &orthoProjection, zero);
        renderer_DrawRect(&gameState->renderer, p + Vec2(q, 0) + shadowOffset,
                          h, w, crossHairShadowColour, &orthoProjection, zero);
        renderer_DrawRect(&gameState->renderer, p - Vec2(q, 0) + shadowOffset,
                          h, w, crossHairShadowColour, &orthoProjection, zero);

        renderer_DrawRect(&gameState->renderer, p + Vec2(0, q), w, h,
                          crossHairColour, &orthoProjection, zero);
        renderer_DrawRect(&gameState->renderer, p - Vec2(0, q), w, h,
                          crossHairColour, &orthoProjection, zero);
        renderer_DrawRect(&gameState->renderer, p + Vec2(q, 0), h, w,
                          crossHairColour, &orthoProjection, zero);
        renderer_DrawRect(&gameState->renderer, p - Vec2(q, 0), h, w,
                          crossHairColour, &orthoProjection, zero);

        if (gameState->hitMarkerTimeRemaining > 0.0f)
        {
            renderer_DrawTexturedRect(&gameState->renderer, p, 64.0f, 64.0f,
                                      gameState->hitMarkerTexture,
                                      &orthoProjection, zero);
        }

    }

    if (gameState->showInventory)
    {
        renderer_DrawRect(&gameState->renderer, Vec2(0), (float)windowWidth, (float)windowHeight,
                Vec4(0.8, 0.8, 0.8, 0.6), &orthoProjection);
        InventoryState *inventory = &gameState->playerInventoryStates[gameState->localPlayerIndex];
        for (u32 i = 0; i < inventory->count; ++i)
        {
            InventoryItem item = inventory->items[i];
            const char *name = "Unknown";
            if (item.type == InventoryItem_Wood)
                name = "Wood";
            else if (item.type == InventoryItem_Stone)
                name = "Stone";

            char buffer[80];
            snprintf(buffer, sizeof(buffer), "%s (%u)", name, item.quantity);
            b32 isHover = false;
            if (ui_Button(gameState, Vec2(20.0f, windowHeight - 300.0f + i * 30), buffer,
                  &gameState->testFont, &gameState->inputSystem, &orthoProjection,
                  &isHover))
            {
            }
        }

    }

    if (gameState->debugTextBufferLength > 0)
    {
        DrawString(gameState, Vec2(10, 20), gameState->debugTextBuffer,
                   &gameState->testFont, Vec4(1, 1, 1, 1), &orthoProjection);
    }

}

internal void HandleMainMenuState(GameState *gameState, GameMemory *memory, float dt, u32 windowWidth, u32 windowHeight)
{
    glViewport(0, 0, windowWidth, windowHeight);
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.9f, 0.9f, 0.9f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    float aspectRatio = (float)windowWidth / (float)windowHeight;
    mat4 projection =
            Orthographic(0, (float)windowWidth, 0, (float)windowHeight);

    renderer_DrawString(&gameState->renderer, Vec2(windowWidth * 0.5f, 50.0f),
                        "Shooter Prototype", &gameState->fontMontserratLarge,
                        Vec4(0.08, 0.08, 0.08, 1), &projection,
                        TEXT_ALIGN_CENTER);

    b32 isHover = false;
    if (ui_Button(gameState, Vec2(20.0f, windowHeight - 300.0f), "Host Game",
                  &gameState->testFont, &gameState->inputSystem, &projection,
                  &isHover))
    {
        PlaySound(&gameState->audioMixer, gameState->uiClickClip);
        memory->performPlatformOp(PlatformOp_StartServer);
        LOG_DEBUG("Host Game");
    }
    if (ui_Button(gameState, Vec2(20.0f, windowHeight - 240.0f),
                  "Join Local Game", &gameState->testFont,
                  &gameState->inputSystem, &projection, &isHover))
    {
        PlaySound(&gameState->audioMixer, gameState->uiClickClip);
        memory->performPlatformOp(PlatformOp_ConnectToServer);
        LOG_DEBUG("Join Local Game");
        gameState->state = ClientGameState_Connecting;
    }
    if (ui_Button(gameState, Vec2(20.0f, windowHeight - 100.0f), "Quit",
                  &gameState->testFont, &gameState->inputSystem, &projection,
                  &isHover))
    {
        PlaySound(&gameState->audioMixer, gameState->uiClickClip);
        memory->performPlatformOp(PlatformOp_Exit);
        LOG_DEBUG("Exit");
    }
    if (isHover && !gameState->isHoveringOverButton)
    {
        gameState->isHoveringOverButton = true;
        PlaySound(&gameState->audioMixer, gameState->uiRolloverClip);
    }
    else if (gameState->isHoveringOverButton && !isHover)
    {
        gameState->isHoveringOverButton = false;
    }
}

internal void HandleConnectingState(GameState *gameState, GameMemory *memory, float dt, u32 windowWidth, u32 windowHeight)
{
    u8 packetBuffer[400]; // TODO: Allocate packet based on negotiated bandwidth
    Bitstream packetBitstream = {};
    bitstream_InitForWriting(&packetBitstream, packetBuffer, sizeof(packetBuffer));

    PacketHeader packetHeader = {};
    packetHeader.timestamp = (u64)ToMicroseconds(memory->getTime());
    packetHeader.returnTimestamp = gameState->packetReturnTimestamp;
    packetHeader.sequenceNumber = gameState->packetSequenceNumber++;
    packetHeader.dataType = PacketDataType_ConnectionRequest;
    // Inflight packets have a lifetime of 1 second
    AddInFlightPacket(&gameState->ackSystem, packetHeader.sequenceNumber, 1.0f);
    GetAcksToSend(&gameState->ackSystem, packetHeader.acks, ArrayCount(packetHeader.acks));

    bitstream_WriteValue(&packetBitstream, &packetHeader);

    ConnectionRequestData connectionRequest = {};
    connectionRequest.version = 1;
    strcpy(connectionRequest.identifier, "test-client");
    bitstream_WriteValue(&packetBitstream, &connectionRequest);

    u32 packetLength = bitstream_GetLengthInBytes(&packetBitstream);
    NetSim_QueueOutgoingPacket(&gameState->netSim, packetBuffer, packetLength,
            (float)memory->getTime());
    gameState->outgoingPacketSize = SafeTruncateU32ToU16(packetLength);
    gameState->samplePacketsSent++;
    gameState->totalPacketsSent++;



    glViewport(0, 0, windowWidth, windowHeight);
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.9f, 0.9f, 0.9f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    float aspectRatio = (float)windowWidth / (float)windowHeight;
    mat4 projection = Orthographic(0, (float)windowWidth, 0, (float)windowHeight);

    DrawString(gameState, Vec2(windowWidth * 0.5f, windowHeight * 0.5f), "Connecting...", &gameState->testFont, Vec4(0.1, 0.1, 0.1, 1.0), &projection, TEXT_ALIGN_CENTER);
}

internal void HandleConnectedState(GameState *gameState, GameMemory *memory, float dt, u32 windowWidth, u32 windowHeight)
{
    InputSystem *inputSystem = &gameState->inputSystem;

    PlayerCommand playerCommand = {};
    if (!gameState->isConsoleVisible)
    {
        if (WasKeyPressed(inputSystem, K_F1))
        {
            gameState->useFreeRoamCamera = !gameState->useFreeRoamCamera;
            if (!gameState->useFreeRoamCamera)
            {
                memory->setMouseCursor(MOUSE_CURSOR_HIDDEN);
            }
        }

        if (gameState->useFreeRoamCamera)
        {
            UpdateCamera(&gameState->camera, inputSystem, dt, memory, windowWidth,
                    windowHeight);
        }
        else
        {
            if (WasKeyPressed(inputSystem, K_TAB))
            {
                gameState->showInventory = !gameState->showInventory;
                if (!gameState->showInventory)
                {
                    memory->setMouseCursor(MOUSE_CURSOR_HIDDEN);
                }
                else
                {
                    memory->setMouseCursor(MOUSE_CURSOR_VISIBLE);
                }
            }
            if (!gameState->showInventory)
            {
                playerCommand = CreatePlayerCommand(inputSystem, windowWidth, windowHeight);
            }
        }
    }

    u8 packetBuffer[400];
    Bitstream packetBitstream;
    bitstream_InitForWriting(&packetBitstream, packetBuffer, sizeof(packetBuffer));

    PacketHeader packetHeader = {};
    packetHeader.timestamp = (u64)ToMicroseconds(memory->getTime());
    packetHeader.returnTimestamp = gameState->packetReturnTimestamp;
    packetHeader.sequenceNumber = gameState->packetSequenceNumber++;
    packetHeader.dataType = PacketDataType_GameClientCommand;
    // Inflight packets have a lifetime of 1 second
    AddInFlightPacket(&gameState->ackSystem, packetHeader.sequenceNumber, 1.0f);
    GetAcksToSend(&gameState->ackSystem, packetHeader.acks, ArrayCount(packetHeader.acks));
    bitstream_WriteValue(&packetBitstream, &packetHeader);

    GameClientCommandData gameClientCommand = {};
    gameClientCommand.clientTick = gameState->currentTick;
    gameClientCommand.playerCommand = playerCommand;
    bitstream_WriteValue(&packetBitstream, &gameClientCommand);

    u32 packetLength = bitstream_GetLengthInBytes(&packetBitstream);
    NetSim_QueueOutgoingPacket(&gameState->netSim, packetBuffer, packetLength,
            (float)memory->getTime());
    gameState->outgoingPacketSize = SafeTruncateU32ToU16(packetLength);
    gameState->samplePacketsSent++;
    gameState->totalPacketsSent++;

    gameState->timeElapsed += dt;
    gameState->globalT += dt;
    gameState->currentTick++;

    //float fixedDt = 1.0f / 60.0f;
    //PhysicsUpdate(memory->clientPhysicsSystem, fixedDt);

    for (u32 i = 0; i < gameState->eventCount; ++i)
    {
        GameEvent event = gameState->events[i];
        switch (event.type)
        {
            case GameEvent_GunShot:
                {
                    vec3 position = event.gunShot.position;
                    LOG_DEBUG("gunshot: %g %g %g", position.x, position.y, position.z);
                    PlaySound(&gameState->audioMixer, gameState->gunShotClip);
                }
                break;
            case GameEvent_BulletImpact:
                Debug_DrawPoint(event.bulletImpact.position, Vec3(0, 1, 0), 0.2f, 10.0f);
                break;
            case GameEvent_HitMarker:
                if (event.hitMarker.owner == gameState->localPlayerIndex)
                {
                    gameState->hitMarkerTimeRemaining = 0.1f;
                    PlaySound(&gameState->audioMixer, gameState->hitMarkerClip);
                    LOG_DEBUG("Hit Marker!");
                }
                break;
            case GameEvent_AddItemToInventory:
                if (event.addItemToInventory.inventoryIndex == gameState->localPlayerIndex)
                {
                    //LOG_DEBUG("Client add item to inventory");
                    InventoryState *inventory = &gameState->playerInventoryStates[gameState->localPlayerIndex];
                    com_AddItemToInventory(inventory,
                            event.addItemToInventory.itemType, event.addItemToInventory.itemQuantity);
                }
            default:
                break;
        }
    }

    Debug_Printf(gameState, "Test Arena Memory Usage: %u/%u KB\n",
           (u32)(gameState->testArena.used / 1024),
           (u32)(gameState->testArena.size / 1024));
    Debug_Printf(gameState, "Asset Arena Memory Usage: %u/%u KB\n",
           (u32)(gameState->assetArena.used / 1024),
           (u32)(gameState->assetArena.size / 1024));
    PlayerController *player = gameState->players + gameState->localPlayerIndex;
    Debug_Printf(gameState, "Player Index: %u\n", gameState->localPlayerIndex);
    Debug_Printf(gameState, "Position: %g %g %g\n", player->position.x,
            player->position.y, player->position.z);
    Debug_Printf(gameState, "Velocity: %g %g %g\n", player->velocity.x,
            player->velocity.y, player->velocity.z);
    Debug_Printf(gameState, "FPS: %u (%g ms)\n", (u32)(1.0f / dt), dt * 1000.0f);
    double roundTripTime = (double)gameState->roundTripTime / 1000.0;
    double serverTimestep = 1.0 / 60;
    Debug_Printf(gameState, "RTT: %g ms\n", roundTripTime);
    Debug_Printf(gameState, "IN: %u OUT: %u\n", gameState->incomingPacketSize, gameState->outgoingPacketSize);
    Debug_Printf(gameState, "SERVER TICK: %u\n", gameState->currentServerTick);
    Debug_Printf(gameState, "CLIENT TICK: %u\n", gameState->currentTick);
    Debug_Printf(gameState, "LOCAL TIME: %g\n", gameState->currentLocalTime);
    Debug_Printf(gameState, "PACKETS DROPPED: %u\n", gameState->totalPacketsDropped);
    Debug_Printf(gameState, "PACKETS SENT: %u\n", gameState->totalPacketsSent);
    float playerHealth = gameState->playerHealth[gameState->localPlayerIndex];
    Debug_Printf(gameState, "HEALTH: %d\n", (int)playerHealth);

    gameState->sampleTimeRemaining -= dt;
    if (gameState->sampleTimeRemaining <= 0.0f)
    {
        gameState->packetLoss = (float)gameState->samplePacketsDropped /
            (float)gameState->samplePacketsSent;
        gameState->samplePacketsDropped = 0;
        gameState->samplePacketsSent = 0;
        gameState->sampleTimeRemaining = 1.0f;
    }
    Debug_Printf(gameState, "PACKET LOSS: %g\n", gameState->packetLoss);

    float currentServerTime = gameState->currentServerTick * (float)serverTimestep;

    Assert(gameState->playerCount < 4); // Was too lazy to allocate more animation controllers
    for ( u32 i = 0; i < gameState->playerCount; ++i)
    {
        PlayerController *playerController = gameState->players + i;
        Sk_AnimationController *animationController = gameState->playerAnimationControllers + i;
        UpdatePlayerAnimationController(animationController, currentServerTime, Length(playerController->velocity));
        // FIXME: Only draw server-side hit boxes for now
        //UpdateHitboxes(playerController, animationController, &gameState->testArena, Vec3(0.6, 0.75, 0.95));
    }

    if (gameState->hitMarkerTimeRemaining > 0.0f)
        gameState->hitMarkerTimeRemaining -= dt;

    //UpdateViewModelAnimationController(&gameState->playerViewModelAnimationController,
            //currentServerTime);

    PhysicsSystem *physicsSystemToDraw = memory->clientPhysicsSystem;
    if (memory->serverPhysicsSystem)
        physicsSystemToDraw = memory->serverPhysicsSystem;
#if ENABLE_RENDERER
    Render(gameState, physicsSystemToDraw, dt, windowWidth, windowHeight);
#endif
}

internal void HandleAnimationTestState(GameState *gameState, GameMemory *memory, float dt, u32 windowWidth, u32 windowHeight)
{
    InputSystem *inputSystem = &gameState->inputSystem;

    if (!gameState->isConsoleVisible)
    {
        gameState->useFreeRoamCamera = true;
        memory->setMouseCursor(MOUSE_CURSOR_HIDDEN);

        UpdateCamera(&gameState->camera, inputSystem, dt, memory, windowWidth,
                windowHeight);
    }

    gameState->timeElapsed += dt;
    gameState->globalT += dt;
    gameState->currentTick++;

    Debug_Printf(gameState, "Test Arena Memory Usage: %u/%u KB\n",
           (u32)(gameState->testArena.used / 1024),
           (u32)(gameState->testArena.size / 1024));
    Debug_Printf(gameState, "Asset Arena Memory Usage: %u/%u KB\n",
           (u32)(gameState->assetArena.used / 1024),
           (u32)(gameState->assetArena.size / 1024));
    Debug_Printf(gameState, "FPS: %u (%g ms)\n", (u32)(1.0f / dt), dt * 1000.0f);
    Debug_Printf(gameState, "LOCAL TIME: %g\n", gameState->currentLocalTime);

    Sk_UpdatePlaybackState(&gameState->testPlaybackState2, (float)gameState->currentLocalTime);
    Sk_UpdatePlaybackState(&gameState->vmAkPlaybackState, (float)gameState->currentLocalTime);

    glViewport(0, 0, windowWidth, windowHeight);
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.15f, 0.15f, 0.15f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    float aspectRatio = (float)windowWidth / (float)windowHeight;
    mat4 projection = Perspective(Radians(90.0f), aspectRatio, 0.1f, 500.0f);
    mat4 view = Identity();
    mat4 orthoProjection =
            Orthographic(0, (float)windowWidth, 0, (float)windowHeight);

    vec3 cameraPosition = {};
    vec3 cameraRotation = {};

    vec3 listenerUp = Vec3(0, 1, 0);
    vec3 listenerForward = {};
    if (gameState->useFreeRoamCamera)
    {
        view = RotateX(-gameState->camera.rotation.x) *
               RotateY(-gameState->camera.rotation.y) *
               Translate(-gameState->camera.position);
        cameraPosition = gameState->camera.position;
        cameraRotation = gameState->camera.rotation;
        listenerForward = Vec3(Sin(gameState->camera.rotation.y), 0.0f,
                               -Cos(gameState->camera.rotation.y));
    }

    alListener3f(AL_POSITION, cameraPosition.x, cameraPosition.y,
                 -cameraPosition.z);
    vec3 orientation[2] = {listenerForward, listenerUp};
    alListenerfv(AL_ORIENTATION, orientation[0].data);
    Assert(alGetError() == AL_NO_ERROR);

    glDisable(GL_DEPTH_TEST);
    {
        mat4 skyboxViewMatrix = RotateX(-cameraRotation.x) * RotateY(-cameraRotation.y);
        CubeMapShader shader = gameState->renderer.cubeMapShader;
        glUseProgram(shader.program);
        mat4 mvp = projection * skyboxViewMatrix;
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, gameState->skyboxTexture);
        glUniform1i(shader.cubeTexture, 0);
        glEnable(GL_CULL_FACE);
        glCullFace(GL_FRONT);
        OpenGL_DrawStaticMesh(gameState->renderer.cubeMesh);
        glDisable(GL_CULL_FACE);
    }

    glEnable(GL_DEPTH_TEST);
#if 0
    {
        // Draw test skeleton
        Sk_Skeleton *skeleton = &gameState->testSkeleton;
        mat4 *worldTransforms = AllocateArray(&gameState->testArena, mat4,
                                              skeleton->boneCount);
        Sk_CalculatePoseWorldTransform(worldTransforms,
                                       gameState->testPlaybackState.currentPose,
                                       skeleton, &gameState->assetArena);

        mat4 *skinningMatrices =
                AllocateArray(&gameState->testArena, mat4, skeleton->boneCount);
        for (u32 boneIdx = 0; boneIdx < skeleton->boneCount; ++boneIdx)
        {
            skinningMatrices[boneIdx] = worldTransforms[boneIdx] *
                                        gameState->testSkinnedMesh.invBindPose[boneIdx];
        }

        mat4 model = Identity();
        mat4 mvp = projection * view * model;
        SkinnedColourShader shader = gameState->renderer.skinnedColourShader;
        //ColourShader shader = gameState->renderer.colourShader;
        glUseProgram(shader.program);
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        glUniformMatrix4fv(shader.boneTransforms, skeleton->boneCount,
                GL_FALSE, skinningMatrices[0].raw);
        vec4 colour = Vec4(0.1, 0.1, 0.1, 1.0);
        glUniform4fv(shader.colour, 1, colour.data);
        OpenGL_DrawStaticMesh(gameState->testSkinnedMesh.mesh);

        for (u32 boneIdx = 0; boneIdx < skeleton->boneCount; ++boneIdx)
        {
            skinningMatrices[boneIdx] = worldTransforms[boneIdx] *
                                        gameState->testSkinnedMesh2.invBindPose[boneIdx];
        }

        colour = Vec4(0.4, 0.4, 0.4, 1.0);
        glUniform4fv(shader.colour, 1, colour.data);
        glUniformMatrix4fv(shader.boneTransforms, skeleton->boneCount,
                GL_FALSE, skinningMatrices[0].raw);
        OpenGL_DrawStaticMesh(gameState->testSkinnedMesh2.mesh);

        for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
        {
            Debug_DrawAxis(worldTransforms[bone]);

            u8 parent = skeleton->parents[bone];
            if (parent != INVALID_BONE)
            {
                vec3 childPosition = Vec3(worldTransforms[bone].col[3]);
                vec3 parentPosition = Vec3(worldTransforms[parent].col[3]);
                Debug_DrawLine(childPosition, parentPosition, Vec3(1, 0, 1));
            }
        }

        MemoryArenaFree(&gameState->testArena, worldTransforms);
    }
#endif

#if 0
    {
        // Draw test skeleton2
        Sk_Skeleton *skeleton = &gameState->testSkeleton2;
        mat4 *worldTransforms = AllocateArray(&gameState->testArena, mat4,
                                              skeleton->boneCount);
        Sk_CalculatePoseWorldTransform(worldTransforms,
                                       gameState->testPlaybackState2.currentPose,
                                       skeleton, &gameState->assetArena);

        mat4 *skinningMatrices =
                AllocateArray(&gameState->testArena, mat4, skeleton->boneCount);
        for (u32 boneIdx = 0; boneIdx < skeleton->boneCount; ++boneIdx)
        {
            skinningMatrices[boneIdx] = worldTransforms[boneIdx] *
                                        gameState->testSkinnedMesh3.invBindPose[boneIdx];
        }

        mat4 model = Identity();
        mat4 mvp = projection * view * model;
        SkinnedColourShader shader = gameState->renderer.skinnedColourShader;
        //ColourShader shader = gameState->renderer.colourShader;
        glUseProgram(shader.program);
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        glUniformMatrix4fv(shader.boneTransforms, skeleton->boneCount,
                GL_FALSE, skinningMatrices[0].raw);
        vec4 colour = Vec4(0.1, 0.1, 0.1, 1.0);
        glUniform4fv(shader.colour, 1, colour.data);
        OpenGL_DrawStaticMesh(gameState->testSkinnedMesh3.mesh);

        for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
        {
            //Debug_DrawAxis(worldTransforms[bone]);

            u8 parent = skeleton->parents[bone];
            if (parent != INVALID_BONE)
            {
                vec3 childPosition = Vec3(worldTransforms[bone].col[3]);
                vec3 parentPosition = Vec3(worldTransforms[parent].col[3]);
                Debug_DrawLine(childPosition, parentPosition, Vec3(1, 0, 1));
            }
        }

        MemoryArenaFree(&gameState->testArena, worldTransforms);
    }
#endif

#if 1
    {
        // Draw vmAk
        Sk_Skeleton *skeleton = &gameState->vmAkSkeleton;
        mat4 *worldTransforms = AllocateArray(&gameState->testArena, mat4,
                                              skeleton->boneCount);
        Sk_CalculatePoseWorldTransform(worldTransforms,
                                        //skeleton->bindPose,
                                       gameState->vmAkPlaybackState.currentPose,
                                       skeleton, &gameState->assetArena);

        mat4 *skinningMatrices =
                AllocateArray(&gameState->testArena, mat4, skeleton->boneCount);
        for (u32 boneIdx = 0; boneIdx < skeleton->boneCount; ++boneIdx)
        {
            skinningMatrices[boneIdx] = 
                    worldTransforms[boneIdx] *
                    gameState->vmAkHandsMesh.invBindPose[boneIdx];
        }

        mat4 model = Identity();//worldTransforms[1] * RotateZ(PI * 0.5f);
        mat4 mvp = projection * view * model;
        SkinnedColourShader shader = gameState->renderer.skinnedColourShader;
        //ColourShader shader = gameState->renderer.colourShader;
        glUseProgram(shader.program);
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        glUniformMatrix4fv(shader.boneTransforms, skeleton->boneCount,
                GL_FALSE, skinningMatrices[0].raw);
        vec4 colour = Vec4(0.4, 0.2, 0.1, 1.0);
        glUniform4fv(shader.colour, 1, colour.data);
        OpenGL_DrawStaticMesh(gameState->vmAkHandsMesh.mesh);

        for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
        {
            //Debug_DrawAxis(worldTransforms[bone]);

            u8 parent = skeleton->parents[bone];
            if (parent != INVALID_BONE)
            {
                vec3 childPosition = Vec3(worldTransforms[bone].col[3]);
                vec3 parentPosition = Vec3(worldTransforms[parent].col[3]);
                Debug_DrawLine(childPosition, parentPosition, Vec3(1, 0, 1));
            }
        }

        vec2 screenPositions[0xFF];
        ModelImportData *modelData = &gameState->viewModelData;
        Assert(modelData->nodeCount < ArrayCount(screenPositions));
        glDisable(GL_DEPTH_TEST);
        float textHeight = gameState->testFont.ascent -
            gameState->testFont.descent;
        for (u32 i = 0; i < modelData->nodeCount; ++i)
        {
            VertexColourShader vertexColourShader =
                gameState->renderer.vertexColourShader;
            glUseProgram(vertexColourShader.program);
            mat4 nodeModelTransform = CalculateNodeWorldTransform(
                    modelData->nodes, modelData->nodeCount, i);
            mat4 nodeMvp = projection * view * nodeModelTransform * Scale(Vec3(0.14f));
            glUniformMatrix4fv(vertexColourShader.mvp, 1, GL_FALSE,
                               nodeMvp.raw);
            OpenGL_DrawStaticMesh(gameState->renderer.axisMesh);

            vec4 v = nodeMvp * Vec4(0, 0, 0, 1);
            v = v * (1.0f / v.w);
            if (v.x >= -1.0f && v.x < 1.0f && v.y >= -1.0f && v.y < 1.0f &&
                v.z >= -1.0f && v.z < 1.0f)
            {
                vec2 p = CalculateScreenPosition(nodeMvp, (float)windowWidth,
                                                 (float)windowHeight);
                for (u32 j = 0; j < i; ++j)
                {
                    if (Length(p - screenPositions[j]) < 10.0f)
                        p.y += textHeight;
                }
                screenPositions[i] = p;

                char text[80];
                snprintf(text, sizeof(text), "[%u] %s", i, modelData->nodeNames[i]);
                float textWidth =
                        CalculateTextLength(&gameState->testFont, text);
                renderer_DrawRect(&gameState->renderer,
                                  p + Vec2(0, gameState->testFont.descent),
                                  textWidth, textHeight, Vec4(0, 0, 0, 0.4),
                                  &orthoProjection);
                DrawString(gameState, p, text, &gameState->testFont, Vec4(1),
                           &orthoProjection);
            }
        }
        glEnable(GL_DEPTH_TEST);

        ColourDiffuseShader colourShader = gameState->renderer.colourDiffuseShader;
        glUseProgram(colourShader.program);
        for (u32 i = 0; i < 6; ++i)
        {
            u32 index = gameState->vmAkNodes[i];
            Assert(index < skeleton->boneCount);
            Debug_DrawAxis(worldTransforms[index]);
            mvp = projection * view * worldTransforms[index];
            glUniformMatrix4fv(colourShader.mvp, 1, GL_FALSE, mvp.raw);

            float f = 0.1f * i + 0.1f;
            colour = Vec4(f, f, f, 1.0);
            glUniform4fv(colourShader.colour, 1, colour.data);
            OpenGL_DrawStaticMesh(gameState->vmAkMeshes[i]);
        }

        MemoryArenaFree(&gameState->testArena, worldTransforms);
    }
#endif

    {
        glDisable(GL_DEPTH_TEST);
        gameState->debugLinesMesh.numVertices = 0;
        mat4 axisTransforms[1024];
        u32 axisCount = Debug_Render(&gameState->debugSystem,
                                     &gameState->debugLinesMesh, axisTransforms,
                                     ArrayCount(axisTransforms));
        OpenGL_UpdateDynamicMesh(gameState->debugLinesMesh);

        // Draw debug lines
        VertexColourShader shader = gameState->renderer.vertexColourShader;
        glUseProgram(shader.program);
        mat4 mvp = projection * view;
        glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
        OpenGL_DrawDynamicMesh(gameState->debugLinesMesh);

        for (u32 i = 0; i < axisCount; ++i)
        {
            mat4 axisMvp = projection * view * axisTransforms[i];
            glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, axisMvp.raw);
            OpenGL_DrawStaticMesh(gameState->renderer.axisMesh);
        }
    }

    if (gameState->debugTextBufferLength > 0)
    {
        DrawString(gameState, Vec2(10, 20), gameState->debugTextBuffer,
                   &gameState->testFont, Vec4(1, 1, 1, 1), &orthoProjection);
    }
}

internal void HandlePacketReceivedEvent(GameState *gameState, GameMemory *memory, u8* packetData, u32 packetLength)
{
    gameState->incomingPacketSize = SafeTruncateU32ToU16(packetLength);
    PacketHeader *packetHeader = (PacketHeader*)packetData;

    u64 currentTime = (u64)ToMicroseconds(memory->getTime());
    gameState->roundTripTime = currentTime - packetHeader->returnTimestamp;

    if (packetHeader->sequenceNumber >= gameState->expectedPacketSequenceNumber)
    {
        if (OnReceivePacketFromRemote(&gameState->ackSystem, packetHeader->sequenceNumber))
        {
            for (u32 i = 0; i < NET_PACKET_MAX_ACKS; ++i)
            {
                u16 ack = packetHeader->acks[i];
                if (ack != 0)
                {
                    OnPacketReceivedByRemote(&gameState->ackSystem, ack); 
                }
            }

            if (packetHeader->dataType == PacketDataType_ConnectionResponse)
            {
                ConnectionResponseData *response = (ConnectionResponseData*)(packetHeader + 1);
                LOG_DEBUG("Received connection response %u", response->response);
                if (response->response == ConnectionResponse_Ok)
                {
                    LOG_DEBUG("Successfully connected to server");
                    gameState->state = ClientGameState_Connected;
                }
                else
                {
                    gameState->state = ClientGameState_MainMenu;
                }
            }
            else if (packetHeader->dataType == PacketDataType_GameSnapshot)
            {
                GameSnapshotData *gameSnapshot = (GameSnapshotData*)(packetHeader + 1);
                gameState->currentServerTick = gameSnapshot->currentTick;
                gameState->playerCount = gameSnapshot->playerCount;
                for (u32 i = 0; i < gameSnapshot->playerCount; ++i)
                {
                    gameState->players[i].position = gameSnapshot->playerPositions[i];
                    gameState->players[i].viewAngles = gameSnapshot->playerViewAngles[i];
                    gameState->players[i].velocity = gameSnapshot->playerVelocities[i];
                    gameState->playerHealth[i] = gameSnapshot->playerHealth[i];
                }
                gameState->localPlayerIndex = gameSnapshot->playerIndex;

                for (u32 i = 0; i < gameSnapshot->eventCount; ++i)
                {
                    Assert(gameState->eventCount < ArrayCount(gameState->events));
                    gameState->events[gameState->eventCount++] = gameSnapshot->events[i];
                }
            }
            else
            {
                LOG_DEBUG("Unknown packet header data type %u", packetHeader->dataType);
            }
        }
    }
    else
    {
        LOG_DEBUG("Dropped out of order packet %u", packetHeader->sequenceNumber);
    }
}

extern "C" GAME_UPDATE(GameUpdate)
{
    Assert(sizeof(GameState) < memory->persistentStorageSize);
    GameState *gameState = (GameState *)memory->persistentStorageBase;

    if (!memory->isInitialized)
    {
        InitializeOpenGL();
        InitializeOpenAL();
        InitializeGame(gameState, memory);
        AudioInit(&gameState->audioMixer);
        ReloadContent(gameState, memory);
        memory->isInitialized = true;
    }
    else if (memory->wasCodeReloaded)
    {
        double startTime = memory->getTime();
        InitializeOpenGL();
        InitializeOpenAL();
        AudioInit(&gameState->audioMixer);
        ReloadContent(gameState, memory);
        LOG_INFO("Reload took %g seconds", memory->getTime() - startTime);

        console_Puts(&gameState->console, "Game code reloaded!\n");
    }

    gDebugSystem = &gameState->debugSystem;
    gDebugMemory = memory;
    gDebugGameState = gameState;
    //float totalPacketDelay = 0.2f * Sin(2.0f * (float)memory->getTime()) + 0.02f;
    float totalPacketDelay = 0.03f * (0.5f * RandomBinomial(&gameState->rng) + 0.5f);
    //gameState->netSim.outgoingPacketDelay = totalPacketDelay * 0.5f;
    //gameState->netSim.incomingPacketDelay = totalPacketDelay * 0.5f;
    //gameState->netSim.packetLoss = Sin(0.2f * (float)memory->getTime()) > 0.5f ? 0.5f : 0.0f;

    gameState->currentLocalTime += dt;
    gameState->eventCount = 0;

    InputSystem *inputSystem = &gameState->inputSystem;
    for (u32 i = 0; i < MAX_KEYS; ++i)
    {
        inputSystem->prevKeyStates[i] = inputSystem->currentKeyStates[i];
    }
    inputSystem->prevKeyInput = inputSystem->currentKeyInput;
    inputSystem->prevMouseX = inputSystem->mouseX;
    inputSystem->prevMouseY = inputSystem->mouseY;

    for (u32 i = 0; i < count; ++i)
    {
        switch (events[i].type)
        {
        case PlatformEvent_KeyPress:
        {
            inputSystem->currentKeyStates[events[i].key] = 1;
            u32 input = inputSystem->keyBindings[events[i].key];
            inputSystem->currentKeyInput |= input;
            break;
        }
        case PlatformEvent_KeyRelease:
        {
            inputSystem->currentKeyStates[events[i].key] = 0;
            u32 input = inputSystem->keyBindings[events[i].key];
            inputSystem->currentKeyInput &= ~input;
            break;
        }
        case PlatformEvent_MouseMotion:
        {
            inputSystem->mouseX = events[i].mouseMotion.x;
            inputSystem->mouseY = events[i].mouseMotion.y;
            break;
        }
        case PlatformEvent_PacketReceived:
        {
            NetSim_QueueIncomingPacket(&gameState->netSim, events[i].packetReceived.packetData,
                    events[i].packetReceived.packetLength, (float)memory->getTime());
            break;
        }
        case PlatformEvent_Character:
        {
            if (gameState->isConsoleVisible)
            {
                TextInputProcessEvent(&gameState->console.textInput,
                                      TextInputEvent_AddCharacter,
                                      events[i].character);
            }

            break;
        }
        case PlatformEvent_StartServerArg:
        {
            memory->performPlatformOp(PlatformOp_StartServer);
            LOG_DEBUG("Host Game");
            break;
        }
        case PlatformEvent_ConnectToArg:
        {
            if (gameState->state == ClientGameState_MainMenu)
            {
                memory->performPlatformOp(PlatformOp_ConnectToServer);
                LOG_DEBUG("Join Local Game");
                gameState->state = ClientGameState_Connecting;
            }
            break;
        }
        default:
            break;
        }
    }

    u8 packetBuffer[400]; // TODO: Constant for max packet size
    for (u32 incomingPacketCount = 0; incomingPacketCount < 64; ++incomingPacketCount)
    {
        u32 incomingPacketSize = NetSim_GetIncomingPacket(&gameState->netSim,
                (float)memory->getTime(), packetBuffer, sizeof(packetBuffer));
        if (incomingPacketSize > 0)
        {
            HandlePacketReceivedEvent(gameState, memory, packetBuffer, incomingPacketSize);
            //LOG_DEBUG("Received packet from server %u containing %u bytes of data.",
                    //events[i].packetReceived.addressId, events[i].packetReceived.packetLength);
        }
        else
        {
            break;
        }
    }

    if (WasKeyPressed(inputSystem, K_GRAVE_ACCENT))
    {
        gameState->isConsoleVisible = !gameState->isConsoleVisible;
    }
    if (gameState->isConsoleVisible)
    {
        // TODO: Handle key repeats
        if (WasKeyPressed(inputSystem, K_BACKSPACE))
        {
            TextInputProcessEvent(&gameState->console.textInput, TextInputEvent_Backspace);
        }

        if (WasKeyPressed(inputSystem, K_ENTER))
        {
            cmd_Exec(&gameState->cmdSystem, gameState->console.inputBuffer);
            char buffer[console_InputBufferLength + 4];
            snprintf(buffer, sizeof(buffer), "> %s\n", gameState->console.inputBuffer);
            console_Puts(&gameState->console, buffer);
            ClearToZero(gameState->console.inputBuffer, ArrayCount(gameState->console.inputBuffer));
            gameState->console.textInput.length = 0;
            gameState->console.textInput.cursor = 0;
            //console_Execute(&gameState->console);
        }
    }

    u16 droppedPackets[NET_MAX_INFLIGHT_PACKETS];
    u32 droppedPacketsCount = DropUnacknowledgedPackets(&gameState->ackSystem, droppedPackets,
            ArrayCount(droppedPackets), dt);
    gameState->samplePacketsDropped += droppedPacketsCount;
    gameState->totalPacketsDropped += droppedPacketsCount;
    // TODO: Generate dropped packet event

    gameState->renderer.textBuffer.mesh.numVertices = 0;
    gameState->debugTextBufferLength = 0;

    Debug_Cleanup(&gameState->debugSystem, dt);

    switch (gameState->state)
    {
        case ClientGameState_MainMenu:
            HandleMainMenuState(gameState, memory, dt, windowWidth, windowHeight);
            break;
        case ClientGameState_Connected:
            HandleConnectedState(gameState, memory, dt, windowWidth, windowHeight);
            break;
        case ClientGameState_Connecting:
            HandleConnectingState(gameState, memory, dt, windowWidth, windowHeight);
            break;
        case ClientGameState_AnimationTest:
            HandleAnimationTestState(gameState, memory, dt, windowWidth, windowHeight);
            break;
        default:
            break;
    }

    if (gameState->isConsoleVisible)
    {
        mat4 orthoProjection =
                Orthographic(0, (float)windowWidth, 0, (float)windowHeight);
        console_Draw(&gameState->console, &gameState->renderer,
                     &gameState->testFont, &orthoProjection, windowWidth,
                     windowHeight, (float)gameState->currentLocalTime);
    }

    for (u32 outgoingPacketCount = 0; outgoingPacketCount < 8; ++outgoingPacketCount)
    {
        u32 outgoingPacketSize = NetSim_GetOutgoingPacket(&gameState->netSim,
                (float)memory->getTime(), packetBuffer, sizeof(packetBuffer));
        if (outgoingPacketSize > 0)
        {
            memory->sendPacketToServer(packetBuffer, outgoingPacketSize);
        }
        else
        {
            break;
        }
    }
}
