#ifdef _MSC_VER
#include <ws2tcpip.h>

#else
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>

#define INVALID_SOCKET -1
#define closesocket(SOCKET) close(SOCKET)
#define SOCKET int

#endif

#define PORT_NUMBER 18000

global SOCKET serverSocket = INVALID_SOCKET;
global SOCKET clientSocket = INVALID_SOCKET;
global struct addrinfo *serverAddress;

internal bool sock_StartServer()
{
  bool result = false;
  struct sockaddr_in address;
  uint16_t portNumber = PORT_NUMBER;

  ZeroStruct( address );

  serverSocket = socket( AF_INET, SOCK_DGRAM, 0 );
  if ( serverSocket != INVALID_SOCKET )
  {
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = htonl( INADDR_ANY );
    address.sin_port = htons( portNumber );
    if ( bind( serverSocket, (struct sockaddr *)&address,
          sizeof( address ) ) == 0 )
    {
      result = true;
    }
    else
    {
      LOG_ERROR( "Failed to bind server socket: %s",
              strerror(errno));
    }
  }
  else
  {
    LOG_ERROR( "Failed to open server socket" );
  }

  return result;
}

internal bool sock_ConnectToServer( const char *address, const char *port )
{
  bool result = false;
  struct addrinfo hints;
  ZeroStruct( hints );

  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_protocol = IPPROTO_UDP;
  hints.ai_flags = 0;

  ZeroStruct( serverAddress );
  int addrInfoResult = getaddrinfo( address, port, &hints, &serverAddress );
  if ( addrInfoResult == 0 )
  {
    for ( auto p = serverAddress; p != NULL; p = p->ai_next )
    {
      LOG_DEBUG( "ai_family: %s", p->ai_family == AF_INET ? "AF_INET" : "UNKNOWN" );
      LOG_DEBUG( "ai_socktype: %s", p->ai_socktype == SOCK_DGRAM ? "SOCK_DGRAM" : "SOCK_STREAM" );
      LOG_DEBUG( "ai_protocol: %s", p->ai_protocol == IPPROTO_UDP ? "IPPROTO_UDP" : "UNKNOWN" );
      LOG_DEBUG( "ai_flags: %u", p->ai_flags );
      LOG_DEBUG( "ai_addrlen: %u", p->ai_addrlen );
    }
    //ASSERT( serverAddress->ai_next == NULL );
    clientSocket = socket( serverAddress->ai_family,
                           serverAddress->ai_socktype,
                           serverAddress->ai_protocol );
    if ( clientSocket != INVALID_SOCKET )
    {
        result = true;
    }
    else
    {
      LOG_ERROR( "Failed to open client socket" );
    }
  }
  else
  {
    LOG_ERROR( "getaddrinfo failed: %d", addrInfoResult );
  }

  return result;
}

internal void sock_ShutdownClient()
{
  if ( clientSocket != INVALID_SOCKET )
  {
    closesocket( clientSocket );
    clientSocket = INVALID_SOCKET;
  }
  if ( serverAddress != NULL )
  {
    freeaddrinfo( serverAddress );
    serverAddress = NULL;
  }
}

internal void sock_ShutdownServer()
{
  if ( serverSocket != INVALID_SOCKET )
  {
    closesocket( serverSocket );
    serverSocket = INVALID_SOCKET;
  }
}

internal void sock_CleanupSockets()
{
  sock_ShutdownClient();
  sock_ShutdownServer();
}

inline bool sock_CompareAddresses( struct sockaddr_storage *a,
                                    struct sockaddr_storage *b )
{
  if ( ( a->ss_family == b->ss_family ) )
  {
    if ( a->ss_family == AF_INET )
    {
      auto a4 = (sockaddr_in *)a;
      auto b4 = (sockaddr_in *)b;
      return ( ( a4->sin_addr.s_addr == b4->sin_addr.s_addr ) &&
               ( a4->sin_port == b4->sin_port ) );
    }
    else if ( a->ss_family == AF_INET6 )
    {
      auto a6 = (sockaddr_in6 *)a;
      auto b6 = (sockaddr_in6 *)b;
      auto addr1 = (uint32_t *)a6->sin6_addr.s6_addr;
      auto addr2 = (uint32_t *)b6->sin6_addr.s6_addr;
      return ( ( addr1[0] == addr2[0] ) && ( addr1[1] == addr2[1] ) &&
               ( addr1[2] == addr2[2] ) && ( addr1[3] == addr2[3] ) &&
               ( a6->sin6_port == b6->sin6_port ) );
    }
    else
    {
      LOG_WARN( "Unknown address family" );
    }
  }
  return false;
}

internal int NonBlockingReceiveFrom(int socket, void *data, u32 length, struct sockaddr *receiveAddress, u32 *receiveAddressLength)
{
    Assert(socket != INVALID_SOCKET);

    fd_set fds;
    struct timeval tv;
    FD_ZERO(&fds);
    FD_SET(socket, &fds);

    tv.tv_sec = 0;
    tv.tv_usec = 0;

    int ret = select(socket + 1, &fds, NULL, NULL, &tv);
    if (ret == -1)
    {
        LOG_ERROR("select error: %s", strerror(errno));
        return -1;
    }
    else if (ret == 0)
    {
        return 0; // no data available
    }
    else
    {
#if PLATFORM_WINDOWS
        int dataAvailable = recvfrom(socket, (char*)data, length, 0, receiveAddress,
                                     (int*)receiveAddressLength);
#else
        int dataAvailable = recvfrom(socket, (char*)data, length, 0, receiveAddress,
                                     receiveAddressLength);
#endif

        // We could handle all of the various error codes for recvfrom here to reduce code duplication.

        return dataAvailable;
    }
#if 0
    Platform_ServerReceivePacketEvent event = {};
    int ret = recvfrom( serverSocket, (char*)event.data, sizeof(event.data), 0,
                        (sockaddr *)&receivedAddress, &receivedAddressLength );
    if ( ret > 0 )
    {
      auto address = (struct sockaddr_in *)&receivedAddress;
      event.addressIpv4 = address->sin_addr.s_addr;
      event.port = address->sin_port;
      event.length = (uint32_t)ret;
      event.time = glfwGetTime();
      evt_Push( &platformEventQueue, &event, Platform_ServerReceivePacketEvent );
      return true;
    }
    else
    {
      return false;
    }
#endif
}

#if 0
internal bool sock_PollClient()
{
  if ( clientSocket == INVALID_SOCKET )
  {
    return false;
  }

  fd_set fds;
  struct timeval tv;
  FD_ZERO(&fds);
  FD_SET(clientSocket, &fds);

  tv.tv_sec = 0;
  tv.tv_usec = 0;

  int ret = select(clientSocket+1, &fds, NULL, NULL, &tv);
  if ( ret == -1)
  {
    LOG_ERROR( "select error: %s", strerror(errno));
    return false;
  }
  else if ( ret == 0 )
  {
    return false; // no data available
  }
  else
  {
    Platform_ClientReceivePacketEvent event = {};
    int ret = recv( clientSocket, ( char* )event.data, sizeof(event.data), 0 );
    if ( ret > 0 )
    {
      event.length = (uint32_t)ret;
      event.time = glfwGetTime();
      evt_Push( &platformEventQueue, &event, Platform_ClientReceivePacketEvent );
      return true;
    }
    else
    {
      return false;
    }
  }
}
#endif

internal void sock_ServerSendPacket( u8 *data, u32 length, struct sockaddr_storage *address)
{
  if ( serverSocket != INVALID_SOCKET )
  {
    int ret = sendto( serverSocket, (char*)data, (int)length, 0, (struct sockaddr *)address,
        sizeof( *address ) );
    if ( ret != (int)length )
    {
      LOG_WARN( "Server failed to send packet of length %d to client. ret = %d", length, ret );
    }
  }
}

internal void sock_ClientSendPacket( uint8_t *data, int len )
{
  if ( clientSocket != INVALID_SOCKET )
  {
    int ret = sendto( clientSocket, (char*)data, len, 0, serverAddress->ai_addr,
        (int)serverAddress->ai_addrlen );
    if ( ret != len )
    {
      LOG_WARN( "Client failed to send packet of length %d to server. ret = %d", len, ret );
    }
  }
}

internal void sock_Initialize()
{
#ifdef _MSC_VER
  WSADATA wsaData;
  int wsaResult = WSAStartup( MAKEWORD( 2, 2 ), &wsaData );
  Assert( wsaResult == 0 );
#endif
}

internal void sock_Shutdown()
{
#ifdef _MSC_VER
  WSACleanup();
#endif
}
