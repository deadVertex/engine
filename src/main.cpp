#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <cstdio>
#include <dlfcn.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>

#include "game.h"
#include "logging.h"
#include "socket_layer.cpp"
#include "bullet_physics.cpp"

#define GAME_LIB "./game.so"

u32 log_activeChannels = -1;

struct GameCode
{
    void *handle;
    GameUpdateFunction *update;
    GameServerUpdateFunction *serverUpdate;

    bool isValid;
    time_t lastWriteTime;
};

internal time_t GetFileLastWriteTime(const char *path)
{
    struct stat attr;
    stat(path, &attr);
    return attr.st_mtime;
}

internal GameCode LoadGameCode(const char *libraryName)
{
    GameCode result = {};
    result.lastWriteTime = GetFileLastWriteTime(libraryName);
    result.handle = dlopen(libraryName, RTLD_NOW);
    if (result.handle)
    {
        auto error = dlerror();
        if (error)
        {
            fprintf(stderr, "Open: %s\n", error);
        }
        result.update =
                (GameUpdateFunction *)dlsym(result.handle, "GameUpdate");
        result.serverUpdate = (GameServerUpdateFunction*)
            dlsym(result.handle, "GameServerUpdate");
        error = dlerror();
        if (error)
        {
            fprintf(stderr, "GameUpdate: %s\n", error);
        }

        result.isValid = result.update && result.serverUpdate;
    }
    else
    {
        fprintf(stderr, "%s\n", dlerror());
    }

    return result;
}

internal void UnloadGameCode(GameCode *gameCode)
{
    if (gameCode->handle)
    {
        dlclose(gameCode->handle);
        gameCode->handle = 0;
        auto error = dlerror();
        if (error)
        {
            fprintf(stderr, "Error while closing game code library: %s\n",
                    error);
        }
    }
    gameCode->isValid = false;
    gameCode->update = NULL;
}

internal void *AllocateMemory(size_t numBytes, size_t baseAddress = 0)
{
    void *result = mmap((void *)baseAddress, numBytes, PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (result == MAP_FAILED)
    {
        return NULL;
    }
    return result;
}

void FreeMemory(void *p) { munmap(p, 0); }

// NOTE: bytesToRead must equal the size of the file, if great we will enter an
// infinite loop.
internal bool ReadFile(int file, void *buf, int bytesToRead)
{
    while (bytesToRead)
    {
        int bytesRead = read(file, buf, bytesToRead);
        if (bytesRead == -1)
        {
            return false;
        }
        bytesToRead -= bytesRead;
        buf = (u8 *)buf + bytesRead;
    }
    return true;
}

internal DEBUG_READ_ENTIRE_FILE(DebugReadEntireFile)
{
    ReadFileResult result = {};
    int file = open(path, O_RDONLY);
    if (file != -1)
    {
        struct stat fileStatus;
        if (fstat(file, &fileStatus) != -1)
        {
            result.size = SafeTruncateU64ToU32(fileStatus.st_size);
            // NOTE: Size + 1 to allow for null to be written if needed.
            result.memory = AllocateMemory(result.size + 1);
            if (result.memory)
            {
                if (!ReadFile(file, result.memory, result.size))
                {
                    FreeMemory(result.memory);
                    result.memory = nullptr;
                    result.size = 0;
                }
            }
            else
            {
                result.size = 0;
            }
        }
        close(file);
    }
    return result;
}

internal DEBUG_FREE_FILE_MEMORY(DebugFreeFileMemory) { FreeMemory(memory); }

internal PlatformEvent platformEvents[1024];
internal u32 platformEventCount;

#define KEY_HELPER(NAME)                                                       \
    case GLFW_KEY_##NAME:                                                      \
        return K_##NAME;
internal u8 ConvertKey(int key)
{
    if (key >= GLFW_KEY_SPACE && key <= GLFW_KEY_GRAVE_ACCENT)
    {
        return (u8)key;
    }
    switch (key)
    {
        KEY_HELPER(BACKSPACE);
        KEY_HELPER(TAB);
        KEY_HELPER(INSERT);
        KEY_HELPER(HOME);
        KEY_HELPER(PAGE_UP);
    // Can't use KEY_HELPER( DELETE ) as windows has a #define for DELETE
    case GLFW_KEY_DELETE:
        return K_DELETE;
        KEY_HELPER(END);
        KEY_HELPER(PAGE_DOWN);
        KEY_HELPER(ENTER);

        KEY_HELPER(LEFT_SHIFT);
    case GLFW_KEY_LEFT_CONTROL:
        return K_LEFT_CTRL;
        KEY_HELPER(LEFT_ALT);
        KEY_HELPER(RIGHT_SHIFT);
    case GLFW_KEY_RIGHT_CONTROL:
        return K_RIGHT_CTRL;
        KEY_HELPER(RIGHT_ALT);

        KEY_HELPER(LEFT);
        KEY_HELPER(RIGHT);
        KEY_HELPER(UP);
        KEY_HELPER(DOWN);

        KEY_HELPER(ESCAPE);

        KEY_HELPER(F1);
        KEY_HELPER(F2);
        KEY_HELPER(F3);
        KEY_HELPER(F4);
        KEY_HELPER(F5);
        KEY_HELPER(F6);
        KEY_HELPER(F7);
        KEY_HELPER(F8);
        KEY_HELPER(F9);
        KEY_HELPER(F10);
        KEY_HELPER(F11);
        KEY_HELPER(F12);
    case GLFW_KEY_KP_0:
        return K_NUM0;
    case GLFW_KEY_KP_1:
        return K_NUM1;
    case GLFW_KEY_KP_2:
        return K_NUM2;
    case GLFW_KEY_KP_3:
        return K_NUM3;
    case GLFW_KEY_KP_4:
        return K_NUM4;
    case GLFW_KEY_KP_5:
        return K_NUM5;
    case GLFW_KEY_KP_6:
        return K_NUM6;
    case GLFW_KEY_KP_7:
        return K_NUM7;
    case GLFW_KEY_KP_8:
        return K_NUM8;
    case GLFW_KEY_KP_9:
        return K_NUM9;
    case GLFW_KEY_KP_DECIMAL:
        return K_NUM_DECIMAL;
    case GLFW_KEY_KP_DIVIDE:
        return K_NUM_DIVIDE;
    case GLFW_KEY_KP_MULTIPLY:
        return K_NUM_MULTIPLY;
    case GLFW_KEY_KP_SUBTRACT:
        return K_NUM_MINUS;
    case GLFW_KEY_KP_ADD:
        return K_NUM_PLUS;
    case GLFW_KEY_KP_ENTER:
        return K_NUM_ENTER;
    }
    return K_UNKNOWN;
}

internal void KeyCallback(GLFWwindow *window, int key, int scancode, int action,
                          int mods)
{
    PlatformEvent event = {};
    event.key = ConvertKey(key);
    if (action == GLFW_RELEASE)
        event.type = PlatformEvent_KeyRelease;
    else if (action == GLFW_PRESS)
        event.type = PlatformEvent_KeyPress;

    if (platformEventCount < ArrayCount(platformEvents))
    {
        platformEvents[platformEventCount++] = event;
    }
}

internal u8 ConvertMouseButton(int button)
{
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT:
        return K_MOUSE_BUTTON_LEFT;
    case GLFW_MOUSE_BUTTON_MIDDLE:
        return K_MOUSE_BUTTON_MIDDLE;
    case GLFW_MOUSE_BUTTON_RIGHT:
        return K_MOUSE_BUTTON_RIGHT;
    }
    return K_UNKNOWN;
}

internal void MouseButtonCallback(GLFWwindow *window, int button, int action,
                                  int mods)
{
    PlatformEvent event = {};
    event.key = ConvertMouseButton(button);
    event.type = (action == GLFW_PRESS) ? PlatformEvent_KeyPress
                                        : PlatformEvent_KeyRelease;
    if (platformEventCount < ArrayCount(platformEvents))
    {
        platformEvents[platformEventCount++] = event;
    }

    Unused(window);
    Unused(mods);
}

internal void CharacterCallback(GLFWwindow *window, u32 codepoint)
{
    PlatformEvent event = {};
    event.type = PlatformEvent_Character;
    event.character = codepoint;
    if (platformEventCount < ArrayCount(platformEvents))
    {
        platformEvents[platformEventCount++] = event;
    }
}

global u32 windowWidth = 1024;
global u32 windowHeight = 768;
internal void WindowResizeCallback( GLFWwindow *window, int width, int height )
{
    windowWidth = width;
    windowHeight = height;
}

global bool gWindowHasFocus = true;
internal void WindowFocusCallback(GLFWwindow *window, int focused)
{
    gWindowHasFocus = focused == GLFW_TRUE;
}

global GLFWwindow *window;
internal void SetMouseCursor(u8 cursor)
{
    if (cursor == MOUSE_CURSOR_HIDDEN)
    {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
    else if (cursor == MOUSE_CURSOR_VISIBLE)
    {
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
}

internal SEND_PACKET_TO_SERVER(SendPacketToServer)
{
    Assert(length > 0);
    sock_ClientSendPacket(data, (int)length);
}

global struct sockaddr_storage clientAddresses[1024];
global u32 clientAddressCount = 1; // Skip 0th index to reserve it for invalid address id

internal SEND_PACKET_TO_CLIENT(SendPacketToClient)
{
    Assert(length > 0 && clientId > 0);
    Assert(clientId < clientAddressCount);
    sock_ServerSendPacket(data, length, &clientAddresses[clientId]);
}

internal DEBUG_GET_TIME(DebugGetTime)
{
    return glfwGetTime();
}

global GameMemory gameMemory;
global GameMemory gameServerMemory;
global PhysicsSystem serverPhysicsSystem;
global bool gIsServerRunning = false;
global bool gIsClientRunning = false;
internal void StartServer()
{
    if (!gIsServerRunning)
    {
        sock_StartServer();
        LOG_DEBUG("Started listen server");

        gameServerMemory.persistentStorageSize = 16 * 1024 * 1024;
        gameServerMemory.persistentStorageBase =
            AllocateMemory(gameServerMemory.persistentStorageSize, 0);
        gameServerMemory.readEntireFile = &DebugReadEntireFile;
        gameServerMemory.freeFileMemory = &DebugFreeFileMemory;
        gameServerMemory.sendPacketToClient = &SendPacketToClient;

        serverPhysicsSystem = CreatePhysicsSystem();
        gameServerMemory.serverPhysicsSystem = &serverPhysicsSystem;


        gameMemory.serverPhysicsSystem = &serverPhysicsSystem;
        gIsServerRunning = true;
    }
}

internal void StartClient()
{
    if (!gIsClientRunning)
    {
        if (sock_ConnectToServer("localhost", "18000"))
        {
            LOG_DEBUG("Successfully opened UDP socket to server '%s'", "localhost");
        }
        gIsClientRunning = true;
    }
}

global bool gIsRunning = true;
internal PERFORM_PLATFORM_OP(PerformPlatformOp)
{
    switch (op)
    {
        case PlatformOp_StartServer:
            StartServer();
            break;
        case PlatformOp_ConnectToServer:
            StartClient();
            break;
        case PlatformOp_Exit:
            gIsRunning = false;
            break;
        default:
            break;
    }
}

int main(int argc, char **argv)
{
    const rlim_t stackSize = 16 * 1024 * 1024;
    struct rlimit r1;
    if (getrlimit(RLIMIT_STACK, &r1) == 0)
    {
        if (r1.rlim_cur < stackSize)
        {
            r1.rlim_cur = stackSize;
            int stackResult = setrlimit(RLIMIT_STACK, &r1);
            if (stackResult != 0)
            {
                printf("setrlimit failed, returned %d", stackResult);
                return -1;
            }
        }
    }

    PlatformEvent initEvents[64];
    u32 initEventCount = 0;
    for (int i = 1; i < argc; ++i)
    {
        if (strcmp(argv[i], "--listen") == 0)
        {
            PlatformEvent event = {};
            event.type = PlatformEvent_StartServerArg;
            if (initEventCount < ArrayCount(initEvents))
            {
                initEvents[initEventCount++] = event;
            }
        }
        else if (strcmp(argv[i], "--connect") == 0)
        {
            PlatformEvent event = {};
            event.type = PlatformEvent_ConnectToArg;
            if (initEventCount < ArrayCount(initEvents))
            {
                initEvents[initEventCount++] = event;
            }
        }
    }

    const char *connectToAddress = "localhost";

    if (glfwInit() != GLFW_TRUE)
    {
        printf("Failed to initialize GLFW.\n");
        return -1;
    }
    glfwWindowHint(GLFW_DECORATED, false);
    glfwWindowHint(GLFW_RESIZABLE, false);

    window = glfwCreateWindow(1024, 768, "Engine", NULL, NULL);

    if (window == NULL)
    {
        printf("Failed to create window.\n");
        glfwTerminate();
        return -1;
    }
    glfwSetMouseButtonCallback(window, MouseButtonCallback);
    glfwSetKeyCallback(window, KeyCallback);
    glfwSetFramebufferSizeCallback( window, WindowResizeCallback );
    glfwSetWindowFocusCallback(window, WindowFocusCallback);
    glfwSetCharCallback(window, CharacterCallback);

    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);

    GameCode gameCode = LoadGameCode(GAME_LIB);
    if (!gameCode.isValid)
    {
        fprintf(stderr, "Failed to load game code.\n");
        glfwTerminate();
        return -1;
    }

    //PhysicsSystem physicsSystem = CreatePhysicsSystem();

    gameMemory.persistentStorageSize = 16 * 1024 * 1024;
    gameMemory.persistentStorageBase =
            AllocateMemory(gameMemory.persistentStorageSize, 0);
    gameMemory.readEntireFile = &DebugReadEntireFile;
    gameMemory.freeFileMemory = &DebugFreeFileMemory;
    gameMemory.setMouseCursor = &SetMouseCursor;
    gameMemory.getTime = &DebugGetTime;
    gameMemory.sendPacketToServer = &SendPacketToServer;
    gameMemory.performPlatformOp = &PerformPlatformOp;
    //gameMemory.clientPhysicsSystem = &physicsSystem;
    gameMemory.serverPhysicsSystem = NULL;

    i32 serverUpdateRate = 60;
    double serverFixedTimestep = 1.0 / serverUpdateRate;
    double timeOfLastServerUpdate = 0;
    i32 frameRateCap = 301;
    double minFrameTime = 1.0 / (double)frameRateCap;
    double maxFrameTime = 0.25;
    double currentTime = glfwGetTime();
    double usleepMaxError = 0.0;
    double nanosleepMaxError = 0.0;
    while (gIsRunning)
    {
        double newTime = glfwGetTime();
        double dt = newTime - currentTime;
        if (dt > maxFrameTime)
            dt = maxFrameTime;

        /* u32 fps = 1.0 / dt; */
        /* printf("FPS: %u (%g ms)\n", fps, dt * 1000.0); */

        currentTime = newTime;

        platformEventCount = 0;
        for (u32 i = 0; i < initEventCount; ++i)
        {
            if (platformEventCount < ArrayCount(platformEvents))
            {
                platformEvents[platformEventCount++] = initEvents[i];
            }
        }
        initEventCount = 0;

        glfwPollEvents();
        if (glfwWindowShouldClose(window))
        {
            gIsRunning = false;
        }

        if (gWindowHasFocus)
        {
            double x, y;
            glfwGetCursorPos(window, &x, &y);
            PlatformEvent event;
            event.type = PlatformEvent_MouseMotion;
            event.mouseMotion.x = x;
            event.mouseMotion.y = y;
            if (platformEventCount < ArrayCount(platformEvents))
            {
                platformEvents[platformEventCount++] = event;
            }
        }

        time_t newWriteTime = GetFileLastWriteTime(GAME_LIB);
        if (newWriteTime != gameCode.lastWriteTime)
        {
            UnloadGameCode(&gameCode);
            auto newGameCode = LoadGameCode(GAME_LIB);
            if (newGameCode.isValid)
            {
                gameCode = newGameCode;
                gameMemory.wasCodeReloaded = true;
                printf("Game code reloaded!\n");
            }
        }

        if (gIsServerRunning)
        {
#define MAX_PACKET_LENGTH 400
#define MAX_PACKETS_PER_SERVER_TICK 64
            // TODO: Handle case where we may need to perform multiple server updates due to frame hitch
            if (glfwGetTime() - timeOfLastServerUpdate > serverFixedTimestep)
            {
                timeOfLastServerUpdate = glfwGetTime();
                // TODO: Handle packets from more than one client or more than one packet per server tick
                char packetData[MAX_PACKETS_PER_SERVER_TICK][MAX_PACKET_LENGTH]; // 25.6KB
                sockaddr_storage receiveAddress;
                PlatformEvent serverEvents[MAX_PACKETS_PER_SERVER_TICK];
                u32 serverEventCount = 0;

                for (u32 packetCount = 0; packetCount < MAX_PACKETS_PER_SERVER_TICK; ++packetCount)
                {
                    u32 receiveAddressLength = sizeof(receiveAddress);
                    int dataLength = NonBlockingReceiveFrom((int)serverSocket, packetData[packetCount], sizeof(packetData[packetCount]),
                            (struct sockaddr *)&receiveAddress,
                            &receiveAddressLength);
                    if (dataLength > 0)
                    {
                        if (dataLength > MAX_PACKET_LENGTH)
                        {
                            // Drop packet - some other error occurred
                            LOG_WARN("Error occurred while receiving packet from client: %d", dataLength);
                            continue;
                        }

                        // Map receiveAddress to clientId
                        u32 clientId = 0;

                        // Check cache to see if we have already received a packet from this client
                        for (u32 i = 0; i < clientAddressCount; ++i)
                        {
                            if (sock_CompareAddresses(&receiveAddress, &clientAddresses[i]))
                            {
                                clientId = i;
                                break;
                            }
                        }

                        // If address not in cache then this must be a new client connection so we add it to the cache
                        if (clientId == 0)
                        {
                            // TODO: Use freelist
                            if (clientAddressCount < ArrayCount(clientAddresses))
                            {
                                clientAddresses[clientAddressCount] = receiveAddress;
                                clientId = clientAddressCount++;
                            }
                            else
                            {
                                // Refuse connection - server is full, assuming we are using a freelist
                                // This might be a good reason to move client address management into game code
                                // as that way we can use the same mechanism for refusing connections as it uses.
                                continue;
                            }
                        }

                        serverEvents[packetCount].type = PlatformEvent_PacketReceived;
                        serverEvents[packetCount].packetReceived.packetData = (u8*)packetData[packetCount];
                        serverEvents[packetCount].packetReceived.packetLength = SafeTruncateU32ToU16(dataLength);
                        serverEvents[packetCount].packetReceived.addressId = clientId;
                        serverEventCount++;
                    }
                }

                // Server Update
                gameCode.serverUpdate(&gameServerMemory, (float)dt, serverEvents, serverEventCount);

                // TODO: Free packet memory
            }
        }

        if (gIsClientRunning)
        {
#define NET_SERVER_ADDRESS_ID 0xFFFFFFFF
            // Receive packets from server
            // TODO: Support receiving more than 1 packet per frame
            u8 packetData[MAX_PACKET_LENGTH];
            for (u32 packetCount = 0; packetCount < 1; ++packetCount)
            {
                u32 receiveAddressLength = sizeof(serverAddress);
                int dataLength = NonBlockingReceiveFrom((int)clientSocket, packetData, sizeof(packetData),
                        NULL, 0);
                if (dataLength > 0)
                {
                    if (dataLength > MAX_PACKET_LENGTH)
                    {
                        // Drop packet - some other error occurred
                        LOG_WARN("Error occurred while receiving packet from client: %d", dataLength);
                        continue;
                    }

                    PlatformEvent packetReceivedEvent = {};
                    packetReceivedEvent.type = PlatformEvent_PacketReceived;
                    packetReceivedEvent.packetReceived.packetData = packetData;
                    packetReceivedEvent.packetReceived.packetLength = SafeTruncateU32ToU16(dataLength);
                    packetReceivedEvent.packetReceived.addressId = NET_SERVER_ADDRESS_ID;

                    if (platformEventCount < ArrayCount(platformEvents))
                        platformEvents[platformEventCount++] = packetReceivedEvent;
                }
            }
        }

        gameCode.update(&gameMemory, dt, platformEvents, platformEventCount, windowWidth, windowHeight);
        gameMemory.wasCodeReloaded = false;

        glfwSwapBuffers(window);
        double totalFrameTime = glfwGetTime() - currentTime;
        if (totalFrameTime < minFrameTime)
        {
            double remainder = minFrameTime - totalFrameTime;
            /* printf("totalFrameTime: %g ms\n", totalFrameTime * 1000.0); */
#if 1
            u32 microsecondsRemaining = (u32)(remainder * 1000.0 * 1000.0);
            if (microsecondsRemaining > 2000)
            {
                microsecondsRemaining -= 1000; // account of overhead of usleep
                /* printf("microsecondsRemaining: %u\n", microsecondsRemaining); */
                double usleepStart = glfwGetTime();
                usleep(microsecondsRemaining);
                double usleepTime = glfwGetTime() - usleepStart;
                double usleepError = usleepTime - ((double)microsecondsRemaining / (1000.0 * 1000.0));
                if (usleepError > usleepMaxError)
                    usleepMaxError = usleepError;
                /* printf("usleepTime: %g ms error %g ms, max error %g ms\n", usleepTime * 1000.0, usleepError * 1000.0, usleepMaxError * 1000.0); */
            }
            totalFrameTime = glfwGetTime() - currentTime;
            remainder = minFrameTime - totalFrameTime;
            if (remainder > 0.0)
            {
                double targetTime = glfwGetTime() + remainder;
                while (glfwGetTime() <= targetTime);
            }
#else
            struct timespec nanosecondsToSleep = {};
            nanosecondsToSleep.tv_nsec = remainder * 1000.0 * 1000.0 * 1000.0;
            double nanosleepStart = glfwGetTime();
            while(clock_nanosleep(CLOCK_MONOTONIC, 0, &nanosecondsToSleep, &nanosecondsToSleep) && errno == EINTR);
            double nanosleepTime = glfwGetTime() - nanosleepStart;
            double nanosleepError = nanosleepTime - remainder;
            if (nanosleepError > nanosleepMaxError)
                nanosleepMaxError = nanosleepError;
            printf("remainder: %g ms, nanosleepTime: %g ms error %g ms, max error %g ms\n", remainder * 1000.0, nanosleepTime * 1000.0, nanosleepError * 1000.0, nanosleepMaxError * 1000.0);
#endif

            double waitedFrameTime = glfwGetTime() - currentTime;
            /* printf("target: %g ms waitedFrameTime: %g ms error: %g ms\n", minFrameTime * 1000.0, waitedFrameTime * 1000.0, (waitedFrameTime - minFrameTime) * 1000.0); */
        }
    }
    if (serverPhysicsSystem.state != NULL)
        DestroyPhysicsSystem(serverPhysicsSystem);

    //DestroyPhysicsSystem(physicsSystem);
    sock_CleanupSockets();
    glfwTerminate();
    return 0;
}
