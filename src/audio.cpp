#include <AL/al.h>
#include <AL/alc.h>

struct WaveHeader
{
  u32 riffId;
  u32 size;
  u32 waveId;
};

#define RIFF_CODE( a, b, c, d )                                                \
  ( ( ( u32 )( a ) << 0 ) | ( ( u32 )( b ) << 8 ) |                  \
  ( ( u32 )( c ) << 16 ) | ( ( u32 )( d ) << 24 ) )

enum
{
  WaveChunkIdFormat = RIFF_CODE( 'f', 'm', 't', ' ' ),
  WaveChunkIdData   = RIFF_CODE( 'd', 'a', 't', 'a' ),
  WaveChunkIdRiff   = RIFF_CODE( 'R', 'I', 'F', 'F' ),
  WaveChunkIdWave   = RIFF_CODE( 'W', 'A', 'V', 'E' ),
};

struct WaveChunk
{
  u32 id;
  u32 size;
};

// TODO: Put this in pragma pack
struct WaveFormat
{
  u16 formatTag;
  u16 channelCount;
  u32 samplesPerSecond;
  u32 averageBytesPerSecond;
  u16 blockAlign;
  u16 bitsPerSample;
  u16 cbSize;
  u16 validBitsPerSample;
  u32 channelMask;
  u8 subFormat[16];
};

struct ChunkIterator
{
    u8 *at;
    u8 *stop;
};

inline ChunkIterator ParseChunkAt( void *at, void *stop )
{
    ChunkIterator iter;

    iter.at = (u8 *)at;
    iter.stop = (u8 *)stop;

    return iter;
}

inline ChunkIterator NextChunk( ChunkIterator iter )
{
  WaveChunk *chunk = (WaveChunk *)iter.at;
  u32 size = ( chunk->size + 1 ) & ~1;
  iter.at += sizeof( WaveChunk ) + size;

  return iter;
}

inline bool IsValid( ChunkIterator iter )
{
  bool result = ( iter.at < iter.stop );
  return result;
}

inline void *GetChunkData( ChunkIterator iter )
{
  void *result = ( iter.at + sizeof( WaveChunk ) );
  return result;
}

inline u32 GetType( ChunkIterator iter )
{
    WaveChunk *chunk = (WaveChunk *)iter.at;
    return chunk->id;
}

inline u32 GetChunkDataSize( ChunkIterator iter )
{
    WaveChunk *chunk = (WaveChunk *)iter.at;
    return chunk->size;
}

internal u32 CreateAudioClip(const void *assetData, u32 assetDataLength)
{
  WaveHeader *header = ( WaveHeader *)assetData;

  Assert( assetDataLength != 0 );
  Assert( header->riffId == WaveChunkIdRiff );
  Assert( header->waveId == WaveChunkIdWave );

  u32 channelCount = 0;
  u32 sampleDataSize = 0;
  i16 *sampleData = 0;
  u32 samplesPerSecond = 0;
  for ( ChunkIterator iter = ParseChunkAt(
        header + 1, (u8 *)( header + 1 ) + header->size - 4 );
        IsValid( iter ); iter = NextChunk( iter ) )
  {
    switch ( GetType( iter ) )
    {
    case WaveChunkIdFormat:
    {
      WaveFormat *format = (WaveFormat *)GetChunkData( iter );
      Assert( format->formatTag == 1 ); // Must be PCM data
      Assert( format->bitsPerSample == 16 ); // 16-bit audio
      Assert( format->blockAlign ==
              ( sizeof( i16 ) * format->channelCount ) );
      channelCount = format->channelCount;
      samplesPerSecond = format->samplesPerSecond;
      break;
    }
    case WaveChunkIdData:
      sampleData = (i16 *)GetChunkData( iter );
      sampleDataSize = GetChunkDataSize( iter );
      break;
    }
  }

  alGetError();
  u32 buffer;
  alGenBuffers(1, &buffer);
  u32 format = channelCount == 2 ? AL_FORMAT_STEREO16 : AL_FORMAT_MONO16;
  alBufferData( buffer, format, sampleData, sampleDataSize,
          samplesPerSecond );
  Assert( alGetError() == AL_NO_ERROR );

  return (u32)buffer;
}

void DestroyAudioClip(u32 buffer)
{
    alDeleteBuffers(1, &buffer);
}

// TODO: Query OpenAL for these
#define MAX_AUDIO_SOURCES 32
#define MAX_AUDIO_BUFFERS 64

struct AudioMixer
{
    u32 sources[MAX_AUDIO_SOURCES];
};

void AudioInit(AudioMixer *mixer)
{
    alGenSources(MAX_AUDIO_SOURCES, mixer->sources);
}

void AudioDeinit(AudioMixer *mixer)
{
    alDeleteSources(MAX_AUDIO_SOURCES, mixer->sources);
}

void PlaySound(AudioMixer *mixer, u32 buffer)
{
    // TODO: All that other audio state
    u32 source = 0;
    bool sourceFound = false;
    for (u32 i = 0; i < MAX_AUDIO_SOURCES; ++i)
    {
        int value;
        alGetSourcei(mixer->sources[i], AL_SOURCE_STATE, &value);

        if (value == AL_INITIAL || value == AL_STOPPED)
        {
            source = mixer->sources[i];
            sourceFound = true;
            break;
        }
    }

    if (sourceFound)
    {
        alSourcei(source, AL_BUFFER, buffer);
        Assert(alGetError() == AL_NO_ERROR);
        alSourcePlay(source);
    }
    else
    {
        LOG_ERROR("Failed to play sound because no source was available");
    }
}
