#pragma once

#include <cstdint>
#include <cstdio>
#include <cstring>

typedef uint8_t u8;
typedef uint32_t u32;
typedef int32_t i32;
typedef uint64_t u64;
typedef uint16_t u16;
typedef int16_t i16;
typedef float f32;
typedef uint32_t b32;

#define internal static
#define global static

#define ArrayCount(array) (sizeof(array) / sizeof(array[0]))

#define DEBUG_BREAK() (*(int *)0 = 0)

#define Assert(condition)                                                      \
    if (!(condition))                                                          \
    {                                                                          \
        printf("ASSERT_FAILED: %s(%d): %s\n\t%s\n", __FILE__, __LINE__,        \
               __FUNCTION__, #condition);                                      \
        DEBUG_BREAK();                                                          \
    }

#define Unused(x) (void)x
#define Bit( N ) ( 1 << ( N ) )

inline u32 SafeTruncateU64ToU32(u64 value)
{
    Assert(value <= 0xFFFFFFFF);
    u32 result = (u32)value;
    return result;
}

inline u8 SafeTruncateU32ToU8(u32 value)
{
    Assert(value <= 0xFF);
    u8 result = (u8)value;
    return result;
}

inline u16 SafeTruncateU32ToU16(u32 value)
{
    Assert(value <= 0xFFFF);
    u16 result = (u16)value;
    return result;
}

inline void ClearToZero(void *memory, size_t length) { memset(memory, 0, length); }

#define ZeroStruct( STRUCT ) ClearToZero( &STRUCT, sizeof( STRUCT ) )
#define ZeroPointerToStruct(POINTER) ClearToZero(POINTER, sizeof(*POINTER))

struct MemoryArena
{
    size_t size, used;
    u8 *base;
};

inline void MemoryArenaInitialize(MemoryArena *arena, size_t size, u8 *base)
{
    arena->size = size;
    arena->base = base;
    arena->used = 0;
}

#define AllocateStruct(ARENA, TYPE)                                            \
    (TYPE *)MemoryArenaAllocate(ARENA, sizeof(TYPE))

#define AllocateArray(ARENA, TYPE, SIZE)                                       \
    (TYPE *)MemoryArenaAllocate(ARENA, sizeof(TYPE) * (SIZE))

inline void *MemoryArenaAllocate(MemoryArena *arena, size_t size)
{
    Assert(arena->used + size < arena->size);
    void *result = arena->base + arena->used;
    arena->used += size;
    ClearToZero(result, size);
    return result;
}

// NOTE: This also frees all memory that was allocated after the given pointer.
inline void MemoryArenaFree(MemoryArena *arena, void *memory)
{
    Assert(memory >= arena->base);
    Assert(memory < arena->base + arena->used);
    size_t size = (arena->base + arena->used) - (u8 *)memory;
    arena->used -= size;
}
