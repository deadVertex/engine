struct TextInputState
{
    char *buffer;
    u32 capacity;
    u32 length;
    u32 cursor;
};

enum
{
    TextInputEvent_AddCharacter,
    TextInputEvent_Backspace,
    TextInputEvent_Delete,
    TextInputEvent_Enter,
};

TextInputState TextInputCreate(char *buffer, u32 capacity)
{
    Assert(buffer != NULL);
    Assert(capacity > 0);

    TextInputState result = {};
    result.buffer = buffer;
    result.capacity = capacity;

    return result;
}

inline void StringCopy(const char *src, char *dst, u32 length)
{
    for (u32 i = 0; i < length; ++i)
    {
        dst[i] = src[i];
    }
}

void TextInputProcessEvent(TextInputState *state, u32 eventType, u32 character = 0)
{
    char *cursor = state->buffer + state->cursor;
    switch (eventType)
    {
        case TextInputEvent_AddCharacter:
            if (state->length + 1 < state->capacity)
            {
                Assert(character != 0); // Need to provide a character for this event
                if (state->cursor < state->length)
                {
                    // Need to shift up other characters
                    // FIXME: Step into this
                    StringCopy(cursor, cursor + 1, state->length - state->cursor);
                }

                // FIXME: Don't blindly cast to ASCII
                state->buffer[state->cursor] = (char)character;
                state->cursor++;
                state->length++;
            }
            break;
        case TextInputEvent_Backspace:
            if (state->cursor > 0)
            {
                if (state->cursor == state->length)
                {
                    // FIXME: Step into this
                    StringCopy(cursor + 1, cursor, state->length - state->cursor);
                }
                state->cursor--;
                state->length--;
                state->buffer[state->length] = 0;
            }
            break;
        case TextInputEvent_Delete:
            //if (state->cursor < state->length)
            //{
                //// FIXME: Step into this
                //strncpy(state->cursor, state->cursor + 1, state->length - start->cursor);
            //}
            break;
        case TextInputEvent_Enter:
            break;
    }
}
