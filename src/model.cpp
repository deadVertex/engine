struct Node
{
    quat rotation;
    vec3 position;
    vec3 scale;
    i32 parent;
};

struct NodeToMeshMapping
{
    u32 node;
    u32 mesh;
};

struct BoneWeightPair
{
    float weight;
    u32 bone;
};

struct MeshImportData
{
    vec3 *vertexPositions;
    vec3 *vertexNormals;
    vec2 *vertexTextureCoords;
    u32 *indices;
    u32 vertexCount;
    u32 indexCount;
    BoneWeightPair *boneWeightPairs;
    u32 boneWeightPairCount;
    mat4 *boneInvBindPoses;
    u32 boneCount;
};

struct ModelImportData
{
    MeshImportData *meshes;
    u32 meshCount;

    NodeToMeshMapping *nodeToMeshMappings;
    u32 nodeToMeshMappingCount;

    Node *nodes;
    const char **nodeNames;
    u32 nodeCount;
};

enum
{
    ModelImport_None = 0,
    ModelImport_Normals = Bit(0),
    ModelImport_TextureCoords = Bit(1),
    ModelImport_VertexWeights = Bit(2),
    ModelImport_Hierarchy = Bit(3),
    ModelImport_Meshes = Bit(4),
};

// NOTE: Only triangles are supported
internal u32 CopyIndicesToArray(u32 *indices, u32 length, aiMesh *mesh)
{
    u32 count = 0;
    for (u32 faceIdx = 0; faceIdx < mesh->mNumFaces; ++faceIdx)
    {
        aiFace face = mesh->mFaces[faceIdx];
        Assert(face.mNumIndices == 3);
        for (u32 j = 0; j < face.mNumIndices; ++j)
        {
            indices[count++] = face.mIndices[j];
        }
    }

    return count;
}

internal u32 CountNodesInHeirarchy(const aiNode *node, bool isRoot = true)
{
	u32 result = node->mNumChildren;
    for (u32 childIdx = 0; childIdx < node->mNumChildren; ++childIdx)
    {
		result += CountNodesInHeirarchy(node->mChildren[childIdx], false);
    }

    if (isRoot)
        result += 1;

	return result;
}

internal void FlattenTree(const aiNode **assetNodes, i32 *parents, u32 *length, const aiNode *assetNode, u32 parent = -1)
{
    u32 index = (*length)++;
    assetNodes[index] = assetNode;
    parents[index] = parent;
    for (u32 i = 0; i < assetNode->mNumChildren; ++i)
        FlattenTree(assetNodes, parents, length, assetNode->mChildren[i], (i32)index);
}

internal u32 CountNodeToMeshMappings(const aiNode **assetNodes, u32 length)
{
    u32 count = 0;
    for (u32 i = 0; i < length; ++i)
    {
        count += assetNodes[i]->mNumMeshes;
    }

    return count;
}

internal void CopyNodeToMeshMappings(NodeToMeshMapping *mappings, u32 mappingsLength,
                                    const aiNode **nodes, u32 nodesLength)
{
    u32 count = 0;
    for (u32 nodeIdx = 0; nodeIdx < nodesLength; ++nodeIdx)
    {
        const aiNode *node = nodes[nodeIdx];
        for (u32 meshIdx = 0; meshIdx < node->mNumMeshes; ++meshIdx)
        {
            NodeToMeshMapping mapping;
            mapping.node = nodeIdx;
            mapping.mesh = node->mMeshes[meshIdx];

            Assert(count < mappingsLength);
            mappings[count++] = mapping;
        }
    }
}

internal char *AllocateString(MemoryArena *arena, const char *string);

void AssimpLogCallback(const char *msg, char *user)
{
    LOG_DEBUG("%s", msg);
}

internal ModelImportData ImportModel(const void *fileData, u32 fileDataLength, MemoryArena *arena, u32 flags, float scale = 1.0f)
{
    ModelImportData result = {};

    aiLogStream logStream = {};
    logStream.callback = &AssimpLogCallback;
    //aiAttachLogStream(&logStream);
    //aiEnableVerboseLogging(AI_TRUE);
    aiPropertyStore *properties = aiCreatePropertyStore();
    aiSetImportPropertyFloat(properties, AI_CONFIG_GLOBAL_SCALE_FACTOR_KEY, scale);
    const aiScene *scene = aiImportFileFromMemoryWithProperties(
            (const char *)fileData, fileDataLength,
            aiProcess_Triangulate | aiProcess_JoinIdenticalVertices |
                    aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph |
                    aiProcess_LimitBoneWeights | aiProcess_GlobalScale,
            "", properties);
    //aiDetachLogStream(&logStream);

    if ( !scene )
    {
      LOG_ERROR( "Model import failed: %s", aiGetErrorString() );
      return result;
    }

    if (flags & ModelImport_Hierarchy)
    {
        // Organize asset nodes into linear array for easier processing
        result.nodeCount = CountNodesInHeirarchy(scene->mRootNode);
        result.nodeNames = AllocateArray(arena, const char *, result.nodeCount);
        result.nodes = AllocateArray(arena, Node, result.nodeCount);

        // FIXME: Use temp array for this allocation
        const aiNode **assetNodes = AllocateArray(arena, const aiNode*, result.nodeCount);
        i32 *parents = AllocateArray(arena, i32, result.nodeCount);
        u32 treeLength = 0;
        FlattenTree(assetNodes, parents, &treeLength, scene->mRootNode);
        Assert(treeLength == result.nodeCount);

        for (u32 nodeIdx = 0; nodeIdx < result.nodeCount; ++nodeIdx)
        {
            Node *node = result.nodes + nodeIdx;
            const aiNode *assetNode = assetNodes[nodeIdx];
            i32 parent = parents[nodeIdx];

            aiVector3t<float> scaling;
            aiQuaterniont<float> rotation;
            aiVector3t<float> position;
            assetNode->mTransformation.Decompose(scaling, rotation, position);

            node->position = Vec3(position.x, position.y, position.z);
            node->rotation = Quat(rotation.x, rotation.y, rotation.z, rotation.w);
            node->scale = Vec3(scaling.x, scaling.y, scaling.z);
            node->parent = parent;

            result.nodeNames[nodeIdx] = AllocateString(arena, assetNodes[nodeIdx]->mName.data);
        }

        result.nodeToMeshMappingCount =
                CountNodeToMeshMappings(assetNodes, result.nodeCount);
        result.nodeToMeshMappings = AllocateArray(
                arena, NodeToMeshMapping, result.nodeToMeshMappingCount);
        CopyNodeToMeshMappings(result.nodeToMeshMappings, result.nodeToMeshMappingCount,
                               assetNodes, result.nodeCount);

        // FIXME: Need to use a temp arena
        //MemoryArenaFree(arena, parents);
        //MemoryArenaFree(arena, assetNodes);
    }

    if (flags & ModelImport_Meshes)
    {
        // Allocate array for mesh data
        result.meshCount = scene->mNumMeshes;
        result.meshes = AllocateArray(arena, MeshImportData, scene->mNumMeshes);

        for (u32 meshIdx = 0; meshIdx < scene->mNumMeshes; ++meshIdx)
        {
            aiMesh *mesh = scene->mMeshes[meshIdx];
            MeshImportData *meshData = result.meshes + meshIdx;

            // Allocate array in mesh data for indices
            meshData->indexCount = mesh->mNumFaces * 3;
            meshData->indices = AllocateArray(arena, u32, meshData->indexCount);

            // Copy indices into allocated array
            u32 count = CopyIndicesToArray(meshData->indices, meshData->indexCount, mesh);
            Assert(count == meshData->indexCount);

            // Allocate array for vertex positions
            meshData->vertexCount = mesh->mNumVertices;
            meshData->vertexPositions = AllocateArray(arena, vec3, mesh->mNumVertices);

            // Copy vertex positions into allocated array
            for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
            {
                aiVector3D v = mesh->mVertices[vertexIdx];
                meshData->vertexPositions[vertexIdx] = Vec3(v.x, v.y, v.z);
            }

            if (flags & ModelImport_Normals)
            {
                Assert(mesh->mNormals != NULL);
                meshData->vertexNormals = AllocateArray(arena, vec3, mesh->mNumVertices);

                // Copy vertex normals into allocated array
                for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
                {
                    aiVector3D v = mesh->mNormals[vertexIdx];
                    meshData->vertexNormals[vertexIdx] = Vec3(v.x, v.y, v.z);
                }
            }

            if (flags & ModelImport_TextureCoords)
            {
                // FIXME: Support multiple sets of texture coordinates
                Assert(mesh->mTextureCoords[0] != NULL);

                meshData->vertexTextureCoords = AllocateArray(arena, vec2, mesh->mNumVertices);

                // Copy vertex normals into allocated array
                for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
                {
                    aiVector3D v = mesh->mTextureCoords[0][vertexIdx];
                    meshData->vertexTextureCoords[vertexIdx] = Vec2(v.x, v.y);
                }
            }

            if (flags & ModelImport_VertexWeights)
            {
                // Foreach vertex
                meshData->boneWeightPairCount =
                    MAX_BONES * mesh->mNumVertices;
                meshData->boneWeightPairs = AllocateArray(
                        arena, BoneWeightPair, meshData->boneWeightPairCount);
                meshData->boneInvBindPoses = AllocateArray(
                        arena, mat4, MAX_BONES);
                meshData->boneCount = mesh->mNumBones;

                ClearToZero(meshData->boneWeightPairs,
                        meshData->boneWeightPairCount * sizeof(BoneWeightPair));
                // FIXME: While correct, it looks better if they are zeroed for unknown
                // bone inverse bind poses so that they are scaled to 0 and not drawn
                //for (u32 i = 0; i < MAX_BONES; ++i)
                    //meshData->boneInvBindPoses[i] = Identity();

                bool isSet[MAX_BONES] = { false };
                for (u32 meshBoneIdx = 0; meshBoneIdx < mesh->mNumBones; ++meshBoneIdx)
                {
                    aiBone* bone = mesh->mBones[meshBoneIdx];

                    // Convert bone->mName to index
                    u32 nodeIdx = 0;
                    for (nodeIdx = 0; nodeIdx < result.nodeCount; ++nodeIdx)
                    {
                        if (!strcmp(bone->mName.data, result.nodeNames[nodeIdx]))
                            break;
                    }
                    Assert(nodeIdx != result.nodeCount);
                    aiVector3t<float> scaling;
                    aiQuaterniont<float> rotation;
                    aiVector3t<float> position;
                    bone->mOffsetMatrix.Decompose(scaling, rotation, position);
                    vec3 s = Vec3(scaling.x, scaling.y, scaling.z);
                    vec3 p = Vec3(position.x, position.y, position.z);
                    quat r = Quat(rotation.x, rotation.y, rotation.z,
                                  rotation.w);
                    Assert(isSet[nodeIdx] == false);
                    meshData->boneInvBindPoses[nodeIdx] = Translate(p) *
                        Rotate(r) * Scale(s);
                    isSet[nodeIdx] = true;

                    for (u32 weightIdx = 0; weightIdx < bone->mNumWeights; ++weightIdx)
                    {
                        aiVertexWeight weight = bone->mWeights[weightIdx];
                        Assert(weight.mVertexId < mesh->mNumVertices);
                        u32 idx = nodeIdx + (weight.mVertexId * MAX_BONES);
                        Assert(meshData->boneWeightPairs[idx].bone == 0);
                        Assert(meshData->boneWeightPairs[idx].weight == 0.0f);
                        meshData->boneWeightPairs[idx].bone = nodeIdx;
                        meshData->boneWeightPairs[idx].weight = weight.mWeight;
                        //LOG_DEBUG("%u,%u: bone = %u, weight = %g", weight.mVertexId, idx,
                                //nodeIdx, weight.mWeight);
                    }
                }
            }
        }
    }

    aiReleaseImport(scene);
    aiReleasePropertyStore(properties);

    return result;
}

// Sorts largest to smallest
inline int BoneWeightPairCompare(const void *a, const void *b)
{
    BoneWeightPair p0 = *(BoneWeightPair*)b;
    BoneWeightPair p1 = *(BoneWeightPair*)a;
    if (p0.weight < p1.weight)
        return -1;
    else if (p0.weight == p1.weight)
        return 0;
    else
        return 1;
}

OpenGL_StaticMesh CreateMeshFromData(MeshImportData *data, MemoryArena *arena, mat4 *transform = NULL)
{
    Assert(data->vertexNormals);
    Assert(data->vertexTextureCoords);
    u32 vertexSize = sizeof(vec3) * 2 + sizeof(vec2);

    bool isSkeletalMesh = false;
    if (data->boneCount > 0)
    {
        Assert(data->boneWeightPairs != NULL);
        Assert(data->boneWeightPairCount > 0);

        vertexSize += sizeof(u32) + sizeof(vec3);
        isSkeletalMesh = true;
    }

    void *vertices = MemoryArenaAllocate(arena, vertexSize * data->vertexCount);

    u8 *vertex = (u8*)vertices;
    for (u32 i = 0; i < data->vertexCount; ++i)
    {
        if (transform != NULL)
        {
            vec4 v = *transform * Vec4(data->vertexPositions[i], 1.0);
            *(vec3*)vertex = Vec3(v);
        }
        else
        {
            *(vec3*)vertex = data->vertexPositions[i];
        }
        vertex += sizeof(vec3);

        *(vec3*)vertex = data->vertexNormals[i];
        vertex += sizeof(vec3);

        *(vec2*)vertex = data->vertexTextureCoords[i];
        vertex += sizeof(vec2);

        if (isSkeletalMesh)
        {
            BoneWeightPair *pairs = data->boneWeightPairs + i * MAX_BONES;
            // TODO: Replace with radix sort
            qsort(pairs, MAX_BONES, sizeof(BoneWeightPair), &BoneWeightPairCompare);

            float total = pairs[0].weight + pairs[1].weight + pairs[2].weight;
            Assert(total <= 1.01f);

            *(vec3 *)vertex =
                    Vec3(pairs[0].weight, pairs[1].weight, pairs[2].weight);
            // 4th bone weight is calculated from 1 minus the sum of the other bone weights.
            vertex += sizeof(vec3);

            Assert(pairs[0].bone > 0);

            *(u32 *)vertex = (pairs[0].bone << 24) |
                             (pairs[1].bone << 16) |
                             (pairs[2].bone <<  8) |
                             (pairs[3].bone      );
            vertex += sizeof(u32);
        }
    }

    OpenGL_VertexAttribute attribs[5];
    attribs[0].index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
    attribs[0].numComponents = 3;
    attribs[0].componentType = GL_FLOAT;
    attribs[0].normalized = GL_FALSE;
    attribs[0].offset = 0;
    attribs[1].index = OPENGL_VERTEX_ATTRIBUTE_NORMAL;
    attribs[1].numComponents = 3;
    attribs[1].componentType = GL_FLOAT;
    attribs[1].normalized = GL_FALSE;
    attribs[1].offset = sizeof( float ) * 3;
    attribs[2].index = OPENGL_VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
    attribs[2].numComponents = 2;
    attribs[2].componentType = GL_FLOAT;
    attribs[2].normalized = GL_FALSE;
    attribs[2].offset = sizeof( float ) * 6;
    attribs[3].index = OPENGL_VERTEX_ATTRIBUTE_BONE_WEIGHT;
    attribs[3].numComponents = 3;
    attribs[3].componentType = GL_FLOAT;
    attribs[3].normalized = GL_FALSE;
    attribs[3].offset = sizeof(float) * 8;
    attribs[4].index = OPENGL_VERTEX_ATTRIBUTE_BONE_ID;
    attribs[4].numComponents = 1;
    attribs[4].componentType = GL_UNSIGNED_INT;
    attribs[4].normalized = GL_FALSE;
    attribs[4].offset = sizeof(float) * 11;
    OpenGL_StaticMesh result = OpenGL_CreateStaticMesh(
            (float *)vertices, data->vertexCount, data->indices,
            data->indexCount, vertexSize, attribs, isSkeletalMesh ? 5 : 3,
            GL_TRIANGLES);

    MemoryArenaFree(arena, vertices);

    return result;
}

internal void ScaleModelData(ModelImportData *data, float scale)
{
    for (u32 i = 0; i < data->nodeCount; ++i)
        data->nodes[i].position *= scale;

    for (u32 meshIdx = 0; meshIdx < data->meshCount; ++meshIdx)
    {
        MeshImportData *meshData = data->meshes + meshIdx;
        for (u32 vertexIdx = 0; vertexIdx < meshData->vertexCount; vertexIdx++)
            meshData->vertexPositions[vertexIdx] *= scale;
    }
}

inline mat4 CalculateNodeTransform(Node *node)
{
    mat4 result = Translate(node->position) * Rotate(node->rotation) * Scale(node->scale);
    return result;
}

inline mat4 CalculateNodeInverseTransform(Node *node)
{
    mat4 result = Scale(1.0f / node->scale.x) * Rotate(-node->rotation) *
                  Translate(-node->position);
    return result;
}

internal mat4 CalculateNodeWorldTransform(Node *nodes, u32 count, u32 index)
{
    Assert(index < count);
    Node *node = nodes + index;

    mat4 result = CalculateNodeTransform(node);
    if (node->parent >= 0)
    {
        mat4 parentTransform = CalculateNodeWorldTransform(nodes, count, node->parent);
        result = parentTransform * result;
    }
    return result;
}

internal mat4 CalculateNodeInvWorldTransform(Node *nodes, u32 count, u32 index)
{
    Assert(index < count);
    Node *node = nodes + index;

    mat4 result = CalculateNodeInverseTransform(node);
    if (node->parent >= 0)
    {
        mat4 parentTransform = CalculateNodeInvWorldTransform(nodes, count, node->parent);
        result = result * parentTransform;
    }
    return result;
}
