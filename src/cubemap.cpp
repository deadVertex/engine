const char *cubeMapVertexSource =
"#version 330\n"
"layout(location = 0) in vec3 vertexPosition;\n"
"uniform mat4 mvp = mat4(1);"
"out vec3 textureCoordinates;"
"void main()"
"{"
"  textureCoordinates = vertexPosition;"
"  gl_Position = mvp * vec4(vertexPosition, 1.0);"
"}\n";

const char *cubeMapFragmentSource =
"#version 330\n"
"in vec3 textureCoordinates;"
"out vec4 outputColour;"
"uniform samplerCube cubeTexture;"
"void main()"
"{"
"   outputColour = vec4(textureLod(cubeTexture, textureCoordinates, 0.0 ).rgb, 1);\n"
"}\n";

struct CubeMapShader
{
    u32 program;
    int mvp;
    int cubeTexture;
};

CubeMapShader CreateCubeMapShader()
{
    CubeMapShader result = {};
    result.program = OpenGL_CreateShader(cubeMapVertexSource, cubeMapFragmentSource);
    Assert(result.program);
    result.mvp = glGetUniformLocation(result.program, "mvp");
    result.cubeTexture = glGetUniformLocation(result.program, "cubeTexture");
    
    return result;
}


internal void SetupCubeMapSide( GLenum side, const void *pixels, u32 w,
                                u32 h, bool useMipMap = false,
                                u32 level = 0 )
{
  glTexImage2D( side, level, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE,
                pixels );
}

internal void CopySubImage( u8 *subImageData, const u8 *imageData,
                            u32 subImageWidth, u32 subImageHeight,
                            u32 numChannels, u32 xStart,
                            u32 yStart, u32 imagePitch )
{
  u32 subImagePitch = subImageWidth * numChannels;
  for ( u32 y = yStart; y < yStart + subImageHeight; ++y )
  {
    u32 start = y * imagePitch + xStart * numChannels;
    memcpy( subImageData + ( y - yStart ) * subImagePitch, imageData + start,
            subImagePitch );
  }
}

void SetupCrossCubeMap( const u8 *data, u32 w, u32 h, u32 n,
                        MemoryArena *arena, bool useMipMap = false,
                        u32 level = 0 )
{
  u32 faceSize = w / 4;
  Assert( h / faceSize == 3 );
  u32 pitch = w * n;
  u32 facePitch = faceSize * n;

  u8 *buffer = AllocateArray( arena, u8, faceSize * faceSize * n );

  CopySubImage( buffer, data, faceSize, faceSize, n, faceSize, 0, pitch );
  SetupCubeMapSide( GL_TEXTURE_CUBE_MAP_POSITIVE_Y, buffer, faceSize, faceSize,
                    useMipMap, level );

  CopySubImage( buffer, data, faceSize, faceSize, n, 0, faceSize, pitch );
  SetupCubeMapSide( GL_TEXTURE_CUBE_MAP_NEGATIVE_X, buffer, faceSize, faceSize,
                    useMipMap, level );

  CopySubImage( buffer, data, faceSize, faceSize, n, faceSize, faceSize,
                pitch );
  SetupCubeMapSide( GL_TEXTURE_CUBE_MAP_POSITIVE_Z, buffer, faceSize, faceSize,
                    useMipMap, level );

  CopySubImage( buffer, data, faceSize, faceSize, n, faceSize * 2, faceSize,
                pitch );
  SetupCubeMapSide( GL_TEXTURE_CUBE_MAP_POSITIVE_X, buffer, faceSize, faceSize,
                    useMipMap, level );

  CopySubImage( buffer, data, faceSize, faceSize, n, faceSize * 3, faceSize,
                pitch );
  SetupCubeMapSide( GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, buffer, faceSize, faceSize,
                    useMipMap, level );

  CopySubImage( buffer, data, faceSize, faceSize, n, faceSize, faceSize * 2,
                pitch );
  SetupCubeMapSide( GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, buffer, faceSize, faceSize,
                    useMipMap, level );

  MemoryArenaFree(arena, buffer);
}

u32 CreateCubeMapCrossTexture(void *memory, u32 length, MemoryArena *arena)
{
    u32 result;
    int w, h, n;
    stbi_set_flip_vertically_on_load(0);
    auto data =
            stbi_load_from_memory((const u8 *)memory, length, &w, &h, &n, 3);
    n = 3;
    if (!data)
    {
        LOG_ERROR("Failed to import image");
        return 0;
    }

    glGenTextures(1, &result);
    glBindTexture(GL_TEXTURE_CUBE_MAP, result);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    SetupCrossCubeMap(data, w, h, n, arena);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    stbi_image_free(data);

    return result;
}
