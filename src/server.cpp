/*
 * TODO:
 * - Fix DOS vulnerability when allocating resources on client request. Use a connection token
 * - Server not rebuilding hitboxes on code reload
 * - Per client network events
 *  - Data table for items
 *  - inventory UI [x]
 *  - Tab to toggle inventory [x]
 *  - F1 to toggle debug camera [x]
 *  - UI for transfering items
 *  - Containers
 *  - On demand updates for container contents
 *  - Per client update snapshots
 *  - Network entity ids
 *  - Support for entities other than players
 */
#include "utils.h"
#include "bitstream.h"

#include "ack_system.cpp"

#include "bullet_system.cpp"

#define NET_PACKET_MAX_ACKS 4

struct PacketHeader
{
    u64 timestamp; // microseconds
    u64 returnTimestamp; // microseconds
    u16 sequenceNumber; // TODO: Handle wrapping where 0 is treated as an invalid value
                        // Wrap will occur roughly every 17 minutes
    u8 dataType;
    u16 acks[NET_PACKET_MAX_ACKS];
};

enum
{
    PacketDataType_None,
    PacketDataType_ConnectionRequest,
    PacketDataType_ConnectionResponse,
    PacketDataType_GameSnapshot,
    PacketDataType_GameClientCommand,
};

struct ConnectionRequestData
{
    u32 version;
    char identifier[80];
};

enum
{
    ConnectionResponse_Ok,
    ConnectionResponse_ServerFull,
};

struct ConnectionResponseData
{
    u8 response;
};

enum
{
    ServerGameState_Uninitialized,
    ServerGameState_WaitingForPlayers,
    ServerGameState_Ready,
};

enum
{
    GameEvent_None,
    GameEvent_GunShot,
    GameEvent_BulletImpact,
    GameEvent_HitMarker,
    GameEvent_AddItemToInventory,
};

struct GameEvent
{
    u8 type;
    union
    {
        struct
        {
            vec3 position;
        } gunShot;

        struct
        {
            vec3 position;
            vec3 normal;
        } bulletImpact;

        struct
        {
            u32 owner;
        } hitMarker;

        struct
        {
            u32 inventoryIndex;
            u32 itemType;
            u32 itemQuantity;
        } addItemToInventory;
    };
};

struct GameSnapshotData
{
    u32 currentTick;
    u32 playerIndex;
    vec3 playerPositions[4];
    vec2 playerViewAngles[4];
    vec3 playerVelocities[4];
    float playerHealth[4];
    u32 playerCount;
    GameEvent events[4];
    u8 eventCount;
};

enum
{
    PlayerAction_None = 0,
    PlayerAction_PrimaryFire = Bit(0),
    PlayerAction_SecondaryFire = Bit(1),
    PlayerAction_Use = Bit(2)
};

struct PlayerCommand
{
    vec2 angles;
    float forwardMove;
    float rightMove;
    float upMove;
    u8 actions;
};

struct GameClientCommandData
{
    u32 clientTick;
    PlayerCommand playerCommand;
};

enum
{
    ClientConnectionState_Inactive,
    ClientConnectionState_Connecting,
    ClientConnectionState_Connected,
};

struct ServerClientState
{
    u64 inTimestamp;
    u32 clientId;
    u8 connectionState;
    char identifier[80];
    u32 playerControllerIndex;
    u16 nextSequenceNumber;
    u16 expectedSequenceNumber;
    AckSystem ackSystem;
};

struct PlayerController
{
    vec3 position;
    vec3 velocity;
    vec2 viewAngles;
    b32 onGroundPlane;
    b32 isWalking;
    u32 broadphaseProxy; // FIXME: Don't want physics implementation bleeding out
};

enum
{
    InventoryItem_None,
    InventoryItem_Wood,
    InventoryItem_Stone,
};

struct InventoryItem
{
    u32 type;
    u32 quantity;
};

#define MAX_INVENTORY_ITEMS 4
struct InventoryState
{
    InventoryItem items[MAX_INVENTORY_ITEMS];
    u32 count;
};

struct AnimationClipState
{
    AnimationClip clip;
    BonePose *currentPose;
    float rate; // This must not be adjusted at runtime!
    float startT;
};

struct BlendTree
{
    AnimationClipState clipStates[4];
    BonePose *blendedPose;
};

struct AnimationController
{
    BlendTree blendTree;
    Skeleton *skeleton;
    BonePose *currentPose;
};

enum
{
    PlayerHitBox_Head,
    PlayerHitBox_Body,
    PlayerHitBox_LeftUpperArm,
    PlayerHitBox_LeftLowerArm,
    PlayerHitBox_RightUpperArm,
    PlayerHitBox_RightLowerArm,
    PlayerHitBox_LeftUpperLeg,
    PlayerHitBox_LeftLowerLeg,
    PlayerHitBox_RightUpperLeg,
    PlayerHitBox_RightLowerLeg,
    MaxPlayerHitBoxes,
};

global const char *gPlayerHitBoxNames[] =
{
    "PlayerHitBox_Head",
    "PlayerHitBox_Body",
    "PlayerHitBox_LeftUpperArm",
    "PlayerHitBox_LeftLowerArm",
    "PlayerHitBox_RightUpperArm",
    "PlayerHitBox_RightLowerArm",
    "PlayerHitBox_LeftUpperLeg",
    "PlayerHitBox_LeftLowerLeg",
    "PlayerHitBox_RightUpperLeg",
    "PlayerHitBox_RightLowerLeg",
    "MaxPlayerHitBoxes",
};

struct PlayerHitBoxes
{
    u32 handles[MaxPlayerHitBoxes];
    b32 isValid;
};

enum
{
    PlayerAnimation_Idle,
    PlayerAnimation_Walk,
    PlayerAnimation_Run,


    PlayerAnimation_Max,
};

#define MAX_CLIENTS 64
struct ServerGameState
{
    MemoryArena testArena;
    MemoryArena assetArena; // NOTE: This is cleared on a code reload
    ServerClientState clients[MAX_CLIENTS];
    u32 clientCount;
    u8 state;
    u32 currentTick;
    double currentTime;
    PlayerController playerControllers[64];
    Sk_AnimationController playerAnimationControllers[64];
    PlayerHitBoxes playerHitBoxes[64];
    InventoryState playerInventoryStates[64];
    float playerHealth[64];
    float playerLastShootTime[64];
    u32 playerControllerCount;
    //Scene playerModelScene;
    //SkeletonPose playerBindPose;
    Sk_Skeleton playerSkeleton;
    Sk_AnimationClip playerRunAnimation;
    Sk_AnimationClip playerIdleAnimation;
    Sk_AnimationClip playerWalkAnimation;
    GameEvent events[64];
    u32 eventCount;
    BulletSystem bulletSystem;
    HeightMap terrainHeightMap;
    Terrain terrainData;
    PhysicsSystem *physicsSystem;
    PhysicsShapeHandle playerShape;
    PhysicsShapeHandle terrainShape;
    PhysicsBodyHandle terrainBody;
};

global PhysicsShapeHandle gPlayerShape2 = PHYSICS_INVALID_HANDLE;

internal void UpdateViewAngles(PlayerController *player, PlayerCommand cmd)
{
    vec2 viewAngles = cmd.angles;
    viewAngles.x = Clamp(viewAngles.x, -PI * 0.5f, PI * 0.5f);
    if (viewAngles.y > PI)
    {
        viewAngles.y -= 2.0f * PI;
    }
    else if (viewAngles.y < -PI)
    {
        viewAngles.y += 2.0f * PI;
    }
    player->viewAngles = viewAngles;
}

internal void GroundTrace(PhysicsSystem *physicsSystem, PlayerController *player)
{
    PhysicsSweepTestParameters parameters = {};
    parameters.shape = gPlayerShape2;
    parameters.start = player->position;
    parameters.end = player->position + Vec3(0, -0.25f, 0);
    parameters.filterGroup = COLLISION_TYPE_DEFAULT;
    parameters.filterMask = COLLISION_TYPE_STATIC;
    PhysicsSweepTestResult result =
        PhysicsSweepTest(physicsSystem, &parameters);
    if (!result.hasHit)
    {
        player->onGroundPlane = false;
        player->isWalking = false;
        return;
    }

    player->onGroundPlane = true;
    player->isWalking = true;
}

struct CC_MoveResult
{
    vec3 position;
    vec3 velocity;
};

internal CC_MoveResult CC_Move(PhysicsSystem *physicsSystem, PlayerController *player, float dt)
{
    /* vec3 clipPlanes[MAX_CLIP_PLANES]; */
    /* u32 clipPlaneCount = 0; */

    vec3 outPosition = player->position;
    vec3 outVelocity = player->velocity;

    float timeLeft = dt;

    /* if (player->onGroundPlane) */
    /* { */
    /*     clipPlaneCount = 1; */
    /*     clipPlanes[0] = player->groundTrace.hitNormal; */
    /* } */

    /* if (!IsZero(player->velocity)) */
    /*     clipPlanes[clipPlaneCount++] = Normalize(player->velocity); */

    for (u32 bumpCount = 0; bumpCount < 4; ++bumpCount)
    {
        if (IsZero(outVelocity))
            break;

        vec3 delta = outVelocity * dt;
        vec3 end = outPosition + delta;

        PhysicsSweepTestParameters parameters = {};
        parameters.shape = gPlayerShape2;
        parameters.start = outPosition;
        parameters.end = outPosition + delta;
        parameters.filterGroup = COLLISION_TYPE_DEFAULT;
        parameters.filterMask = COLLISION_TYPE_STATIC;

        PhysicsSweepTestResult result =
            PhysicsSweepTest(physicsSystem, &parameters);

        if (result.hasHit)
        {
            //LOG_DEBUG("CC_Move has hit");
            //LOG_DEBUG("hitPoint: %g %g %g", result.hitPoint.x,
                    //result.hitPoint.y, result.hitPoint.z);
            vec3 hitPoint = outPosition + delta * result.hitFraction;
            //LOG_DEBUG("v: %g %g %g", v.x, v.y, v.z);
            //LOG_DEBUG("hitFraction: %g", result.hitFraction);
            //LOG_DEBUG("hitNormal: %g %g %g",
                    //result.hitNormal.x, result.hitNormal.y, result.hitNormal.z);
            outPosition = hitPoint;
            timeLeft -= timeLeft * result.hitFraction;

            delta = delta - Dot(delta, result.hitNormal) * result.hitNormal;
            outVelocity = outVelocity -
                       Dot(outVelocity, result.hitNormal) * result.hitNormal;
        }
        else
        {
            // Full move
            outPosition = end;
            break;
        }
    }

    CC_MoveResult result;
    result.position = outPosition;
    result.velocity = outVelocity;

    return result;
}

inline void Accelerate(PlayerController *player, vec3 wishdir,
                         float wishspeed, float accel, float dt)
{
    // WARNING: Has strafe jumping max speed bug
    /* float currentspeed = Dot(player->velocity, wishdir); */
    /* float addspeed = wishspeed - currentspeed; */
    /* if (addspeed <= 0.0f) */
    /*     return; */

    float accelspeed = accel * dt * wishspeed;
    //accelspeed = Min(accelspeed, addspeed); 
    //Debug_Printf(gameState, "accelspeed: %g\n", accelspeed);
    player->velocity += accelspeed * wishdir;
}

internal void UpdatePlayer(PhysicsSystem *physicsSystem, PlayerController *player,
                           PlayerCommand cmd, float dt)
{
    UpdateViewAngles(player, cmd);

    GroundTrace(physicsSystem, player);

    vec3 gravity = Vec3(0, -10, 0);
    //gravity = Vec3(0);
    if (player->onGroundPlane)
    {
        float friction = 3.0f; // 12.0f;
        player->velocity -= player->velocity * friction * dt;

        float fmove = cmd.forwardMove;
        float smove = cmd.rightMove;

        vec4 v = RotateY(player->viewAngles.y) * Vec4(0,0,1,0);
        vec3 forward = Vec3(v.x, v.y, v.z);

        v = RotateY(player->viewAngles.y) * Vec4(1,0,0,0);
        vec3 right = Vec3(v.x, v.y, v.z);

        vec3 wishvel = forward * fmove + right * smove;
        //Debug_Printf(gameState, "wishvel: %g %g %g\n", wishvel.x, wishvel.y, wishvel.z);
        float wishspeed = Length(wishvel);
        vec3 wishdir = {};
        if (!IsZero(wishvel))
            wishdir = Normalize(wishvel);

        float speed = 30.0f; // 150.0f;
        Accelerate(player, wishdir, wishspeed, speed, dt);

        CC_MoveResult result = CC_Move(physicsSystem, player, dt);
        player->position = result.position;
        player->velocity = result.velocity;
    }
    else
    {
        player->velocity += gravity * dt;
        CC_MoveResult result = CC_Move(physicsSystem, player, dt);
        player->position = result.position;
        player->velocity = result.velocity;
    }
}

internal u32 AllocatePlayerController(ServerGameState *gameState)
{
    Assert(gameState->playerControllerCount < ArrayCount(gameState->playerControllers));
    u32 result = gameState->playerControllerCount++;
    PlayerController *playerController = gameState->playerControllers + result;

    playerController->velocity = Vec3(0);
    playerController->position = Vec3(-14, 8, 6);

    gameState->playerHealth[result] = 100.0f;

    return result;
}

#if 0
internal BlendTree AllocateBlendTree(MemoryArena *arena, Skeleton *skeleton)
{
    BlendTree result = {};
    for (u32 i = 0; i < ArrayCount(result.clipStates); ++i)
    {
        result.clipStates[i].currentPose = AllocateArray(arena, BonePose, skeleton->boneCount);
    }
    result.blendedPose = AllocateArray(arena, BonePose, skeleton->boneCount);
    return result;
}
#endif

internal void ReloadContent(ServerGameState *gameState, GameMemory *memory)
{
    // Clear asset arena
    gameState->assetArena.used = 0;

    PhysicsDestroyBody(memory->serverPhysicsSystem, gameState->terrainBody);
    PhysicsDestroyShape(memory->serverPhysicsSystem, gameState->terrainShape);

    ReadFileResult heightMapFile = memory->readEntireFile("../content/heightmap_small.png");
    if (heightMapFile.memory != NULL)
    {
        gameState->terrainHeightMap =
                CreateHeightMap(heightMapFile.memory, heightMapFile.size,
                                &gameState->assetArena);

        TerrainParameters terrainParams = {};
        terrainParams.origin = Vec3(-100, 0, 0);
        terrainParams.scale = Vec3(200, 40, 200);
        terrainParams.uvScale = 128.0f;
        terrainParams.rows = 64;
        terrainParams.cols = 64;
        Terrain terrainData = 
                CreateTerrain(gameState->terrainHeightMap, terrainParams,
                              &gameState->assetArena);
        gameState->terrainData = terrainData;

        // FIXME: Handle code reloads
        PhysicsShapeParameters shapeParameters = {};
        shapeParameters.type = PHYSICS_TERRAIN_SHAPE;
        shapeParameters.heightfieldWidth = gameState->terrainHeightMap.width;
        shapeParameters.heightfieldLength = gameState->terrainHeightMap.height;
        shapeParameters.heightfieldData = gameState->terrainHeightMap.values;
        shapeParameters.minHeight = 0.0f;
        shapeParameters.maxHeight = 1.0f;
        shapeParameters.scale = Vec3(200.0f / shapeParameters.heightfieldWidth, 40.0f,
                200.0f / shapeParameters.heightfieldLength);
        gameState->terrainShape = PhysicsCreateShape(
                memory->serverPhysicsSystem, shapeParameters);

        PhysicsBodyParameters bodyParameters = {};
        bodyParameters.position = Vec3(100.0f, 20, 100.0f) + terrainParams.origin;
        bodyParameters.orientation = Quat();
        bodyParameters.shape = gameState->terrainShape;
        bodyParameters.type = RIGID_BODY_STATIC;
        bodyParameters.collidesWith = COLLISION_TYPE_DEFAULT;

        gameState->terrainBody = PhysicsCreateBody(
                memory->serverPhysicsSystem, bodyParameters);
    }

    ReadFileResult playerModelFile =
            memory->readEntireFile("../content/xbot.fbx");
    if (playerModelFile.memory != NULL)
    {
        // TODO: Use temp arena
        ModelImportData modelData =
                ImportModel(playerModelFile.memory, playerModelFile.size,
                            &gameState->assetArena, ModelImport_Hierarchy, 0.01f);
        //ScaleModelData(&modelData, 0.01f);
        Sk_CreateSkeleton(&gameState->playerSkeleton, &modelData, &gameState->assetArena);
        //LoadSkeletonPose(playerModelFile.memory, playerModelFile.size,
                //&gameState->playerBindPose, &gameState->assetArena, 0.01f);
        memory->freeFileMemory(playerModelFile.memory);
    }

    ReadFileResult runAnimationFile =
            memory->readEntireFile("../content/Running.fbx");
    if (runAnimationFile.memory != NULL)
    {
        AnimationImportData data = ImportAnimationData(runAnimationFile.memory,
                                                       runAnimationFile.size,
                                                       &gameState->assetArena, 0.01f);
        //ScaleAnimationData(&data, 0.01f);
        Sk_CreateAnimationClip(&gameState->playerRunAnimation, &data, &gameState->assetArena,
                &gameState->playerSkeleton, true);
        memory->freeFileMemory(runAnimationFile.memory);
        //CreateAnimationClip(runAnimationFile.memory, runAnimationFile.size,
                            //&gameState->playerRunAnimation,
                            //gameState->playerBindPose.skeleton, true,
                            //&gameState->assetArena, 0.01f);
    }

    ReadFileResult idleAnimationFile = memory->readEntireFile("../content/Idle.fbx");
    if (idleAnimationFile.memory != NULL)
    {
        AnimationImportData data = ImportAnimationData(idleAnimationFile.memory,
                                                       idleAnimationFile.size,
                                                       &gameState->assetArena, 0.01f);
        //ScaleAnimationData(&data, 0.01f);
        Sk_CreateAnimationClip(&gameState->playerIdleAnimation, &data, &gameState->assetArena,
                &gameState->playerSkeleton, true);
        memory->freeFileMemory(idleAnimationFile.memory);
        //CreateAnimationClip(idleAnimationFile.memory, idleAnimationFile.size,
                //&gameState->playerIdleAnimation, gameState->playerBindPose.skeleton, true,
                //&gameState->assetArena, 0.01f);
    }

    ReadFileResult walkingAnimationFile = memory->readEntireFile("../content/Walking.fbx");
    if (walkingAnimationFile.memory != NULL)
    {
        AnimationImportData data = ImportAnimationData(walkingAnimationFile.memory,
                                                       walkingAnimationFile.size,
                                                       &gameState->assetArena, 0.01f);
        //ScaleAnimationData(&data, 0.01f);
        Sk_CreateAnimationClip(&gameState->playerWalkAnimation, &data, &gameState->assetArena,
                &gameState->playerSkeleton, true);
        memory->freeFileMemory(walkingAnimationFile.memory);
        //CreateAnimationClip(walkingAnimationFile.memory, walkingAnimationFile.size,
                //&gameState->playerWalkAnimation, gameState->playerBindPose.skeleton, true,
                //&gameState->assetArena, 0.01f);
    }

    // FIXME: Only allocating enough for 4 players for now
    for (u32 i = 0; i < 4; ++i)
    {
        Sk_AnimationController *controller = gameState->playerAnimationControllers + i;
        Sk_InitializeAnimationController(controller,
            PlayerAnimation_Max, &gameState->playerSkeleton, &gameState->assetArena);

        Sk_InitialisePlaybackState(
                &controller->playbackStates[PlayerAnimation_Idle],
                &gameState->playerIdleAnimation, &gameState->assetArena);
        Sk_InitialisePlaybackState(
                &controller->playbackStates[PlayerAnimation_Walk],
                &gameState->playerWalkAnimation, &gameState->assetArena);
        Sk_InitialisePlaybackState(
                &controller->playbackStates[PlayerAnimation_Run],
                &gameState->playerRunAnimation, &gameState->assetArena);
    }

    gPlayerShape2 = gameState->playerShape;
}

#if 0
internal void UpdatePose(AnimationClipState *state, float globalT)
{
    AnimationClip clip = state->clip;
    float t = state->rate * (globalT - state->startT); // Map global clock to local clock
    // TODO: handle precision issues

    int sampleIndex0 = Floor(t * clip.framesPerSecond);
    int sampleIndex1 = sampleIndex0 + 1; // Avoid chance of sampleIndices being the same

    float secondsPerFrame = 1.0f / clip.framesPerSecond;
    float start = sampleIndex0 * secondsPerFrame;
    float lerpT = (t - start) / secondsPerFrame;

    int s0 = sampleIndex0 % (int)clip.frameCount;
    int s1 = sampleIndex1 % (int)clip.frameCount;
    if (t < 0.0f)
    {
        s0 = ((int)clip.frameCount + s0) % (int)clip.frameCount;
        s1 = ((int)clip.frameCount + s1) % (int)clip.frameCount;
    }

    AnimationSample sample0 = clip.samples[s0];
    AnimationSample sample1 = clip.samples[s1];

    LerpPose(sample0.pose, sample1.pose, lerpT, state->currentPose,
            clip.skeleton->boneCount);
}
#endif

internal void UpdatePlayerAnimationController(Sk_AnimationController *controller, float globalT, float playerSpeed)
{
    float walkBlendFactor = 0.3f;

    float blendFactor = playerSpeed / 8.0f;

    if (blendFactor < walkBlendFactor)
    {
        // Blend between walking and idle animation
        Sk_UpdatePlaybackState(
                &controller->playbackStates[PlayerAnimation_Idle], globalT);
        Sk_UpdatePlaybackState(
                &controller->playbackStates[PlayerAnimation_Walk], globalT);

        blendFactor = MapToUnitRange(blendFactor, 0.0f, walkBlendFactor);
        Sk_LerpPose(
                controller->currentPose, controller->skeleton->boneCount,
                controller->playbackStates[PlayerAnimation_Idle].currentPose,
                controller->playbackStates[PlayerAnimation_Walk].currentPose,
                blendFactor);
    }
    else
    {
        // Blend between walking and running animation
        Sk_UpdatePlaybackState(
                &controller->playbackStates[PlayerAnimation_Walk], globalT);
        Sk_UpdatePlaybackState(
                &controller->playbackStates[PlayerAnimation_Run], globalT);

        blendFactor = MapToUnitRange(blendFactor, walkBlendFactor, 1.0f);
        Sk_LerpPose(
                controller->currentPose, controller->skeleton->boneCount,
                controller->playbackStates[PlayerAnimation_Walk].currentPose,
                controller->playbackStates[PlayerAnimation_Run].currentPose,
                blendFactor);
    }
}

internal void UpdatePlayerHitBoxes(PhysicsSystem *physicsSystem, PlayerHitBoxes *hitBoxes,
        PlayerController *player, Sk_AnimationController *animationController,
        MemoryArena *tempArena, vec3 colour)
{
    mat4 modelMatrix = Translate(player->position - Vec3(0.0f, 1.0f, 0.0f)) *
                       RotateY(player->viewAngles.y - PI) * Scale(0.01f);

    Sk_Skeleton *skeleton = animationController->skeleton;
    mat4 *boneTransforms = AllocateArray(tempArena, mat4,
            skeleton->boneCount);

    Assert(animationController->currentPose);
    Sk_CalculatePoseWorldTransform(boneTransforms,
                                   animationController->currentPose,
                                   animationController->skeleton, tempArena);
    for (u32 boneIdx = 0; boneIdx < skeleton->boneCount; ++boneIdx)
    {
        Debug_DrawAxis(modelMatrix * boneTransforms[boneIdx] * Scale(0.1f));
    }

    u32 boneIndices[MaxPlayerHitBoxes];
    boneIndices[PlayerHitBox_Head] = 9;
    boneIndices[PlayerHitBox_Body] = 6;
    boneIndices[PlayerHitBox_LeftUpperArm] = 11;
    boneIndices[PlayerHitBox_LeftLowerArm] = 12;
    boneIndices[PlayerHitBox_RightUpperArm] = 30;
    boneIndices[PlayerHitBox_RightLowerArm] = 31;
    boneIndices[PlayerHitBox_LeftUpperLeg] = 48;
    boneIndices[PlayerHitBox_LeftLowerLeg] = 49;
    boneIndices[PlayerHitBox_RightUpperLeg] = 52;
    boneIndices[PlayerHitBox_RightLowerLeg] = 53;


    Sk_BonePose childPoses[MaxPlayerHitBoxes];
    for (u32 i = 0; i < MaxPlayerHitBoxes; ++i)
    {
        childPoses[i] = animationController->currentPose[boneIndices[i] + 1];
    }
    //auto p = animationController->currentPose[boneIndices[PlayerHitBox_LeftUpperLeg]+1];
    //auto p2 = animationController->currentPose[boneIndices[PlayerHitBox_LeftLowerLeg]+1];

    //auto p3 = animationController->currentPose[boneIndices[PlayerHitBox_LeftUpperArm]+1];
    //auto p4 = animationController->currentPose[boneIndices[PlayerHitBox_LeftLowerArm]+1];

    mat4 localTransforms[MaxPlayerHitBoxes];
    localTransforms[PlayerHitBox_Head] = Translate(Vec3(0, 0.05, 0));
    localTransforms[PlayerHitBox_Body] = Identity();

    for (u32 i = PlayerHitBox_LeftUpperArm; i <= PlayerHitBox_RightLowerArm; ++i)
    {
        localTransforms[i] = Translate(childPoses[i].translation * 0.5f) * RotateZ(PI * 0.5f);
    }

    for (u32 i = PlayerHitBox_LeftUpperLeg; i <= PlayerHitBox_RightLowerLeg; ++i)
    {
        localTransforms[i] = Translate(childPoses[i].translation * 0.5f);
    }

    vec3 worldPositions[MaxPlayerHitBoxes];
    quat worldOrientations[MaxPlayerHitBoxes];
    for (u32 i = 0; i < MaxPlayerHitBoxes; ++i)
    {
        mat4 worldTransform = modelMatrix * boneTransforms[boneIndices[i]] * localTransforms[i];
        DecomposeRotationAndTranslation(worldTransform, worldOrientations + i, worldPositions + i);
    }

    float radi[MaxPlayerHitBoxes];
    radi[PlayerHitBox_Head]          = 0.1f;
    radi[PlayerHitBox_Body]          = 0.12f;
    radi[PlayerHitBox_LeftUpperArm]  = 0.05f;
    radi[PlayerHitBox_LeftLowerArm]  = 0.05f;
    radi[PlayerHitBox_RightUpperArm] = 0.05f;
    radi[PlayerHitBox_RightLowerArm] = 0.05f;
    radi[PlayerHitBox_LeftUpperLeg]  = 0.08f;
    radi[PlayerHitBox_LeftLowerLeg]  = 0.08f;
    radi[PlayerHitBox_RightUpperLeg] = 0.08f;
    radi[PlayerHitBox_RightLowerLeg] = 0.08f;

    float height[MaxPlayerHitBoxes];
    height[PlayerHitBox_Head]          = 0.15f;
    height[PlayerHitBox_Body]          = 0.38f;
    height[PlayerHitBox_LeftUpperArm]  = 0.3f;
    height[PlayerHitBox_LeftLowerArm]  = 0.3f;
    height[PlayerHitBox_RightUpperArm] = 0.3f;
    height[PlayerHitBox_RightLowerArm] = 0.3f;
    height[PlayerHitBox_LeftUpperLeg]  = 0.3f;
    height[PlayerHitBox_LeftLowerLeg]  = 0.3f;
    height[PlayerHitBox_RightUpperLeg] = 0.3f;
    height[PlayerHitBox_RightLowerLeg] = 0.3f;

    if (!hitBoxes->isValid)
    {
        for (u32 i = 0; i < MaxPlayerHitBoxes; ++i)
        {
            PhysicsShapeParameters shapeParameters = {};
            shapeParameters.type = PHYSICS_CAPSULE_SHAPE;
            shapeParameters.radius = radi[i];
            shapeParameters.height = height[i];

            PhysicsShapeHandle shape = PhysicsCreateShape(physicsSystem, shapeParameters);

            PhysicsBodyParameters bodyParameters = {};
            bodyParameters.position = worldPositions[i];
            bodyParameters.orientation = Quat();
            bodyParameters.shape = shape;
            bodyParameters.type = RIGID_BODY_KINEMATIC;
            bodyParameters.collidesWith = COLLISION_TYPE_DEFAULT;

            hitBoxes->handles[i] =
                    PhysicsCreateBody(physicsSystem, bodyParameters);
        }
        hitBoxes->isValid = true;
    }
    else
    {
        PhysicsUpdateKinematicBodyParameters updateParameters = {};
        updateParameters.handles = hitBoxes->handles;
        updateParameters.positions = worldPositions;
        updateParameters.orientations = worldOrientations;
        updateParameters.count = MaxPlayerHitBoxes;

        PhysicsUpdateKinematicBodies(physicsSystem, &updateParameters);
    }


    MemoryArenaFree(tempArena, boneTransforms);
}

internal void DoShooting(ServerGameState *gameState, PlayerController *owner, u32 index, float dt)
{
    // FIXME: global for eyeOffset from player position
    vec3 rayStart = owner->position + Vec3(0, 0.6, 0); // game.cpp : Render()
    vec3 rayDirection = Normalize( Vec3( -Sin( owner->viewAngles.y ),
                Tan( owner->viewAngles.x ),
                -Cos( owner->viewAngles.y ) ) );
    float rayDistance = 100.0f;

    BulletSystem_Spawn(&gameState->bulletSystem, rayStart, rayDirection, 20000.0f, dt,
            index, 60.0f);
}

internal void com_AddItemToInventory(InventoryState *inventory, u32 type, u32 quantity = 1)
{
    bool found = false;
    for (u32 i = 0; i < inventory->count; ++i)
    {
        if (inventory->items[i].type == type)
        {
            found = true;
            inventory->items[i].quantity += quantity;
            //LOG_DEBUG("Adding to existing inventory slot for item %u %u",
                    //type, inventory->items[i].quantity);
            break;
        }
    }

    if (!found)
    {
        //LOG_DEBUG("New item slot added to inventory for item %u %u",
                //type, quantity);
        Assert(inventory->count < MAX_INVENTORY_ITEMS);
        InventoryItem *item = inventory->items + inventory->count++;
        item->type = type;
        item->quantity = quantity;
    }
}

internal void sv_AddItemToInventory(ServerGameState *gameState, u32 inventoryIndex,
        u32 itemType, u32 itemQuantity = 1)
{
    Assert(gameState->eventCount < ArrayCount(gameState->events));
    GameEvent event = {};
    event.type = GameEvent_AddItemToInventory;
    event.addItemToInventory.inventoryIndex = inventoryIndex;
    event.addItemToInventory.itemType = itemType;
    event.addItemToInventory.itemQuantity = itemQuantity;
    gameState->events[gameState->eventCount++] = event;

    InventoryState *inventory = &gameState->playerInventoryStates[inventoryIndex];
    com_AddItemToInventory(inventory, itemType, itemQuantity);
}

internal void HandlePacketReceivedEvent(ServerGameState *gameState, u32 clientId, u8 *packetData, u32 packetLength)
{
    //LOG_DEBUG("Received packet from client %u containing %u bytes of data.",
            //clientId, packetLength);
    Bitstream inPacketBitstream;
    bitstream_InitForReading(&inPacketBitstream, packetData, packetLength);

    PacketHeader inPacketHeader = {};
    if (bitstream_ReadValue(&inPacketBitstream, &inPacketHeader))
    {
        // Check if client id belongs to an already connected client
        ServerClientState *client = NULL;
        for (u32 clientIndex = 0; clientIndex < gameState->clientCount; ++clientIndex)
        {
            if (gameState->clients[clientIndex].clientId == clientId)
            {
                client = gameState->clients + clientIndex;
                break;
            }
        }

        if (client == NULL)
        {
            if (inPacketHeader.dataType == PacketDataType_ConnectionRequest)
            {
                b32 acceptConnection = false;
                ConnectionRequestData connectionRequest = {};
                if (bitstream_ReadValue(&inPacketBitstream, &connectionRequest))
                {
                    if (connectionRequest.version == 1)
                    {
                        acceptConnection = true;
                    }
                    else
                    {
                        LOG_WARN("Connection refused - version mismatch");
                    }
                }

                if (acceptConnection)
                {
                    if (gameState->clientCount < ArrayCount(gameState->clients))
                    {
                        // Allocate new client
                        client = gameState->clients + gameState->clientCount++;
                        client->clientId = clientId;
                        client->nextSequenceNumber = 1;
                        client->expectedSequenceNumber = inPacketHeader.sequenceNumber + 1;
                        client->connectionState = ClientConnectionState_Connecting;
                        InitAckSystem(&client->ackSystem);
                        if (!OnReceivePacketFromRemote(&client->ackSystem, inPacketHeader.sequenceNumber))
                        {
                            Assert(!"Ack system not cleared for new client");
                        }
                        // Send Acknowledge inPacketHeader.sequenceNumber
                        strncpy(client->identifier, connectionRequest.identifier, ArrayCount(client->identifier));
                        LOG_DEBUG("New client '%s' connected!", client->identifier);
                        LOG_DEBUG("New connection accepted for client %u", clientId);
                        client->playerControllerIndex = AllocatePlayerController(gameState);

                        // Start game as we have at least one player
                        if (gameState->state == ServerGameState_WaitingForPlayers)
                        {
                            gameState->state = ServerGameState_Ready;
                        }
                        // TODO: Generate client connected networking event
                    }
                    else
                    {
                        // Refuse connection - server is full
                        LOG_WARN("Connection refused - server is full");
                        // TODO: Send server is full reply
                    }
                }
            }
            else
            {
                LOG_WARN("Connection refused - invalid connection packet");
            }
        }
        else
        {
            if (inPacketHeader.sequenceNumber >= client->expectedSequenceNumber)
            {
                client->expectedSequenceNumber = inPacketHeader.sequenceNumber + 1;
                if (OnReceivePacketFromRemote(&client->ackSystem, inPacketHeader.sequenceNumber))
                {
                    for (u32 i = 0; i < NET_PACKET_MAX_ACKS; ++i)
                    {
                        u16 ack = inPacketHeader.acks[i];
                        if (ack != 0)
                        {
                            OnPacketReceivedByRemote(&client->ackSystem, ack); 
                        }
                    }
                    client->inTimestamp = inPacketHeader.timestamp;
                    if (inPacketHeader.dataType == PacketDataType_GameClientCommand)
                    {
                        GameClientCommandData gameClientCommand = {};
                        if (bitstream_ReadValue(&inPacketBitstream, &gameClientCommand) )
                        {
                            // Determine dt to use, this may be too difficult and we will need to use a fixed
                            // dt for sampling client input
                            PlayerController *playerController = gameState->playerControllers +
                                client->playerControllerIndex;
                            float fixedDt = 1.0f / 60.0f; // Assuming 60Hz client send rate
                            UpdatePlayer(gameState->physicsSystem, playerController,
                                    gameClientCommand.playerCommand, fixedDt);

                            if (gameClientCommand.playerCommand.actions & PlayerAction_PrimaryFire)
                            {
                                u32 index = client->playerControllerIndex;
                                float lastShootTime = gameState->playerLastShootTime[index];
                                if (gameState->currentTime - lastShootTime > 0.2f)
                                {
                                    Assert(gameState->eventCount < ArrayCount(gameState->events));
                                    GameEvent event = {};
                                    event.type = GameEvent_GunShot;
                                    event.gunShot.position = playerController->position;
                                    gameState->events[gameState->eventCount++] = event;

                                    DoShooting(gameState, playerController, index, fixedDt);
                                    gameState->playerLastShootTime[index] = (float)gameState->currentTime;
                                }

                                sv_AddItemToInventory(gameState, index, InventoryItem_Wood, 10);
                            }
                        }
                    }
                }
            }
            else
            {
                LOG_DEBUG("Drop out of order packet %u", inPacketHeader.sequenceNumber);
            }
        }
    }
    else
    {
        LOG_WARN("Invalid packet header");
    }
}

internal void InitializeGame(ServerGameState *gameState, GameMemory *memory)
{
    size_t total = memory->persistentStorageSize - sizeof(ServerGameState);
    size_t testArenaSize = (64 * 1024);
    Assert(total > testArenaSize);
    total -= testArenaSize;
    MemoryArenaInitialize(&gameState->testArena, testArenaSize,
                          (u8 *)memory->persistentStorageBase +
                                  sizeof(ServerGameState));

    MemoryArenaInitialize(&gameState->assetArena,
                          memory->persistentStorageSize - sizeof(ServerGameState) -
                                  testArenaSize,
                          (u8 *)memory->persistentStorageBase +
                                  sizeof(ServerGameState) + testArenaSize);

    gameState->state = ServerGameState_WaitingForPlayers;


    PhysicsShapeParameters params = {};
    params.type = PHYSICS_CAPSULE_SHAPE;
    params.radius = 1.0f;
    params.height = 2.0f;
    //params.type = PHYSICS_BOX_SHAPE;
    //params.dimensions = Vec3(1, 2, 1);
    gameState->playerShape = PhysicsCreateShape(memory->serverPhysicsSystem, params);

    {
        PhysicsShapeParameters shapeParameters = {};
        shapeParameters.type = PHYSICS_BOX_SHAPE;
        shapeParameters.dimensions = Vec3(100.0f, 0.2f, 100.0f);
        PhysicsShapeHandle shape = PhysicsCreateShape(memory->serverPhysicsSystem,
                shapeParameters);

        PhysicsBodyParameters bodyParameters = {};
        bodyParameters.position = Vec3(0.0f);
        bodyParameters.orientation = quat();
        bodyParameters.shape = shape;
        bodyParameters.type = RIGID_BODY_STATIC;
        bodyParameters.collidesWith = COLLISION_TYPE_ALL;
        PhysicsBodyHandle body = PhysicsCreateBody(memory->serverPhysicsSystem,
                bodyParameters);
    }

    {
        PhysicsShapeParameters shapeParameters = {};
        shapeParameters.type = PHYSICS_BOX_SHAPE;
        shapeParameters.dimensions = Vec3(1.5f);
        PhysicsShapeHandle shape = PhysicsCreateShape(memory->serverPhysicsSystem,
                shapeParameters);

        PhysicsBodyParameters bodyParameters = {};
        bodyParameters.position = Vec3(0.0f, 10.0f, 0.0f);
        bodyParameters.orientation = quat();
        bodyParameters.shape = shape;
        bodyParameters.mass = 10.0f;
        bodyParameters.type = RIGID_BODY_DYNAMIC;
        bodyParameters.collidesWith = COLLISION_TYPE_ALL;
        PhysicsBodyHandle body = PhysicsCreateBody(memory->serverPhysicsSystem,
                bodyParameters);
    }
}

extern "C" GAME_SERVER_UPDATE(GameServerUpdate)
{
    Assert(sizeof(ServerGameState) < memory->persistentStorageSize);
    ServerGameState *gameState = (ServerGameState *)memory->persistentStorageBase;

    if (!memory->isInitialized)
    {
        InitializeGame(gameState, memory);
        ReloadContent(gameState, memory);
        memory->isInitialized = true;
    }
    else if (memory->wasCodeReloaded)
    {
        ReloadContent(gameState, memory);
    }

    gameState->physicsSystem = memory->serverPhysicsSystem;

    gameState->eventCount = 0;

    for (u32 i = 0; i < count; ++i)
    {
        switch (events[i].type)
        {
        case PlatformEvent_PacketReceived:
        {
            u32 clientId = events[i].packetReceived.addressId;
            u32 packetLength = events[i].packetReceived.packetLength;
            void *packetData = events[i].packetReceived.packetData;

            HandlePacketReceivedEvent(gameState, clientId, (u8*)packetData, packetLength);
            break;
        }
        default:
            break;
        }
    }

    for ( u32 clientIndex = 0; clientIndex < gameState->clientCount; ++clientIndex)
    {
        auto client = gameState->clients + clientIndex;
        u16 droppedPackets[NET_MAX_INFLIGHT_PACKETS];
        u32 droppedPacketsCount = DropUnacknowledgedPackets(&client->ackSystem, droppedPackets,
                ArrayCount(droppedPackets), dt);
        // TODO: Generate dropped packet event
    }

    gameState->bulletSystem.gravity = Vec3(0, -50, 0);
    BulletImpactEvent impactEvents[MAX_BULLETS];
    u32 impactEventCount = BulletSystem_Update(&gameState->bulletSystem,
            gameState->physicsSystem, dt,
            impactEvents, ArrayCount(impactEvents));
    for (u32 impactEventIndex = 0; impactEventIndex < impactEventCount; ++impactEventIndex)
    {
        BulletImpactEvent *impactEvent = impactEvents + impactEventIndex;

        Assert(gameState->eventCount < ArrayCount(gameState->events));
        GameEvent event = {};
        event.type = GameEvent_BulletImpact;
        event.bulletImpact.position = impactEvent->hitPoint;
        event.bulletImpact.normal  = impactEvent->hitNormal;
        gameState->events[gameState->eventCount++] = event;

        for (u32 playerIndex = 0; playerIndex < gameState->playerControllerCount; ++playerIndex)
        {
            PlayerController *playerController = gameState->playerControllers + playerIndex;
            Assert(impactEvent->hitBody != PHYSICS_INVALID_HANDLE);
            PlayerHitBoxes *playerHitBoxes = gameState->playerHitBoxes + playerIndex;
            for (u32 hitBoxIndex = 0; hitBoxIndex < MaxPlayerHitBoxes; ++hitBoxIndex)
            {
                if (impactEvent->hitBody == playerHitBoxes->handles[hitBoxIndex])
                {
                    LOG_DEBUG("Player %u hit on hitbox '%s'", playerIndex,
                            gPlayerHitBoxNames[hitBoxIndex]);
                    gameState->playerHealth[playerIndex] -= 40.0f;

                    Assert(gameState->eventCount < ArrayCount(gameState->events));
                    GameEvent hitMarkerEvent = {};
                    hitMarkerEvent.type = GameEvent_HitMarker;
                    hitMarkerEvent.hitMarker.owner = impactEvent->owner;
                    gameState->events[gameState->eventCount++] = hitMarkerEvent;
                    break;
                }
            }
        }

    }

    float fixedDt = 1.0f / 60.0f;
    PhysicsUpdate(memory->serverPhysicsSystem, fixedDt);

    gameState->currentTick += 1;
    gameState->currentTime = gameState->currentTick * dt; // dt is fixed

    Assert(gameState->playerControllerCount < 4); // Was too lazy to allocate more animation controllers
    for ( u32 i = 0; i < gameState->playerControllerCount; ++i)
    {
        PlayerController *playerController = gameState->playerControllers + i;
        Sk_AnimationController *animationController = gameState->playerAnimationControllers + i;
        PlayerHitBoxes *playerHitBoxes = gameState->playerHitBoxes + i;
        UpdatePlayerAnimationController(animationController, (float)gameState->currentTime, Length(playerController->velocity));
        UpdatePlayerHitBoxes(gameState->physicsSystem, playerHitBoxes, playerController, animationController, &gameState->testArena, Vec3(0.2, 0.4, 1.0));
    }

    GameSnapshotData gameSnapshot = {};
    gameSnapshot.currentTick = gameState->currentTick;
    Assert(gameState->playerControllerCount < ArrayCount(gameSnapshot.playerPositions));
    gameSnapshot.playerCount = gameState->playerControllerCount;
    for (u32 i = 0; i < gameSnapshot.playerCount; ++i)
    {
        gameSnapshot.playerPositions[i] = gameState->playerControllers[i].position;
        gameSnapshot.playerViewAngles[i] = gameState->playerControllers[i].viewAngles;
        gameSnapshot.playerVelocities[i] = gameState->playerControllers[i].velocity;
        gameSnapshot.playerHealth[i] = gameState->playerHealth[i];
    }

    for (u32 i = 0; i < gameState->eventCount; ++i)
    {
        if ( gameSnapshot.eventCount == ArrayCount(gameSnapshot.events))
            break;

        gameSnapshot.events[gameSnapshot.eventCount++] = gameState->events[i];
    }

    u8 packetBuffer[400];
    for (u32 i = 0; i < gameState->clientCount; ++i)
    {
        ServerClientState *client = gameState->clients + i;

        Bitstream packetBitstream;
        bitstream_InitForWriting(&packetBitstream, packetBuffer, sizeof(packetBuffer));

        PacketHeader outPacketHeader = {};
        outPacketHeader.returnTimestamp = client->inTimestamp;
        outPacketHeader.sequenceNumber = client->nextSequenceNumber++;
        // Inflight packets have a lifetime of 1 second
        AddInFlightPacket(&client->ackSystem, outPacketHeader.sequenceNumber, 1.0f);
        GetAcksToSend(&client->ackSystem, outPacketHeader.acks, ArrayCount(outPacketHeader.acks));
        outPacketHeader.dataType = 
            (client->connectionState == ClientConnectionState_Connecting) ?
            (u8)PacketDataType_ConnectionResponse :
            (u8)PacketDataType_GameSnapshot;
        //LOG_DEBUG("returnTimestamp: %u", client->inTimestamp);
        bitstream_WriteValue(&packetBitstream, &outPacketHeader);

        if (client->connectionState == ClientConnectionState_Connecting)
        {
            ConnectionResponseData outResponse = {};
            outResponse.response = ConnectionResponse_Ok;
            bitstream_WriteValue(&packetBitstream, &outResponse);

            client->connectionState = ClientConnectionState_Connected;
        }
        else
        {
            gameSnapshot.playerIndex = client->playerControllerIndex;
            bitstream_WriteValue(&packetBitstream, &gameSnapshot);
        }

        memory->sendPacketToClient(packetBuffer,
                bitstream_GetLengthInBytes(&packetBitstream), client->clientId);
        ClearToZero(packetBuffer, bitstream_GetLengthInBytes(&packetBitstream));
        packetBitstream.size = 0;
    }

}
