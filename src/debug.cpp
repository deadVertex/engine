#include "debug.h"

enum
{
    DebugElement_Line,
    DebugElement_Point,
    DebugElement_Axis,
};

struct DebugElement
{
    u32 type;
    float timeRemaining;
    union
    {
        struct
        {
            vec3 start;
            vec3 end;
            vec3 colour;
        } line;

        struct
        {
            vec3 position;
            vec3 colour;
            float scale;
        } point;

        struct
        {
            mat4 transform;
        } axis;
    };
};

struct DebugLineVertex
{
    vec3 position;
    vec3 colour;
};

struct DebugSystem
{
    DebugElement elements[4092];
    u32 count;
};

internal void Debug_DrawLine(DebugSystem *debugSystem, vec3 start, vec3 end, vec3 colour, float lifetime)
{
    if (debugSystem->count < ArrayCount(debugSystem->elements))
    {
        DebugElement *element = debugSystem->elements + debugSystem->count++;
        element->type = DebugElement_Line;
        element->timeRemaining = lifetime;
        element->line.start = start;
        element->line.end = end;
        element->line.colour = colour;
    }
}

internal void Debug_DrawPoint(DebugSystem *debugSystem, vec3 position, vec3 colour, float scale, float lifetime)
{
    if (debugSystem->count < ArrayCount(debugSystem->elements))
    {
        DebugElement *element = debugSystem->elements + debugSystem->count++;
        element->type = DebugElement_Point;
        element->timeRemaining = lifetime;
        element->point.position = position;
        element->point.scale = scale;
        element->point.colour = colour;
    }
}

internal void Debug_DrawAxis(DebugSystem *debugSystem, mat4 transform, float lifetime)
{
    if (debugSystem->count < ArrayCount(debugSystem->elements))
    {
        DebugElement *element = debugSystem->elements + debugSystem->count++;
        element->type = DebugElement_Axis;
        element->timeRemaining = lifetime;
        element->axis.transform = transform;
    }
}

internal void Debug_Cleanup(DebugSystem *debugSystem, float dt)
{
    u32 i = 0;
    while (i < debugSystem->count)
    {
        DebugElement *element = debugSystem->elements + i;
        if (element->timeRemaining < 0.0f)
        {
            // Free object
            DebugElement *lastElement = debugSystem->elements + (--debugSystem->count);
            *element = *lastElement;
            // NOTE: Run loop again with same value because new value is in its place.
        }
        else
        {
            element->timeRemaining -= dt;
            i++;
        }
    }
}

inline void Debug_AddLineVertices(OpenGL_DynamicMesh *mesh, vec3 start, vec3 end, vec3 colour)
{
    if (mesh->numVertices + 2 < mesh->maxVertices)
    {
        DebugLineVertex p0, p1;
        p0.position = start;
        p0.colour = colour;

        p1.position = end;
        p1.colour = colour;

        DebugLineVertex *vertices = (DebugLineVertex*)mesh->vertices;
        vertices[mesh->numVertices++] = p0;
        vertices[mesh->numVertices++] = p1;
    }
}

internal u32 Debug_Render(DebugSystem *debugSystem, OpenGL_DynamicMesh *linesMesh,
        mat4 *axisTransforms, u32 axisCount)
{
    u32 count = 0;
    for (u32 i = 0; i < debugSystem->count; ++i)
    {
        DebugElement *element = debugSystem->elements + i;
        switch (element->type)
        {
            case DebugElement_Line:
                Debug_AddLineVertices(linesMesh,
                        element->line.start, element->line.end, element->line.colour);
                break;
            case DebugElement_Point:
                {
                    vec3 p = element->point.position;
                    float s = element->point.scale;
                    Debug_AddLineVertices(linesMesh, p - Vec3(s, 0.0f, 0.0f),
                            p + Vec3(s, 0.0f, 0.0f), element->point.colour);
                    Debug_AddLineVertices(linesMesh, p - Vec3(0.0f, s, 0.0f),
                            p + Vec3(0.0f, s, 0.0f), element->point.colour);
                    Debug_AddLineVertices(linesMesh, p - Vec3(0.0f, 0.0f, s),
                            p + Vec3(0.0f, 0.0f, s), element->point.colour);
                }
                break;
            case DebugElement_Axis:
                if (count < axisCount)
                {
                    axisTransforms[count++] = element->axis.transform;
                }
                break;
            default:
                break;
        }
    }

    return count;
}
