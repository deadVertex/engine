#pragma once

#include "utils.h"

#include "input.h"

#include "bullet_physics.h"

struct ReadFileResult
{
    void *memory;
    u32 size;
};

#define DEBUG_READ_ENTIRE_FILE(NAME) ReadFileResult NAME(const char *path)
typedef DEBUG_READ_ENTIRE_FILE(DebugReadEntireFileFunction);

#define DEBUG_FREE_FILE_MEMORY(NAME) void NAME(void *memory)
typedef DEBUG_FREE_FILE_MEMORY(DebugFreeFileMemoryFunction);

#define DEBUG_GET_TIME(NAME) double NAME()
typedef DEBUG_GET_TIME(DebugGetTimeFunction);

enum
{
    MOUSE_CURSOR_VISIBLE,
    MOUSE_CURSOR_HIDDEN
};
#define SET_MOUSE_CURSOR(NAME) void NAME(u8 cursor)
typedef SET_MOUSE_CURSOR(SetMouseCursorFunction);

#define SEND_PACKET_TO_SERVER(NAME) void NAME(u8 *data, u32 length)
typedef SEND_PACKET_TO_SERVER(SendPacketToServerFunction);

#define SEND_PACKET_TO_CLIENT(NAME) void NAME(u8 *data, u32 length, u32 clientId)
typedef SEND_PACKET_TO_CLIENT(SendPacketToClientFunction);

#define PERFORM_PLATFORM_OP(NAME) void NAME(u32 op)
typedef PERFORM_PLATFORM_OP(PerformPlatformOpFunction);

enum
{
    PlatformOp_StartServer,
    PlatformOp_ConnectToServer,
    PlatformOp_Exit,
};

struct GameMemory
{
    bool isInitialized;
    void *persistentStorageBase;
    size_t persistentStorageSize;

    bool wasCodeReloaded;

    DebugReadEntireFileFunction *readEntireFile;
    DebugFreeFileMemoryFunction *freeFileMemory;
    DebugGetTimeFunction *getTime;
    SetMouseCursorFunction *setMouseCursor;
    SendPacketToServerFunction *sendPacketToServer;
    SendPacketToClientFunction *sendPacketToClient;
    PerformPlatformOpFunction *performPlatformOp;

    PhysicsSystem *clientPhysicsSystem;
    PhysicsSystem *serverPhysicsSystem;
};

enum
{
    PlatformEvent_KeyPress,
    PlatformEvent_KeyRelease,
    PlatformEvent_MouseMotion,
    PlatformEvent_PacketReceived,
    PlatformEvent_Character,


    // TODO: Replace this with command system
    PlatformEvent_StartServerArg,
    PlatformEvent_ConnectToArg,
};

struct PlatformEvent
{
    u8 type;
    union {
        u8 key;
        u32 character;

        struct
        {
            double x;
            double y;
        } mouseMotion;

        struct
        {
            u8 *packetData;
            u16 packetLength;
            u32 addressId;
        } packetReceived;
    };
};

#define GAME_UPDATE(NAME)                                                      \
    void NAME(GameMemory *memory, float dt, PlatformEvent *events, u32 count,  \
              u32 windowWidth, u32 windowHeight)
typedef GAME_UPDATE(GameUpdateFunction);

extern "C" GAME_UPDATE(GameUpdate);

#define GAME_SERVER_UPDATE(NAME) void NAME(GameMemory *memory, float dt, PlatformEvent *events, u32 count)
typedef GAME_SERVER_UPDATE(GameServerUpdateFunction);

extern "C" GAME_SERVER_UPDATE(GameServerUpdate);
