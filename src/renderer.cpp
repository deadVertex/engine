#undef global
#undef internal
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <vector>

#define internal static
#define global static

#if !defined( STB_RECT_PACK_IMPLEMENTATION )
#define STB_RECT_PACK_IMPLEMENTATION
#include <stb_rect_pack.h>
#endif

#if !defined( STB_TRUETYPE_IMPLEMENTATION )
#define STB_TRUETYPE_IMPLEMENTATION
#include <stb_truetype.h>
#endif

#include "cubemap.cpp"

const char *colourVertexSource =
"#version 330\n"
"layout (location=0) in vec3 vertexPosition;\n"
"uniform mat4 mvp = mat4(1);"
"void main()"
"{"
"  gl_Position = mvp * vec4(vertexPosition, 1.0);\n"
"}\n";

const char *colourFragmentSource =
"#version 330\n"
"out vec4 outputColour;"
"uniform vec4 colour;"
"void main()"
"{"
"  outputColour = colour;"
"}\n";

const char *vertexColourVertexSource =
"#version 330\n"
"layout (location=0) in vec3 vertexPosition;\n"
"layout (location=3) in vec4 vertexColour;\n"
"uniform mat4 mvp = mat4(1);"
"out vec4 colour;"
"void main()"
"{"
"  colour = vertexColour;\n"
"  gl_Position = mvp * vec4(vertexPosition, 1.0);\n"
"}\n";

const char *vertexColourFragmentSource =
"#version 330\n"
"out vec4 outputColour;\n"
"in vec4 colour;\n"
"void main()\n"
"{\n"
"  outputColour = colour;\n"
"}\n";

const char *diffuseTextureVertexSource =
"#version 330\n"
"layout(location = 0) in vec3 vertexPosition;\n"
"layout (location=1) in vec3 vertexNormal;\n"
"layout(location = 2) in vec2 vertexTextureCoordinate;\n"
"uniform mat4 mvp = mat4(1);\n"
"uniform vec2 textureScaling = vec2(1);\n"
"out vec2 textureCoordinate;\n"
"out vec3 normal;\n"
"void main()\n"
"{\n"
"   textureCoordinate = textureScaling * vertexTextureCoordinate;\n"
"   normal = vertexNormal;\n"
"   gl_Position = mvp * vec4( vertexPosition, 1.0 );\n"
"}\n";

const char *diffuseTextureFragmentSource =
"#version 330\n"
"in vec2 textureCoordinate;\n"
"in vec3 normal;\n"
"uniform sampler2D diffuseTexture;\n"
"out vec4 outputColour;\n"
"void main()\n"
"{\n"
"  vec3 lightDirection = normalize(vec3(0.5, -1, -0.3));\n"
"  float nDotL = max(0.0,dot(normal, -lightDirection));\n"
"  nDotL = 0.5 * nDotL + 0.5;\n"
"  vec3 colour = texture2D( diffuseTexture, textureCoordinate.st ).rgb;\n"
"  outputColour = vec4(colour * nDotL, 1.0);\n"
"}\n";

const char *particleTextureVertexSource =
"#version 330\n"
"layout(location = 0) in vec3 vertexPosition;\n"
"layout(location = 2) in vec2 vertexTextureCoordinate;\n"
"uniform mat4 mvp = mat4(1);\n"
"uniform vec2 textureScaling = vec2(1);\n"
"out vec2 textureCoordinate;\n"
"void main()\n"
"{\n"
"   textureCoordinate = textureScaling * vertexTextureCoordinate;\n"
"   gl_Position = mvp * vec4( vertexPosition, 1.0 );\n"
"}\n";

const char *particleTextureFragmentSource =
"#version 330\n"
"in vec2 textureCoordinate;\n"
"uniform sampler2D colourTexture;\n"
"uniform vec4 mixColour = vec4(1.0);\n"
"out vec4 outputColour;\n"
"void main()\n"
"{\n"
"   vec4 colour = texture2D( colourTexture, textureCoordinate.st ).rgba;\n"
"   outputColour = colour * mixColour;\n"
"}\n";

const char *colourDiffuseVertexSource =
"#version 330\n"
"layout (location=0) in vec3 vertexPosition;\n"
"layout (location=1) in vec3 vertexNormal;\n"
"uniform mat4 mvp = mat4(1);"
"out vec3 normal;\n"
"void main()"
"{"
"  normal = vertexNormal;\n"
"  gl_Position = mvp * vec4(vertexPosition, 1.0);\n"
"}\n";

const char *colourDiffuseFragmentSource =
"#version 330\n"
"in vec3 normal;\n"
"out vec4 outputColour;"
"uniform vec4 colour;"
"void main()"
"{"
"  vec3 lightDirection = normalize(vec3(0.5, -1, -0.3));\n"
"  float nDotL = max(0.0,dot(normal, -lightDirection));\n"
"  nDotL = 0.5 * nDotL + 0.5;\n"
"  outputColour = colour * nDotL;\n"
"  outputColour.a = 1.0;"
"}\n";

const char *skinningVertexSource =
"#version 330\n"
"const int MAX_BONES = 255;\n"
"layout (location=0) in vec3 vertexPosition;\n"
"layout (location=1) in vec3 vertexNormal;\n"
"layout (location=2) in vec2 vertexTextureCoord;\n"
"layout (location=4) in uint vertexBoneIds;\n"
"layout (location=5) in vec3 vertexWeights;\n"
"uniform mat4 mvp = mat4(1);\n"
"uniform mat4 boneTransforms[MAX_BONES];\n"
"out vec4 debugColour;\n"
"out vec3 normal;\n"
"void main()"
"{"
"  mat4 totalTransform = mat4(0);\n"
"  uint boneId0 = (vertexBoneIds & uint(0xFF000000)) >> 24;\n"
"  uint boneId1 = (vertexBoneIds & uint(0x00FF0000)) >> 16;\n"
"  uint boneId2 = (vertexBoneIds & uint(0x0000FF00)) >>  8;\n"
"  uint boneId3 =  vertexBoneIds & uint(0x000000FF);\n"
"  //debugColour = (boneId3 > 0u) ? vec4(1,0,0,1) : vec4(0,0,1,1);\n"
"  //debugColour = vec4(float(boneId0) / 60.0, float(boneId1) / 60.0, float(boneId2) / 60.0, 1);\n"
"  totalTransform += boneTransforms[boneId0] * vertexWeights[0];\n"
"  totalTransform += boneTransforms[boneId1] * vertexWeights[1];\n"
"  totalTransform += boneTransforms[boneId2] * vertexWeights[2];\n"
"  float weightSum = vertexWeights[0] + vertexWeights[1] + vertexWeights[2];\n"
"  float implicitWeight = 1.0 - weightSum;\n"
"  //debugColour = vec4(vertexWeights[0], vertexWeights[1], vertexWeights[2], 1.0);\n"
"  //debugColour = vec4(float(boneId0) / 20.0, float(boneId0) / 40.0, float(boneId0) / 60.0, 1);\n"
"  totalTransform += boneTransforms[boneId3] * implicitWeight;\n"
"  normal = normalize(vec3(totalTransform * vec4(vertexNormal, 0)));\n"
"  gl_Position = mvp * totalTransform * vec4(vertexPosition, 1);\n"
"}\n";

const char *skinningFragmentSource =
"#version 330\n"
"in vec4 debugColour;"
"in vec3 normal;\n"
"out vec4 outputColour;"
"uniform vec4 colour;"
"void main()"
"{"
"  vec3 lightDirection = normalize(vec3(0.5, -1, -0.3));\n"
"  float nDotL = max(0.0,dot(normal, -lightDirection));\n"
"  nDotL = 0.5 * nDotL + 0.5;\n"
"  outputColour = colour * nDotL;\n"
"  outputColour.a = 1.0;"
"}\n";

const char *textVertexSource =
"#version 330\n"
"layout (location = 0) in vec4 vertex;\n"
"uniform mat4 viewProjection = mat4(1);"
"uniform mat4 model = mat4(1);"
"out vec2 textureCoordinates;"
"void main()"
"{"
"  textureCoordinates = vertex.zw;"
"  gl_Position = viewProjection * model * vec4( vertex.xy, 0.0, 1.0 );"
"}\n";

const char *textFragmentSource =
"#version 330\n"
"in vec2 textureCoordinates;"
"out vec4 outputColour;"
"uniform sampler2D glyphSheet;"
"uniform vec4 colour = vec4( 1 );"
"void main()"
"{"
"  outputColour = colour;"
"  outputColour.a = texture( glyphSheet, textureCoordinates ).a;"
"  outputColour.a *= colour.a;"
"}\n";

#define MULTI_LINE_STRING(...) #__VA_ARGS__
const char *grassVertexSource = MULTI_LINE_STRING(
#version 330\n
layout (location=0) in vec3 vertexPosition;\n
layout (location=2) in vec2 vertexTextureCoordinates;\n
layout (location=8) in mat4 instanceTransform;\n
layout (location=12) in vec3 instanceColour;\n
uniform mat4 mvp;\n
uniform float time;\n
out vec2 textureCoordinates;\n
out vec3 colour;\n
void main()\n
{\n
    textureCoordinates = vertexTextureCoordinates;\n
    colour = instanceColour;\n
    vec3 worldP = vec3(instanceTransform * vec4(vertexPosition, 1.0));\n
    // Multiply by vertexPosition.y as it is in the 1.0 range
    worldP.x += (sin(time * 5.0 + worldP.x) + sin(time * 5.0 + worldP.z * 0.5)) * vertexPosition.y * 0.3;
    worldP.z += (cos(time * 5.0 + worldP.x * 2.0) + cos(time * 5.0 + worldP.z * 1.2)) * vertexPosition.y * 0.3;
    gl_Position = mvp * vec4(worldP, 1);\n
}\n
);

const char *grassFragmentSource = MULTI_LINE_STRING(
#version 330\n
uniform sampler2D diffuseTexture;
in vec2 textureCoordinates;\n
in vec3 colour;\n
out vec4 outputColour;\n
void main()\n
{\n
    float alpha = texture2D(diffuseTexture, textureCoordinates).r;\n
    if (alpha < 0.5)
        discard;
    outputColour = vec4(colour, alpha);\n
}\n
);

const char *rockVertexSource = MULTI_LINE_STRING(
#version 330\n
layout(location = 0) in vec3 vertexPosition;\n
layout (location=1) in vec3 vertexNormal;\n
layout (location=8) in mat4 instanceTransform;\n
layout(location = 2) in vec2 vertexTextureCoordinate;\n
uniform mat4 mvp = mat4(1);\n
out vec2 textureCoordinate;\n
out vec3 normal;\n
void main()\n
{\n
   textureCoordinate = vertexTextureCoordinate;\n
   normal = normalize(vec3(instanceTransform * vec4(vertexNormal, 0)));\n
   gl_Position = mvp * instanceTransform * vec4(vertexPosition, 1);\n
}\n
);

const char *rockFragmentSource = MULTI_LINE_STRING(
#version 330\n
in vec2 textureCoordinate;\n
in vec3 normal;\n
uniform sampler2D diffuseTexture;\n
out vec4 outputColour;\n
void main()\n
{\n
  vec3 lightDirection = normalize(vec3(0.5, -1, -0.3));\n
  float nDotL = max(0.0,dot(normal, -lightDirection));\n
  nDotL = 0.5 * nDotL + 0.5;\n
  vec3 colour = texture2D( diffuseTexture, textureCoordinate ).rgb;\n
  outputColour = vec4(colour * nDotL, 1.0);\n
}\n
);

struct ColourShader
{
  u32 program;
  int mvp;
  int colour;
};

ColourShader CreateColourShader()
{
  ColourShader result = {};
  result.program = OpenGL_CreateShader( colourVertexSource, colourFragmentSource );
  Assert( result.program );

  result.mvp = glGetUniformLocation( result.program, "mvp" );
  result.colour = glGetUniformLocation( result.program, "colour" );

  return result;
}

struct VertexColourShader
{
  u32 program;
  int mvp;
};

VertexColourShader CreateVertexColourShader()
{
  VertexColourShader result = {};
  result.program =
    OpenGL_CreateShader( vertexColourVertexSource, vertexColourFragmentSource );
  Assert( result.program );

  result.mvp = glGetUniformLocation( result.program, "mvp" );
  return result;
}

struct DiffuseTextureShader
{
    u32 program;
    int mvp;
    int diffuseTexture;
    int textureScaling;
};

DiffuseTextureShader CreateDiffuseTextureShader()
{
    DiffuseTextureShader result = {};
    result.program = OpenGL_CreateShader(diffuseTextureVertexSource,
            diffuseTextureFragmentSource);
    Assert(result.program);

    result.diffuseTexture = glGetUniformLocation(result.program, "diffuseTexture");
    result.mvp = glGetUniformLocation( result.program, "mvp" );
    result.textureScaling = glGetUniformLocation(result.program, "textureScaling");
    return result;
}

struct ParticleTextureShader
{
    u32 program;
    int mvp;
    int colourTexture;
    int textureScaling;
    int mixColour;
};

ParticleTextureShader CreateParticleTextureShader()
{
    ParticleTextureShader result = {};
    result.program = OpenGL_CreateShader(particleTextureVertexSource,
            particleTextureFragmentSource);
    Assert(result.program);

    result.colourTexture = glGetUniformLocation(result.program, "colourTexture");
    result.mvp = glGetUniformLocation( result.program, "mvp" );
    result.textureScaling = glGetUniformLocation(result.program, "textureScaling");
    result.mixColour = glGetUniformLocation(result.program, "mixColour");
    return result;
}

struct ColourDiffuseShader
{
    u32 program;
    int mvp;
    int colour;
};

ColourDiffuseShader CreateColourDiffuseShader()
{
    ColourDiffuseShader result = {};
    result.program = OpenGL_CreateShader(colourDiffuseVertexSource,
            colourDiffuseFragmentSource);
    Assert(result.program);

    result.colour = glGetUniformLocation(result.program, "colour");
    result.mvp = glGetUniformLocation( result.program, "mvp" );

    return result;
}

struct SkinnedColourShader
{
    u32 program;
    int mvp;
    int boneTransforms;
    int colour;
};

SkinnedColourShader CreateSkinnedColourShader()
{
    SkinnedColourShader result = {};
    result.program = OpenGL_CreateShader(skinningVertexSource,
            skinningFragmentSource);
    Assert(result.program);
    result.colour = glGetUniformLocation(result.program, "colour");
    result.mvp = glGetUniformLocation( result.program, "mvp" );
    result.boneTransforms = glGetUniformLocation(result.program, "boneTransforms");

    return result;
}

struct TextShader
{
    u32 program;
    int viewProjection;
    int model;
    int colour;
    int glyphSheet;
};

TextShader CreateTextShader()
{
    TextShader result = {};
    result.program = OpenGL_CreateShader(textVertexSource, textFragmentSource);
    Assert(result.program);
    result.colour = glGetUniformLocation(result.program, "colour");
    result.viewProjection = glGetUniformLocation(result.program, "viewProjection");
    result.model = glGetUniformLocation(result.program, "model");
    result.glyphSheet = glGetUniformLocation(result.program, "glyphSheet");

    return result;
}

struct GrassShader
{
    u32 program;
    int mvp;
    int diffuseTexture;
    int time;
};

GrassShader CreateGrassShader()
{
    GrassShader result = {};
    result.program = OpenGL_CreateShader(grassVertexSource, grassFragmentSource);
    Assert(result.program);
    result.mvp = glGetUniformLocation(result.program, "mvp");
    result.diffuseTexture = glGetUniformLocation(result.program, "diffuseTexture");
    result.time = glGetUniformLocation(result.program, "time");

    return result;
}

struct RockShader
{
    u32 program;
    int mvp;
    int diffuseTexture;
};

RockShader CreateRockShader()
{
    RockShader result = {};
    result.program = OpenGL_CreateShader(rockVertexSource, rockFragmentSource);
    Assert(result.program);
    result.mvp = glGetUniformLocation(result.program, "mvp");
    result.diffuseTexture = glGetUniformLocation(result.program, "diffuseTexture");

    return result;
}

struct GrassVertex
{
    vec3 position;
    vec2 textureCoordinate;
};

OpenGL_StaticMesh CreateGrassMesh()
{
    GrassVertex vertices[6*3];
    vertices[0].position = Vec3(-0.5f, 1.0f, 0.0f);
    vertices[0].textureCoordinate = Vec2(0.0f, 1.0f);
    vertices[1].position = Vec3(-0.5f, 0.0f, 0.0f);
    vertices[1].textureCoordinate = Vec2(0.0f, 0.0f);
    vertices[2].position = Vec3(0.5f, 0.0f, 0.0f);
    vertices[2].textureCoordinate = Vec2(1.0f, 0.0f);
    vertices[3].position = Vec3(-0.5f, 1.0f, 0.0f);
    vertices[3].textureCoordinate = Vec2(0.0f, 1.0f);
    vertices[4].position = Vec3(0.5f, 0.0f, 0.0f);
    vertices[4].textureCoordinate = Vec2(1.0f, 0.0f);
    vertices[5].position = Vec3(0.5f, 1.0f, 0.0f);
    vertices[5].textureCoordinate = Vec2(1.0f, 1.0f);

    float angle[2] = { PI / 3.0f, PI / -3.0f };
    for (u32 i = 0; i < 6; ++i)
    {
        vertices[i+6] = vertices[i];
        vertices[i+12] = vertices[i];

        vertices[i + 6].position.x = Cos(angle[0]) * vertices[i].position.x -
                                     Sin(angle[0]) * vertices[i].position.z;
        vertices[i + 6].position.z = Sin(angle[0]) * vertices[i].position.x +
                                     Cos(angle[0]) * vertices[i].position.z;

        vertices[i + 12].position.x = Cos(angle[1]) * vertices[i].position.x -
                                      Sin(angle[1]) * vertices[i].position.z;
        vertices[i + 12].position.z = Sin(angle[1]) * vertices[i].position.x +
                                      Cos(angle[1]) * vertices[i].position.z;
    }

    OpenGL_VertexAttribute attributes[2];
    attributes[0].index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
    attributes[0].numComponents = 3;
    attributes[0].componentType = GL_FLOAT;
    attributes[0].normalized = GL_FALSE;
    attributes[0].offset = 0;
    attributes[1].index = OPENGL_VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
    attributes[1].numComponents = 2;
    attributes[1].componentType = GL_FLOAT;
    attributes[1].normalized = GL_FALSE;
    attributes[1].offset = sizeof(vec3);
    OpenGL_StaticMesh result =
            OpenGL_CreateStaticMesh(vertices, ArrayCount(vertices), NULL, 0,
                                    sizeof(GrassVertex), attributes, 2, GL_TRIANGLES);

    return result;
}

struct SceneNode
{
    quat localRotation;
    vec3 localPosition;
    float localScale;
    mat4 transform;
    u32 parentIdx;
    u32 meshes[4];
    u32 meshCount;
};

#define SceneNode_ParentIndex 1024
#define SceneNode_InvalidMeshIndex 8

struct SceneMesh
{
    OpenGL_StaticMesh geometry;
    bool isSkeletalMesh;
};

struct Scene
{
    SceneNode nodes[255];
    SceneMesh meshes[16];
    u32 nodeCount;
    u32 meshCount;
};

internal void DeleteScene(Scene *scene)
{
    for (u32 i = 0; i < scene->meshCount; ++i)
    {
        OpenGL_DeleteStaticMesh(scene->meshes[i].geometry);
    }

    scene->meshCount = 0;
    scene->nodeCount = 0;
}

#define BONE_NAME_LENGTH 100
#define MAX_BONES 0xFF
#define INVALID_BONE 0xFF

struct Bone
{
    mat4 invBindPose;
	const char *name;
	u8 parent;
};

struct Skeleton
{
	u32 boneCount;
	Bone *bones;
};

struct BonePose
{
	quat rotation;
	vec3 translation;
	f32 scale;
};

struct SkeletonPose
{
	Skeleton *skeleton;
	BonePose *localPose;
};

struct AnimationSample
{
    BonePose *pose;
};

struct AnimationClip
{
    Skeleton *skeleton;
    AnimationSample *samples;
    f32 framesPerSecond;
    u32 frameCount;
    bool isLooping;
};


inline bool StringEqual(const char *a, const char *b)
{
	while (*a == *b)
	{
		if (*a == '\0')
			return true;

		a++;
		b++;
	}

	return false;
}

void LerpPose(BonePose *pose0, BonePose *pose1, float t, BonePose *resultPose, u32 boneCount)
{
    for (u32 boneIndex = 0; boneIndex < boneCount; ++boneIndex)
    {
        BonePose *bone0 = pose0 + boneIndex;
        BonePose *bone1 = pose1 + boneIndex;
        BonePose *resultBone = resultPose + boneIndex;

        resultBone->translation = Lerp(bone0->translation, bone1->translation, t);
        resultBone->rotation = Lerp(bone0->rotation, bone1->rotation, t);
        resultBone->scale = Lerp(bone0->scale, bone1->scale, t);
    }
}

// NOTE: Super inefficient, we should calculate and cache local transform of each bone to
// avoid recalculating it for each child
mat4 CalculateBonePoseWorldTransform(Skeleton skeleton, BonePose *pose, u8 boneIdx)
{
    Assert(boneIdx < skeleton.boneCount);
    BonePose *bonePose = pose + boneIdx;
    Bone bone = skeleton.bones[boneIdx];

    mat4 result = Translate(bonePose->translation) * Rotate(bonePose->rotation) * Scale(bonePose->scale);
    if (bone.parent != INVALID_BONE)
    {
        mat4 parentTransform = CalculateBonePoseWorldTransform(skeleton, pose, bone.parent);
        result = parentTransform * result;
    }
    return result;
}

mat4 CalculateBonePoseWorldTransform(SkeletonPose pose, u8 boneIdx)
{
    return CalculateBonePoseWorldTransform(*pose.skeleton, pose.localPose, boneIdx);
}

mat4 CalculateBoneInvBindPose(SkeletonPose bindPose, u8 boneIdx)
{
    Assert(boneIdx < bindPose.skeleton->boneCount);
    BonePose *bonePose = bindPose.localPose + boneIdx;
    Bone *bone = bindPose.skeleton->bones + boneIdx;
    
    // TODO: Rotation, Scale
    mat4 result = Scale(1.0f / bonePose->scale) * Rotate(-bonePose->rotation) * Translate(-bonePose->translation);
    if (bone->parent != INVALID_BONE)
    {
        mat4 parentTransform = CalculateBoneInvBindPose(bindPose, bone->parent);
        result = result * parentTransform;
    }
    return result;
}

u8 FindBoneByName(Skeleton *skeleton, const char *name)
{
	for (u32 i = 0; i < skeleton->boneCount; ++i)
	{
		if (StringEqual(skeleton->bones[i].name, name))
		{
			return SafeTruncateU32ToU8(i);
		}
	}
	return INVALID_BONE;
}

internal char *AllocateString(MemoryArena *arena, const char *string)
{
	u32 len = 0;
	const char *cursor = string;
	while (*cursor++ != '\0')
		len++;	

	len += 1;
	char *result = (char*)MemoryArenaAllocate(arena, len);
	for (u32 i = 0; i < len; ++i)
		result[i] = string[i];		
	
	return result;
}

aiVectorKey FindVectorKeyForFrame(aiVectorKey *keys, u32 count, u32 frameIndex)
{
    Assert(count > 0);
    for (u32 i = 0; i < count; ++i)
    {
        if (keys[i].mTime == frameIndex)
        {
            return keys[i];
        }
        else if (keys[i].mTime > frameIndex)
        {
            return (i > 0) ? keys[i-1] : keys[0];
        }
    }
    return keys[0];
}

aiQuatKey FindQuatKeyForFrame(aiQuatKey *keys, u32 count, u32 frameIndex)
{
    Assert(count > 0);
    for (u32 i = 0; i < count; ++i)
    {
        if (keys[i].mTime == frameIndex)
        {
            return keys[i];
        }
        else if (keys[i].mTime > frameIndex)
        {
            return (i > 0) ? keys[i-1] : keys[0];
        }
    }
    return keys[0];
}

internal void RecurseSceneNodes(SkeletonPose *bindPose, const aiNode *assetNode, MemoryArena *arena, float unitScale, const aiNode *parentAssetNode = NULL)
{
	u8 parentBoneIdx = INVALID_BONE;
    if (parentAssetNode != NULL)
    {
		parentBoneIdx = FindBoneByName(bindPose->skeleton, parentAssetNode->mName.data);
    }

    aiVector3t<float> scaling;
    aiQuaterniont<float> rotation;
    aiVector3t<float> position;
    assetNode->mTransformation.Decompose(scaling, rotation, position);

	// Check if scene node corresponds to a bone
	u8 boneIdx = SafeTruncateU32ToU8(bindPose->skeleton->boneCount++);
	if (boneIdx != INVALID_BONE)
	{
        Bone *bone = bindPose->skeleton->bones + boneIdx;
        bone->parent = parentBoneIdx;
        bone->name = AllocateString(arena, assetNode->mName.data);

		BonePose *bonePose = bindPose->localPose + boneIdx;
        bonePose->rotation = Quat(rotation.x, rotation.y, rotation.z, rotation.w);
        bonePose->translation = Vec3(position.x, position.y, position.z) * unitScale;
		bonePose->scale = scaling.x;
	}

    // TODO: Fetch transform
    for (u32 childIdx = 0; childIdx < assetNode->mNumChildren; ++childIdx)
    {
        RecurseSceneNodes(bindPose, assetNode->mChildren[childIdx], arena, unitScale, assetNode);
    }
}

internal void RecurseSceneNodes(Scene *scene, const aiNode *assetNode, SkeletonPose *bindPose, MemoryArena *arena, float unitScale, u32 parentIdx = SceneNode_ParentIndex, const aiNode *parentAssetNode = NULL)
{
    Assert(scene->nodeCount < ArrayCount(scene->nodes));
    u32 nodeIdx = scene->nodeCount++;
    SceneNode *node = scene->nodes + nodeIdx;
    node->parentIdx = parentIdx;
    printf("Name: %s\n", assetNode->mName.data);
    aiVector3t<float> scaling;
    aiQuaterniont<float> rotation;
    aiVector3t<float> position;
    assetNode->mTransformation.Decompose(scaling, rotation, position);
    node->localPosition = Vec3(position.x, position.y, position.z) * unitScale;
    node->localRotation = Quat(rotation.x, rotation.y, rotation.z, rotation.w);
    node->localScale = scaling.x;
    node->transform = Translate(node->localPosition) * Rotate(node->localRotation) * Scale(node->localScale);
    Assert(assetNode->mNumMeshes < ArrayCount(node->meshes));
    memcpy(node->meshes, assetNode->mMeshes, assetNode->mNumMeshes * sizeof(u32));
    node->meshCount = assetNode->mNumMeshes;

	u8 parentBoneIdx = INVALID_BONE;
    if (parentIdx != SceneNode_ParentIndex)
    {
		Assert(parentAssetNode != NULL);
        SceneNode *parentNode = scene->nodes + parentIdx;
        node->transform = parentNode->transform * node->transform;
		parentBoneIdx = FindBoneByName(bindPose->skeleton, parentAssetNode->mName.data);
    }

	// Check if scene node corresponds to a bone
	u8 boneIdx = SafeTruncateU32ToU8(bindPose->skeleton->boneCount++);
	if (boneIdx != INVALID_BONE)
	{
        Bone *bone = bindPose->skeleton->bones + boneIdx;
        bone->parent = parentBoneIdx;
        bone->name = AllocateString(arena, assetNode->mName.data);

		BonePose *bonePose = bindPose->localPose + boneIdx;
        bonePose->rotation = Quat(rotation.x, rotation.y, rotation.z, rotation.w);
        bonePose->translation = Vec3(position.x, position.y, position.z) * unitScale;
		bonePose->scale = scaling.x;
	}

    // TODO: Fetch transform
    for (u32 childIdx = 0; childIdx < assetNode->mNumChildren; ++childIdx)
    {
        RecurseSceneNodes(scene, assetNode->mChildren[childIdx], bindPose, arena, unitScale, nodeIdx, assetNode);
    }
}

// WARN: Be careful for padding
struct SkinnedVertex
{
	vec3 position;
	vec3 boneWeights;
	u32 boneIds;
};

struct Vertex
{
    vec3 position;
    vec3 normal;
    vec2 textureCoordinate;
};


internal u32 CalculateBoneCount(const aiNode *node)
{
	u32 result = node->mNumChildren + 1;
    for (u32 childIdx = 0; childIdx < node->mNumChildren; ++childIdx)
    {
		result += CalculateBoneCount(node->mChildren[childIdx]);
    }
	return result;
}

struct BoneIdxWeightPair
{
    float weight;
    u8 boneIdx;
    u8 padding[3];
};

// Sorts largest to smallest
inline int BoneIdxWeightPairCompare(const void *a, const void *b)
{
    BoneIdxWeightPair p0 = *(BoneIdxWeightPair*)b;
    BoneIdxWeightPair p1 = *(BoneIdxWeightPair*)a;
    if (p0.weight < p1.weight)
        return -1;
    else if (p0.weight == p1.weight)
        return 0;
    else
        return 1;
}

inline void PrintMat4(mat4 m)
{
    for (int i = 0; i < 16; ++i)
    {
        LOG_DEBUG("%d: %g", i, m.raw[i]);
    }
}

internal void LoadSkeletonPose(const void *meshData, u32 length, SkeletonPose *outBindPose, MemoryArena *arena, float unitScale)
{
    const aiScene *scene = aiImportFileFromMemory(
            (const char*)meshData, length,
            aiProcess_Triangulate | aiProcess_JoinIdenticalVertices |
            aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph, "");

    if ( !scene )
    {
      LOG_ERROR( "Model import failed: %s", aiGetErrorString() );
      return;
    }

	SkeletonPose bindPose = {};
	bindPose.skeleton = AllocateStruct(arena, Skeleton);
	
	u32 boneCount = CalculateBoneCount(scene->mRootNode);
	bindPose.skeleton->bones = AllocateArray(arena, Bone, boneCount);
	//bindPose.skeleton->boneCount = boneCount;
	bindPose.localPose = AllocateArray(arena, BonePose, boneCount);
	*outBindPose = bindPose;	

    RecurseSceneNodes(&bindPose, scene->mRootNode, arena, unitScale);

    for (u32 boneIdx = 0; boneIdx < bindPose.skeleton->boneCount; ++boneIdx)
    {
        bindPose.skeleton->bones[boneIdx].invBindPose = CalculateBoneInvBindPose(bindPose, (u8)boneIdx);
    }

    aiReleaseImport(scene);
}

struct CreateSceneParameters
{
    const void *meshData;
    u32 meshDataLength;
    MemoryArena *arena;
    float unitScale;
    bool createSkeletalMesh;
    SkeletonPose *outBindPose;
};
internal void CreateScene(Scene *output, CreateSceneParameters *params)
{
    const aiScene *scene = aiImportFileFromMemory(
            (const char*)params->meshData, params->meshDataLength,
            aiProcess_Triangulate | aiProcess_JoinIdenticalVertices |
            aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph, "");

    if ( !scene )
    {
      LOG_ERROR( "Model import failed: %s", aiGetErrorString() );
      return;
    }

    u32 boneCount = 0;
    SkeletonPose bindPose = {};
    if (params->createSkeletalMesh)
    {
        bindPose.skeleton = AllocateStruct(params->arena, Skeleton);

        boneCount = CalculateBoneCount(scene->mRootNode);
        bindPose.skeleton->bones = AllocateArray(params->arena, Bone, boneCount);
        //bindPose.skeleton->boneCount = boneCount;
        bindPose.localPose = AllocateArray(params->arena, BonePose, boneCount);
        if (params->outBindPose)
        {
            *params->outBindPose = bindPose;	
        }

        RecurseSceneNodes(output, scene->mRootNode, &bindPose, params->arena, params->unitScale);

        for (u32 boneIdx = 0; boneIdx < bindPose.skeleton->boneCount; ++boneIdx)
        {
            bindPose.skeleton->bones[boneIdx].invBindPose = CalculateBoneInvBindPose(bindPose, (u8)boneIdx);
        }
    }

    LOG_DEBUG("Scene Info");
    LOG_DEBUG("  Mesh Count: %u", scene->mNumMeshes);
    LOG_DEBUG("  Material Count: %u", scene->mNumMaterials);
    LOG_DEBUG("  Animation Count: %u", scene->mNumAnimations);

    // NOTE: Only doing this for z-up meshes
    mat4 modelMatrix = Scale(params->unitScale);
    Assert(scene->mNumMeshes < ArrayCount(output->meshes));
    for (u32 meshIdx = 0; meshIdx < scene->mNumMeshes; ++meshIdx)
    {
        aiMesh *mesh = scene->mMeshes[meshIdx];
        u32 indices[0xFFFFF];
        u32 indexCount = 0;
        for (u32 faceIdx = 0; faceIdx < mesh->mNumFaces; ++faceIdx)
        {
            aiFace face = mesh->mFaces[faceIdx];
            for (u32 j = 0; j < face.mNumIndices; ++j)
            {
                Assert(indexCount < ArrayCount(indices));
                indices[indexCount++] = face.mIndices[j];
            }
        }
        LOG_DEBUG("    Index Count: %u", indexCount);
		LOG_DEBUG("	Vertex Count: %u", mesh->mNumVertices);

        if (mesh->mNumBones > 1 && params->createSkeletalMesh)
        {
            SkinnedVertex *vertices = AllocateArray(params->arena, SkinnedVertex, mesh->mNumVertices);
            for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
            {
                vec4 p = modelMatrix * Vec4(mesh->mVertices[vertexIdx].x,
                        mesh->mVertices[vertexIdx].y, mesh->mVertices[vertexIdx].z, 1.0f);
                vertices[vertexIdx].position.x = p.x;
                vertices[vertexIdx].position.y = p.y;
                vertices[vertexIdx].position.z = p.z;
            }

            // TODO: Use temp memory allocator
            BoneIdxWeightPair *boneIdxWeightPairs = 
                AllocateArray(params->arena, BoneIdxWeightPair, mesh->mNumVertices * boneCount);
            ClearToZero(boneIdxWeightPairs, mesh->mNumVertices * boneCount * sizeof(BoneIdxWeightPair));

            Bone *bones = bindPose.skeleton->bones;

            LOG_DEBUG("	Bone Count: %u", mesh->mNumBones);
            for (u32 meshBoneIdx = 0; meshBoneIdx < mesh->mNumBones; ++meshBoneIdx)
            {
                aiBone* bone = mesh->mBones[meshBoneIdx];
                Assert(bone != NULL);
                if (bone != NULL)
                {
                    u8 boneIdx = FindBoneByName(bindPose.skeleton, bone->mName.data);
                    LOG_DEBUG("	Bone Name: %s", bone->mName.data);
                    Assert(boneIdx != INVALID_BONE);
                    //printf("		Weight Count: %u\n", bone->mNumWeights);
                    // bone->mOffsetMatrix
                    for (u32 weightIdx = 0; weightIdx < bone->mNumWeights; ++weightIdx)
                    {
                        aiVertexWeight weight = bone->mWeights[weightIdx];
                        Assert(weight.mVertexId < mesh->mNumVertices);
                        u32 idx = boneIdx + (weight.mVertexId * boneCount);
                        boneIdxWeightPairs[idx].boneIdx = boneIdx;
                        boneIdxWeightPairs[idx].weight = weight.mWeight;
                        //printf("			Vertex ID: %u Weight: %g\n", weight.mVertexId, weight.mWeight);
                    }
                }
            }
            for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
            {
                BoneIdxWeightPair *cursor = boneIdxWeightPairs + vertexIdx * boneCount;
                // TODO: Replace with radix sort
                qsort(cursor, boneCount, sizeof(BoneIdxWeightPair), &BoneIdxWeightPairCompare);

                SkinnedVertex *vertex = vertices + vertexIdx;

                float total = cursor[0].weight + cursor[1].weight + cursor[2].weight;
                Assert(total <= 1.01f);
                vertex->boneWeights.data[0] = cursor[0].weight;
                vertex->boneWeights.data[1] = cursor[1].weight;
                vertex->boneWeights.data[2] = cursor[2].weight;
                // 4th bone weight is calculated from 1 minus the sum of the other bone weights.

                //LOG_DEBUG("boneIds: %u %u %u %u", cursor[0].boneIdx, cursor[1].boneIdx, cursor[2].boneIdx, cursor[3].boneIdx);
                vertex->boneIds = (cursor[0].boneIdx << 24) |
                                  (cursor[1].boneIdx << 16) |
                                  (cursor[2].boneIdx <<  8) |
                                  (cursor[3].boneIdx      );
            }

            OpenGL_VertexAttribute attributes[3];
            attributes[0].index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
            attributes[0].numComponents = 3;
            attributes[0].componentType = GL_FLOAT;
            attributes[0].normalized = GL_FALSE;
            attributes[0].offset = (i32)offsetof(SkinnedVertex, position);
            attributes[1].index = OPENGL_VERTEX_ATTRIBUTE_BONE_ID;
            attributes[1].numComponents = 1;
            attributes[1].componentType = GL_UNSIGNED_INT;
            attributes[1].normalized = GL_FALSE;
            attributes[1].offset = (i32)offsetof(SkinnedVertex, boneIds);
            attributes[2].index = OPENGL_VERTEX_ATTRIBUTE_BONE_WEIGHT;
            attributes[2].numComponents = 3;
            attributes[2].componentType = GL_FLOAT;
            attributes[2].normalized = GL_FALSE;
            attributes[2].offset = (i32)offsetof(SkinnedVertex, boneWeights);
            Assert(sizeof(SkinnedVertex) == (sizeof(float) * 3 + sizeof(u32) + sizeof(float) * 3));
            OpenGL_StaticMesh skeletalMesh = OpenGL_CreateStaticMesh(vertices, mesh->mNumVertices,
                    indices, indexCount, sizeof(SkinnedVertex), attributes,
                    ArrayCount(attributes), GL_TRIANGLES);

            SceneMesh sceneMesh = {};
            sceneMesh.geometry = skeletalMesh;
            sceneMesh.isSkeletalMesh = true;

            Assert(output->meshCount < ArrayCount(output->meshes));
            output->meshes[output->meshCount++] = sceneMesh;

            MemoryArenaFree(params->arena, boneIdxWeightPairs);
            MemoryArenaFree(params->arena, vertices);
        }
        else
        {
            Vertex *vertices = AllocateArray(params->arena, Vertex, mesh->mNumVertices);
            Assert(mesh->mNormals != NULL);
            Assert(mesh->mTextureCoords[0] != NULL);
            for (u32 vertexIdx = 0; vertexIdx < mesh->mNumVertices; ++vertexIdx)
            {
                vec4 p = modelMatrix * Vec4(mesh->mVertices[vertexIdx].x,
                        mesh->mVertices[vertexIdx].y, mesh->mVertices[vertexIdx].z, 1.0f);
                vertices[vertexIdx].position = Vec3(p.x, p.y, p.z);
                vertices[vertexIdx].normal = Vec3(mesh->mNormals[vertexIdx].x,
                                                  mesh->mNormals[vertexIdx].y,
                                                  mesh->mNormals[vertexIdx].z);
                vertices[vertexIdx].textureCoordinate =
                        Vec2(mesh->mTextureCoords[0][vertexIdx].x,
                             mesh->mTextureCoords[0][vertexIdx].y);
            }

            OpenGL_VertexAttribute attribs[3];
            attribs[0].index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
            attribs[0].numComponents = 3;
            attribs[0].componentType = GL_FLOAT;
            attribs[0].normalized = GL_FALSE;
            attribs[0].offset = 0;
            attribs[1].index = OPENGL_VERTEX_ATTRIBUTE_NORMAL;
            attribs[1].numComponents = 3;
            attribs[1].componentType = GL_FLOAT;
            attribs[1].normalized = GL_FALSE;
            attribs[1].offset = sizeof( float ) * 3;
            attribs[2].index = OPENGL_VERTEX_ATTRIBUTE_TEXTURE_COORDINATE;
            attribs[2].numComponents = 2;
            attribs[2].componentType = GL_FLOAT;
            attribs[2].normalized = GL_FALSE;
            attribs[2].offset = sizeof( float ) * 6;
            OpenGL_StaticMesh staticMesh = OpenGL_CreateStaticMesh((float*)vertices, mesh->mNumVertices,
                    indices, indexCount, sizeof(Vertex), attribs, 3, GL_TRIANGLES);

            SceneMesh sceneMesh = {};
            sceneMesh.geometry = staticMesh;
            sceneMesh.isSkeletalMesh = false;

            Assert(output->meshCount < ArrayCount(output->meshes));
            output->meshes[output->meshCount++] = sceneMesh;

            MemoryArenaFree(params->arena, vertices);
        }
    }

    aiReleaseImport(scene);
}

internal void CreateAnimationClip(const void *assetData, u32 assetDataLength, AnimationClip *clip,
        Skeleton *skeleton, bool isLooping, MemoryArena *arena, float unitScale, const char *clipName = NULL)
{
    const aiScene *scene = aiImportFileFromMemory(
            (const char*)assetData, assetDataLength, 0, "");

    if ( !scene )
    {
      LOG_ERROR( "Animation import failed: %s", aiGetErrorString() );
      return;
    }

    printf("Animation count: %u\n", scene->mNumAnimations);

    clip->skeleton = skeleton;
    clip->isLooping = isLooping;

    for (u32 animationIndex = 0; animationIndex < scene->mNumAnimations; ++animationIndex)
    {
        const aiAnimation *animation = scene->mAnimations[animationIndex];
        if (clipName == NULL || !strcmp(animation->mName.data, clipName))
        {
            Assert(animation->mNumChannels <= skeleton->boneCount);
            printf("Name: %s\n", animation->mName.data);
            printf("Frame Count: %g\n", animation->mDuration);
            printf("Rate: %g\n", animation->mTicksPerSecond);
            printf("Channel Count: %u\n", animation->mNumChannels);
            clip->frameCount = (u32)animation->mDuration + 1;
            clip->framesPerSecond = (f32)animation->mTicksPerSecond;
            clip->samples = AllocateArray(arena, AnimationSample, clip->frameCount);
            for (u32 sampleIndex = 0; sampleIndex < clip->frameCount; ++sampleIndex)
            {
                AnimationSample *sample = clip->samples + sampleIndex;
                sample->pose = AllocateArray(arena, BonePose, skeleton->boneCount);
                ClearToZero(sample->pose, skeleton->boneCount * sizeof(BonePose));
                for (u32 boneIndex = 0; boneIndex < skeleton->boneCount; ++boneIndex)
                {
                    sample->pose[boneIndex].rotation = Quat();
                    sample->pose[boneIndex].scale = 1.0f;
                }
            }

            for (u32 channelIndex = 0; channelIndex < animation->mNumChannels; ++channelIndex)
            {
                const aiNodeAnim* node = animation->mChannels[channelIndex];
                u8 boneIndex = FindBoneByName(skeleton, node->mNodeName.data);
                if (boneIndex != INVALID_BONE)
                {
                    for (u32 sampleIndex = 0; sampleIndex < clip->frameCount; ++sampleIndex)
                    {
                        aiVectorKey positionKey = FindVectorKeyForFrame(node->mPositionKeys,
                                node->mNumPositionKeys, sampleIndex);
                        aiQuatKey rotationKey = FindQuatKeyForFrame(node->mRotationKeys,
                                node->mNumRotationKeys, sampleIndex);
                        aiVectorKey scaleKey = FindVectorKeyForFrame(node->mScalingKeys,
                                node->mNumScalingKeys, sampleIndex);

                        BonePose *bonePose = clip->samples[sampleIndex].pose + boneIndex;
                        bonePose->translation = Vec3(positionKey.mValue.x,
                                positionKey.mValue.y, positionKey.mValue.z) * unitScale;

                        bonePose->rotation = Quat(rotationKey.mValue.x,
                                rotationKey.mValue.y, rotationKey.mValue.z, rotationKey.mValue.w);
                        Assert(IsNormalized(bonePose->rotation));

                        bonePose->scale = scaleKey.mValue.x;
                    }
                }
                else
                {
                    LOG_ERROR("Could not find animation channel for bone %s\n", node->mNodeName.data);
                }
            }
        }
    }

    aiReleaseImport(scene);
}

internal OpenGL_StaticMesh CreateMesh(const void *meshData, u32 length)
{
    OpenGL_StaticMesh result = {};

    const aiScene *scene = aiImportFileFromMemory(
            (const char*)meshData, length,
            aiProcess_Triangulate | aiProcess_JoinIdenticalVertices |
            aiProcess_OptimizeMeshes | aiProcess_OptimizeGraph, "");

    if ( !scene )
    {
      LOG_ERROR( "Model import failed: %s", aiGetErrorString() );
      return result;
    }

    // Only processing the root node for now
    aiNode *node = scene->mRootNode;
    aiString name = node->mName;
    printf("========= Importing Scene =========\n");
    printf("Root Node:\n");
    printf("  Name: %s\n", name.data);
    printf("  Child Count: %u\n", node->mNumChildren);
    printf("  Mesh Count: %u\n", node->mNumMeshes);
    Assert(node->mNumMeshes == 1);
    for (u32 i = 0; i < node->mNumMeshes; ++i)
    {
        printf("  Mesh %u\n", i);
        u32 meshIdx = node->mMeshes[i];
        printf("    Mesh Idx: %u\n", meshIdx);
        Assert(meshIdx < scene->mNumMeshes);
        aiMesh *mesh = scene->mMeshes[meshIdx];
        printf("    Vertex Count: %u\n", mesh->mNumVertices);
        printf("    Face Count: %u\n", mesh->mNumFaces);

        u32 indices[1024];
        u32 indexCount = 0;
        for (u32 faceIdx = 0; faceIdx < mesh->mNumFaces; ++faceIdx)
        {
            aiFace face = mesh->mFaces[faceIdx];
            for (u32 j = 0; j < face.mNumIndices; ++j)
            {
                Assert(indexCount < ArrayCount(indices));
                indices[indexCount++] = face.mIndices[j];
            }
        }
        printf("    Index Count: %u\n", indexCount);

        OpenGL_VertexAttribute attribute;
        attribute.index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
        attribute.numComponents = 3;
        attribute.componentType = GL_FLOAT;
        attribute.normalized = GL_FALSE;
        attribute.offset = 0;
        result = OpenGL_CreateStaticMesh((float*)mesh->mVertices, mesh->mNumVertices,
                indices, indexCount, sizeof(float) * 3, &attribute, 1, GL_TRIANGLES);
    }
    printf("===================================\n");

    aiReleaseImport(scene);
    return result;
}

internal u32 CreateTexture(const void *data, u32 length, bool useAlpha = false)
{
    i32 width, height, channelCount;
    stbi_set_flip_vertically_on_load( 1 );

    // Image data must be 4-byte aligned,
    // https://www.khronos.org/opengl/wiki/Common_Mistakes#Texture_upload_and_pixel_reads
    void *pixels = stbi_load_from_memory( (const u8 *)data, length,
            &width, &height, &channelCount, 4 );
    if ( pixels != NULL )
    {
        u32 texture;
        glGenTextures( 1, &texture );
        glBindTexture( GL_TEXTURE_2D, texture );

        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0,
                GL_RGBA, GL_UNSIGNED_BYTE, pixels );

        glGenerateMipmap( GL_TEXTURE_2D );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16 );

        glBindTexture( GL_TEXTURE_2D, 0 );
        stbi_image_free( pixels );
        return texture;
    }
    return 0;
}

struct TriangleVertex
{
    vec3 position;
    vec3 normal;
};

struct TriangleIndices
{
  u32 i,j,k;
};

inline TriangleVertex CreateVertex(float x, float y, float z)
{
    TriangleVertex result;
    result.position = Normalize(Vec3(x, y, z));
    result.normal = result.position;
    return result;
}

inline u32 AddMidPoint( u32 index1, u32 index2,
                             std::vector<TriangleVertex> *vertices )
{
  const auto &v1 = vertices->at(index1);
  const auto &v2 = vertices->at(index2);
  vec3 v = v1.position + v2.position;
  vertices->push_back( CreateVertex(v.x, v.y, v.z) );
  return (u32)vertices->size() - 1;
}

OpenGL_StaticMesh OpenGL_CreateIcosahedronMesh( u32 tesselationLevel )
{
  std::vector<TriangleVertex> vertices;
  std::vector<TriangleIndices> indices;

  float t = ( 1.0f + SquareRoot( 5.0f ) ) * 0.5f;
  vertices.push_back( CreateVertex( -1, t, 0 ) );
  vertices.push_back( CreateVertex( 1, t, 0 ) );
  vertices.push_back( CreateVertex( -1, -t, 0 ) );
  vertices.push_back( CreateVertex( 1, -t, 0 ) );

  vertices.push_back( CreateVertex( 0, -1, t ) );
  vertices.push_back( CreateVertex( 0, 1, t ) );
  vertices.push_back( CreateVertex( 0, -1, -t ) );
  vertices.push_back( CreateVertex( 0, 1, -t ) );

  vertices.push_back( CreateVertex( t, 0, -1 ) );
  vertices.push_back( CreateVertex( t, 0, 1 ) );
  vertices.push_back( CreateVertex( -t, 0, -1 ) );
  vertices.push_back( CreateVertex( -t, 0, 1 ) );

  indices.push_back( TriangleIndices{ 0, 11, 5 } );
  indices.push_back( TriangleIndices{ 0, 5, 1 } );
  indices.push_back( TriangleIndices{ 0, 1, 7 } );
  indices.push_back( TriangleIndices{ 0, 7, 10 } );
  indices.push_back( TriangleIndices{ 0, 10, 11 } );

  indices.push_back( TriangleIndices{ 1, 5, 9 } );
  indices.push_back( TriangleIndices{ 5, 11, 4 } );
  indices.push_back( TriangleIndices{ 11, 10, 2 } );
  indices.push_back( TriangleIndices{ 10, 7, 6 } );
  indices.push_back( TriangleIndices{ 7, 1, 8 } );

  indices.push_back( TriangleIndices{ 3, 9, 4 } );
  indices.push_back( TriangleIndices{ 3, 4, 2 } );
  indices.push_back( TriangleIndices{ 3, 2, 6 } );
  indices.push_back( TriangleIndices{ 3, 6, 8 } );
  indices.push_back( TriangleIndices{ 3, 8, 9 } );

  indices.push_back( TriangleIndices{ 4, 9, 5 } );
  indices.push_back( TriangleIndices{ 2, 4, 11 } );
  indices.push_back( TriangleIndices{ 6, 2, 10 } );
  indices.push_back( TriangleIndices{ 8, 6, 7 } );
  indices.push_back( TriangleIndices{ 9, 8, 1 } );

  for ( u32 i = 0; i < tesselationLevel; ++i )
  {
    std::vector<TriangleIndices> newIndices;
    for ( size_t j = 0; j < indices.size(); ++j )
    {
      u32 a = AddMidPoint( indices[j].i, indices[j].j, &vertices );
      u32 b = AddMidPoint( indices[j].j, indices[j].k, &vertices );
      u32 c = AddMidPoint( indices[j].k, indices[j].i, &vertices );

      newIndices.push_back( TriangleIndices{ indices[j].i, a, c } );
      newIndices.push_back( TriangleIndices{ indices[j].j, b, a } );
      newIndices.push_back( TriangleIndices{ indices[j].k, c, b } );
      newIndices.push_back( TriangleIndices{ a, b, c } );
    }

    indices = newIndices;
  }

  OpenGL_VertexAttribute attribs[2];
  attribs[0].index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
  attribs[0].numComponents = 3;
  attribs[0].componentType = GL_FLOAT;
  attribs[0].normalized = GL_FALSE;
  attribs[0].offset = 0;
  attribs[1].index = OPENGL_VERTEX_ATTRIBUTE_NORMAL;
  attribs[1].numComponents = 3;
  attribs[1].componentType = GL_FLOAT;
  attribs[1].normalized = GL_FALSE;
  attribs[1].offset = sizeof(float) * 3;

  return OpenGL_CreateStaticMesh(
    vertices.data(), (u32)vertices.size(), (u32 *)indices.data(),
    (u32)indices.size() * 3, sizeof( vec3 ) * 2, attribs, 2, GL_TRIANGLES );
}

struct Glyph
{
    float u0, v0, u1, v1, x, y, w, h, advance, leftSideBearing;
};

#define GLYPH_COUNT 128
struct Font
{
    Glyph glyphs[GLYPH_COUNT];
    u32 texture;
    float ascent, descent, lineGap;
};

Font CreateFont(const void *assetData, u32 assetDataLength, MemoryArena *arena,
                u32 bitmapWidth, u32 bitmapHeight, float pixelHeight)
{
    Font result = {};

    i32 fontOffset = stbtt_GetFontOffsetForIndex((const u8 *)assetData, 0);
    if (fontOffset < 0)
    {
        LOG_ERROR("Invalid font offset");
        return result;
    }
    stbtt_fontinfo fontInfo;
    if (stbtt_InitFont(&fontInfo, (const u8 *)assetData, fontOffset))
    {
        float scale = stbtt_ScaleForPixelHeight(&fontInfo, pixelHeight);

        i32 ascent, descent, lineGap;
        stbtt_GetFontVMetrics(&fontInfo, &ascent, &descent, &lineGap);
        result.ascent = ascent * scale;
        result.descent = descent * scale;
        result.lineGap = lineGap * scale;

        u8 *bitmap =
                (u8 *)MemoryArenaAllocate(arena, bitmapWidth * bitmapHeight);

        stbrp_rect rects[GLYPH_COUNT];
        u8 *bitmaps[GLYPH_COUNT];
        for (u32 i = 0; i < GLYPH_COUNT; ++i)
        {
            i32 ix0, iy0, ix1, iy1;
            stbtt_GetCodepointBitmapBox(&fontInfo, i, scale, scale, &ix0, &iy0,
                                        &ix1, &iy1);
            i32 w = ix1 - ix0;
            i32 h = iy1 - iy0;
            bitmaps[i] = (u8 *)MemoryArenaAllocate(arena, w * h);
            stbtt_MakeCodepointBitmap(&fontInfo, bitmaps[i], w, h, w, scale,
                                      scale, i);

            Glyph *glyph = result.glyphs + i;
            glyph->w = (float)w;
            glyph->h = (float)h;
            glyph->x = (float)ix0;
            glyph->y = (float)iy0;
            i32 advance, leftSideBearing;
            stbtt_GetCodepointHMetrics(&fontInfo, i, &advance,
                                       &leftSideBearing);
            glyph->advance = advance * scale;
            glyph->leftSideBearing = leftSideBearing * scale;

            rects[i].w = (stbrp_coord)w;
            rects[i].h = (stbrp_coord)h;
        }

        stbrp_context context;
        stbrp_node nodes[512];
        stbrp_init_target(&context, bitmapWidth, bitmapWidth, nodes,
                          ArrayCount(nodes));

        stbrp_pack_rects(&context, rects, ArrayCount(rects));

        for (u32 i = 0; i < GLYPH_COUNT; ++i)
        {
            stbrp_rect *rect = rects + i;
            Glyph *glyph = result.glyphs + i;
            glyph->u0 = rect->x / (float)bitmapWidth;
            glyph->v0 = rect->y / (float)bitmapHeight;
            glyph->u1 = (rect->x + rect->w) / (float)bitmapWidth;
            glyph->v1 = (rect->y + rect->h) / (float)bitmapHeight;

            for (u32 y = 0; y < rect->h; ++y)
            {
                u8 *dst = bitmap + ((rect->y + y) * bitmapWidth + rect->x);
                u8 *src = bitmaps[i] + y * rect->w;
                memcpy(dst, src, rect->w);
            }
        }

        glGenTextures(1, &result.texture);
        glBindTexture(GL_TEXTURE_2D, result.texture);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, bitmapWidth, bitmapHeight, 0,
                     GL_ALPHA, GL_UNSIGNED_BYTE, bitmap);
        glBindTexture(GL_TEXTURE_2D, 0);
        // NOTE: This frees all of the other bitmaps as well.
        MemoryArenaFree(arena, bitmap);
    }
    return result;
}

internal void DeleteFont(Font font)
{
    DeleteTexture(font.texture);
}

struct TextBufferVertex
{
    float x;
    float y;
    float u;
    float v;
};

#define MAX_TEXT_BUFFER_VERTICES (64 * 1024)
struct TextBuffer
{
    TextBufferVertex vertices[MAX_TEXT_BUFFER_VERTICES];
    OpenGL_DynamicMesh mesh;
};

void InitializeTextBuffer(TextBuffer *buffer)
{
    OpenGL_VertexAttribute vertexAttribute;
    vertexAttribute.index = OPENGL_VERTEX_ATTRIBUTE_POSITION;
    vertexAttribute.numComponents = 4;
    vertexAttribute.componentType = GL_FLOAT;
    vertexAttribute.normalized = GL_FALSE;
    vertexAttribute.offset = 0;
    buffer->mesh = OpenGL_CreateDynamicMesh(
            buffer->vertices, ArrayCount(buffer->vertices),
            sizeof(buffer->vertices[0]), &vertexAttribute, 1, GL_TRIANGLES);
}

internal void UpdateTextBuffer(TextBuffer *buffer)
{
    OpenGL_UpdateDynamicMesh(buffer->mesh);
}

internal float CalculateTextLength(Font *font, const char *str, u32 length = 0)
{
    float result = 0.0f;
    float current = 0.0f;
    u32 count = 0;
    while (*str)
    {
        if (length > 0 && count >= length)
        {
            break;
        }
        count++;

        if (*str >= 32)
        {
            current += font->glyphs[(i32)(*str)].advance;
        }
        else if (*str == '\n')
        {
            if (current > result)
            {
                result = current;
            }
            current = 0.0f;
        }
        else if (*str == '\t')
        {
            current += (i32)font->glyphs[32].advance * 2;
        }
        str++;
    }
    if (current > result)
    {
        result = current;
    }
    return result;
}

float AddCharacterToBuffer(TextBuffer *buffer, Font *font, i32 c, float x,
                           float y)
{
    Assert(buffer->mesh.numVertices + 6 < MAX_TEXT_BUFFER_VERTICES);
    Assert(c < GLYPH_COUNT);
    Glyph glyph = font->glyphs[c];
    float fontHeight = font->ascent + font->descent + font->lineGap;
    TextBufferVertex topLeft, bottomLeft, bottomRight, topRight;
    topLeft.x = glyph.x + x;
    topLeft.y = glyph.y + y + fontHeight;
    topLeft.u = glyph.u0;
    topLeft.v = glyph.v0;

    bottomLeft.x = glyph.x + x;
    bottomLeft.y = glyph.y + glyph.h + y + fontHeight;
    bottomLeft.u = glyph.u0;
    bottomLeft.v = glyph.v1;

    bottomRight.x = glyph.x + glyph.w + x;
    bottomRight.y = glyph.y + glyph.h + y + fontHeight;
    bottomRight.u = glyph.u1;
    bottomRight.v = glyph.v1;

    topRight.x = glyph.x + glyph.w + x;
    topRight.y = glyph.y + y + fontHeight;
    topRight.u = glyph.u1;
    topRight.v = glyph.v0;

    auto vertex = buffer->vertices + buffer->mesh.numVertices;
    *vertex++ = topRight;
    *vertex++ = topLeft;
    *vertex++ = bottomLeft;

    *vertex++ = bottomLeft;
    *vertex++ = bottomRight;
    *vertex++ = topRight;
    buffer->mesh.numVertices += 6;

    return x + glyph.advance;
}

struct AddToTextBufferResult
{
    u32 start;
    u32 count;
};

AddToTextBufferResult AddToTextBuffer(TextBuffer *textBuffer, const char *text,
                                      Font *font)
{
    Assert(font->texture != 0);

    u32 start = textBuffer->mesh.numVertices;

    float xOffset = 0.0f;
    float yOffset = 0.0f;
    const char *cursor = text;
    while (*cursor)
    {
        if (*cursor > 32)
        {
            xOffset = AddCharacterToBuffer(textBuffer, font,
                                           *cursor, xOffset, yOffset);
        }
        else if (*cursor == ' ')
        {
            xOffset += font->glyphs[32].advance;
        }
        else if (*cursor == '\n')
        {
            // Double line spacing
            yOffset += (font->ascent + font->descent + font->lineGap) * 1.5f;
            xOffset = 0.0f;
        }
        else if (*cursor == '\t')
        {
            xOffset += font->glyphs[32].advance * 2;
        }
        cursor++;
    }
    u32 count = textBuffer->mesh.numVertices - start;

    UpdateTextBuffer(textBuffer);

    AddToTextBufferResult result = {};
    result.start = start;
    result.count = count;

    return result;
}

struct InstanceDataBuffer
{
    u32 numVertices;
    u32 numIndices;
    u32 primitive;

    u32 vao;
    u32 verticesVbo;
    u32 indicesVbo;
    u32 transformsVbo;
    u32 coloursVbo;
};

struct renderer_State
{
    OpenGL_StaticMesh quadMesh;
    OpenGL_StaticMesh axisMesh;
    OpenGL_StaticMesh wireframeCubeMesh;
    OpenGL_StaticMesh cubeMesh;
    OpenGL_StaticMesh sphereMesh;
    ColourShader colourShader;
    VertexColourShader vertexColourShader;
    DiffuseTextureShader diffuseTextureShader;
    ColourDiffuseShader colourDiffuseShader;
    SkinnedColourShader skinnedColourShader;
    ParticleTextureShader particleTextureShader;
    TextShader textShader;
    CubeMapShader cubeMapShader;
    GrassShader grassShader;
    RockShader rockShader;
    InstanceDataBuffer grassInstancingDataBuffer;

    TextBuffer textBuffer;
};

internal void DeleteInstanceDataBuffer(InstanceDataBuffer buffer)
{
    if (buffer.vao != 0)
    {
        glBindVertexArray(0);
        glDeleteVertexArrays(1, &buffer.vao);
        glDeleteBuffers(1, &buffer.verticesVbo);
        glDeleteBuffers(1, &buffer.indicesVbo);
        glDeleteBuffers(1, &buffer.transformsVbo);
        glDeleteBuffers(1, &buffer.coloursVbo);
    }
}

internal InstanceDataBuffer CreateInstanceDataBuffer(u32 vao, u32 verticesVbo, u32 numVertices, u32 indicesVbo, u32 numIndices, u32 primitive)
{
    InstanceDataBuffer result = {};
    result.numVertices = numVertices;
    result.primitive = primitive;
    result.vao = vao;
    result.verticesVbo = verticesVbo;
    result.indicesVbo = indicesVbo;
    result.numIndices = numIndices;
    glBindVertexArray(vao);

    glGenBuffers(1, &result.transformsVbo);
    glBindBuffer(GL_ARRAY_BUFFER, result.transformsVbo);

    u32 location = OPENGL_VERTEX_ATTRIBUTE_INSTANCE_TRANSFORM;
    char *pointer = NULL;
    for (u32 i = 0; i < 4; ++i)
    {
        glEnableVertexAttribArray(location + i);
        glVertexAttribPointer(location + i, 4, GL_FLOAT, GL_FALSE, sizeof(mat4),
                              pointer + i * sizeof(vec4));
        glVertexAttribDivisor(location + i, 1);
    }

    glGenBuffers(1, &result.coloursVbo);
    glBindBuffer(GL_ARRAY_BUFFER, result.coloursVbo);

    glEnableVertexAttribArray(OPENGL_VERTEX_ATTRIBUTE_INSTANCE_COLOUR);
    glVertexAttribPointer(OPENGL_VERTEX_ATTRIBUTE_INSTANCE_COLOUR, 3, GL_FLOAT,
                          GL_FALSE, sizeof(vec3), NULL);
    glVertexAttribDivisor(OPENGL_VERTEX_ATTRIBUTE_INSTANCE_COLOUR,1);

    glBindVertexArray(0);

    return result;
}

internal void renderer_ReloadContent(renderer_State *renderer)
{
    OpenGL_DeleteStaticMesh(renderer->quadMesh);
    OpenGL_DeleteStaticMesh(renderer->axisMesh);
    OpenGL_DeleteStaticMesh(renderer->wireframeCubeMesh);
    OpenGL_DeleteStaticMesh(renderer->cubeMesh);
    OpenGL_DeleteStaticMesh(renderer->sphereMesh);
    OpenGL_DeleteShader(renderer->colourShader.program);
    OpenGL_DeleteShader(renderer->vertexColourShader.program);
    OpenGL_DeleteShader(renderer->diffuseTextureShader.program);
    OpenGL_DeleteShader(renderer->colourDiffuseShader.program);
    OpenGL_DeleteShader(renderer->skinnedColourShader.program);
    OpenGL_DeleteShader(renderer->textShader.program);
    OpenGL_DeleteShader(renderer->particleTextureShader.program);
    OpenGL_DeleteShader(renderer->cubeMapShader.program);
    OpenGL_DeleteShader(renderer->grassShader.program);
    OpenGL_DeleteShader(renderer->rockShader.program);

    renderer->quadMesh = OpenGL_CreateQuadMesh();
    renderer->axisMesh = OpenGL_CreateAxisMesh();
    renderer->wireframeCubeMesh = OpenGL_CreateWireframeCubeMesh();
    renderer->cubeMesh = OpenGL_CreateCubeMesh();
    renderer->sphereMesh = OpenGL_CreateIcosahedronMesh(3);
    renderer->colourShader = CreateColourShader();
    renderer->vertexColourShader = CreateVertexColourShader();
    renderer->diffuseTextureShader = CreateDiffuseTextureShader();
    renderer->colourDiffuseShader = CreateColourDiffuseShader();
    renderer->skinnedColourShader = CreateSkinnedColourShader();
    renderer->textShader = CreateTextShader();
    renderer->particleTextureShader = CreateParticleTextureShader();
    renderer->cubeMapShader = CreateCubeMapShader();
    renderer->grassShader = CreateGrassShader();
    renderer->rockShader = CreateRockShader();

    OpenGL_DeleteDynamicMesh(renderer->textBuffer.mesh);
    InitializeTextBuffer(&renderer->textBuffer);

    DeleteInstanceDataBuffer(renderer->grassInstancingDataBuffer);
    OpenGL_StaticMesh grassMesh = CreateGrassMesh();
    renderer->grassInstancingDataBuffer = CreateInstanceDataBuffer(
            grassMesh.vao, grassMesh.vbo, grassMesh.numVertices,
            0, 0,
            grassMesh.primitive);
}


enum
{
  TEXT_ALIGN_LEFT,
  TEXT_ALIGN_CENTER,
  TEXT_ALIGN_RIGHT,
};

void renderer_DrawString(renderer_State *renderer, vec2 p, const char *text, Font *font,
                vec4 colour, mat4 *viewProjection,
                u32 alignment = TEXT_ALIGN_LEFT)
{
    AddToTextBufferResult result = AddToTextBuffer(&renderer->textBuffer, text, font);

    TextShader shader = renderer->textShader;
    glUseProgram(shader.program);
    glUniformMatrix4fv(shader.viewProjection, 1, GL_FALSE, viewProjection->raw);
    float width = CalculateTextLength(font, text);
    if (alignment == TEXT_ALIGN_CENTER)
    {
        p.x -= width * 0.5f;
    }
    else if (alignment == TEXT_ALIGN_RIGHT)
    {
        p.x -= width;
    }
    mat4 model = Translate(Vec3(p.x, p.y, 0.0f));
    glUniformMatrix4fv(shader.model, 1, GL_FALSE, model.raw);
    glUniform4fv(shader.colour, 1, colour.data);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, font->texture);
    glUniform1i(shader.glyphSheet, 0);

    OpenGL_DrawDynamicMesh(renderer->textBuffer.mesh, result.start, result.count);
}

internal void renderer_DrawRect(renderer_State *renderer, vec2 p, float w,
                                float h, vec4 colour, mat4 *projection,
                                vec3 anchorPoint = Vec3(1, 1, 0))
{
    ColourShader shader = renderer->colourShader;
    glUseProgram(shader.program);
    mat4 mvp = *projection * Translate(Vec3(p.x, p.y, 0.0)) *
               Scale(Vec3(w, h, 0.0f) * 0.5f) * Translate(anchorPoint);
    glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
    glUniform4fv(shader.colour, 1, colour.data);
    OpenGL_DrawStaticMesh(renderer->quadMesh);
}

internal void renderer_DrawTexturedRect(renderer_State *renderer, vec2 p,
                                        float w, float h, u32 texture,
                                        mat4 *projection,
                                        vec3 anchorPoint = Vec3(1, 1, 0))
{
    ParticleTextureShader shader = renderer->particleTextureShader;
    glUseProgram(shader.program);
    mat4 mvp = *projection * Translate(Vec3(p.x, p.y, 0.0)) *
               Scale(Vec3(w, h, 0.0f) * 0.5f) * Translate(anchorPoint);
    glUniformMatrix4fv(shader.mvp, 1, GL_FALSE, mvp.raw);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(shader.colourTexture, 0);
    OpenGL_DrawStaticMesh(renderer->quadMesh);
}
