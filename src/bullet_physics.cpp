#include "bullet_physics.h"

#include "logging.h"

//#pragma clang diagnostic push
//#pragma clang diagnostic ignored "-Woverloaded-virtual"
#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>

#define PHYSICS_MAX_SHAPES 0x1000
#define PHYSICS_MAX_BODIES 0x2000

#define PHYSICS_HANDLE_INDEX_MASK 0x1FFFFFFF
#define PHYSICS_HANDLE_GENERATION_MASK 0x60000000
#define PHYSICS_HANDLE_MAX_GENERATION 0x3
#define PHYSICS_HANDLE_VALID_BIT_MASK 0x80000000

u32 log_activeChannels = (u32)-1;

inline uint32_t GetPhysicsHandleIndex( uint32_t handle )
{
  return PHYSICS_HANDLE_INDEX_MASK & handle;
}

inline uint32_t GetPhysicsHandleGeneration( uint32_t handle )
{
  uint32_t result = PHYSICS_HANDLE_GENERATION_MASK & handle;
  result = result >> 29;
  return result;
}

inline bool IsPhysicsHandleValid(u32 handle)
{
    u32 result = handle & PHYSICS_HANDLE_VALID_BIT_MASK;
    return (result != 0);
}

inline uint32_t CreatePhysicsHandle( uint32_t index, uint32_t generation )
{
  Assert( generation <= PHYSICS_HANDLE_MAX_GENERATION );
  Assert( index <= PHYSICS_HANDLE_INDEX_MASK );
  Assert( index != 0 );

  uint32_t result = index;
  result |= ( generation << 29 );

  result |= PHYSICS_HANDLE_VALID_BIT_MASK; // Set handle valid bit

  Assert(IsPhysicsHandleValid(result));
  return result;
}

class BulletDebugDraw;

struct PhysicsShape
{
    btCollisionShape *shape;
    u32 generation;
    PhysicsShapeParameters parameters;
};

struct PhysicsBody
{
    btMotionState *motionState;
    btRigidBody *body;
    btCollisionShape *shape;
    u32 generation;
};

struct PhysicsSystemState
{
  btBroadphaseInterface *broadphase;
  btDefaultCollisionConfiguration *collisionConfiguration;
  btCollisionDispatcher *dispatcher;
  btSequentialImpulseConstraintSolver *solver;
  btDiscreteDynamicsWorld *dynamicsWorld;
  BulletDebugDraw *debugDraw;
  //btGhostPairCallback *ghostPairCallback;

  PhysicsBody bodies[PHYSICS_MAX_BODIES];
  //Character characters[MAX_CHARACTERS];
  PhysicsShape shapes[PHYSICS_MAX_SHAPES];
  //Trigger triggers[MAX_TRIGGERS];
};


class BulletDebugDraw : public btIDebugDraw
{
public:
  BulletDebugDraw();

  virtual void drawLine( const btVector3 &from, const btVector3 &to,
                         const btVector3 &color );

  virtual void drawContactPoint( const btVector3 &PointOnB,
                                 const btVector3 &normalOnB, btScalar distance,
                                 int lifeTime, const btVector3 &color );

  virtual void reportErrorWarning( const char *warningString );

  virtual void draw3dText( const btVector3 &location, const char *textString );

  virtual void setDebugMode( int debugMode ) { m_nMode = debugMode; }

  virtual int getDebugMode() const { return m_nMode; }

  u32 getLines(PhysicsDebugLine *lines, u32 count);

private:
  uint32_t m_nMode;
  PhysicsDebugLine m_lines[PHYSICS_DEBUG_MAX_LINES];
  uint32_t m_numLines;
};


inline btVector3 ToBullet( vec3 v )
{
  return btVector3(v.x, v.y, v.z);
}

inline btQuaternion ToBullet( quat q )
{
  return btQuaternion( q.x, q.y, q.z, q.w );
}

inline vec3 FromBullet( const btVector3 &v )
{
  return Vec3( v.x(), v.y(), v.z() );
}

inline quat FromBullet( const btQuaternion &q )
{
  return Quat( q.getX(), q.getY(), q.getZ(), q.getW());
}

class KinematicRigidBodyMotionState : public btMotionState
{
public:
    void getWorldTransform( btTransform &worldTrans ) const override
    {
        worldTrans.setIdentity();
        worldTrans.setOrigin( ToBullet( m_position ) );
        worldTrans.setRotation( ToBullet( m_orientation ) );
        btTransform t;
        t.setIdentity();
        t.setOrigin( ToBullet( m_localOrigin ) );
        worldTrans = worldTrans * t;
    }

    void setWorldTransform( const btTransform &worldTrans ) override
    {
    }

    void setPosition( vec3 position )
    {
        m_position = position;
    }

    void setOrientation( quat orientation )
    {
        m_orientation = orientation;
    }

    void setLocalOrigin( vec3 localOrigin )
    {
        m_localOrigin = localOrigin;
    }

private:
    vec3 m_position, m_localOrigin;
    quat m_orientation;
};

struct SingleConvexResultCallback
  : public btCollisionWorld::ClosestConvexResultCallback
{
  SingleConvexResultCallback( const btVector3 &from, const btVector3 &to )
    : ClosestConvexResultCallback( from, to )
  {
      m_size = 0;
      m_ignored = NULL;
  }

  btScalar addSingleResult( btCollisionWorld::LocalConvexResult &convexResult,
                            bool normalInWorldSpace ) override
  {
    if ( !convexResult.m_hitCollisionObject->hasContactResponse() )
    {
      return btScalar( 1.0 );
    }

    for ( uint32_t i = 0; i < m_size; ++i )
    {
      if ( m_ignored[i] == convexResult.m_hitCollisionObject )
      {
        return btScalar( 1.0 );
      }
    }

    return ClosestConvexResultCallback::addSingleResult( convexResult,
                                                         normalInWorldSpace );
  }

  btCollisionObject **m_ignored;
  uint32_t m_size;
};

struct SingleRayResultCallback
  : public btCollisionWorld::ClosestRayResultCallback
{
  SingleRayResultCallback( const btVector3 &from, const btVector3 &to )
    : ClosestRayResultCallback( from, to ), m_ignored( 0 ), m_size( 0 )
  {
  }

  virtual btScalar addSingleResult( btCollisionWorld::LocalRayResult &rayResult,
                                    bool normalInWorldSpace )
  {
    if ( !rayResult.m_collisionObject->hasContactResponse() )
    {
      return btScalar( 1.0 );
    }

    for ( uint32_t i = 0; i < m_size; ++i )
    {
      if ( m_ignored[i] == rayResult.m_collisionObject )
      {
        return btScalar( 1.0 );
      }
    }

    return ClosestRayResultCallback::addSingleResult( rayResult,
                                                      normalInWorldSpace );
  }

  btCollisionObject **m_ignored;
  uint32_t m_size;
};

extern "C" CREATE_PHYSICS_SYSTEM(CreatePhysicsSystem)
{
    PhysicsSystem *result = new PhysicsSystem;
    if (result)
    {
        ClearToZero(result, sizeof(PhysicsSystem));

        PhysicsSystemState *state = new PhysicsSystemState;
        result->state = state;
        if (state)
        {
            state->broadphase = new btDbvtBroadphase;
            state->collisionConfiguration = new btDefaultCollisionConfiguration;

            state->dispatcher =
                    new btCollisionDispatcher(state->collisionConfiguration);

            state->solver = new btSequentialImpulseConstraintSolver;

            state->dynamicsWorld = new btDiscreteDynamicsWorld(
                    state->dispatcher, state->broadphase, state->solver,
                    state->collisionConfiguration);
            state->dynamicsWorld->setGravity(btVector3(0, -10, 0));

            state->debugDraw = new BulletDebugDraw();
            state->dynamicsWorld->setDebugDrawer(state->debugDraw);

            // state->ghostPairCallback = new btGhostPairCallback;
            // state->broadphase->getOverlappingPairCache()
            //->setInternalGhostPairCallback( state->ghostPairCallback );
            //

            ClearToZero(state->shapes, sizeof(state->shapes));
            ClearToZero(state->bodies, sizeof(state->bodies));

            result->update = &PhysicsUpdate_;
            result->draw = &PhysicsDraw_;
            result->createShape = &PhysicsCreateShape_;
            result->createBody = &PhysicsCreateBody_;
            result->sweepTest = &PhysicsSweepTest_;
            result->updateKinematicBodies = &PhysicsUpdateKinematicBodies_;
            result->updateShape = &PhysicsUpdateShape_;
            result->raycast = &PhysicsRaycast_;
            result->destroyBody = &PhysicsDestroyBody_;
            result->destroyShape = &PhysicsDestroyShape_;
        }
    }

    return result;
}

extern "C" DESTROY_PHYSICS_SYSTEM(DestroyPhysicsSystem)
{
  PhysicsSystemState *state = physicsSystem->state;
  delete state->dynamicsWorld;
  delete state->debugDraw;
  delete state->solver;
  delete state->dispatcher;
  delete state->collisionConfiguration;
  delete state->broadphase;
  //delete state->ghostPairCallback;
  delete state;
  delete physicsSystem;
}

inline PhysicsShape* GetShapeData(PhysicsSystemState *state, PhysicsShapeHandle handle)
{
  if (!IsPhysicsHandleValid(handle))
  {
    return NULL;
  }

  uint32_t idx = GetPhysicsHandleIndex( handle );
  if ( idx < PHYSICS_MAX_SHAPES )
  {
    auto entry = state->shapes + idx;
    if ( entry->shape != NULL && entry->generation == GetPhysicsHandleGeneration( handle ) )
    {
      return entry;
    }
  }

  return NULL;
}

inline btCollisionShape* GetShape( PhysicsSystemState *state, PhysicsShapeHandle handle )
{
    auto data = GetShapeData(state, handle);
    Assert(data);
    return data->shape;
}

inline PhysicsBody* GetBodyData(PhysicsSystemState *state, PhysicsBodyHandle handle)
{
  if (!IsPhysicsHandleValid(handle))
  {
    return NULL;
  }

  uint32_t idx = GetPhysicsHandleIndex( handle );
  if ( idx < PHYSICS_MAX_BODIES )
  {
    auto entry = state->bodies + idx;
    if ( entry->body != NULL && entry->generation == GetPhysicsHandleGeneration( handle ) )
    {
      return entry;
    }
  }

  return NULL;
}

inline btRigidBody* GetBody( PhysicsSystemState *state, PhysicsBodyHandle handle )
{
    auto data= GetBodyData(state, handle);
    Assert(data);
    return data->body;
}

PHYSICS_UPDATE( PhysicsUpdate_ )
{
  state->dynamicsWorld->stepSimulation( dt, 10 );
}

PHYSICS_DRAW( PhysicsDraw_ )
{
    state->dynamicsWorld->debugDrawWorld();
    return state->debugDraw->getLines(lines, count);
}

PHYSICS_CREATE_SHAPE( PhysicsCreateShape_ )
{
  btCollisionShape *shape = NULL;

  switch ( params.type )
  {
    case PHYSICS_SPHERE_SHAPE:
        {
            btSphereShape *sphere = new btSphereShape(params.radius);
            shape = sphere;
        } break;
    case PHYSICS_BOX_SHAPE:
        {
            vec3 dimensions = params.dimensions * 0.5f;
            btBoxShape *box = new btBoxShape(ToBullet(dimensions));
            shape = box;
        } break;
    case PHYSICS_CAPSULE_SHAPE:
        {
            btCapsuleShape *capsule = new btCapsuleShape(params.radius, params.height);
            shape = capsule;
        } break;
    case PHYSICS_TRIANGLE_MESH_SHAPE:
        {
            btTriangleMesh *mesh = new btTriangleMesh();
            for (u32 i = 0; i < params.count; i+=3)
            {
                mesh->addTriangle(ToBullet(params.vertices[i]),
                                  ToBullet(params.vertices[i + 1]),
                                  ToBullet(params.vertices[i + 2]));
            }
            shape = new btBvhTriangleMeshShape(mesh, true);
        } break;
    case PHYSICS_TERRAIN_SHAPE:
        {
            btHeightfieldTerrainShape *terrain = new btHeightfieldTerrainShape(
                    params.heightfieldWidth, params.heightfieldLength, params.heightfieldData, 1.0f,
                    params.minHeight, params.maxHeight, 1, PHY_FLOAT, false);
            terrain->setUseDiamondSubdivision(true);
            shape = terrain;
        } break;
    default:
      // Unknown shape
      break;
  }

  Assert( shape );
  if (!IsZero(params.scale))
  {
      shape->setLocalScaling(ToBullet(params.scale));
  }

  // TODO: Use freelist to speed this up
  for ( uint32_t i = 1; i < PHYSICS_MAX_SHAPES; ++i )
  {
    if ( state->shapes[ i ].shape == NULL )
    {
      state->shapes[ i ].shape = shape;
      state->shapes[i].parameters = params;
      return CreatePhysicsHandle( i, state->shapes[ i ].generation );
    }
  }

  return PHYSICS_INVALID_HANDLE;
}

PHYSICS_CREATE_BODY( PhysicsCreateBody_ )
{
    PhysicsBodyHandle result = PHYSICS_INVALID_HANDLE;
    auto shape = GetShape(state, params.shape);
    Assert(shape);

    for (uint32_t i = 1; i < PHYSICS_MAX_BODIES; ++i)
    {
        auto entry = state->bodies + i;
        if (entry->body == NULL)
        {
            entry->shape = shape;
            result = CreatePhysicsHandle(i, state->bodies[i].generation);

            int filterGroup;
            if (params.type == RIGID_BODY_KINEMATIC)
            {
                params.mass = 0.0f;
                filterGroup = btBroadphaseProxy::KinematicFilter;
            }
            else if (params.type == RIGID_BODY_STATIC)
            {
                params.mass = 0.0f;
                filterGroup = btBroadphaseProxy::StaticFilter;
            }
            else
            {
                filterGroup = btBroadphaseProxy::DefaultFilter;
            }

            if (params.type == RIGID_BODY_KINEMATIC)
            {
                auto motionState = new KinematicRigidBodyMotionState;
                motionState->setPosition(params.position);
                motionState->setLocalOrigin(params.localOrigin);
                entry->motionState = motionState;
            }
            else
            {
                entry->motionState = new btDefaultMotionState(btTransform(
                        btQuaternion(0, 0, 0, 1), ToBullet(params.position)));
            }

            btVector3 inertia;
            if (params.mass > 0.0f || params.type != RIGID_BODY_DYNAMIC)
            {
                entry->shape->calculateLocalInertia(params.mass, inertia);
            }

            btRigidBody::btRigidBodyConstructionInfo ci(
                    params.mass, entry->motionState, entry->shape, inertia);

            entry->body = new btRigidBody(ci);

            size_t userPointer = (size_t)result;
            entry->body->setUserPointer((void *)userPointer);

            if (params.type == RIGID_BODY_KINEMATIC)
            {
                entry->body->setCollisionFlags(
                        entry->body->getCollisionFlags() |
                        btCollisionObject::CF_KINEMATIC_OBJECT);
                entry->body->setActivationState(DISABLE_DEACTIVATION);
            }

            int filterMask = 0;
            if (params.collidesWith == COLLISION_TYPE_ALL)
            {
                filterMask = btBroadphaseProxy::AllFilter;
            }
            else
            {
                if (params.collidesWith & COLLISION_TYPE_DEFAULT)
                {
                    filterMask |= btBroadphaseProxy::DefaultFilter;
                }
                if (params.collidesWith & COLLISION_TYPE_CHARACTER)
                {
                    filterMask |= btBroadphaseProxy::CharacterFilter;
                }
                if (params.collidesWith & COLLISION_TYPE_KINEMATIC)
                {
                    filterMask |= btBroadphaseProxy::KinematicFilter;
                }
                if (params.collidesWith & COLLISION_TYPE_STATIC)
                {
                    filterMask |= btBroadphaseProxy::StaticFilter;
                }
            }

            state->dynamicsWorld->addRigidBody(entry->body, filterGroup,
                                               filterMask);
            return result;
        }
    }

    return result;
}

#define MAX_IGNORED 256
PHYSICS_SWEEP_TEST( PhysicsSweepTest_ )
{
  PhysicsSweepTestResult result = {};
  btConvexShape *shape = (btConvexShape*)GetShape(state, params->shape);
  // TODO: Check that it is safe to cast to convex shape
  Assert(shape);

  btTransform startTrans, endTrans;
  startTrans.setIdentity();
  endTrans.setIdentity();
  startTrans.setOrigin( ToBullet( params->start ) );
  endTrans.setOrigin( ToBullet( params->end ) );

  SingleConvexResultCallback callback( ToBullet( params->start ),
          ToBullet( params->end ) );

  if (params->filterGroup == COLLISION_TYPE_ALL)
  {
      callback.m_collisionFilterGroup = btBroadphaseProxy::AllFilter;
  }
  else
  {
      if (params->filterGroup & COLLISION_TYPE_DEFAULT)
          callback.m_collisionFilterGroup |= btBroadphaseProxy::DefaultFilter;
      if (params->filterGroup & COLLISION_TYPE_CHARACTER)
          callback.m_collisionFilterGroup |= btBroadphaseProxy::CharacterFilter;
      if (params->filterGroup & COLLISION_TYPE_KINEMATIC)
          callback.m_collisionFilterGroup |= btBroadphaseProxy::KinematicFilter;
      if (params->filterGroup & COLLISION_TYPE_STATIC)
          callback.m_collisionFilterGroup |= btBroadphaseProxy::StaticFilter;
  }

  if (params->filterMask == COLLISION_TYPE_ALL)
  {
      callback.m_collisionFilterMask = btBroadphaseProxy::AllFilter;
  }
  else
  {
      callback.m_collisionFilterMask = 0;
      if (params->filterMask & COLLISION_TYPE_DEFAULT)
          callback.m_collisionFilterMask |= btBroadphaseProxy::DefaultFilter;
      if (params->filterMask & COLLISION_TYPE_CHARACTER)
          callback.m_collisionFilterMask |= btBroadphaseProxy::CharacterFilter;
      if (params->filterMask & COLLISION_TYPE_KINEMATIC)
          callback.m_collisionFilterMask |= btBroadphaseProxy::KinematicFilter;
      if (params->filterMask & COLLISION_TYPE_STATIC)
          callback.m_collisionFilterMask |= btBroadphaseProxy::StaticFilter;
  }

  btCollisionObject *ignoredCollisionObjects[MAX_IGNORED];
  uint32_t numIgnoredCollisionObjects = 0;
  for ( uint32_t i = 0; i < params->numIgnored; ++i )
  {
      auto body = GetBody( state, params->ignored[i] );
      if ( body )
      {
          ignoredCollisionObjects[numIgnoredCollisionObjects++] = body;
      }
  }
  if ( numIgnoredCollisionObjects > 0 )
  {
      callback.m_ignored = ignoredCollisionObjects;
      callback.m_size = numIgnoredCollisionObjects;
  }

  state->dynamicsWorld->convexSweepTest(
          shape, startTrans, endTrans, callback,
          state->dynamicsWorld->getDispatchInfo().m_allowedCcdPenetration );

  if ( callback.hasHit() )
  {
      result.hasHit = true;
      result.hitNormal = FromBullet( callback.m_hitNormalWorld );
      result.hitPoint = FromBullet( callback.m_hitPointWorld );
      result.hitFraction = callback.m_closestHitFraction;
      size_t value =
          (size_t)callback.m_hitCollisionObject->getUserPointer();
      if (value)
      {
          result.physicsHandle = (PhysicsBodyHandle)value;
      }
  }
  return result;
}

PHYSICS_UPDATE_KINEMATIC_BODIES( PhysicsUpdateKinematicBodies_ )
{
    for (u32 i = 0; i < params->count; ++i)
    {
        auto body = GetBody(state, params->handles[i]);
        Assert(body);
        // TODO: Assert that body is kinematic
        auto motionState = (KinematicRigidBodyMotionState*)body->getMotionState();
        motionState->setPosition(params->positions[i]);
        motionState->setOrientation(params->orientations[i]);
    }
}

PHYSICS_UPDATE_SHAPE(PhysicsUpdateShape_)
{
    auto shapeData = GetShapeData(state, params->shape);
    Assert(shapeData);
    Assert(shapeData->shape);

    Assert(shapeData->parameters.type == params->type);

    switch(params->type)
    {
        case PHYSICS_SPHERE_SHAPE:
            {
                btSphereShape *sphere = (btSphereShape*)shapeData->shape;
                Assert(!"Not implemented");
            }
            break;
        case PHYSICS_BOX_SHAPE:
            {
                btBoxShape *box = (btBoxShape*)shapeData->shape;
                Assert(!"Not implemented");
            }
            break;
        case PHYSICS_CAPSULE_SHAPE:
            {
                btCapsuleShape *capsule = (btCapsuleShape*)shapeData->shape;
                btVector3 scaling(params->radius, params->height, params->radius);
                capsule->setLocalScaling(scaling);
            }
            break;
        default:
            Assert(!"Unknown shape");
            break;
    }
}

PHYSICS_RAYCAST(PhysicsRaycast_)
{
    SingleRayResultCallback callback(ToBullet(params->start), ToBullet(params->end));
    if (params->filterGroup == COLLISION_TYPE_ALL)
    {
        callback.m_collisionFilterGroup = btBroadphaseProxy::AllFilter;
    }
    else
    {
        if (params->filterGroup & COLLISION_TYPE_DEFAULT)
            callback.m_collisionFilterGroup |= btBroadphaseProxy::DefaultFilter;
        if (params->filterGroup & COLLISION_TYPE_CHARACTER)
            callback.m_collisionFilterGroup |= btBroadphaseProxy::CharacterFilter;
        if (params->filterGroup & COLLISION_TYPE_KINEMATIC)
            callback.m_collisionFilterGroup |= btBroadphaseProxy::KinematicFilter;
        if (params->filterGroup & COLLISION_TYPE_STATIC)
            callback.m_collisionFilterGroup |= btBroadphaseProxy::StaticFilter;
    }

    if (params->filterMask == COLLISION_TYPE_ALL)
    {
        callback.m_collisionFilterMask = btBroadphaseProxy::AllFilter;
    }
    else
    {
        callback.m_collisionFilterMask = 0;
        if (params->filterMask & COLLISION_TYPE_DEFAULT)
        {
            callback.m_collisionFilterMask |= btBroadphaseProxy::DefaultFilter;
        }
        if (params->filterMask & COLLISION_TYPE_CHARACTER)
        {
            callback.m_collisionFilterMask |=
                    btBroadphaseProxy::CharacterFilter;
        }
        if (params->filterMask & COLLISION_TYPE_KINEMATIC)
        {
            callback.m_collisionFilterMask |=
                    btBroadphaseProxy::KinematicFilter;
        }
        if (params->filterMask & COLLISION_TYPE_STATIC)
        {
            callback.m_collisionFilterMask |= btBroadphaseProxy::StaticFilter;
        }
    }

    btCollisionObject *ignoredCollisionObjects[MAX_IGNORED];
    uint32_t numIgnoredCollisionObjects = 0;
    Assert(params->ignoredCount < MAX_IGNORED);
    for (uint32_t i = 0; i < params->ignoredCount; ++i)
    {
        auto body = GetBody(state, params->ignored[i]);
        Assert(body);
        ignoredCollisionObjects[numIgnoredCollisionObjects++] = body;
    }
    if (numIgnoredCollisionObjects > 0)
    {
        callback.m_ignored = ignoredCollisionObjects;
        callback.m_size = numIgnoredCollisionObjects;
    }

    state->dynamicsWorld->rayTest(ToBullet(params->start),
                                  ToBullet(params->end), callback);

    if (callback.m_collisionObject)
    {
        result->point = FromBullet(callback.m_hitPointWorld);
        result->normal = FromBullet(callback.m_hitNormalWorld);
        result->fraction = callback.m_closestHitFraction;
        // TODO: Utility functions for converting handles to and from user pointers
        size_t handle = (size_t)callback.m_collisionObject->getUserPointer();
        result->body = (PhysicsBodyHandle)handle;
        result->hasHit = true;
        return true;
    }
    return false;
}

PHYSICS_DESTROY_BODY(PhysicsDestroyBody_)
{
    if (handle != PHYSICS_INVALID_HANDLE)
    {
        auto bodyData = GetBodyData(state, handle);
        Assert(bodyData);

        Assert(bodyData->body);
        state->dynamicsWorld->removeRigidBody(bodyData->body);
        delete bodyData->body;
        bodyData->body = NULL;

        Assert(bodyData->motionState);
        delete bodyData->motionState;
        bodyData->motionState= NULL;

        bodyData->shape = NULL; // TODO: Flag to also delete collision shape
        bodyData->generation = (bodyData->generation + 1) % PHYSICS_HANDLE_MAX_GENERATION;
    }
}

PHYSICS_DESTROY_SHAPE(PhysicsDestroyShape_)
{
    if (handle != PHYSICS_INVALID_HANDLE)
    {
        auto shapeData = GetShapeData(state, handle);
        Assert(shapeData);

        Assert(shapeData->shape);
        delete shapeData->shape;
        shapeData->shape = NULL;

        ClearToZero(&shapeData->parameters, sizeof(shapeData->parameters));
        shapeData->generation = (shapeData->generation + 1) % PHYSICS_HANDLE_MAX_GENERATION;
    }
}

BulletDebugDraw::BulletDebugDraw()
  : m_nMode( btIDebugDraw::DBG_DrawWireframe ), m_numLines( 0 )
{
}

uint32_t BulletDebugDraw::getLines( PhysicsDebugLine *lines,
                                       uint32_t count )
{
  uint32_t n = Min(count, m_numLines);
  for ( uint32_t i = 0; i < n; ++i )
  {
      lines[i] = m_lines[i];
  }
  m_numLines = 0;
  return n;
}

void BulletDebugDraw::drawLine( const btVector3 &from, const btVector3 &to,
                                const btVector3 &color )
{
  if ( m_numLines < PHYSICS_DEBUG_MAX_LINES )
  {
      PhysicsDebugLine *line = m_lines + m_numLines++;
      line->to = FromBullet(to);
      line->from = FromBullet(from);
      line->colour = FromBullet(color);
  }
}

void BulletDebugDraw::drawContactPoint( const btVector3 &PointOnB,
                                        const btVector3 &normalOnB,
                                        btScalar distance, int lifeTime,
                                        const btVector3 &color )
{
}

void BulletDebugDraw::reportErrorWarning( const char *warningString )
{
  LOG_WARN("Bullet Warning:%s\n", warningString );
}

void BulletDebugDraw::draw3dText( const btVector3 &location,
                                  const char *textString )
{
}
