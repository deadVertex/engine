#define NET_MAX_INFLIGHT_PACKETS 120 // 2 second window at 60 Hz send rate
struct InflightPacket
{
    float lifeTime;
    u16 sequenceNumber;
    //u16 acks[NET_PACKET_MAX_ACKS];
};

struct AckSystem
{
    InflightPacket inFlightPackets[NET_MAX_INFLIGHT_PACKETS];
    u16 lastPacketsReceived[16];
    u32 head;
    u32 tail;
};

internal void InitAckSystem(AckSystem *ackSystem)
{
    ZeroPointerToStruct(ackSystem);
}

internal void AddInFlightPacket(AckSystem *ackSystem, u16 sequenceNumber, float lifeTime)
{
    Assert(sequenceNumber != 0);
    for (u32 i = 0; i < NET_MAX_INFLIGHT_PACKETS; ++i)
    {
        InflightPacket *packet = ackSystem->inFlightPackets + i;
        if (packet->sequenceNumber == 0)
        {
            packet->sequenceNumber = sequenceNumber;
            packet->lifeTime = lifeTime;
            return;
        }
    }
    Assert(!"Failed to find free slot in inFlightPackets buffer, use a bigger buffer or shorter lifetime");
}

internal void OnPacketReceivedByRemote(AckSystem *ackSystem, u16 sequenceNumber)
{
    for (u32 i = 0; i < NET_MAX_INFLIGHT_PACKETS; ++i)
    {
        if (ackSystem->inFlightPackets[i].sequenceNumber == sequenceNumber)
        {
            ackSystem->inFlightPackets[i].sequenceNumber = 0;
            break;
        }
    }
}

internal bool OnReceivePacketFromRemote(AckSystem *ackSystem, u16 sequenceNumber)
{
    u32 tail = (ackSystem->tail + 1) % ArrayCount(ackSystem->lastPacketsReceived);
    if (tail == ackSystem->head)
    {
        LOG_ERROR("Received more packets than able to acknowledge, packet should be dropped");
        return false;
    }
    ackSystem->lastPacketsReceived[tail] = sequenceNumber;
    ackSystem->tail = tail;
    return true;
}

internal u32 DropUnacknowledgedPackets(AckSystem *ackSystem, u16 *droppedPackets, u32 length, float dt)
{
    u32 count = 0;
    for (u32 i = 0; i < NET_MAX_INFLIGHT_PACKETS; ++i)
    {
        InflightPacket *packet = ackSystem->inFlightPackets + i;
        if (packet->sequenceNumber != 0)
        {
            packet->lifeTime -= dt;
            if (packet->lifeTime <= 0.0f)
            {
                // Drop packet and add it buffer
                Assert(count < length);
                droppedPackets[count++] = packet->sequenceNumber;

                packet->sequenceNumber = 0;
                packet->lifeTime = 0.0f;
            }
        }
    }

    return count;
}

internal u32 GetAcksToSend(AckSystem *ackSystem, u16 *acks, u32 length)
{
    u32 count = 0;
    for ( count = 0; count < length; ++count)
    {
        if (ackSystem->head == ackSystem->tail)
        {
            break;
        }

        acks[count] = ackSystem->lastPacketsReceived[ackSystem->head];
        ackSystem->lastPacketsReceived[ackSystem->head] = 0; // This is just for debugging
        
        ackSystem->head = (ackSystem->head + 1) % ArrayCount(ackSystem->lastPacketsReceived);
    }

    return count;
}
