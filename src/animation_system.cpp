struct Sk_BonePose
{
	quat rotation;
	vec3 translation;
	f32 scaling;
};

struct Sk_Skeleton
{
    mat4 *invBindPose;
    const char **names;
    u8 *parents;
    Sk_BonePose *bindPose;
    u32 boneCount;
};

struct AnimationChannelData
{
    const char *name;
    Node *samples;
};

struct AnimationClipData
{
    AnimationChannelData *channels;
    u32 channelCount;
    u32 frameCount;
    f32 framesPerSecond;
    const char *name;
};

struct AnimationImportData
{
    AnimationClipData *clips;
    u32 clipCount;
};

struct Sk_AnimationClip
{
    Sk_Skeleton *skeleton;
    Sk_BonePose *samples;
    f32 samplesPerSecond;
    u32 sampleCount;
    bool isLooping;
};

struct Sk_AnimationPlaybackState
{
    Sk_AnimationClip *clip;
    Sk_BonePose *currentPose;
    float rate; // This must not be adjusted at runtime!
    float startT;
};

struct Sk_AnimationController
{
    Sk_AnimationPlaybackState *playbackStates;
    u32 count;
    Sk_BonePose *currentPose;
    Sk_Skeleton *skeleton;
};

struct Sk_SkinnedMesh
{
    OpenGL_StaticMesh mesh;
    mat4 *invBindPose;
};

void Sk_CalculatePoseWorldInvTransforms(mat4 *invWorldTransforms, Sk_BonePose *pose,
                                    Sk_Skeleton *skeleton, MemoryArena *arena)
{
    mat4 *invTransforms = AllocateArray(arena, mat4, skeleton->boneCount);

    for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
    {
        Sk_BonePose *bonePose = skeleton->bindPose + bone;
        invTransforms[bone] = Scale(1.0f / bonePose->scaling) *
                                Rotate(-bonePose->rotation) *
                                Translate(-bonePose->translation);
    }

    for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
    {
        u32 parent = (u32)skeleton->parents[bone];
        if (parent != INVALID_BONE)
        {
            Assert(parent < bone);
            invWorldTransforms[bone] = invTransforms[bone] * invWorldTransforms[parent];
        }
        else
        {
            invWorldTransforms[bone] = invTransforms[bone];
        }
    }

    MemoryArenaFree(arena, invTransforms);
}

internal void Sk_CreateSkeleton(Sk_Skeleton *skeleton, ModelImportData *data, MemoryArena *arena)
{
    // TODO: Currently we convert all nodes to bones, in the future we should filter
    Assert(data->nodeCount < MAX_BONES);
    skeleton->boneCount = data->nodeCount;
    skeleton->invBindPose = AllocateArray(arena, mat4, skeleton->boneCount);
    skeleton->names = AllocateArray(arena, const char*, skeleton->boneCount);
    skeleton->parents = AllocateArray(arena, u8, skeleton->boneCount);
    skeleton->bindPose = AllocateArray(arena, Sk_BonePose, skeleton->boneCount);

    for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
    {
        Node *node = data->nodes + bone;

        skeleton->names[bone] = data->nodeNames[bone];
        skeleton->parents[bone] = node->parent >= 0
                                          ? SafeTruncateU32ToU8(node->parent)
                                          : INVALID_BONE;

        Sk_BonePose *bonePose = skeleton->bindPose + bone;
        bonePose->rotation = node->rotation;
        bonePose->translation = node->position;
        bonePose->scaling = node->scale.x;
    }

    Sk_CalculatePoseWorldInvTransforms(skeleton->invBindPose, skeleton->bindPose,
                                       skeleton, arena);
}

internal void Sk_CreateAnimationClip(Sk_AnimationClip *clip, AnimationImportData *data, MemoryArena *arena,
        Sk_Skeleton *skeleton, bool isLooping, const char *name = NULL)
{
    Assert(data->clipCount > 0);
    AnimationClipData *clipData = NULL;
    if (name != NULL)
    {
        for (u32 i = 0; i < data->clipCount; ++i)
        {
            if (StringEqual(data->clips[i].name, name))
            {
                clipData = &data->clips[i];
                break;
            }
        }
        if (clipData == NULL)
        {
            LOG_WARN("Could not find animation clip: '%s'", name);
        }
    }

    if (clipData == NULL)
    {
        clipData = &data->clips[0];
    }

    clip->skeleton = skeleton;
    clip->samplesPerSecond = clipData->framesPerSecond;
    clip->sampleCount = clipData->frameCount;

    i32 channelIndices[MAX_BONES];
    // Map channel to bone index
    for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
    {
        channelIndices[bone] = -1;
        const char *boneName = skeleton->names[bone];
        for (u32 channelIndex = 0; channelIndex < clipData->channelCount; ++channelIndex)
        {
            AnimationChannelData *channel = clipData->channels + channelIndex;
            if (!strcmp(boneName, channel->name))
            {
                channelIndices[bone] = channelIndex;
                break;
            }
        }
    }

    u32 totalSamples = skeleton->boneCount * clip->sampleCount;
    clip->samples = AllocateArray(arena, Sk_BonePose, totalSamples);
    for (u32 sampleIndex = 0; sampleIndex < clip->sampleCount; ++sampleIndex)
    {
        for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
        {
            Sk_BonePose *bonePose = &clip->samples[sampleIndex * skeleton->boneCount + bone];
            if (channelIndices[bone] != -1)
            {
                // FIXME: Inefficient!
                AnimationChannelData *channel = clipData->channels + channelIndices[bone];
                bonePose->rotation = channel->samples[sampleIndex].rotation;
                bonePose->translation = channel->samples[sampleIndex].position;
                bonePose->scaling = channel->samples[sampleIndex].scale.x;
            }
            else
            {
                bonePose->rotation = Quat();
                bonePose->translation = Vec3(0.0f);
                bonePose->scaling = 1.0f;
            }
        }
    }
    clip->isLooping = isLooping;
}

internal void Sk_InitialisePlaybackState(Sk_AnimationPlaybackState *state, Sk_AnimationClip *clip, MemoryArena *arena)
{
    Assert(clip);
    state->clip = clip;
    state->rate = 1.0f;

    u32 boneCount = clip->skeleton->boneCount;
    state->currentPose = AllocateArray(arena, Sk_BonePose, boneCount);
}

internal void
Sk_InitializeAnimationController(Sk_AnimationController *controller, u32 count,
                                 Sk_Skeleton *skeleton, MemoryArena *arena)
{
    controller->playbackStates = AllocateArray(arena, Sk_AnimationPlaybackState, count);
    controller->count = count;
    controller->skeleton = skeleton;
    controller->currentPose = AllocateArray(arena, Sk_BonePose, skeleton->boneCount);
}

internal void Sk_LerpPose(Sk_BonePose *resultPose, u32 boneCount, Sk_BonePose *pose0, Sk_BonePose *pose1, float t)
{
    for (u32 boneIndex = 0; boneIndex < boneCount; ++boneIndex)
    {
        Sk_BonePose *bone0 = pose0 + boneIndex;
        Sk_BonePose *bone1 = pose1 + boneIndex;
        Sk_BonePose *resultBone = resultPose + boneIndex;

        resultBone->translation = Lerp(bone0->translation, bone1->translation, t);
        resultBone->rotation = Lerp(bone0->rotation, bone1->rotation, t);
        resultBone->scaling = Lerp(bone0->scaling, bone1->scaling, t);
    }
}

internal void Sk_UpdatePlaybackState(Sk_AnimationPlaybackState *state, float globalT)
{
    Sk_AnimationClip *clip = state->clip;
    float t = state->rate * (globalT - state->startT); // Map global clock to local clock
    // TODO: handle precision issues

    int sampleIndex0 = Floor(t * clip->samplesPerSecond);
    int sampleIndex1 = sampleIndex0 + 1; // Avoid chance of sampleIndices being the same

    float secondsPerSample = 1.0f / clip->samplesPerSecond;
    float start = sampleIndex0 * secondsPerSample;
    float lerpT = (t - start) / secondsPerSample;

    int s0 = sampleIndex0 % (int)clip->sampleCount;
    int s1 = sampleIndex1 % (int)clip->sampleCount;
    if (t < 0.0f)
    {
        s0 = ((int)clip->sampleCount + s0) % (int)clip->sampleCount;
        s1 = ((int)clip->sampleCount + s1) % (int)clip->sampleCount;
    }

    u32 boneCount = clip->skeleton->boneCount;
    u32 poseIndex0 = s0 * boneCount;
    u32 poseIndex1 = s1 * boneCount;

    Sk_LerpPose(state->currentPose, boneCount, clip->samples + poseIndex0,
                clip->samples + poseIndex1, lerpT);
}

void Sk_CalculatePoseWorldTransform(mat4 *worldTransforms, Sk_BonePose *pose,
                                    Sk_Skeleton *skeleton, MemoryArena *arena)
{
    mat4 *localTransforms = AllocateArray(arena, mat4, skeleton->boneCount);

    for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
    {
        Sk_BonePose *bonePose = pose + bone;
        localTransforms[bone] = Translate(bonePose->translation) *
                                Rotate(bonePose->rotation) *
                                Scale(bonePose->scaling);
    }

    for (u32 bone = 0; bone < skeleton->boneCount; ++bone)
    {
        u32 parent = (u32)skeleton->parents[bone];
        if (parent != INVALID_BONE)
        {
            Assert(parent < bone);
            worldTransforms[bone] = worldTransforms[parent] * localTransforms[bone];
        }
        else
        {
            worldTransforms[bone] = localTransforms[bone];
        }
    }

    MemoryArenaFree(arena, localTransforms);
}

internal AnimationImportData ImportAnimationData(const void *assetData,
                                                 u32 assetDataLength,
                                                 MemoryArena *arena, float scale = 1.0f)
{
    AnimationImportData result = {};
    aiPropertyStore *properties = aiCreatePropertyStore();
    aiSetImportPropertyFloat(properties, AI_CONFIG_GLOBAL_SCALE_FACTOR_KEY, scale);
    const aiScene *scene = aiImportFileFromMemoryWithProperties(
            (const char *)assetData, assetDataLength,
            aiProcess_GlobalScale, "", properties);

    if ( !scene )
    {
      LOG_ERROR( "Animation import failed: %s", aiGetErrorString() );
      return result;
    }

    result.clipCount = scene->mNumAnimations;
    result.clips = AllocateArray(arena, AnimationClipData, result.clipCount);

    for (u32 animationIndex = 0; animationIndex < scene->mNumAnimations; ++animationIndex)
    {
        const aiAnimation *animation = scene->mAnimations[animationIndex];
        AnimationClipData *clip = result.clips + animationIndex;

        clip->name = AllocateString(arena, animation->mName.data);
        clip->frameCount = (u32)animation->mDuration + 1;
        clip->framesPerSecond = (f32)animation->mTicksPerSecond;
        clip->channelCount = animation->mNumChannels;
        clip->channels = AllocateArray(arena, AnimationChannelData, clip->channelCount);

        for (u32 channelIndex = 0; channelIndex < animation->mNumChannels; ++channelIndex)
        {
            const aiNodeAnim* node = animation->mChannels[channelIndex];
            AnimationChannelData *channel = clip->channels + channelIndex;
            channel->name = AllocateString(arena, node->mNodeName.data);
            channel->samples = AllocateArray(arena, Node, clip->frameCount);

            for (u32 sampleIndex = 0; sampleIndex < clip->frameCount; ++sampleIndex)
            {
                aiVectorKey positionKey = FindVectorKeyForFrame(
                        node->mPositionKeys, node->mNumPositionKeys,
                        sampleIndex);
                aiQuatKey rotationKey = FindQuatKeyForFrame(
                        node->mRotationKeys, node->mNumRotationKeys,
                        sampleIndex);
                aiVectorKey scaleKey = FindVectorKeyForFrame(
                        node->mScalingKeys, node->mNumScalingKeys,
                        sampleIndex);

                Node *sample = channel->samples + sampleIndex;
                sample->position =
                    Vec3(positionKey.mValue.x, positionKey.mValue.y,
                            positionKey.mValue.z);

                sample->rotation =
                    Quat(rotationKey.mValue.x, rotationKey.mValue.y,
                            rotationKey.mValue.z, rotationKey.mValue.w);
                Assert(IsNormalized(sample->rotation));

                sample->scale = Vec3(scaleKey.mValue.x, scaleKey.mValue.y,
                        scaleKey.mValue.z);
            }
        }
    }

    aiReleaseImport(scene);
    aiReleasePropertyStore(properties);

    return result;
}

internal void Sk_CreateSkinnedMesh(Sk_SkinnedMesh *skinnedMesh,
                                   MeshImportData *meshData, MemoryArena *arena,
                                   mat4 *transform = NULL)
{
    skinnedMesh->mesh = CreateMeshFromData(meshData, arena, transform);
    skinnedMesh->invBindPose = AllocateArray(arena, mat4, MAX_BONES);
    for (u32 i = 0; i < MAX_BONES; ++i)
    {
        skinnedMesh->invBindPose[i] = meshData->boneInvBindPoses[i];
    }
}

internal void ScaleAnimationData(AnimationImportData *data, float scale)
{
    for (u32 clipIndex = 0; clipIndex < data->clipCount; ++clipIndex)
    {
        AnimationClipData *clipData = data->clips + clipIndex;
        for (u32 channelIndex = 0; channelIndex < clipData->channelCount; ++channelIndex)
        {
            for (u32 sampleIndex = 0; sampleIndex < clipData->frameCount; ++sampleIndex)
            {
                Node *sample = clipData->channels[channelIndex].samples + sampleIndex;
                sample->position *= scale;
            }
        }
    }
}
